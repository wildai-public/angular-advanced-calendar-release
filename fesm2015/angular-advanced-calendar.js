import { Component, Input, Output, EventEmitter, ViewChild, ViewContainerRef, Inject, Optional, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
let /** @type {?} */ moment = require('moment');
/**
 * @param {?} a
 * @param {?} b
 * @return {?}
 */
function isSameDay(a, b) {
    return a && b && a.getFullYear() === b.getFullYear() && a.getMonth() === b.getMonth() && a.getDate() === b.getDate();
}
/**
 * @param {?} date
 * @return {?}
 */
function getWeekNumber(date) {
    return moment(date).week();
}
/**
 * @param {?} date
 * @return {?}
 */
function getFirstDayOfMonth(date) {
    return moment(date).startOf('month').toDate();
}
/**
 * @param {?} date
 * @return {?}
 */
function getFirstDayOfFirstWeekOfMonth(date) {
    return moment(date).startOf('month').startOf('isoWeek');
}
/**
 * @param {?} date
 * @return {?}
 */
function getLastDayOfMonth(date) {
    return moment(date).endOf('month').toDate();
}
/**
 * @param {?} date
 * @return {?}
 */
function geLastDayOfLastWeekOfMonth(date) {
    return moment(date).endOf('month').endOf('isoWeek').toDate();
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
let /** @type {?} */ moment$1 = require('moment');
/**
 * Dispatches events in an array of dates depending
 * based on the date of each event.
 */
class EventDispatcher {
    /**
     * Creates a new event dispatcher
     * @param {?} dateAccessor the date label to use in the events
     * @param {?} startDate the start date of the period in which to dispatch events
     * @param {?} endDate the end date of the period (inclusive) in which to dispatch events
     */
    constructor(dateAccessor, startDate, endDate) {
        this.dateAccessor = dateAccessor;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    /**
     * @return {?}
     */
    clear() {
        this.dispatch = [];
        for (let /** @type {?} */ i = 0; i < this.getTotalNumberOfDays(); i++) {
            this.dispatch[i] = [];
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    add(event) {
        let /** @type {?} */ date = event[this.dateAccessor];
        let /** @type {?} */ dispatchIndex = this.getDayNumber(date);
        if (this.dispatch[dispatchIndex]) {
            this.dispatch[dispatchIndex].push(event);
        }
    }
    /**
     * @param {?} date
     * @return {?}
     */
    getEvents(date) {
        return this.dispatch[this.getDayNumber(date)];
    }
    /**
     * @param {?} startDate
     * @param {?} endDate
     * @return {?}
     */
    getEventsBetween(startDate, endDate) {
        return this.dispatch.slice(this.getDayNumber(startDate), this.getDayNumber(endDate) + 1);
    }
    /**
     * Returns an array of the dates
     * between startDate and endDate.
     * @return {?}
     */
    getDateArray() {
        let /** @type {?} */ days = [];
        let /** @type {?} */ current = moment$1(this.startDate);
        let /** @type {?} */ last = moment$1(this.endDate);
        while (current < last) {
            days.push(current.toDate());
            current = current.add(1, 'day');
        }
        return days;
    }
    /**
     * @return {?}
     */
    getWeekArray() {
        let /** @type {?} */ weeks = [];
        let /** @type {?} */ current = moment$1(this.startDate);
        let /** @type {?} */ last = moment$1(this.endDate);
        while (current < last) {
            weeks.push({ startDate: current.toDate(), endDate: current.add(6, 'day').toDate() });
            current = current.add(1, 'day');
        }
        return weeks;
    }
    /**
     * Returns the index of a date in
     * the [startDate - endDate] period.
     * @param {?} date the date for which to compute the index
     * @return {?}
     */
    getDayNumber(date) {
        return moment$1(date).diff(moment$1(this.startDate), 'days');
    }
    /**
     * Returns the total number of days
     * in the [startDate - endDate] period.
     * @return {?}
     */
    getTotalNumberOfDays() {
        return this.getDayNumber(this.endDate) + 1;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
// unsupported: template constraints.
/**
 * @abstract
 * @template DataType, ComponentType
 */
class DynamicComponentFactory {
    /**
     * @param {?} componentFactoryResolver
     * @param {?} componentType
     */
    constructor(componentFactoryResolver, componentType) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.factory = this.componentFactoryResolver.resolveComponentFactory(componentType);
    }
    /**
     * Inserts a component in a view container
     * @param {?} container the container in which we want to display this component
     * @param {?} data the data of the component to display
     * @param {?=} callbacks a map of callbacks to subscribe to
     * @param {?=} index the position in the container where to add the component
     * @return {?}
     */
    insertDynamicComponent(container, data, callbacks, index) {
        let /** @type {?} */ componentRef = container.createComponent(this.factory, index);
        let /** @type {?} */ instance = /** @type {?} */ (componentRef.instance);
        instance.updateData(data);
        if (callbacks) {
            for (let /** @type {?} */ name in callbacks) {
                instance.subscribe(name, callbacks[name]);
            }
        }
        return componentRef;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
class EventComponentFactory extends DynamicComponentFactory {
    /**
     * @param {?} componentFactoryResolver
     * @param {?} componentType
     */
    constructor(componentFactoryResolver, componentType) {
        super(componentFactoryResolver, componentType);
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
class DayCellComponentFactory extends DynamicComponentFactory {
    /**
     * @param {?} componentFactoryResolver
     * @param {?} componentType
     */
    constructor(componentFactoryResolver, componentType) {
        super(componentFactoryResolver, componentType);
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
// unsupported: template constraints.
/**
 * @template DataType, ComponentType
 */
class DynamicComponentArrayManager {
    constructor() {
        /**
         * Stores the previous array state
         */
        this.oldDataArray = [];
        /**
         * A numeric map used to store the dynamic views
         */
        this.dynamicViewComponentMap = {};
    }
    /**
     * @return {?}
     */
    clear() {
        for (let /** @type {?} */ id in this.dynamicViewComponentMap) {
            if (this.dynamicViewComponentMap[id]) {
                this.dynamicViewComponentMap[id].destroy();
                delete this.dynamicViewComponentMap[id];
            }
        }
        this.oldDataArray = [];
    }
    /**
     * Updates, add or remove data views based
     * on the old data list
     * @param {?} container
     * @param {?} factory
     * @param {?} dataArray
     * @param {?} dataIdAccessor
     * @param {?=} dynamicComponentCallbacks
     * @param {?=} onAddView
     * @param {?=} onUpdateView
     * @param {?=} onDeleteView
     * @return {?}
     */
    updateContainer(container, factory, dataArray, dataIdAccessor, dynamicComponentCallbacks, onAddView, onUpdateView, onDeleteView) {
        if (!container) {
            throw 'child container (#childContainer) could not be found in the container view';
        }
        for (let /** @type {?} */ data of dataArray) {
            let /** @type {?} */ alreadyPresent = false;
            for (let /** @type {?} */ oldData of this.oldDataArray) {
                if (data[dataIdAccessor] === oldData[dataIdAccessor]) {
                    alreadyPresent = true;
                    break;
                }
            }
            if (alreadyPresent) {
                this.updateDynamicComponent(container, data, dataIdAccessor, onUpdateView);
            }
            else {
                this.addDynamicComponent(container, factory, data, dataIdAccessor, dynamicComponentCallbacks, onAddView);
            }
        }
        for (let /** @type {?} */ oldData of this.oldDataArray) {
            let /** @type {?} */ stillPresent = false;
            for (let /** @type {?} */ data of dataArray) {
                if (data[dataIdAccessor] === oldData[dataIdAccessor]) {
                    stillPresent = true;
                    break;
                }
            }
            if (!stillPresent) {
                this.removeDynamicComponent(container, oldData, dataIdAccessor, onDeleteView);
            }
        }
        this.oldDataArray = dataArray;
    }
    /**
     * @param {?} container
     * @param {?} data
     * @param {?} dataIdAccessor
     * @param {?=} onDeleteView
     * @return {?}
     */
    removeDynamicComponent(container, data, dataIdAccessor, onDeleteView) {
        if (typeof data[dataIdAccessor] !== 'number') {
            throw 'id (' + dataIdAccessor + ') not found in data structure (delete dynamic component method)';
        }
        else if (!this.dynamicViewComponentMap[data[dataIdAccessor]]) {
            throw 'dynamic view not found in the dynamic view container (delete dynamic component method)';
        }
        else {
            this.dynamicViewComponentMap[data[dataIdAccessor]].destroy();
            delete this.dynamicViewComponentMap[data[dataIdAccessor]];
            if (onDeleteView)
                onDeleteView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
        }
    }
    /**
     * @param {?} container
     * @param {?} factory
     * @param {?} data
     * @param {?} dataIdAccessor
     * @param {?} dynamicComponentCallbacks
     * @param {?=} onAddView
     * @return {?}
     */
    addDynamicComponent(container, factory, data, dataIdAccessor, dynamicComponentCallbacks, onAddView) {
        if (typeof data[dataIdAccessor] !== 'number') {
            throw 'id (' + dataIdAccessor + ') not found in data structure (add dynamic component method)';
        }
        else if (this.dynamicViewComponentMap[data[dataIdAccessor]]) {
            throw 'dynamic view already present in the dynamic view container (add dynamic component method)';
        }
        else {
            let /** @type {?} */ componentRef = factory.insertDynamicComponent(container, data, dynamicComponentCallbacks);
            this.dynamicViewComponentMap[data[dataIdAccessor]] = componentRef;
            if (onAddView)
                onAddView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
        }
    }
    /**
     * Updates an data view model
     * @param {?} container
     * @param {?} data the new data model to use
     * @param {?} dataIdAccessor
     * @param {?=} onUpdateView
     * @return {?}
     */
    updateDynamicComponent(container, data, dataIdAccessor, onUpdateView) {
        if (typeof data[dataIdAccessor] !== 'number') {
            throw 'id (' + dataIdAccessor + ') not found in data structure (add dynamic component method)';
        }
        else if (!this.dynamicViewComponentMap[data[dataIdAccessor]]) {
            throw 'dynamic view not found in the dynamic view container (add dynamic component method)';
        }
        else {
            this.dynamicViewComponentMap[data[dataIdAccessor]].instance.updateData(data);
            if (onUpdateView)
                onUpdateView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
        }
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
class WeekSummaryComponentFactory extends DynamicComponentFactory {
    /**
     * @param {?} componentFactoryResolver
     * @param {?} componentType
     */
    constructor(componentFactoryResolver, componentType) {
        super(componentFactoryResolver, componentType);
    }
    /**
     * @param {?} container
     * @param {?} data
     * @param {?=} callbacks
     * @return {?}
     */
    insertDynamicComponent(container, data, callbacks) {
        let /** @type {?} */ position = this.findWeekPositionInContainer(container, data);
        return (position >= 0) ? super.insertDynamicComponent(container, data, callbacks, position) : undefined;
    }
    /**
     * @param {?} container
     * @param {?} data
     * @return {?}
     */
    findWeekPositionInContainer(container, data) {
        let /** @type {?} */ index;
        for (index = 0; index < container.length; index++) {
            let /** @type {?} */ componentInstance = container.get(index)['_view']['nodes'][1]['instance'];
            if (componentInstance.data.date && isSameDay(componentInstance.data.date, data.endDate)) {
                return index + 1;
            }
        }
        return -1;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
let /** @type {?} */ moment$2 = require('moment');
let /** @type {?} */ hash = require('object-hash');
const /** @type {?} */ DAY_CELL_COMPONENT_FACTORY_PROVIDER = 'DayCellComponentFactory';
const /** @type {?} */ EVENT_COMPONENT_FACTORY_PROVIDER = 'EventComponentFactory';
const /** @type {?} */ WEEK_SUMMARY_COMPONENT_FACTORY = 'WeekSummaryComponentFactory';
class CalendarComponent {
    /**
     * @param {?} dayCellComponentFactory
     * @param {?} eventComponentFactory
     * @param {?} weekSummaryComponentFactory
     */
    constructor(dayCellComponentFactory, eventComponentFactory, weekSummaryComponentFactory) {
        this.dayCellComponentFactory = dayCellComponentFactory;
        this.eventComponentFactory = eventComponentFactory;
        this.weekSummaryComponentFactory = weekSummaryComponentFactory;
        /**
         * Week days label to display in the header
         * (default is english).
         */
        this.headers = [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday',
        ];
        /**
         * A list of events to display in the callendar.
         */
        this.events = [];
        /**
         * The name of the date attribute to use
         * when displaying the events (default is 'date').
         */
        this.dateAccessor = 'date';
        /**
         * The name of the id attribute to use
         * when displaying the events (default is 'id').
         */
        this.idAccessor = 'id';
        this.dynamicDayCellViewManager = new DynamicComponentArrayManager();
        this.dynamicWeekSummaryViewManager = new DynamicComponentArrayManager();
        this.oldHash = '';
        /**
         * Triggers a delete event emission
         * @param event the event to delete
         */
        this.deleteEmitter = new EventEmitter();
        /**
         * Triggers an update event emission
         * @param event the event to update
         */
        this.updateEmitter = new EventEmitter();
        /**
         * Triggers an add event emission
         * @param event the event to add
         */
        this.addEmitter = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngDoCheck() {
        let /** @type {?} */ newHash = hash.sha1(this.events);
        if (newHash !== this.oldHash) {
            this.updateCalendar();
            this.oldHash = newHash;
        }
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes["startDate"] || changes["endDate"] || changes["dateAccessor"]) {
            let /** @type {?} */ startDate = (changes["startDate"]) ? changes["startDate"].currentValue : this.startDate;
            let /** @type {?} */ endDate = (changes["endDate"]) ? changes["endDate"].currentValue : this.endDate;
            let /** @type {?} */ dateAccessor = (changes["dateAccessor"]) ? changes["dateAccessor"].currentValue : this.dateAccessor;
            this.dispatcher = new EventDispatcher(dateAccessor, startDate, endDate);
            this.updateCalendar();
        }
    }
    /**
     * @return {?}
     */
    updateCalendar() {
        if (!this.dayCellComponentFactory) {
            this.days = [];
            throw 'day cell component factory (day-component-factory) was not set!';
        }
        else if (!this.dayGridContainer) {
            this.days = [];
            throw 'day grid (dayGrid) not found in template !';
        }
        else {
            this.dispatchEvents();
            this.updateDayCellViews();
            if (this.weekSummaryComponentFactory) {
                this.updateWeekSummaryCellViews();
            }
        }
    }
    /**
     * Sorts events by date
     * @return {?}
     */
    dispatchEvents() {
        this.dispatcher.clear();
        for (let /** @type {?} */ event of this.events) {
            this.dispatcher.add(event);
        }
        this.days = this.dispatcher.getDateArray().map(date => ({
            id: this.dispatcher.getDayNumber(date),
            date: date,
            calendarStartDate: this.startDate,
            calendarEndDate: this.endDate,
            events: this.dispatcher.getEvents(date),
            dateAccessor: this.dateAccessor,
        }));
    }
    /**
     * @return {?}
     */
    updateDayCellViews() {
        this.dynamicDayCellViewManager.updateContainer(this.dayGridContainer, this.dayCellComponentFactory, this.days, 'id', {
            add: this.onAdd.bind(this),
            update: this.onUpdate.bind(this),
            delete: this.onDelete.bind(this)
        }, this.updateDayCell.bind(this), this.updateDayCell.bind(this), this.updateDayCell.bind(this));
    }
    /**
     * @return {?}
     */
    updateWeekSummaryCellViews() {
        this.weeks = this.dispatcher.getWeekArray().map(week => ({
            id: getWeekNumber(week.startDate),
            startDate: week.startDate,
            endDate: week.endDate,
            calendarStartDate: this.startDate,
            calendarEndDate: this.endDate,
            events: this.dispatcher.getEventsBetween(week.startDate, week.endDate),
            dateAccessor: this.dateAccessor,
        }));
        if (this.startDate !== this.oldStartDate || this.endDate !== this.oldEndDate) {
            this.dynamicWeekSummaryViewManager.clear();
            this.oldStartDate = this.startDate;
            this.oldEndDate = this.endDate;
        }
        this.dynamicWeekSummaryViewManager.updateContainer(this.dayGridContainer, this.weekSummaryComponentFactory, this.weeks, 'id', {
            add: this.onAdd.bind(this),
            update: this.onUpdate.bind(this),
            delete: this.onDelete.bind(this)
        });
    }
    /**
     * Triggers a day cell view internal update
     * @param {?} data
     * @param {?} component the component ref of the day cell to update
     * @return {?}
     */
    updateDayCell(data, component) {
        let /** @type {?} */ instance = /** @type {?} */ (component.instance);
        instance.updateEventViews(this.eventComponentFactory, this.idAccessor);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onDelete(event) {
        this.deleteEmitter.emit(event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onUpdate(event) {
        this.updateEmitter.emit(event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onAdd(event) {
        this.addEmitter.emit(event);
    }
}
CalendarComponent.decorators = [
    { type: Component, args: [{
                selector: 'angular-advanced-calendar',
                template: `<h5 *ngFor='let name of headers' class='header'> {{name}} </h5>
<ng-template #dayGrid></ng-template>
<h5 *ngFor='let name of headers' class='footer'> {{name}} </h5>`,
                styles: [`@import url(https://fonts.googleapis.com/css?family=Raleway);.footer,.header{text-align:center;font-family:Raleway,sans-serif}`]
            },] },
];
/** @nocollapse */
CalendarComponent.ctorParameters = () => [
    { type: DayCellComponentFactory, decorators: [{ type: Inject, args: [DAY_CELL_COMPONENT_FACTORY_PROVIDER,] }] },
    { type: EventComponentFactory, decorators: [{ type: Optional }, { type: Inject, args: [EVENT_COMPONENT_FACTORY_PROVIDER,] }] },
    { type: WeekSummaryComponentFactory, decorators: [{ type: Optional }, { type: Inject, args: [WEEK_SUMMARY_COMPONENT_FACTORY,] }] }
];
CalendarComponent.propDecorators = {
    startDate: [{ type: Input, args: ['start-date',] }],
    endDate: [{ type: Input, args: ['end-date',] }],
    headers: [{ type: Input, args: ['headers',] }],
    events: [{ type: Input, args: ['events',] }],
    dateAccessor: [{ type: Input, args: ['date-accessor',] }],
    idAccessor: [{ type: Input, args: ['id-accessor',] }],
    dayGridContainer: [{ type: ViewChild, args: ['dayGrid', { read: ViewContainerRef },] }],
    deleteEmitter: [{ type: Output, args: ['delete',] }],
    updateEmitter: [{ type: Output, args: ['update',] }],
    addEmitter: [{ type: Output, args: ['add',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class AngularAdvancedCalendarModule {
}
AngularAdvancedCalendarModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                ],
                declarations: [
                    CalendarComponent,
                ],
                exports: [
                    CalendarComponent
                ]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
class DynamicComponent {
    /**
     * @param {?} data
     * @return {?}
     */
    updateData(data) {
        this.data = data;
    }
}
DynamicComponent.propDecorators = {
    data: [{ type: Input, args: ['data',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType, CrudType
 */
class DynamicCrudComponent extends DynamicComponent {
    constructor() {
        super(...arguments);
        this.deleteEmitter = new EventEmitter();
        this.updateEmitter = new EventEmitter();
        this.addEmitter = new EventEmitter();
    }
    /**
     * @param {?} name
     * @param {?} callback
     * @return {?}
     */
    subscribe(name, callback) {
        switch (name) {
            case 'update':
                this.updateEmitter.subscribe(callback);
                break;
            case 'delete':
                this.deleteEmitter.subscribe(callback);
                break;
            case 'add':
                this.addEmitter.subscribe(callback);
                break;
            default:
                throw 'callback type \'' + name + '\' is not a known event callback !';
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onDelete(event) {
        this.deleteEmitter.emit(event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onUpdate(event) {
        this.updateEmitter.emit(event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onAdd(event) {
        this.addEmitter.emit(event);
    }
}
DynamicCrudComponent.propDecorators = {
    deleteEmitter: [{ type: Output, args: ['delete',] }],
    updateEmitter: [{ type: Output, args: ['update',] }],
    addEmitter: [{ type: Output, args: ['add',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
class AbstractDayCellComponent extends DynamicCrudComponent {
    constructor() {
        super(...arguments);
        this.dynamicEventViewManager = new DynamicComponentArrayManager();
    }
    /**
     * Method called to update the event views in the day cell
     * @param {?} eventComponentFactory
     * @param {?} idAccessor
     * @return {?}
     */
    updateEventViews(eventComponentFactory, idAccessor) {
        if (eventComponentFactory && this.eventViewContainer) {
            this.dynamicEventViewManager.updateContainer(this.eventViewContainer, eventComponentFactory, this.data.events, idAccessor, {
                delete: this.onDelete.bind(this),
                update: this.onUpdate.bind(this)
            });
        }
    }
}
AbstractDayCellComponent.propDecorators = {
    eventViewContainer: [{ type: ViewChild, args: ['eventViewsContainer', { read: ViewContainerRef },] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
class AbstractWeekSummaryComponent extends DynamicCrudComponent {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
class AbstractEventComponent extends DynamicCrudComponent {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { AngularAdvancedCalendarModule, AbstractDayCellComponent, DayCellComponentFactory, AbstractWeekSummaryComponent, WeekSummaryComponentFactory, AbstractEventComponent, EventComponentFactory, isSameDay, getWeekNumber, getFirstDayOfMonth, getFirstDayOfFirstWeekOfMonth, getLastDayOfMonth, geLastDayOfLastWeekOfMonth, CalendarComponent as ɵa, DynamicCrudComponent as ɵc, DynamicComponent as ɵd, DynamicComponentFactory as ɵb };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvdXRpbHMvZGF0ZS50cyIsIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvdXRpbHMvZGlzcGF0Y2hlci50cyIsIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvdmlld3MvZHluYW1pYy9keW5hbWljLmZhY3RvcnkudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2V2ZW50L2V2ZW50LmZhY3RvcnkudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2RheS1jZWxsL2RheS1jZWxsLmZhY3RvcnkudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2R5bmFtaWMvZHluYW1pYy1tYW5hZ2VyLmNvbXBvbmVudC50cyIsIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvdmlld3Mvd2Vlay1zdW1tYXJ5L3dlZWstc3VtbWFyeS5mYXRvcnkudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2NhbGVuZGFyL2NhbGVuZGFyLmNvbXBvbmVudC50cyIsIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci5tb2R1bGUudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2R5bmFtaWMvZHluYW1pYy5jb21wb25lbnQudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2R5bmFtaWMtY3J1ZC9keW5hbWljLWNydWQuY29tcG9uZW50LnRzIiwibmc6Ly9hbmd1bGFyLWFkdmFuY2VkLWNhbGVuZGFyL2xpYi92aWV3cy9kYXktY2VsbC9hYnN0cmFjdC1kYXktY2VsbC5jb21wb25lbnQudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL3dlZWstc3VtbWFyeS9hYnN0cmFjdC13ZWVrLXN1bW1hcnkuY29tcG9uZW50LnRzIiwibmc6Ly9hbmd1bGFyLWFkdmFuY2VkLWNhbGVuZGFyL2xpYi92aWV3cy9ldmVudC9hYnN0cmFjdC1ldmVudC5jb21wb25lbnQudHMiXSwic291cmNlc0NvbnRlbnQiOlsibGV0IG1vbWVudCA9IHJlcXVpcmUoJ21vbWVudCcpO1xuXG5leHBvcnQgZnVuY3Rpb24gaXNTYW1lRGF5KGEgOiBEYXRlLCBiIDogRGF0ZSkgOiBib29sZWFuXG57XG4gIHJldHVybiBmYWxzZSB8fCAoYSAmJiBiICYmIGEuZ2V0RnVsbFllYXIoKSA9PT0gYi5nZXRGdWxsWWVhcigpICYmIGEuZ2V0TW9udGgoKSA9PT0gYi5nZXRNb250aCgpICYmIGEuZ2V0RGF0ZSgpID09PSBiLmdldERhdGUoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRXZWVrTnVtYmVyKGRhdGUgOiBEYXRlKSA6IG51bWJlclxue1xuICByZXR1cm4gbW9tZW50KGRhdGUpLndlZWsoKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEZpcnN0RGF5T2ZNb250aChkYXRlIDogRGF0ZSkgOiBEYXRlXG57XG4gIHJldHVybiBtb21lbnQoZGF0ZSkuc3RhcnRPZignbW9udGgnKS50b0RhdGUoKTtcblxufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Rmlyc3REYXlPZkZpcnN0V2Vla09mTW9udGgoZGF0ZSA6IERhdGUpIDogRGF0ZVxue1xuICByZXR1cm4gbW9tZW50KGRhdGUpLnN0YXJ0T2YoJ21vbnRoJykuc3RhcnRPZignaXNvV2VlaycpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0TGFzdERheU9mTW9udGgoZGF0ZSA6IERhdGUpIDogRGF0ZVxue1xuICByZXR1cm4gbW9tZW50KGRhdGUpLmVuZE9mKCdtb250aCcpLnRvRGF0ZSgpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2VMYXN0RGF5T2ZMYXN0V2Vla09mTW9udGgoZGF0ZSA6IERhdGUpIDogRGF0ZVxue1xuICByZXR1cm4gbW9tZW50KGRhdGUpLmVuZE9mKCdtb250aCcpLmVuZE9mKCdpc29XZWVrJykudG9EYXRlKCk7XG59IiwibGV0IG1vbWVudCA9IHJlcXVpcmUoJ21vbWVudCcpO1xuXG4vKipcbiAqIERpc3BhdGNoZXMgZXZlbnRzIGluIGFuIGFycmF5IG9mIGRhdGVzIGRlcGVuZGluZ1xuICogYmFzZWQgb24gdGhlIGRhdGUgb2YgZWFjaCBldmVudC5cbiAqL1xuZXhwb3J0IGNsYXNzIEV2ZW50RGlzcGF0Y2hlclxue1xuICBwcml2YXRlIGRpc3BhdGNoIDogYW55W11bXTtcblxuICAvKipcbiAgICogQ3JlYXRlcyBhIG5ldyBldmVudCBkaXNwYXRjaGVyXG4gICAqIEBwYXJhbSBkYXRlQWNjZXNzb3IgdGhlIGRhdGUgbGFiZWwgdG8gdXNlIGluIHRoZSBldmVudHNcbiAgICogQHBhcmFtIHN0YXJ0RGF0ZSB0aGUgc3RhcnQgZGF0ZSBvZiB0aGUgcGVyaW9kIGluIHdoaWNoIHRvIGRpc3BhdGNoIGV2ZW50c1xuICAgKiBAcGFyYW0gZW5kRGF0ZSB0aGUgZW5kIGRhdGUgb2YgdGhlIHBlcmlvZCAoaW5jbHVzaXZlKSBpbiB3aGljaCB0byBkaXNwYXRjaCBldmVudHNcbiAgICovXG4gIHB1YmxpYyBjb25zdHJ1Y3RvcihwdWJsaWMgcmVhZG9ubHkgZGF0ZUFjY2Vzc29yIDogc3RyaW5nLCBwdWJsaWMgcmVhZG9ubHkgc3RhcnREYXRlIDogRGF0ZSwgcHVibGljIHJlYWRvbmx5IGVuZERhdGUgOiBEYXRlKSB7IH1cblxuICBwdWJsaWMgY2xlYXIoKSA6IHZvaWRcbiAge1xuICAgIHRoaXMuZGlzcGF0Y2ggPSBbXTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZ2V0VG90YWxOdW1iZXJPZkRheXMoKTsgaSsrKVxuICAgIHtcbiAgICAgIHRoaXMuZGlzcGF0Y2hbaV0gPSBbXTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgYWRkKGV2ZW50IDogYW55KVxuICB7XG4gICAgbGV0IGRhdGUgPSBldmVudFt0aGlzLmRhdGVBY2Nlc3Nvcl07XG4gICAgbGV0IGRpc3BhdGNoSW5kZXggPSB0aGlzLmdldERheU51bWJlcihkYXRlKTtcbiAgICBpZiAodGhpcy5kaXNwYXRjaFtkaXNwYXRjaEluZGV4XSlcbiAgICB7XG4gICAgICB0aGlzLmRpc3BhdGNoW2Rpc3BhdGNoSW5kZXhdLnB1c2goZXZlbnQpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBnZXRFdmVudHMoZGF0ZSA6IERhdGUpIDogYW55W11cbiAge1xuICAgIHJldHVybiB0aGlzLmRpc3BhdGNoW3RoaXMuZ2V0RGF5TnVtYmVyKGRhdGUpXTtcbiAgfVxuXG4gIHB1YmxpYyBnZXRFdmVudHNCZXR3ZWVuKHN0YXJ0RGF0ZSA6IERhdGUsIGVuZERhdGUgOiBEYXRlKVxuICB7XG4gICAgcmV0dXJuIHRoaXMuZGlzcGF0Y2guc2xpY2UodGhpcy5nZXREYXlOdW1iZXIoc3RhcnREYXRlKSwgdGhpcy5nZXREYXlOdW1iZXIoZW5kRGF0ZSkgKyAxKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIGFuIGFycmF5IG9mIHRoZSBkYXRlc1xuICAgKiBiZXR3ZWVuIHN0YXJ0RGF0ZSBhbmQgZW5kRGF0ZS5cbiAgICovXG4gIHB1YmxpYyBnZXREYXRlQXJyYXkoKVxuICB7XG4gICAgbGV0IGRheXMgPSBbXTtcbiAgICBsZXQgY3VycmVudCA9IG1vbWVudCh0aGlzLnN0YXJ0RGF0ZSk7XG4gICAgbGV0IGxhc3QgID0gbW9tZW50KHRoaXMuZW5kRGF0ZSk7XG4gICAgd2hpbGUoY3VycmVudCA8IGxhc3QpXG4gICAge1xuICAgICAgZGF5cy5wdXNoKGN1cnJlbnQudG9EYXRlKCkpO1xuICAgICAgY3VycmVudCA9IGN1cnJlbnQuYWRkKDEsICdkYXknKTtcbiAgICB9XG4gICAgcmV0dXJuIGRheXM7XG4gIH1cblxuICBwdWJsaWMgZ2V0V2Vla0FycmF5KClcbiAge1xuICAgIGxldCB3ZWVrcyA9IFtdO1xuICAgIGxldCBjdXJyZW50ID0gbW9tZW50KHRoaXMuc3RhcnREYXRlKTtcbiAgICBsZXQgbGFzdCAgPSBtb21lbnQodGhpcy5lbmREYXRlKTtcbiAgICB3aGlsZShjdXJyZW50IDwgbGFzdClcbiAgICB7XG4gICAgICB3ZWVrcy5wdXNoKHsgc3RhcnREYXRlOiBjdXJyZW50LnRvRGF0ZSgpLCBlbmREYXRlOiBjdXJyZW50LmFkZCg2LCAnZGF5JykudG9EYXRlKCkgfSk7XG4gICAgICBjdXJyZW50ID0gY3VycmVudC5hZGQoMSwgJ2RheScpO1xuICAgIH1cbiAgICByZXR1cm4gd2Vla3M7XG4gIH1cblxuICAvKipcbiAgICogUmV0dXJucyB0aGUgaW5kZXggb2YgYSBkYXRlIGluXG4gICAqIHRoZSBbc3RhcnREYXRlIC0gZW5kRGF0ZV0gcGVyaW9kLlxuICAgKiBAcGFyYW0gZGF0ZSB0aGUgZGF0ZSBmb3Igd2hpY2ggdG8gY29tcHV0ZSB0aGUgaW5kZXhcbiAgICovXG4gIHB1YmxpYyBnZXREYXlOdW1iZXIoZGF0ZSA6IERhdGUpXG4gIHtcbiAgICByZXR1cm4gbW9tZW50KGRhdGUpLmRpZmYobW9tZW50KHRoaXMuc3RhcnREYXRlKSwgJ2RheXMnKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSB0b3RhbCBudW1iZXIgb2YgZGF5c1xuICAgKiBpbiB0aGUgW3N0YXJ0RGF0ZSAtIGVuZERhdGVdIHBlcmlvZC5cbiAgICovXG4gIHB1YmxpYyBnZXRUb3RhbE51bWJlck9mRGF5cygpXG4gIHtcbiAgICByZXR1cm4gdGhpcy5nZXREYXlOdW1iZXIodGhpcy5lbmREYXRlKSArIDE7XG4gIH1cbn0iLCJpbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50IH0gZnJvbSBcIi4vZHluYW1pYy5jb21wb25lbnRcIjtcbmltcG9ydCB7IENvbXBvbmVudFJlZiwgVmlld0NvbnRhaW5lclJlZiwgQ29tcG9uZW50RmFjdG9yeSwgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBUeXBlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFN0cmluZ01hcCB9IGZyb20gXCIuLi8uLi90eXBlcy9tYXBzXCI7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBEeW5hbWljQ29tcG9uZW50RmFjdG9yeTxEYXRhVHlwZSwgQ29tcG9uZW50VHlwZSBleHRlbmRzIER5bmFtaWNDb21wb25lbnQ8RGF0YVR5cGU+Plxue1xuICBwcml2YXRlIGZhY3RvcnkgOiBDb21wb25lbnRGYWN0b3J5PENvbXBvbmVudFR5cGU+O1xuXG4gIHB1YmxpYyBjb25zdHJ1Y3Rvcihwcml2YXRlIHJlYWRvbmx5IGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlciA6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29tcG9uZW50VHlwZSA6IFR5cGU8Q29tcG9uZW50VHlwZT4pXG4gIHtcbiAgICB0aGlzLmZhY3RvcnkgPSB0aGlzLmNvbXBvbmVudEZhY3RvcnlSZXNvbHZlci5yZXNvbHZlQ29tcG9uZW50RmFjdG9yeShjb21wb25lbnRUeXBlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBJbnNlcnRzIGEgY29tcG9uZW50IGluIGEgdmlldyBjb250YWluZXJcbiAgICogQHBhcmFtIGNvbnRhaW5lciB0aGUgY29udGFpbmVyIGluIHdoaWNoIHdlIHdhbnQgdG8gZGlzcGxheSB0aGlzIGNvbXBvbmVudFxuICAgKiBAcGFyYW0gZGF0YSB0aGUgZGF0YSBvZiB0aGUgY29tcG9uZW50IHRvIGRpc3BsYXlcbiAgICogQHBhcmFtIGNhbGxiYWNrcyBhIG1hcCBvZiBjYWxsYmFja3MgdG8gc3Vic2NyaWJlIHRvXG4gICAqIEBwYXJhbSBpbmRleCB0aGUgcG9zaXRpb24gaW4gdGhlIGNvbnRhaW5lciB3aGVyZSB0byBhZGQgdGhlIGNvbXBvbmVudFxuICAgKi9cbiAgcHVibGljIGluc2VydER5bmFtaWNDb21wb25lbnQoY29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZiwgZGF0YSA6IERhdGFUeXBlLCBjYWxsYmFja3M/OiBTdHJpbmdNYXA8RnVuY3Rpb24+LCBpbmRleD8gOiBudW1iZXIpIDogQ29tcG9uZW50UmVmPENvbXBvbmVudFR5cGU+XG4gIHtcbiAgICBsZXQgY29tcG9uZW50UmVmID0gY29udGFpbmVyLmNyZWF0ZUNvbXBvbmVudCh0aGlzLmZhY3RvcnksIGluZGV4KTtcbiAgICBsZXQgaW5zdGFuY2UgPSA8Q29tcG9uZW50VHlwZT5jb21wb25lbnRSZWYuaW5zdGFuY2U7XG4gICAgaW5zdGFuY2UudXBkYXRlRGF0YShkYXRhKTtcbiAgICBpZiAoY2FsbGJhY2tzKVxuICAgIHtcbiAgICAgIGZvciAobGV0IG5hbWUgaW4gY2FsbGJhY2tzKVxuICAgICAge1xuICAgICAgICBpbnN0YW5jZS5zdWJzY3JpYmUobmFtZSwgY2FsbGJhY2tzW25hbWVdKTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGNvbXBvbmVudFJlZjtcbiAgfVxufSIsImltcG9ydCB7IER5bmFtaWNDb21wb25lbnRGYWN0b3J5IH0gZnJvbSBcIi4uL2R5bmFtaWMvZHluYW1pYy5mYWN0b3J5XCI7XG5pbXBvcnQgeyBBYnN0cmFjdEV2ZW50Q29tcG9uZW50IH0gZnJvbSBcIi4vYWJzdHJhY3QtZXZlbnQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBUeXBlLCBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRXZlbnRDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlPiBleHRlbmRzIER5bmFtaWNDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlLCBBYnN0cmFjdEV2ZW50Q29tcG9uZW50PERhdGFUeXBlPj5cbntcbiAgcHVibGljIGNvbnN0cnVjdG9yKGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlciA6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29tcG9uZW50VHlwZSA6IFR5cGU8QWJzdHJhY3RFdmVudENvbXBvbmVudDxEYXRhVHlwZT4+KVxuICB7XG4gICAgc3VwZXIoY29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb21wb25lbnRUeXBlKTtcbiAgfVxufSIsImltcG9ydCB7IER5bmFtaWNDb21wb25lbnRGYWN0b3J5IH0gZnJvbSAnLi4vZHluYW1pYy9keW5hbWljLmZhY3RvcnknO1xuaW1wb3J0IHsgQ2FsZW5kYXJEYXlEYXRhIH0gZnJvbSAnLi4vLi4vdHlwZXMvY2FsZW5kYXItZGF5JztcbmltcG9ydCB7IEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudCB9IGZyb20gJy4vYWJzdHJhY3QtZGF5LWNlbGwuY29tcG9uZW50JztcbmltcG9ydCB7IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgVHlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRGF5Q2VsbENvbXBvbmVudEZhY3Rvcnk8RGF0YVR5cGU+IGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudEZhY3Rvcnk8Q2FsZW5kYXJEYXlEYXRhPERhdGFUeXBlPiwgQWJzdHJhY3REYXlDZWxsQ29tcG9uZW50PERhdGFUeXBlPj5cbntcbiAgcHVibGljIGNvbnN0cnVjdG9yKGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlciA6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29tcG9uZW50VHlwZSA6IFR5cGU8QWJzdHJhY3REYXlDZWxsQ29tcG9uZW50PERhdGFUeXBlPj4pXG4gIHtcbiAgICBzdXBlcihjb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIGNvbXBvbmVudFR5cGUpO1xuICB9XG59IiwiaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudCB9IGZyb20gXCIuL2R5bmFtaWMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBWaWV3Q29udGFpbmVyUmVmLCBDb21wb25lbnRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudEZhY3RvcnkgfSBmcm9tIFwiLi9keW5hbWljLmZhY3RvcnlcIjtcbmltcG9ydCB7IFN0cmluZ01hcCwgTnVtZXJpY01hcCB9IGZyb20gXCIuLi8uLi90eXBlcy9tYXBzXCI7XG5cbmV4cG9ydCBjbGFzcyBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyPERhdGFUeXBlLCBDb21wb25lbnRUeXBlIGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudDxEYXRhVHlwZT4+XG57XG5cbiAgLyoqXG4gICAqIFN0b3JlcyB0aGUgcHJldmlvdXMgYXJyYXkgc3RhdGVcbiAgICovXG4gIHByaXZhdGUgb2xkRGF0YUFycmF5IDogRGF0YVR5cGVbXSA9IFtdO1xuICBcbiAgcHVibGljIGNsZWFyKClcbiAge1xuICAgIGZvciAobGV0IGlkIGluIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXApXG4gICAge1xuICAgICAgaWYgKHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbaWRdKVxuICAgICAge1xuICAgICAgICB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2lkXS5kZXN0cm95KCk7XG4gICAgICAgIGRlbGV0ZSB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2lkXTtcbiAgICAgIH1cbiAgICB9XG4gICAgdGhpcy5vbGREYXRhQXJyYXkgPSBbXTtcbiAgfVxuXG4gIC8qKlxuICAgKiBVcGRhdGVzLCBhZGQgb3IgcmVtb3ZlIGRhdGEgdmlld3MgYmFzZWRcbiAgICogb24gdGhlIG9sZCBkYXRhIGxpc3RcbiAgICovXG4gIHB1YmxpYyB1cGRhdGVDb250YWluZXIoXG4gICAgY29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZixcbiAgICBmYWN0b3J5IDogRHluYW1pY0NvbXBvbmVudEZhY3Rvcnk8RGF0YVR5cGUsIENvbXBvbmVudFR5cGU+LFxuICAgIGRhdGFBcnJheSA6IERhdGFUeXBlW10sXG4gICAgZGF0YUlkQWNjZXNzb3IgOiBzdHJpbmcsXG4gICAgZHluYW1pY0NvbXBvbmVudENhbGxiYWNrcz8gOiBTdHJpbmdNYXA8RnVuY3Rpb24+LFxuICAgIG9uQWRkVmlldz8gOiBGdW5jdGlvbixcbiAgICBvblVwZGF0ZVZpZXc/IDogRnVuY3Rpb24sXG4gICAgb25EZWxldGVWaWV3PyA6IEZ1bmN0aW9uXG4gICkgOiB2b2lkXG4gIHtcbiAgICBpZiAgKCFjb250YWluZXIpXG4gICAge1xuICAgICAgdGhyb3cgJ2NoaWxkIGNvbnRhaW5lciAoI2NoaWxkQ29udGFpbmVyKSBjb3VsZCBub3QgYmUgZm91bmQgaW4gdGhlIGNvbnRhaW5lciB2aWV3J1xuICAgIH1cbiAgICBmb3IgKGxldCBkYXRhIG9mIGRhdGFBcnJheSlcbiAgICB7XG4gICAgICBsZXQgYWxyZWFkeVByZXNlbnQgPSBmYWxzZTtcbiAgICAgIGZvciAobGV0IG9sZERhdGEgb2YgdGhpcy5vbGREYXRhQXJyYXkpXG4gICAgICB7XG4gICAgICAgIGlmIChkYXRhW2RhdGFJZEFjY2Vzc29yXSA9PT0gb2xkRGF0YVtkYXRhSWRBY2Nlc3Nvcl0pXG4gICAgICAgIHtcbiAgICAgICAgICBhbHJlYWR5UHJlc2VudCA9IHRydWU7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGlmIChhbHJlYWR5UHJlc2VudClcbiAgICAgIHtcbiAgICAgICAgdGhpcy51cGRhdGVEeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciwgZGF0YSwgZGF0YUlkQWNjZXNzb3IsIG9uVXBkYXRlVmlldyk7XG4gICAgICB9XG4gICAgICBlbHNlXG4gICAgICB7XG4gICAgICAgIHRoaXMuYWRkRHluYW1pY0NvbXBvbmVudChjb250YWluZXIsIGZhY3RvcnksIGRhdGEsIGRhdGFJZEFjY2Vzc29yLCBkeW5hbWljQ29tcG9uZW50Q2FsbGJhY2tzLCBvbkFkZFZpZXcpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZvciAobGV0IG9sZERhdGEgb2YgdGhpcy5vbGREYXRhQXJyYXkpXG4gICAge1xuICAgICAgbGV0IHN0aWxsUHJlc2VudCA9IGZhbHNlO1xuICAgICAgZm9yIChsZXQgZGF0YSBvZiBkYXRhQXJyYXkpXG4gICAgICB7XG4gICAgICAgIGlmIChkYXRhW2RhdGFJZEFjY2Vzc29yXSA9PT0gb2xkRGF0YVtkYXRhSWRBY2Nlc3Nvcl0pXG4gICAgICAgIHtcbiAgICAgICAgICBzdGlsbFByZXNlbnQgPSB0cnVlO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAoIXN0aWxsUHJlc2VudClcbiAgICAgIHtcbiAgICAgICAgdGhpcy5yZW1vdmVEeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciwgb2xkRGF0YSwgZGF0YUlkQWNjZXNzb3IsIG9uRGVsZXRlVmlldyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdGhpcy5vbGREYXRhQXJyYXkgPSBkYXRhQXJyYXk7XG4gIH1cblxuICAvKipcbiAgICogQSBudW1lcmljIG1hcCB1c2VkIHRvIHN0b3JlIHRoZSBkeW5hbWljIHZpZXdzXG4gICAqL1xuICBwcml2YXRlIGR5bmFtaWNWaWV3Q29tcG9uZW50TWFwIDogTnVtZXJpY01hcDxDb21wb25lbnRSZWY8Q29tcG9uZW50VHlwZT4+ID0ge307XG5cbiAgcHJpdmF0ZSByZW1vdmVEeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWYsIGRhdGEgOiBEYXRhVHlwZSwgZGF0YUlkQWNjZXNzb3IgOiBzdHJpbmcsIG9uRGVsZXRlVmlldz8gOiBGdW5jdGlvbikgOiB2b2lkXG4gIHtcbiAgICBpZiAodHlwZW9mIGRhdGFbZGF0YUlkQWNjZXNzb3JdICE9PSAnbnVtYmVyJylcbiAgICB7XG4gICAgICB0aHJvdyAnaWQgKCcgKyBkYXRhSWRBY2Nlc3NvciArICcpIG5vdCBmb3VuZCBpbiBkYXRhIHN0cnVjdHVyZSAoZGVsZXRlIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlIGlmICghdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0pXG4gICAge1xuICAgICAgdGhyb3cgJ2R5bmFtaWMgdmlldyBub3QgZm91bmQgaW4gdGhlIGR5bmFtaWMgdmlldyBjb250YWluZXIgKGRlbGV0ZSBkeW5hbWljIGNvbXBvbmVudCBtZXRob2QpJztcbiAgICB9XG4gICAgZWxzZVxuICAgIHtcbiAgICAgIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbZGF0YVtkYXRhSWRBY2Nlc3Nvcl1dLmRlc3Ryb3koKTtcbiAgICAgIGRlbGV0ZSB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXTtcbiAgICAgIGlmIChvbkRlbGV0ZVZpZXcpIG9uRGVsZXRlVmlldyhkYXRhLCB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBhZGREeW5hbWljQ29tcG9uZW50KFxuICAgIGNvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWYsXG4gICAgZmFjdG9yeSA6IER5bmFtaWNDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlLCBDb21wb25lbnRUeXBlPixcbiAgICBkYXRhIDogRGF0YVR5cGUsXG4gICAgZGF0YUlkQWNjZXNzb3IgOiBzdHJpbmcsXG4gICAgZHluYW1pY0NvbXBvbmVudENhbGxiYWNrcyA6IFN0cmluZ01hcDxGdW5jdGlvbj4sXG4gICAgb25BZGRWaWV3PyA6IEZ1bmN0aW9uXG4gICkgOiB2b2lkXG4gIHtcbiAgICBpZiAodHlwZW9mIGRhdGFbZGF0YUlkQWNjZXNzb3JdICE9PSAnbnVtYmVyJylcbiAgICB7XG4gICAgICB0aHJvdyAnaWQgKCcgKyBkYXRhSWRBY2Nlc3NvciArICcpIG5vdCBmb3VuZCBpbiBkYXRhIHN0cnVjdHVyZSAoYWRkIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlIGlmICh0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSlcbiAgICB7XG4gICAgICB0aHJvdyAnZHluYW1pYyB2aWV3IGFscmVhZHkgcHJlc2VudCBpbiB0aGUgZHluYW1pYyB2aWV3IGNvbnRhaW5lciAoYWRkIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlXG4gICAge1xuICAgICAgbGV0IGNvbXBvbmVudFJlZiA9IGZhY3RvcnkuaW5zZXJ0RHluYW1pY0NvbXBvbmVudChjb250YWluZXIsIGRhdGEsIGR5bmFtaWNDb21wb25lbnRDYWxsYmFja3MpO1xuICAgICAgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0gPSBjb21wb25lbnRSZWY7XG4gICAgICBpZiAob25BZGRWaWV3KSBvbkFkZFZpZXcoZGF0YSwgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0pO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBVcGRhdGVzIGFuIGRhdGEgdmlldyBtb2RlbFxuICAgKiBAcGFyYW0gZGF0YSB0aGUgbmV3IGRhdGEgbW9kZWwgdG8gdXNlXG4gICAqL1xuICBwcml2YXRlIHVwZGF0ZUR5bmFtaWNDb21wb25lbnQoY29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZiwgZGF0YSA6IERhdGFUeXBlLCBkYXRhSWRBY2Nlc3NvciA6IHN0cmluZywgb25VcGRhdGVWaWV3PyA6IEZ1bmN0aW9uKSA6IHZvaWRcbiAge1xuICAgIGlmICh0eXBlb2YgZGF0YVtkYXRhSWRBY2Nlc3Nvcl0gIT09ICdudW1iZXInKVxuICAgIHtcbiAgICAgIHRocm93ICdpZCAoJyArIGRhdGFJZEFjY2Vzc29yICsgJykgbm90IGZvdW5kIGluIGRhdGEgc3RydWN0dXJlIChhZGQgZHluYW1pYyBjb21wb25lbnQgbWV0aG9kKSc7XG4gICAgfVxuICAgIGVsc2UgaWYgKCF0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSlcbiAgICB7XG4gICAgICB0aHJvdyAnZHluYW1pYyB2aWV3IG5vdCBmb3VuZCBpbiB0aGUgZHluYW1pYyB2aWV3IGNvbnRhaW5lciAoYWRkIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlXG4gICAge1xuICAgICAgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0uaW5zdGFuY2UudXBkYXRlRGF0YShkYXRhKTtcbiAgICAgIGlmIChvblVwZGF0ZVZpZXcpIG9uVXBkYXRlVmlldyhkYXRhLCB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSk7XG4gICAgfVxuICB9XG59IiwiaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudEZhY3RvcnkgfSBmcm9tIFwiLi4vZHluYW1pYy9keW5hbWljLmZhY3RvcnlcIjtcbmltcG9ydCB7IENhbGVuZGFyV2Vla0RhdGEgfSBmcm9tIFwiLi4vLi4vdHlwZXMvY2FsZW5kYXItd2Vla1wiO1xuaW1wb3J0IHsgQWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudCB9IGZyb20gXCIuL2Fic3RyYWN0LXdlZWstc3VtbWFyeS5jb21wb25lbnRcIjtcbmltcG9ydCB7IENvbXBvbmVudFJlZiwgVmlld0NvbnRhaW5lclJlZiwgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBUeXBlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFN0cmluZ01hcCB9IGZyb20gXCIuLi8uLi90eXBlcy9tYXBzXCI7XG5pbXBvcnQgeyBpc1NhbWVEYXkgfSBmcm9tIFwiLi4vLi4vdXRpbHMvZGF0ZVwiO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgV2Vla1N1bW1hcnlDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlPiBleHRlbmRzIER5bmFtaWNDb21wb25lbnRGYWN0b3J5PENhbGVuZGFyV2Vla0RhdGE8RGF0YVR5cGU+LCBBYnN0cmFjdFdlZWtTdW1tYXJ5Q29tcG9uZW50PERhdGFUeXBlPj5cbntcbiAgcHVibGljIGNvbnN0cnVjdG9yKGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlciA6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29tcG9uZW50VHlwZSA6IFR5cGU8QWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudDxEYXRhVHlwZT4+KVxuICB7XG4gICAgc3VwZXIoY29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb21wb25lbnRUeXBlKTtcbiAgfVxuXG4gIHB1YmxpYyBpbnNlcnREeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWYsIGRhdGEgOiBDYWxlbmRhcldlZWtEYXRhPERhdGFUeXBlPiwgY2FsbGJhY2tzPzogU3RyaW5nTWFwPEZ1bmN0aW9uPikgOiBDb21wb25lbnRSZWY8QWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudDxEYXRhVHlwZT4+XG4gIHtcbiAgICBsZXQgcG9zaXRpb24gPSB0aGlzLmZpbmRXZWVrUG9zaXRpb25JbkNvbnRhaW5lcihjb250YWluZXIsIGRhdGEpO1xuICAgIHJldHVybiAocG9zaXRpb24gPj0gMCkgPyBzdXBlci5pbnNlcnREeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciwgZGF0YSwgY2FsbGJhY2tzLCBwb3NpdGlvbikgOiB1bmRlZmluZWQ7XG4gIH1cblxuICBwcml2YXRlIGZpbmRXZWVrUG9zaXRpb25JbkNvbnRhaW5lcihjb250YWluZXIgOiBWaWV3Q29udGFpbmVyUmVmLCBkYXRhIDogQ2FsZW5kYXJXZWVrRGF0YTxEYXRhVHlwZT4pIDogbnVtYmVyXG4gIHtcbiAgICBsZXQgaW5kZXg7XG4gICAgbGV0IGZvdW5kID0gZmFsc2U7XG4gICAgZm9yIChpbmRleCA9IDA7ICFmb3VuZCAmJiBpbmRleCA8IGNvbnRhaW5lci5sZW5ndGg7IGluZGV4KyspXG4gICAge1xuICAgICAgbGV0IGNvbXBvbmVudEluc3RhbmNlID0gY29udGFpbmVyLmdldChpbmRleClbJ192aWV3J11bJ25vZGVzJ11bMV1bJ2luc3RhbmNlJ107XG4gICAgICBpZiAoY29tcG9uZW50SW5zdGFuY2UuZGF0YS5kYXRlICYmIGlzU2FtZURheShjb21wb25lbnRJbnN0YW5jZS5kYXRhLmRhdGUsIGRhdGEuZW5kRGF0ZSkpXG4gICAgICB7XG4gICAgICAgIHJldHVybiBpbmRleCArIDE7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiAtMTtcbiAgfVxufSIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBTaW1wbGVDaGFuZ2VzLCBDb21wb25lbnRSZWYsIFZpZXdDaGlsZCwgVmlld0NvbnRhaW5lclJlZiwgSG9zdEJpbmRpbmcsIEluamVjdCwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgZ2V0V2Vla051bWJlciB9IGZyb20gJy4uLy4uL3V0aWxzL2RhdGUnO1xuaW1wb3J0IHsgRXZlbnREaXNwYXRjaGVyIH0gZnJvbSAnLi4vLi4vdXRpbHMvZGlzcGF0Y2hlcic7XG5pbXBvcnQgeyBBYnN0cmFjdERheUNlbGxDb21wb25lbnQgfSBmcm9tICcuLi9kYXktY2VsbC9hYnN0cmFjdC1kYXktY2VsbC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ2FsZW5kYXJEYXlEYXRhIH0gZnJvbSAnLi4vLi4vdHlwZXMvY2FsZW5kYXItZGF5JztcbmltcG9ydCB7IEV2ZW50IH0gZnJvbSAnLi4vLi4vdHlwZXMvZXZlbnQnO1xuaW1wb3J0IHsgQ2FsZW5kYXJXZWVrRGF0YSB9IGZyb20gJy4uLy4uL3R5cGVzL2NhbGVuZGFyLXdlZWsnO1xuaW1wb3J0IHsgQWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudCB9IGZyb20gJy4uL3dlZWstc3VtbWFyeS9hYnN0cmFjdC13ZWVrLXN1bW1hcnkuY29tcG9uZW50JztcbmltcG9ydCB7IEV2ZW50Q29tcG9uZW50RmFjdG9yeSB9IGZyb20gJy4uL2V2ZW50L2V2ZW50LmZhY3RvcnknO1xuaW1wb3J0IHsgRGF5Q2VsbENvbXBvbmVudEZhY3RvcnkgfSBmcm9tICcuLi9kYXktY2VsbC9kYXktY2VsbC5mYWN0b3J5JztcbmltcG9ydCB7IER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXIgfSBmcm9tICcuLi9keW5hbWljL2R5bmFtaWMtbWFuYWdlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgV2Vla1N1bW1hcnlDb21wb25lbnRGYWN0b3J5IH0gZnJvbSAnLi4vd2Vlay1zdW1tYXJ5L3dlZWstc3VtbWFyeS5mYXRvcnknO1xuXG5sZXQgbW9tZW50ID0gcmVxdWlyZSgnbW9tZW50Jyk7XG5sZXQgaGFzaCA9IHJlcXVpcmUoJ29iamVjdC1oYXNoJyk7XG5cbmV4cG9ydCBjb25zdCBEQVlfQ0VMTF9DT01QT05FTlRfRkFDVE9SWV9QUk9WSURFUiA9ICdEYXlDZWxsQ29tcG9uZW50RmFjdG9yeSc7XG5leHBvcnQgY29uc3QgRVZFTlRfQ09NUE9ORU5UX0ZBQ1RPUllfUFJPVklERVIgICAgPSAnRXZlbnRDb21wb25lbnRGYWN0b3J5JztcbmV4cG9ydCBjb25zdCBXRUVLX1NVTU1BUllfQ09NUE9ORU5UX0ZBQ1RPUlkgICAgICA9ICdXZWVrU3VtbWFyeUNvbXBvbmVudEZhY3RvcnknO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhbmd1bGFyLWFkdmFuY2VkLWNhbGVuZGFyJyxcbiAgdGVtcGxhdGU6IGA8aDUgKm5nRm9yPSdsZXQgbmFtZSBvZiBoZWFkZXJzJyBjbGFzcz0naGVhZGVyJz4ge3tuYW1lfX0gPC9oNT5cbjxuZy10ZW1wbGF0ZSAjZGF5R3JpZD48L25nLXRlbXBsYXRlPlxuPGg1ICpuZ0Zvcj0nbGV0IG5hbWUgb2YgaGVhZGVycycgY2xhc3M9J2Zvb3Rlcic+IHt7bmFtZX19IDwvaDU+YCxcbiAgc3R5bGVzOiBbYEBpbXBvcnQgdXJsKGh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1SYWxld2F5KTsuZm9vdGVyLC5oZWFkZXJ7dGV4dC1hbGlnbjpjZW50ZXI7Zm9udC1mYW1pbHk6UmFsZXdheSxzYW5zLXNlcmlmfWBdXG59KVxuZXhwb3J0IGNsYXNzIENhbGVuZGFyQ29tcG9uZW50XG57XG4gIC8qKlxuICAgKiBGaXJzdCBkYXRlIG9mIHRoZSBwZXJpb2QgdG8gZGlzcGxheSBpbiB0aGUgY2FsZW5kYXJcbiAgICovXG4gIEBJbnB1dCgnc3RhcnQtZGF0ZScpIHByaXZhdGUgc3RhcnREYXRlIDogRGF0ZTtcblxuICAvKipcbiAgICogTGFzdCBkYXRlIG9mIHRoZSBwZXJpb2QgdG8gZGlzcGxheSBpbiB0aGUgY2FsZW5kYXJcbiAgICovXG4gIEBJbnB1dCgnZW5kLWRhdGUnKSBwcml2YXRlIGVuZERhdGUgOiBEYXRlO1xuICBcbiAgLyoqXG4gICAqIFdlZWsgZGF5cyBsYWJlbCB0byBkaXNwbGF5IGluIHRoZSBoZWFkZXJcbiAgICogKGRlZmF1bHQgaXMgZW5nbGlzaCkuXG4gICAqL1xuICBASW5wdXQoJ2hlYWRlcnMnKSBwdWJsaWMgaGVhZGVycyA6IHN0cmluZ1tdID0gW1xuICAgICdNb25kYXknLFxuICAgICdUdWVzZGF5JyxcbiAgICAnV2VkbmVzZGF5JyxcbiAgICAnVGh1cnNkYXknLFxuICAgICdGcmlkYXknLFxuICAgICdTYXR1cmRheScsXG4gICAgJ1N1bmRheScsXG4gIF07XG5cbiAgLyoqXG4gICAqIEEgbGlzdCBvZiBldmVudHMgdG8gZGlzcGxheSBpbiB0aGUgY2FsbGVuZGFyLlxuICAgKi9cbiAgQElucHV0KCdldmVudHMnKSBwcml2YXRlIGV2ZW50cyA6IEV2ZW50W10gPSBbXTtcbiAgXG4gIC8qKlxuICAgKiBUaGUgbmFtZSBvZiB0aGUgZGF0ZSBhdHRyaWJ1dGUgdG8gdXNlXG4gICAqIHdoZW4gZGlzcGxheWluZyB0aGUgZXZlbnRzIChkZWZhdWx0IGlzICdkYXRlJykuXG4gICAqL1xuICBASW5wdXQoJ2RhdGUtYWNjZXNzb3InKSBwcml2YXRlIGRhdGVBY2Nlc3NvciA6IHN0cmluZyA9ICdkYXRlJztcblxuICAvKipcbiAgICogVGhlIG5hbWUgb2YgdGhlIGlkIGF0dHJpYnV0ZSB0byB1c2VcbiAgICogd2hlbiBkaXNwbGF5aW5nIHRoZSBldmVudHMgKGRlZmF1bHQgaXMgJ2lkJykuXG4gICAqL1xuICBASW5wdXQoJ2lkLWFjY2Vzc29yJykgcHJpdmF0ZSBpZEFjY2Vzc29yIDogc3RyaW5nID0gJ2lkJztcblxuICBwdWJsaWMgY29uc3RydWN0b3IoXG4gICAgQEluamVjdChEQVlfQ0VMTF9DT01QT05FTlRfRkFDVE9SWV9QUk9WSURFUikgcHVibGljIGRheUNlbGxDb21wb25lbnRGYWN0b3J5IDogRGF5Q2VsbENvbXBvbmVudEZhY3Rvcnk8RXZlbnQ+LFxuICAgIEBPcHRpb25hbCgpIEBJbmplY3QoRVZFTlRfQ09NUE9ORU5UX0ZBQ1RPUllfUFJPVklERVIpIHB1YmxpYyBldmVudENvbXBvbmVudEZhY3RvcnkgOiBFdmVudENvbXBvbmVudEZhY3Rvcnk8RXZlbnQ+LFxuICAgIEBPcHRpb25hbCgpIEBJbmplY3QoV0VFS19TVU1NQVJZX0NPTVBPTkVOVF9GQUNUT1JZKSBwdWJsaWMgd2Vla1N1bW1hcnlDb21wb25lbnRGYWN0b3J5IDogV2Vla1N1bW1hcnlDb21wb25lbnRGYWN0b3J5PEV2ZW50PlxuICApe31cblxuICAvKiAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gKi9cblxuICAvKipcbiAgICogVmlldyBjb250YWluZXIgc3RvcmluZyB0aGUgZHluYW1pYyBjb21wb25lbnRzXG4gICAqL1xuICBAVmlld0NoaWxkKCdkYXlHcmlkJywgeyByZWFkOiBWaWV3Q29udGFpbmVyUmVmIH0pXG4gIHByaXZhdGUgZGF5R3JpZENvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWY7XG4gIHByaXZhdGUgZHluYW1pY0RheUNlbGxWaWV3TWFuYWdlciA6IER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXI8RXZlbnQsIEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudDxFdmVudD4+XG4gICAgPSBuZXcgRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlcjxFdmVudCwgQWJzdHJhY3REYXlDZWxsQ29tcG9uZW50PEV2ZW50Pj4oKTtcbiAgcHJpdmF0ZSBkeW5hbWljV2Vla1N1bW1hcnlWaWV3TWFuYWdlciA6IER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXI8RXZlbnQsIEFic3RyYWN0V2Vla1N1bW1hcnlDb21wb25lbnQ8RXZlbnQ+PlxuICAgID0gbmV3IER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXI8RXZlbnQsIEFic3RyYWN0V2Vla1N1bW1hcnlDb21wb25lbnQ8RXZlbnQ+PigpO1xuXG4gIC8qIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAqL1xuXG4gIHByaXZhdGUgZGlzcGF0Y2hlciA6IEV2ZW50RGlzcGF0Y2hlcjtcbiAgcHJpdmF0ZSBkYXlzIDogQ2FsZW5kYXJEYXlEYXRhPEV2ZW50PltdO1xuICBwcml2YXRlIHdlZWtzIDogQ2FsZW5kYXJXZWVrRGF0YTxFdmVudD5bXTtcbiAgXG4gIHByaXZhdGUgb2xkSGFzaCA6IHN0cmluZyA9ICcnO1xuICBuZ0RvQ2hlY2soKVxuICB7XG4gICAgbGV0IG5ld0hhc2ggPSBoYXNoLnNoYTEodGhpcy5ldmVudHMpO1xuICAgIGlmIChuZXdIYXNoICE9PSB0aGlzLm9sZEhhc2gpXG4gICAge1xuICAgICAgdGhpcy51cGRhdGVDYWxlbmRhcigpO1xuICAgICAgdGhpcy5vbGRIYXNoID0gbmV3SGFzaDtcbiAgICB9XG4gIH1cblxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzIDogU2ltcGxlQ2hhbmdlcylcbiAge1xuICAgIGlmIChjaGFuZ2VzLnN0YXJ0RGF0ZSB8fCBjaGFuZ2VzLmVuZERhdGUgfHwgY2hhbmdlcy5kYXRlQWNjZXNzb3IpXG4gICAge1xuICAgICAgbGV0IHN0YXJ0RGF0ZSA9IChjaGFuZ2VzLnN0YXJ0RGF0ZSkgPyBjaGFuZ2VzLnN0YXJ0RGF0ZS5jdXJyZW50VmFsdWUgOiB0aGlzLnN0YXJ0RGF0ZTtcbiAgICAgIGxldCBlbmREYXRlID0gKGNoYW5nZXMuZW5kRGF0ZSkgPyBjaGFuZ2VzLmVuZERhdGUuY3VycmVudFZhbHVlIDogdGhpcy5lbmREYXRlO1xuICAgICAgbGV0IGRhdGVBY2Nlc3NvciA9IChjaGFuZ2VzLmRhdGVBY2Nlc3NvcikgPyBjaGFuZ2VzLmRhdGVBY2Nlc3Nvci5jdXJyZW50VmFsdWUgOiB0aGlzLmRhdGVBY2Nlc3NvcjtcblxuICAgICAgdGhpcy5kaXNwYXRjaGVyID0gbmV3IEV2ZW50RGlzcGF0Y2hlcihkYXRlQWNjZXNzb3IsIHN0YXJ0RGF0ZSwgZW5kRGF0ZSk7XG4gICAgICB0aGlzLnVwZGF0ZUNhbGVuZGFyKCk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIE9sZCBzdGFydCBhbmQgZW5kIGRhdGUgb2YgdGhlIGNhbGVuZGFyLlxuICAgKiBVc2VkIHRvIGtlZXAgdHJhY2sgb2YgdGhlIGNoYW5nZXMgaW4gcGVyaW9kXG4gICAqIGluIG9yZGVyIHRvIHVwZGF0ZSB0aGUgdmlldy5cbiAgICovXG4gIHByaXZhdGUgb2xkU3RhcnREYXRlIDogRGF0ZTtcbiAgcHJpdmF0ZSBvbGRFbmREYXRlIDogRGF0ZTtcblxuXG4gIHByaXZhdGUgdXBkYXRlQ2FsZW5kYXIoKVxuICB7XG4gICAgaWYgKCF0aGlzLmRheUNlbGxDb21wb25lbnRGYWN0b3J5KVxuICAgIHtcbiAgICAgIHRoaXMuZGF5cyA9IFtdO1xuICAgICAgdGhyb3cgJ2RheSBjZWxsIGNvbXBvbmVudCBmYWN0b3J5IChkYXktY29tcG9uZW50LWZhY3RvcnkpIHdhcyBub3Qgc2V0ISdcbiAgICB9XG4gICAgZWxzZSBpZiAoIXRoaXMuZGF5R3JpZENvbnRhaW5lcilcbiAgICB7XG4gICAgICB0aGlzLmRheXMgPSBbXTtcbiAgICAgIHRocm93ICdkYXkgZ3JpZCAoZGF5R3JpZCkgbm90IGZvdW5kIGluIHRlbXBsYXRlICEnXG4gICAgfVxuICAgIGVsc2VcbiAgICB7XG4gICAgICB0aGlzLmRpc3BhdGNoRXZlbnRzKCk7XG4gICAgICB0aGlzLnVwZGF0ZURheUNlbGxWaWV3cygpO1xuXG4gICAgICBpZiAodGhpcy53ZWVrU3VtbWFyeUNvbXBvbmVudEZhY3RvcnkpXG4gICAgICB7XG4gICAgICAgIHRoaXMudXBkYXRlV2Vla1N1bW1hcnlDZWxsVmlld3MoKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuXG4gIC8qKlxuICAgKiBTb3J0cyBldmVudHMgYnkgZGF0ZVxuICAgKi9cbiAgcHJpdmF0ZSBkaXNwYXRjaEV2ZW50cygpXG4gIHtcbiAgICB0aGlzLmRpc3BhdGNoZXIuY2xlYXIoKTtcbiAgICBmb3IgKGxldCBldmVudCBvZiB0aGlzLmV2ZW50cylcbiAgICB7XG4gICAgICB0aGlzLmRpc3BhdGNoZXIuYWRkKGV2ZW50KTtcbiAgICB9XG4gICAgdGhpcy5kYXlzID0gdGhpcy5kaXNwYXRjaGVyLmdldERhdGVBcnJheSgpLm1hcChkYXRlID0+ICh7XG4gICAgICBpZDogdGhpcy5kaXNwYXRjaGVyLmdldERheU51bWJlcihkYXRlKSxcbiAgICAgIGRhdGU6IGRhdGUsXG4gICAgICBjYWxlbmRhclN0YXJ0RGF0ZTogdGhpcy5zdGFydERhdGUsXG4gICAgICBjYWxlbmRhckVuZERhdGU6IHRoaXMuZW5kRGF0ZSxcbiAgICAgIGV2ZW50czogdGhpcy5kaXNwYXRjaGVyLmdldEV2ZW50cyhkYXRlKSxcbiAgICAgIGRhdGVBY2Nlc3NvcjogdGhpcy5kYXRlQWNjZXNzb3IsXG4gICAgfSkpO1xuICB9XG5cbiAgcHJpdmF0ZSB1cGRhdGVEYXlDZWxsVmlld3MoKVxuICB7XG4gICAgdGhpcy5keW5hbWljRGF5Q2VsbFZpZXdNYW5hZ2VyLnVwZGF0ZUNvbnRhaW5lcihcbiAgICAgIHRoaXMuZGF5R3JpZENvbnRhaW5lcixcbiAgICAgIHRoaXMuZGF5Q2VsbENvbXBvbmVudEZhY3RvcnksXG4gICAgICB0aGlzLmRheXMsXG4gICAgICAnaWQnLFxuICAgICAge1xuICAgICAgICBhZGQ6IHRoaXMub25BZGQuYmluZCh0aGlzKSxcbiAgICAgICAgdXBkYXRlOiB0aGlzLm9uVXBkYXRlLmJpbmQodGhpcyksXG4gICAgICAgIGRlbGV0ZTogdGhpcy5vbkRlbGV0ZS5iaW5kKHRoaXMpXG4gICAgICB9LFxuICAgICAgdGhpcy51cGRhdGVEYXlDZWxsLmJpbmQodGhpcyksXG4gICAgICB0aGlzLnVwZGF0ZURheUNlbGwuYmluZCh0aGlzKSxcbiAgICAgIHRoaXMudXBkYXRlRGF5Q2VsbC5iaW5kKHRoaXMpXG4gICAgKTtcbiAgfVxuXG4gIHByaXZhdGUgdXBkYXRlV2Vla1N1bW1hcnlDZWxsVmlld3MoKVxuICB7XG4gICAgdGhpcy53ZWVrcyA9IHRoaXMuZGlzcGF0Y2hlci5nZXRXZWVrQXJyYXkoKS5tYXAod2VlayA9PiAoe1xuICAgICAgaWQ6IGdldFdlZWtOdW1iZXIod2Vlay5zdGFydERhdGUpLFxuICAgICAgc3RhcnREYXRlOiB3ZWVrLnN0YXJ0RGF0ZSxcbiAgICAgIGVuZERhdGU6IHdlZWsuZW5kRGF0ZSxcbiAgICAgIGNhbGVuZGFyU3RhcnREYXRlOiB0aGlzLnN0YXJ0RGF0ZSxcbiAgICAgIGNhbGVuZGFyRW5kRGF0ZTogdGhpcy5lbmREYXRlLFxuICAgICAgZXZlbnRzOiB0aGlzLmRpc3BhdGNoZXIuZ2V0RXZlbnRzQmV0d2Vlbih3ZWVrLnN0YXJ0RGF0ZSwgd2Vlay5lbmREYXRlKSxcbiAgICAgIGRhdGVBY2Nlc3NvcjogdGhpcy5kYXRlQWNjZXNzb3IsXG4gICAgfSkpO1xuXG4gICAgaWYgKHRoaXMuc3RhcnREYXRlICE9PSB0aGlzLm9sZFN0YXJ0RGF0ZSB8fCB0aGlzLmVuZERhdGUgIT09IHRoaXMub2xkRW5kRGF0ZSlcbiAgICB7XG4gICAgICB0aGlzLmR5bmFtaWNXZWVrU3VtbWFyeVZpZXdNYW5hZ2VyLmNsZWFyKCk7XG4gICAgICB0aGlzLm9sZFN0YXJ0RGF0ZSA9IHRoaXMuc3RhcnREYXRlO1xuICAgICAgdGhpcy5vbGRFbmREYXRlID0gdGhpcy5lbmREYXRlO1xuICAgIH1cbiAgICBcbiAgICB0aGlzLmR5bmFtaWNXZWVrU3VtbWFyeVZpZXdNYW5hZ2VyLnVwZGF0ZUNvbnRhaW5lcihcbiAgICAgIHRoaXMuZGF5R3JpZENvbnRhaW5lcixcbiAgICAgIHRoaXMud2Vla1N1bW1hcnlDb21wb25lbnRGYWN0b3J5LFxuICAgICAgdGhpcy53ZWVrcyxcbiAgICAgICdpZCcsXG4gICAgICB7XG4gICAgICAgIGFkZDogdGhpcy5vbkFkZC5iaW5kKHRoaXMpLFxuICAgICAgICB1cGRhdGU6IHRoaXMub25VcGRhdGUuYmluZCh0aGlzKSxcbiAgICAgICAgZGVsZXRlOiB0aGlzLm9uRGVsZXRlLmJpbmQodGhpcylcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbiAgLyoqXG4gICAqIFRyaWdnZXJzIGEgZGF5IGNlbGwgdmlldyBpbnRlcm5hbCB1cGRhdGVcbiAgICogQHBhcmFtIGNvbXBvbmVudCB0aGUgY29tcG9uZW50IHJlZiBvZiB0aGUgZGF5IGNlbGwgdG8gdXBkYXRlXG4gICAqL1xuICBwcml2YXRlIHVwZGF0ZURheUNlbGwoZGF0YSA6IENhbGVuZGFyRGF5RGF0YTxFdmVudD4sIGNvbXBvbmVudCA6IENvbXBvbmVudFJlZjxBYnN0cmFjdERheUNlbGxDb21wb25lbnQ8RXZlbnQ+PikgOiB2b2lkXG4gIHtcbiAgICBsZXQgaW5zdGFuY2UgPSA8QWJzdHJhY3REYXlDZWxsQ29tcG9uZW50PEV2ZW50Pj5jb21wb25lbnQuaW5zdGFuY2U7XG4gICAgaW5zdGFuY2UudXBkYXRlRXZlbnRWaWV3cyh0aGlzLmV2ZW50Q29tcG9uZW50RmFjdG9yeSwgdGhpcy5pZEFjY2Vzc29yKTtcbiAgfVxuXG4gIC8qIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAqL1xuXG4gIC8qKlxuICAgKiBUcmlnZ2VycyBhIGRlbGV0ZSBldmVudCBlbWlzc2lvblxuICAgKiBAcGFyYW0gZXZlbnQgdGhlIGV2ZW50IHRvIGRlbGV0ZVxuICAgKi9cbiAgQE91dHB1dCgnZGVsZXRlJylcbiAgcHJpdmF0ZSBkZWxldGVFbWl0dGVyIDogRXZlbnRFbWl0dGVyPEV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXI8RXZlbnQ+KCk7XG4gIHByaXZhdGUgb25EZWxldGUoZXZlbnQ6IEV2ZW50KVxuICB7XG4gICAgdGhpcy5kZWxldGVFbWl0dGVyLmVtaXQoZXZlbnQpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRyaWdnZXJzIGFuIHVwZGF0ZSBldmVudCBlbWlzc2lvblxuICAgKiBAcGFyYW0gZXZlbnQgdGhlIGV2ZW50IHRvIHVwZGF0ZVxuICAgKi9cbiAgQE91dHB1dCgndXBkYXRlJylcbiAgcHJpdmF0ZSB1cGRhdGVFbWl0dGVyIDogRXZlbnRFbWl0dGVyPEV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXI8RXZlbnQ+KCk7XG4gIHByaXZhdGUgb25VcGRhdGUoZXZlbnQ6IEV2ZW50KVxuICB7XG4gICAgdGhpcy51cGRhdGVFbWl0dGVyLmVtaXQoZXZlbnQpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRyaWdnZXJzIGFuIGFkZCBldmVudCBlbWlzc2lvblxuICAgKiBAcGFyYW0gZXZlbnQgdGhlIGV2ZW50IHRvIGFkZFxuICAgKi9cbiAgQE91dHB1dCgnYWRkJylcbiAgcHJpdmF0ZSBhZGRFbWl0dGVyIDogRXZlbnRFbWl0dGVyPEV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXI8RXZlbnQ+KCk7XG4gIHByaXZhdGUgb25BZGQoZXZlbnQ6IEV2ZW50KVxuICB7XG4gICAgdGhpcy5hZGRFbWl0dGVyLmVtaXQoZXZlbnQpO1xuICB9XG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcblxuaW1wb3J0IHsgQ2FsZW5kYXJDb21wb25lbnQgfSBmcm9tICcuL3ZpZXdzL2NhbGVuZGFyL2NhbGVuZGFyLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIENhbGVuZGFyQ29tcG9uZW50LFxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgQ2FsZW5kYXJDb21wb25lbnRcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBBbmd1bGFyQWR2YW5jZWRDYWxlbmRhck1vZHVsZSB7IH1cbiIsImltcG9ydCB7IElucHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIER5bmFtaWNDb21wb25lbnQ8RGF0YVR5cGU+XG57XG4gIEBJbnB1dCgnZGF0YScpXG4gIHB1YmxpYyBkYXRhIDogRGF0YVR5cGU7XG4gIFxuICBwdWJsaWMgdXBkYXRlRGF0YShkYXRhIDogRGF0YVR5cGUpXG4gIHtcbiAgICB0aGlzLmRhdGEgPSBkYXRhO1xuICB9XG5cbiAgcHVibGljIGFic3RyYWN0IHN1YnNjcmliZShuYW1lIDogc3RyaW5nLCBjYWxsYmFjayA6IEZ1bmN0aW9uKSA6IHZvaWQ7XG59IiwiaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudCB9IGZyb20gXCIuLi9keW5hbWljL2R5bmFtaWMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBFdmVudEVtaXR0ZXIsIE91dHB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBEeW5hbWljQ3J1ZENvbXBvbmVudDxEYXRhVHlwZSwgQ3J1ZFR5cGU+IGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudDxEYXRhVHlwZT5cbntcbiAgcHVibGljIHN1YnNjcmliZShuYW1lIDogc3RyaW5nLCBjYWxsYmFjayA6IEZ1bmN0aW9uKSA6IHZvaWRcbiAge1xuICAgIHN3aXRjaChuYW1lKVxuICAgIHtcbiAgICAgIGNhc2UgJ3VwZGF0ZSc6XG4gICAgICAgIHRoaXMudXBkYXRlRW1pdHRlci5zdWJzY3JpYmUoY2FsbGJhY2spO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ2RlbGV0ZSc6XG4gICAgICAgIHRoaXMuZGVsZXRlRW1pdHRlci5zdWJzY3JpYmUoY2FsbGJhY2spO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ2FkZCc6XG4gICAgICAgIHRoaXMuYWRkRW1pdHRlci5zdWJzY3JpYmUoY2FsbGJhY2spO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHRocm93ICdjYWxsYmFjayB0eXBlIFxcJycgKyBuYW1lICsgJ1xcJyBpcyBub3QgYSBrbm93biBldmVudCBjYWxsYmFjayAhJztcbiAgICB9XG4gIH1cblxuICBAT3V0cHV0KCdkZWxldGUnKVxuICBwcml2YXRlIGRlbGV0ZUVtaXR0ZXIgOiBFdmVudEVtaXR0ZXI8Q3J1ZFR5cGU+ID0gbmV3IEV2ZW50RW1pdHRlcjxDcnVkVHlwZT4oKTtcbiAgcHJvdGVjdGVkIG9uRGVsZXRlKGV2ZW50OiBDcnVkVHlwZSlcbiAge1xuICAgIHRoaXMuZGVsZXRlRW1pdHRlci5lbWl0KGV2ZW50KTtcbiAgfVxuXG4gIEBPdXRwdXQoJ3VwZGF0ZScpXG4gIHByaXZhdGUgdXBkYXRlRW1pdHRlciA6IEV2ZW50RW1pdHRlcjxDcnVkVHlwZT4gPSBuZXcgRXZlbnRFbWl0dGVyPENydWRUeXBlPigpO1xuICBwcm90ZWN0ZWQgb25VcGRhdGUoZXZlbnQ6IENydWRUeXBlKVxuICB7XG4gICAgdGhpcy51cGRhdGVFbWl0dGVyLmVtaXQoZXZlbnQpO1xuICB9XG5cbiAgQE91dHB1dCgnYWRkJylcbiAgcHJpdmF0ZSBhZGRFbWl0dGVyIDogRXZlbnRFbWl0dGVyPENydWRUeXBlPiA9IG5ldyBFdmVudEVtaXR0ZXI8Q3J1ZFR5cGU+KCk7XG4gIHByb3RlY3RlZCBvbkFkZChldmVudDogQ3J1ZFR5cGUpXG4gIHtcbiAgICB0aGlzLmFkZEVtaXR0ZXIuZW1pdChldmVudCk7XG4gIH1cbn0iLCJpbXBvcnQgeyBEeW5hbWljQ3J1ZENvbXBvbmVudCB9IGZyb20gXCIuLi9keW5hbWljLWNydWQvZHluYW1pYy1jcnVkLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQ2FsZW5kYXJEYXlEYXRhIH0gZnJvbSBcIi4uLy4uL3R5cGVzL2NhbGVuZGFyLWRheVwiO1xuaW1wb3J0IHsgVmlld0NoaWxkLCBWaWV3Q29udGFpbmVyUmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXIgfSBmcm9tIFwiLi4vZHluYW1pYy9keW5hbWljLW1hbmFnZXIuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBBYnN0cmFjdEV2ZW50Q29tcG9uZW50IH0gZnJvbSBcIi4uL2V2ZW50L2Fic3RyYWN0LWV2ZW50LmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudEZhY3RvcnkgfSBmcm9tIFwiLi4vZHluYW1pYy9keW5hbWljLmZhY3RvcnlcIjtcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudDxEYXRhVHlwZT4gZXh0ZW5kcyBEeW5hbWljQ3J1ZENvbXBvbmVudDxDYWxlbmRhckRheURhdGE8RGF0YVR5cGU+LCBEYXRhVHlwZT5cbntcbiAgLyoqXG4gICAqIFZpZXcgY29udGFpbmVyIHN0b3JpbmcgdGhlIGR5bmFtaWMgY29tcG9uZW50c1xuICAgKi9cbiAgQFZpZXdDaGlsZCgnZXZlbnRWaWV3c0NvbnRhaW5lcicsIHsgcmVhZDogVmlld0NvbnRhaW5lclJlZiB9KVxuICBwcml2YXRlIGV2ZW50Vmlld0NvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWY7XG4gIHByaXZhdGUgZHluYW1pY0V2ZW50Vmlld01hbmFnZXIgOiBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyPERhdGFUeXBlLCBBYnN0cmFjdEV2ZW50Q29tcG9uZW50PERhdGFUeXBlPj5cbiAgICA9IG5ldyBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyPERhdGFUeXBlLCBBYnN0cmFjdEV2ZW50Q29tcG9uZW50PERhdGFUeXBlPj4oKTtcblxuICAvKipcbiAgICogTWV0aG9kIGNhbGxlZCB0byB1cGRhdGUgdGhlIGV2ZW50IHZpZXdzIGluIHRoZSBkYXkgY2VsbFxuICAgKi9cbiAgcHVibGljIHVwZGF0ZUV2ZW50Vmlld3MoZXZlbnRDb21wb25lbnRGYWN0b3J5IDogRHluYW1pY0NvbXBvbmVudEZhY3Rvcnk8RGF0YVR5cGUsIEFic3RyYWN0RXZlbnRDb21wb25lbnQ8RGF0YVR5cGU+PiwgaWRBY2Nlc3NvciA6IHN0cmluZylcbiAge1xuICAgIGlmIChldmVudENvbXBvbmVudEZhY3RvcnkgJiYgdGhpcy5ldmVudFZpZXdDb250YWluZXIpXG4gICAge1xuICAgICAgdGhpcy5keW5hbWljRXZlbnRWaWV3TWFuYWdlci51cGRhdGVDb250YWluZXIoXG4gICAgICAgIHRoaXMuZXZlbnRWaWV3Q29udGFpbmVyLFxuICAgICAgICBldmVudENvbXBvbmVudEZhY3RvcnksXG4gICAgICAgIHRoaXMuZGF0YS5ldmVudHMsXG4gICAgICAgIGlkQWNjZXNzb3IsXG4gICAgICAgIHtcbiAgICAgICAgICBkZWxldGU6IHRoaXMub25EZWxldGUuYmluZCh0aGlzKSxcbiAgICAgICAgICB1cGRhdGU6IHRoaXMub25VcGRhdGUuYmluZCh0aGlzKVxuICAgICAgICB9XG4gICAgICApO1xuICAgIH1cbiAgfVxufSIsImltcG9ydCB7IER5bmFtaWNDcnVkQ29tcG9uZW50IH0gZnJvbSBcIi4uL2R5bmFtaWMtY3J1ZC9keW5hbWljLWNydWQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBDYWxlbmRhcldlZWtEYXRhIH0gZnJvbSBcIi4uLy4uL3R5cGVzL2NhbGVuZGFyLXdlZWtcIjtcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEFic3RyYWN0V2Vla1N1bW1hcnlDb21wb25lbnQ8RGF0YVR5cGU+IGV4dGVuZHMgRHluYW1pY0NydWRDb21wb25lbnQ8Q2FsZW5kYXJXZWVrRGF0YTxEYXRhVHlwZT4sIERhdGFUeXBlPlxue1xufSIsImltcG9ydCB7IER5bmFtaWNDcnVkQ29tcG9uZW50IH0gZnJvbSBcIi4uL2R5bmFtaWMtY3J1ZC9keW5hbWljLWNydWQuY29tcG9uZW50XCI7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBBYnN0cmFjdEV2ZW50Q29tcG9uZW50PERhdGFUeXBlPiBleHRlbmRzIER5bmFtaWNDcnVkQ29tcG9uZW50PERhdGFUeXBlLCBEYXRhVHlwZT5cbntcbn0iXSwibmFtZXMiOlsibW9tZW50Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUEscUJBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Ozs7O0FBRS9CLG1CQUEwQixDQUFRLEVBQUUsQ0FBUTtJQUUxQyxPQUFPLEFBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQyxPQUFPLEVBQUUsQUFBQyxDQUFDO0NBQ2pJOzs7OztBQUVELHVCQUE4QixJQUFXO0lBRXZDLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0NBQzVCOzs7OztBQUVELDRCQUFtQyxJQUFXO0lBRTVDLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztDQUUvQzs7Ozs7QUFFRCx1Q0FBOEMsSUFBVztJQUV2RCxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0NBQ3pEOzs7OztBQUVELDJCQUFrQyxJQUFXO0lBRTNDLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztDQUM3Qzs7Ozs7QUFFRCxvQ0FBMkMsSUFBVztJQUVwRCxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO0NBQzlEOzs7Ozs7QUMvQkQscUJBQUlBLFFBQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7Ozs7O0FBTS9COzs7Ozs7O2dCQVVxQyxZQUFxQixFQUFrQixTQUFnQixFQUFrQixPQUFjO1FBQXZGLGlCQUFZLEdBQVosWUFBWSxDQUFTO1FBQWtCLGNBQVMsR0FBVCxTQUFTLENBQU87UUFBa0IsWUFBTyxHQUFQLE9BQU8sQ0FBTzs7Ozs7SUFFbkgsS0FBSztRQUVWLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ25CLEtBQUsscUJBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQ3BEO1lBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDdkI7Ozs7OztJQUdJLEdBQUcsQ0FBQyxLQUFXO1FBRXBCLHFCQUFJLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3BDLHFCQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsRUFDaEM7WUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMxQzs7Ozs7O0lBR0ksU0FBUyxDQUFDLElBQVc7UUFFMUIsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs7Ozs7OztJQUd6QyxnQkFBZ0IsQ0FBQyxTQUFnQixFQUFFLE9BQWM7UUFFdEQsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Ozs7Ozs7SUFPcEYsWUFBWTtRQUVqQixxQkFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ2QscUJBQUksT0FBTyxHQUFHQSxRQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3JDLHFCQUFJLElBQUksR0FBSUEsUUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNqQyxPQUFNLE9BQU8sR0FBRyxJQUFJLEVBQ3BCO1lBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztZQUM1QixPQUFPLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDakM7UUFDRCxPQUFPLElBQUksQ0FBQzs7Ozs7SUFHUCxZQUFZO1FBRWpCLHFCQUFJLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDZixxQkFBSSxPQUFPLEdBQUdBLFFBQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDckMscUJBQUksSUFBSSxHQUFJQSxRQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2pDLE9BQU0sT0FBTyxHQUFHLElBQUksRUFDcEI7WUFDRSxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ3JGLE9BQU8sR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUNqQztRQUNELE9BQU8sS0FBSyxDQUFDOzs7Ozs7OztJQVFSLFlBQVksQ0FBQyxJQUFXO1FBRTdCLE9BQU9BLFFBQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUNBLFFBQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7Ozs7Ozs7SUFPcEQsb0JBQW9CO1FBRXpCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDOztDQUU5Qzs7Ozs7Ozs7Ozs7QUMzRkQ7Ozs7O2dCQUlzQyx3QkFBbUQsRUFBRSxhQUFtQztRQUF4Riw2QkFBd0IsR0FBeEIsd0JBQXdCLENBQTJCO1FBRXJGLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLHVCQUF1QixDQUFDLGFBQWEsQ0FBQyxDQUFDOzs7Ozs7Ozs7O0lBVS9FLHNCQUFzQixDQUFDLFNBQTRCLEVBQUUsSUFBZSxFQUFFLFNBQStCLEVBQUUsS0FBZTtRQUUzSCxxQkFBSSxZQUFZLEdBQUcsU0FBUyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ2xFLHFCQUFJLFFBQVEscUJBQWtCLFlBQVksQ0FBQyxRQUFRLENBQUEsQ0FBQztRQUNwRCxRQUFRLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLElBQUksU0FBUyxFQUNiO1lBQ0UsS0FBSyxxQkFBSSxJQUFJLElBQUksU0FBUyxFQUMxQjtnQkFDRSxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzthQUMzQztTQUNGO1FBQ0QsT0FBTyxZQUFZLENBQUM7O0NBRXZCOzs7Ozs7QUNsQ0Q7Ozs7QUFJQSwyQkFBc0QsU0FBUSx1QkFBbUU7Ozs7O2dCQUU1Ryx3QkFBbUQsRUFBRSxhQUFzRDtRQUU1SCxLQUFLLENBQUMsd0JBQXdCLEVBQUUsYUFBYSxDQUFDLENBQUM7O0NBRWxEOzs7Ozs7QUNWRDs7OztBQUtBLDZCQUF3RCxTQUFRLHVCQUFzRjs7Ozs7Z0JBRWpJLHdCQUFtRCxFQUFFLGFBQXdEO1FBRTlILEtBQUssQ0FBQyx3QkFBd0IsRUFBRSxhQUFhLENBQUMsQ0FBQzs7Q0FFbEQ7Ozs7Ozs7Ozs7QUNORDs7Ozs7NEJBTXNDLEVBQUU7Ozs7dUNBOEVzQyxFQUFFOzs7OztJQTVFdkUsS0FBSztRQUVWLEtBQUsscUJBQUksRUFBRSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsRUFDM0M7WUFDRSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsRUFDcEM7Z0JBQ0UsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUMzQyxPQUFPLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUN6QztTQUNGO1FBQ0QsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7Ozs7Ozs7Ozs7Ozs7OztJQU9sQixlQUFlLENBQ3BCLFNBQTRCLEVBQzVCLE9BQTBELEVBQzFELFNBQXNCLEVBQ3RCLGNBQXVCLEVBQ3ZCLHlCQUFnRCxFQUNoRCxTQUFxQixFQUNyQixZQUF3QixFQUN4QixZQUF3QjtRQUd4QixJQUFLLENBQUMsU0FBUyxFQUNmO1lBQ0UsTUFBTSw0RUFBNEUsQ0FBQTtTQUNuRjtRQUNELEtBQUsscUJBQUksSUFBSSxJQUFJLFNBQVMsRUFDMUI7WUFDRSxxQkFBSSxjQUFjLEdBQUcsS0FBSyxDQUFDO1lBQzNCLEtBQUsscUJBQUksT0FBTyxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQ3JDO2dCQUNFLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFDcEQ7b0JBQ0UsY0FBYyxHQUFHLElBQUksQ0FBQztvQkFDdEIsTUFBTTtpQkFDUDthQUNGO1lBQ0QsSUFBSSxjQUFjLEVBQ2xCO2dCQUNFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSxZQUFZLENBQUMsQ0FBQzthQUM1RTtpQkFFRDtnQkFDRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLHlCQUF5QixFQUFFLFNBQVMsQ0FBQyxDQUFDO2FBQzFHO1NBQ0Y7UUFFRCxLQUFLLHFCQUFJLE9BQU8sSUFBSSxJQUFJLENBQUMsWUFBWSxFQUNyQztZQUNFLHFCQUFJLFlBQVksR0FBRyxLQUFLLENBQUM7WUFDekIsS0FBSyxxQkFBSSxJQUFJLElBQUksU0FBUyxFQUMxQjtnQkFDRSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxPQUFPLENBQUMsY0FBYyxDQUFDLEVBQ3BEO29CQUNFLFlBQVksR0FBRyxJQUFJLENBQUM7b0JBQ3BCLE1BQU07aUJBQ1A7YUFDRjtZQUNELElBQUksQ0FBQyxZQUFZLEVBQ2pCO2dCQUNFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxZQUFZLENBQUMsQ0FBQzthQUMvRTtTQUNGO1FBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUM7Ozs7Ozs7OztJQVF4QixzQkFBc0IsQ0FBQyxTQUE0QixFQUFFLElBQWUsRUFBRSxjQUF1QixFQUFFLFlBQXdCO1FBRTdILElBQUksT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssUUFBUSxFQUM1QztZQUNFLE1BQU0sTUFBTSxHQUFHLGNBQWMsR0FBRyxpRUFBaUUsQ0FBQztTQUNuRzthQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQzVEO1lBQ0UsTUFBTSx3RkFBd0YsQ0FBQztTQUNoRzthQUVEO1lBQ0UsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQzdELE9BQU8sSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQzFELElBQUksWUFBWTtnQkFBRSxZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzFGOzs7Ozs7Ozs7OztJQUdLLG1CQUFtQixDQUN6QixTQUE0QixFQUM1QixPQUEwRCxFQUMxRCxJQUFlLEVBQ2YsY0FBdUIsRUFDdkIseUJBQStDLEVBQy9DLFNBQXFCO1FBR3JCLElBQUksT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssUUFBUSxFQUM1QztZQUNFLE1BQU0sTUFBTSxHQUFHLGNBQWMsR0FBRyw4REFBOEQsQ0FBQztTQUNoRzthQUNJLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUMzRDtZQUNFLE1BQU0sMkZBQTJGLENBQUM7U0FDbkc7YUFFRDtZQUNFLHFCQUFJLFlBQVksR0FBRyxPQUFPLENBQUMsc0JBQXNCLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSx5QkFBeUIsQ0FBQyxDQUFDO1lBQzlGLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsR0FBRyxZQUFZLENBQUM7WUFDbEUsSUFBSSxTQUFTO2dCQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDcEY7Ozs7Ozs7Ozs7SUFPSyxzQkFBc0IsQ0FBQyxTQUE0QixFQUFFLElBQWUsRUFBRSxjQUF1QixFQUFFLFlBQXdCO1FBRTdILElBQUksT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssUUFBUSxFQUM1QztZQUNFLE1BQU0sTUFBTSxHQUFHLGNBQWMsR0FBRyw4REFBOEQsQ0FBQztTQUNoRzthQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQzVEO1lBQ0UsTUFBTSxxRkFBcUYsQ0FBQztTQUM3RjthQUVEO1lBQ0UsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDN0UsSUFBSSxZQUFZO2dCQUFFLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDMUY7O0NBRUo7Ozs7OztBQzFKRDs7OztBQU9BLGlDQUE0RCxTQUFRLHVCQUEyRjs7Ozs7Z0JBRTFJLHdCQUFtRCxFQUFFLGFBQTREO1FBRWxJLEtBQUssQ0FBQyx3QkFBd0IsRUFBRSxhQUFhLENBQUMsQ0FBQzs7Ozs7Ozs7SUFHMUMsc0JBQXNCLENBQUMsU0FBNEIsRUFBRSxJQUFpQyxFQUFFLFNBQStCO1FBRTVILHFCQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsMkJBQTJCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2pFLE9BQU8sQ0FBQyxRQUFRLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLENBQUMsR0FBRyxTQUFTLENBQUM7Ozs7Ozs7SUFHbEcsMkJBQTJCLENBQUMsU0FBNEIsRUFBRSxJQUFpQztRQUVqRyxxQkFBSSxLQUFLLENBQUM7UUFFVixLQUFLLEtBQUssR0FBRyxDQUFDLEVBQUUsQUFBVSxLQUFLLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFDM0Q7WUFDRSxxQkFBSSxpQkFBaUIsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzlFLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQ3ZGO2dCQUNFLE9BQU8sS0FBSyxHQUFHLENBQUMsQ0FBQzthQUNsQjtTQUNGO1FBQ0QsT0FBTyxDQUFDLENBQUMsQ0FBQzs7Q0FFYjs7Ozs7O0FDbENELEFBY0EscUJBQUlBLFFBQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDL0IscUJBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQztBQUVsQyxBQUFPLHVCQUFNLG1DQUFtQyxHQUFHLHlCQUF5QixDQUFDO0FBQzdFLEFBQU8sdUJBQU0sZ0NBQWdDLEdBQU0sdUJBQXVCLENBQUM7QUFDM0UsQUFBTyx1QkFBTSw4QkFBOEIsR0FBUSw2QkFBNkIsQ0FBQztBQVNqRjs7Ozs7O2dCQTRDd0QsdUJBQXdELEVBQy9DLHFCQUFvRCxFQUN0RCwyQkFBZ0U7UUFGdkUsNEJBQXVCLEdBQXZCLHVCQUF1QixDQUFpQztRQUMvQywwQkFBcUIsR0FBckIscUJBQXFCLENBQStCO1FBQ3RELGdDQUEyQixHQUEzQiwyQkFBMkIsQ0FBcUM7Ozs7O3VCQTlCL0U7WUFDNUMsUUFBUTtZQUNSLFNBQVM7WUFDVCxXQUFXO1lBQ1gsVUFBVTtZQUNWLFFBQVE7WUFDUixVQUFVO1lBQ1YsUUFBUTtTQUNUOzs7O3NCQUsyQyxFQUFFOzs7Ozs0QkFNVSxNQUFNOzs7OzswQkFNVixJQUFJO3lDQWdCcEQsSUFBSSw0QkFBNEIsRUFBMEM7NkNBRTFFLElBQUksNEJBQTRCLEVBQThDO3VCQVF2RCxFQUFFOzs7Ozs2QkFpSmlCLElBQUksWUFBWSxFQUFTOzs7Ozs2QkFXekIsSUFBSSxZQUFZLEVBQVM7Ozs7OzBCQVc1QixJQUFJLFlBQVksRUFBUzs7Ozs7SUF0S3BFLFNBQVM7UUFFUCxxQkFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDckMsSUFBSSxPQUFPLEtBQUssSUFBSSxDQUFDLE9BQU8sRUFDNUI7WUFDRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7U0FDeEI7S0FDRjs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBdUI7UUFFakMsSUFBSSxPQUFPLGlCQUFjLE9BQU8sV0FBUSxJQUFJLE9BQU8sZ0JBQWEsRUFDaEU7WUFDRSxxQkFBSSxTQUFTLEdBQUcsQ0FBQyxPQUFPLGlCQUFjLE9BQU8sY0FBVyxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUN0RixxQkFBSSxPQUFPLEdBQUcsQ0FBQyxPQUFPLGVBQVksT0FBTyxZQUFTLFlBQVksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQzlFLHFCQUFJLFlBQVksR0FBRyxDQUFDLE9BQU8sb0JBQWlCLE9BQU8saUJBQWMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7WUFFbEcsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLGVBQWUsQ0FBQyxZQUFZLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3hFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN2QjtLQUNGOzs7O0lBV08sY0FBYztRQUVwQixJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixFQUNqQztZQUNFLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1lBQ2YsTUFBTSxpRUFBaUUsQ0FBQTtTQUN4RTthQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQy9CO1lBQ0UsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7WUFDZixNQUFNLDRDQUE0QyxDQUFBO1NBQ25EO2FBRUQ7WUFDRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFFMUIsSUFBSSxJQUFJLENBQUMsMkJBQTJCLEVBQ3BDO2dCQUNFLElBQUksQ0FBQywwQkFBMEIsRUFBRSxDQUFDO2FBQ25DO1NBQ0Y7Ozs7OztJQU9LLGNBQWM7UUFFcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN4QixLQUFLLHFCQUFJLEtBQUssSUFBSSxJQUFJLENBQUMsTUFBTSxFQUM3QjtZQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzVCO1FBQ0QsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUs7WUFDdEQsRUFBRSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQztZQUN0QyxJQUFJLEVBQUUsSUFBSTtZQUNWLGlCQUFpQixFQUFFLElBQUksQ0FBQyxTQUFTO1lBQ2pDLGVBQWUsRUFBRSxJQUFJLENBQUMsT0FBTztZQUM3QixNQUFNLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO1lBQ3ZDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWTtTQUNoQyxDQUFDLENBQUMsQ0FBQzs7Ozs7SUFHRSxrQkFBa0I7UUFFeEIsSUFBSSxDQUFDLHlCQUF5QixDQUFDLGVBQWUsQ0FDNUMsSUFBSSxDQUFDLGdCQUFnQixFQUNyQixJQUFJLENBQUMsdUJBQXVCLEVBQzVCLElBQUksQ0FBQyxJQUFJLEVBQ1QsSUFBSSxFQUNKO1lBQ0UsR0FBRyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUMxQixNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2hDLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDakMsRUFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFDN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUM5QixDQUFDOzs7OztJQUdJLDBCQUEwQjtRQUVoQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSztZQUN2RCxFQUFFLEVBQUUsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDakMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTO1lBQ3pCLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTztZQUNyQixpQkFBaUIsRUFBRSxJQUFJLENBQUMsU0FBUztZQUNqQyxlQUFlLEVBQUUsSUFBSSxDQUFDLE9BQU87WUFDN0IsTUFBTSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ3RFLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWTtTQUNoQyxDQUFDLENBQUMsQ0FBQztRQUVKLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLFVBQVUsRUFDNUU7WUFDRSxJQUFJLENBQUMsNkJBQTZCLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDM0MsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ25DLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztTQUNoQztRQUVELElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxlQUFlLENBQ2hELElBQUksQ0FBQyxnQkFBZ0IsRUFDckIsSUFBSSxDQUFDLDJCQUEyQixFQUNoQyxJQUFJLENBQUMsS0FBSyxFQUNWLElBQUksRUFDSjtZQUNFLEdBQUcsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUIsTUFBTSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUNoQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ2pDLENBQ0YsQ0FBQzs7Ozs7Ozs7SUFPSSxhQUFhLENBQUMsSUFBNkIsRUFBRSxTQUF5RDtRQUU1RyxxQkFBSSxRQUFRLHFCQUFvQyxTQUFTLENBQUMsUUFBUSxDQUFBLENBQUM7UUFDbkUsUUFBUSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Ozs7OztJQVdqRSxRQUFRLENBQUMsS0FBWTtRQUUzQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs7Ozs7O0lBU3pCLFFBQVEsQ0FBQyxLQUFZO1FBRTNCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDOzs7Ozs7SUFTekIsS0FBSyxDQUFDLEtBQVk7UUFFeEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Ozs7WUFwUC9CLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsMkJBQTJCO2dCQUNyQyxRQUFRLEVBQUU7O2dFQUVvRDtnQkFDOUQsTUFBTSxFQUFFLENBQUMsZ0lBQWdJLENBQUM7YUFDM0k7Ozs7WUFqQlEsdUJBQXVCLHVCQThEM0IsTUFBTSxTQUFDLG1DQUFtQztZQS9EdEMscUJBQXFCLHVCQWdFekIsUUFBUSxZQUFJLE1BQU0sU0FBQyxnQ0FBZ0M7WUE3RC9DLDJCQUEyQix1QkE4RC9CLFFBQVEsWUFBSSxNQUFNLFNBQUMsOEJBQThCOzs7d0JBekNuRCxLQUFLLFNBQUMsWUFBWTtzQkFLbEIsS0FBSyxTQUFDLFVBQVU7c0JBTWhCLEtBQUssU0FBQyxTQUFTO3FCQWFmLEtBQUssU0FBQyxRQUFROzJCQU1kLEtBQUssU0FBQyxlQUFlO3lCQU1yQixLQUFLLFNBQUMsYUFBYTsrQkFhbkIsU0FBUyxTQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRTs0QkE2Si9DLE1BQU0sU0FBQyxRQUFROzRCQVdmLE1BQU0sU0FBQyxRQUFRO3lCQVdmLE1BQU0sU0FBQyxLQUFLOzs7Ozs7O0FDclFmOzs7WUFLQyxRQUFRLFNBQUM7Z0JBQ1IsT0FBTyxFQUFFO29CQUNQLFlBQVk7aUJBQ2I7Z0JBQ0QsWUFBWSxFQUFFO29CQUNaLGlCQUFpQjtpQkFDbEI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLGlCQUFpQjtpQkFDbEI7YUFDRjs7Ozs7OztBQ2ZEOzs7O0FBRUE7Ozs7O0lBS1MsVUFBVSxDQUFDLElBQWU7UUFFL0IsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7Ozs7bUJBTGxCLEtBQUssU0FBQyxNQUFNOzs7Ozs7O0FDSmY7Ozs7QUFHQSwwQkFBK0QsU0FBUSxnQkFBMEI7Ozs2QkFxQjlDLElBQUksWUFBWSxFQUFZOzZCQU81QixJQUFJLFlBQVksRUFBWTswQkFPL0IsSUFBSSxZQUFZLEVBQVk7Ozs7Ozs7SUFqQ25FLFNBQVMsQ0FBQyxJQUFhLEVBQUUsUUFBbUI7UUFFakQsUUFBTyxJQUFJO1lBRVQsS0FBSyxRQUFRO2dCQUNYLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN2QyxNQUFNO1lBQ1IsS0FBSyxRQUFRO2dCQUNYLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN2QyxNQUFNO1lBQ1IsS0FBSyxLQUFLO2dCQUNSLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNwQyxNQUFNO1lBQ1I7Z0JBQ0UsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLEdBQUcsb0NBQW9DLENBQUM7U0FDMUU7Ozs7OztJQUtPLFFBQVEsQ0FBQyxLQUFlO1FBRWhDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ2hDOzs7OztJQUlTLFFBQVEsQ0FBQyxLQUFlO1FBRWhDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ2hDOzs7OztJQUlTLEtBQUssQ0FBQyxLQUFlO1FBRTdCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQzdCOzs7NEJBbkJBLE1BQU0sU0FBQyxRQUFROzRCQU9mLE1BQU0sU0FBQyxRQUFRO3lCQU9mLE1BQU0sU0FBQyxLQUFLOzs7Ozs7O0FDckNmOzs7O0FBT0EsOEJBQXlELFNBQVEsb0JBQXlEOzs7dUNBUXBILElBQUksNEJBQTRCLEVBQThDOzs7Ozs7OztJQUszRSxnQkFBZ0IsQ0FBQyxxQkFBMkYsRUFBRSxVQUFtQjtRQUV0SSxJQUFJLHFCQUFxQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFDcEQ7WUFDRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsZUFBZSxDQUMxQyxJQUFJLENBQUMsa0JBQWtCLEVBQ3ZCLHFCQUFxQixFQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFDaEIsVUFBVSxFQUNWO2dCQUNFLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ2hDLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7YUFDakMsQ0FDRixDQUFDO1NBQ0g7Ozs7aUNBdEJGLFNBQVMsU0FBQyxxQkFBcUIsRUFBRSxFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRTs7Ozs7OztBQ1o5RDs7OztBQUdBLGtDQUE2RCxTQUFRLG9CQUEwRDtDQUU5SDs7Ozs7O0FDTEQ7Ozs7QUFFQSw0QkFBdUQsU0FBUSxvQkFBd0M7Q0FFdEc7Ozs7Ozs7Ozs7Ozs7OyJ9