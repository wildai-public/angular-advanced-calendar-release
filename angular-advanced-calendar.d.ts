/**
 * Generated bundle index. Do not edit.
 */
export * from './public_api';
export { CalendarComponent as ɵa } from './lib/views/calendar/calendar.component';
export { DynamicCrudComponent as ɵc } from './lib/views/dynamic-crud/dynamic-crud.component';
export { DynamicComponent as ɵd } from './lib/views/dynamic/dynamic.component';
export { DynamicComponentFactory as ɵb } from './lib/views/dynamic/dynamic.factory';
