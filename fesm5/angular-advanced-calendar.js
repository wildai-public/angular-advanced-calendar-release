import { __extends, __values } from 'tslib';
import { Component, Input, Output, EventEmitter, ViewChild, ViewContainerRef, Inject, Optional, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var /** @type {?} */ moment = require('moment');
/**
 * @param {?} a
 * @param {?} b
 * @return {?}
 */
function isSameDay(a, b) {
    return a && b && a.getFullYear() === b.getFullYear() && a.getMonth() === b.getMonth() && a.getDate() === b.getDate();
}
/**
 * @param {?} date
 * @return {?}
 */
function getWeekNumber(date) {
    return moment(date).week();
}
/**
 * @param {?} date
 * @return {?}
 */
function getFirstDayOfMonth(date) {
    return moment(date).startOf('month').toDate();
}
/**
 * @param {?} date
 * @return {?}
 */
function getFirstDayOfFirstWeekOfMonth(date) {
    return moment(date).startOf('month').startOf('isoWeek');
}
/**
 * @param {?} date
 * @return {?}
 */
function getLastDayOfMonth(date) {
    return moment(date).endOf('month').toDate();
}
/**
 * @param {?} date
 * @return {?}
 */
function geLastDayOfLastWeekOfMonth(date) {
    return moment(date).endOf('month').endOf('isoWeek').toDate();
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var /** @type {?} */ moment$1 = require('moment');
/**
 * Dispatches events in an array of dates depending
 * based on the date of each event.
 */
var /**
 * Dispatches events in an array of dates depending
 * based on the date of each event.
 */
EventDispatcher = /** @class */ (function () {
    function EventDispatcher(dateAccessor, startDate, endDate) {
        this.dateAccessor = dateAccessor;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    /**
     * @return {?}
     */
    EventDispatcher.prototype.clear = /**
     * @return {?}
     */
    function () {
        this.dispatch = [];
        for (var /** @type {?} */ i = 0; i < this.getTotalNumberOfDays(); i++) {
            this.dispatch[i] = [];
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    EventDispatcher.prototype.add = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        var /** @type {?} */ date = event[this.dateAccessor];
        var /** @type {?} */ dispatchIndex = this.getDayNumber(date);
        if (this.dispatch[dispatchIndex]) {
            this.dispatch[dispatchIndex].push(event);
        }
    };
    /**
     * @param {?} date
     * @return {?}
     */
    EventDispatcher.prototype.getEvents = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return this.dispatch[this.getDayNumber(date)];
    };
    /**
     * @param {?} startDate
     * @param {?} endDate
     * @return {?}
     */
    EventDispatcher.prototype.getEventsBetween = /**
     * @param {?} startDate
     * @param {?} endDate
     * @return {?}
     */
    function (startDate, endDate) {
        return this.dispatch.slice(this.getDayNumber(startDate), this.getDayNumber(endDate) + 1);
    };
    /**
     * Returns an array of the dates
     * between startDate and endDate.
     * @return {?}
     */
    EventDispatcher.prototype.getDateArray = /**
     * Returns an array of the dates
     * between startDate and endDate.
     * @return {?}
     */
    function () {
        var /** @type {?} */ days = [];
        var /** @type {?} */ current = moment$1(this.startDate);
        var /** @type {?} */ last = moment$1(this.endDate);
        while (current < last) {
            days.push(current.toDate());
            current = current.add(1, 'day');
        }
        return days;
    };
    /**
     * @return {?}
     */
    EventDispatcher.prototype.getWeekArray = /**
     * @return {?}
     */
    function () {
        var /** @type {?} */ weeks = [];
        var /** @type {?} */ current = moment$1(this.startDate);
        var /** @type {?} */ last = moment$1(this.endDate);
        while (current < last) {
            weeks.push({ startDate: current.toDate(), endDate: current.add(6, 'day').toDate() });
            current = current.add(1, 'day');
        }
        return weeks;
    };
    /**
     * Returns the index of a date in
     * the [startDate - endDate] period.
     * @param {?} date the date for which to compute the index
     * @return {?}
     */
    EventDispatcher.prototype.getDayNumber = /**
     * Returns the index of a date in
     * the [startDate - endDate] period.
     * @param {?} date the date for which to compute the index
     * @return {?}
     */
    function (date) {
        return moment$1(date).diff(moment$1(this.startDate), 'days');
    };
    /**
     * Returns the total number of days
     * in the [startDate - endDate] period.
     * @return {?}
     */
    EventDispatcher.prototype.getTotalNumberOfDays = /**
     * Returns the total number of days
     * in the [startDate - endDate] period.
     * @return {?}
     */
    function () {
        return this.getDayNumber(this.endDate) + 1;
    };
    return EventDispatcher;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
// unsupported: template constraints.
/**
 * @abstract
 * @template DataType, ComponentType
 */
var  
// unsupported: template constraints.
/**
 * @abstract
 * @template DataType, ComponentType
 */
DynamicComponentFactory = /** @class */ (function () {
    function DynamicComponentFactory(componentFactoryResolver, componentType) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.factory = this.componentFactoryResolver.resolveComponentFactory(componentType);
    }
    /**
     * Inserts a component in a view container
     * @param {?} container the container in which we want to display this component
     * @param {?} data the data of the component to display
     * @param {?=} callbacks a map of callbacks to subscribe to
     * @param {?=} index the position in the container where to add the component
     * @return {?}
     */
    DynamicComponentFactory.prototype.insertDynamicComponent = /**
     * Inserts a component in a view container
     * @param {?} container the container in which we want to display this component
     * @param {?} data the data of the component to display
     * @param {?=} callbacks a map of callbacks to subscribe to
     * @param {?=} index the position in the container where to add the component
     * @return {?}
     */
    function (container, data, callbacks, index) {
        var /** @type {?} */ componentRef = container.createComponent(this.factory, index);
        var /** @type {?} */ instance = /** @type {?} */ (componentRef.instance);
        instance.updateData(data);
        if (callbacks) {
            for (var /** @type {?} */ name_1 in callbacks) {
                instance.subscribe(name_1, callbacks[name_1]);
            }
        }
        return componentRef;
    };
    return DynamicComponentFactory;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
var  /**
 * @abstract
 * @template DataType
 */
EventComponentFactory = /** @class */ (function (_super) {
    __extends(EventComponentFactory, _super);
    function EventComponentFactory(componentFactoryResolver, componentType) {
        return _super.call(this, componentFactoryResolver, componentType) || this;
    }
    return EventComponentFactory;
}(DynamicComponentFactory));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
var  /**
 * @abstract
 * @template DataType
 */
DayCellComponentFactory = /** @class */ (function (_super) {
    __extends(DayCellComponentFactory, _super);
    function DayCellComponentFactory(componentFactoryResolver, componentType) {
        return _super.call(this, componentFactoryResolver, componentType) || this;
    }
    return DayCellComponentFactory;
}(DynamicComponentFactory));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
// unsupported: template constraints.
/**
 * @template DataType, ComponentType
 */
var 
// unsupported: template constraints.
/**
 * @template DataType, ComponentType
 */
DynamicComponentArrayManager = /** @class */ (function () {
    function DynamicComponentArrayManager() {
        /**
         * Stores the previous array state
         */
        this.oldDataArray = [];
        /**
         * A numeric map used to store the dynamic views
         */
        this.dynamicViewComponentMap = {};
    }
    /**
     * @return {?}
     */
    DynamicComponentArrayManager.prototype.clear = /**
     * @return {?}
     */
    function () {
        for (var /** @type {?} */ id in this.dynamicViewComponentMap) {
            if (this.dynamicViewComponentMap[id]) {
                this.dynamicViewComponentMap[id].destroy();
                delete this.dynamicViewComponentMap[id];
            }
        }
        this.oldDataArray = [];
    };
    /**
     * Updates, add or remove data views based
     * on the old data list
     * @param {?} container
     * @param {?} factory
     * @param {?} dataArray
     * @param {?} dataIdAccessor
     * @param {?=} dynamicComponentCallbacks
     * @param {?=} onAddView
     * @param {?=} onUpdateView
     * @param {?=} onDeleteView
     * @return {?}
     */
    DynamicComponentArrayManager.prototype.updateContainer = /**
     * Updates, add or remove data views based
     * on the old data list
     * @param {?} container
     * @param {?} factory
     * @param {?} dataArray
     * @param {?} dataIdAccessor
     * @param {?=} dynamicComponentCallbacks
     * @param {?=} onAddView
     * @param {?=} onUpdateView
     * @param {?=} onDeleteView
     * @return {?}
     */
    function (container, factory, dataArray, dataIdAccessor, dynamicComponentCallbacks, onAddView, onUpdateView, onDeleteView) {
        if (!container) {
            throw 'child container (#childContainer) could not be found in the container view';
        }
        try {
            for (var dataArray_1 = __values(dataArray), dataArray_1_1 = dataArray_1.next(); !dataArray_1_1.done; dataArray_1_1 = dataArray_1.next()) {
                var data = dataArray_1_1.value;
                var /** @type {?} */ alreadyPresent = false;
                try {
                    for (var _a = __values(this.oldDataArray), _b = _a.next(); !_b.done; _b = _a.next()) {
                        var oldData = _b.value;
                        if (data[dataIdAccessor] === oldData[dataIdAccessor]) {
                            alreadyPresent = true;
                            break;
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                if (alreadyPresent) {
                    this.updateDynamicComponent(container, data, dataIdAccessor, onUpdateView);
                }
                else {
                    this.addDynamicComponent(container, factory, data, dataIdAccessor, dynamicComponentCallbacks, onAddView);
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (dataArray_1_1 && !dataArray_1_1.done && (_d = dataArray_1.return)) _d.call(dataArray_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
        try {
            for (var _e = __values(this.oldDataArray), _f = _e.next(); !_f.done; _f = _e.next()) {
                var oldData = _f.value;
                var /** @type {?} */ stillPresent = false;
                try {
                    for (var dataArray_2 = __values(dataArray), dataArray_2_1 = dataArray_2.next(); !dataArray_2_1.done; dataArray_2_1 = dataArray_2.next()) {
                        var data = dataArray_2_1.value;
                        if (data[dataIdAccessor] === oldData[dataIdAccessor]) {
                            stillPresent = true;
                            break;
                        }
                    }
                }
                catch (e_3_1) { e_3 = { error: e_3_1 }; }
                finally {
                    try {
                        if (dataArray_2_1 && !dataArray_2_1.done && (_g = dataArray_2.return)) _g.call(dataArray_2);
                    }
                    finally { if (e_3) throw e_3.error; }
                }
                if (!stillPresent) {
                    this.removeDynamicComponent(container, oldData, dataIdAccessor, onDeleteView);
                }
            }
        }
        catch (e_4_1) { e_4 = { error: e_4_1 }; }
        finally {
            try {
                if (_f && !_f.done && (_h = _e.return)) _h.call(_e);
            }
            finally { if (e_4) throw e_4.error; }
        }
        this.oldDataArray = dataArray;
        var e_2, _d, e_1, _c, e_4, _h, e_3, _g;
    };
    /**
     * @param {?} container
     * @param {?} data
     * @param {?} dataIdAccessor
     * @param {?=} onDeleteView
     * @return {?}
     */
    DynamicComponentArrayManager.prototype.removeDynamicComponent = /**
     * @param {?} container
     * @param {?} data
     * @param {?} dataIdAccessor
     * @param {?=} onDeleteView
     * @return {?}
     */
    function (container, data, dataIdAccessor, onDeleteView) {
        if (typeof data[dataIdAccessor] !== 'number') {
            throw 'id (' + dataIdAccessor + ') not found in data structure (delete dynamic component method)';
        }
        else if (!this.dynamicViewComponentMap[data[dataIdAccessor]]) {
            throw 'dynamic view not found in the dynamic view container (delete dynamic component method)';
        }
        else {
            this.dynamicViewComponentMap[data[dataIdAccessor]].destroy();
            delete this.dynamicViewComponentMap[data[dataIdAccessor]];
            if (onDeleteView)
                onDeleteView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
        }
    };
    /**
     * @param {?} container
     * @param {?} factory
     * @param {?} data
     * @param {?} dataIdAccessor
     * @param {?} dynamicComponentCallbacks
     * @param {?=} onAddView
     * @return {?}
     */
    DynamicComponentArrayManager.prototype.addDynamicComponent = /**
     * @param {?} container
     * @param {?} factory
     * @param {?} data
     * @param {?} dataIdAccessor
     * @param {?} dynamicComponentCallbacks
     * @param {?=} onAddView
     * @return {?}
     */
    function (container, factory, data, dataIdAccessor, dynamicComponentCallbacks, onAddView) {
        if (typeof data[dataIdAccessor] !== 'number') {
            throw 'id (' + dataIdAccessor + ') not found in data structure (add dynamic component method)';
        }
        else if (this.dynamicViewComponentMap[data[dataIdAccessor]]) {
            throw 'dynamic view already present in the dynamic view container (add dynamic component method)';
        }
        else {
            var /** @type {?} */ componentRef = factory.insertDynamicComponent(container, data, dynamicComponentCallbacks);
            this.dynamicViewComponentMap[data[dataIdAccessor]] = componentRef;
            if (onAddView)
                onAddView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
        }
    };
    /**
     * Updates an data view model
     * @param {?} container
     * @param {?} data the new data model to use
     * @param {?} dataIdAccessor
     * @param {?=} onUpdateView
     * @return {?}
     */
    DynamicComponentArrayManager.prototype.updateDynamicComponent = /**
     * Updates an data view model
     * @param {?} container
     * @param {?} data the new data model to use
     * @param {?} dataIdAccessor
     * @param {?=} onUpdateView
     * @return {?}
     */
    function (container, data, dataIdAccessor, onUpdateView) {
        if (typeof data[dataIdAccessor] !== 'number') {
            throw 'id (' + dataIdAccessor + ') not found in data structure (add dynamic component method)';
        }
        else if (!this.dynamicViewComponentMap[data[dataIdAccessor]]) {
            throw 'dynamic view not found in the dynamic view container (add dynamic component method)';
        }
        else {
            this.dynamicViewComponentMap[data[dataIdAccessor]].instance.updateData(data);
            if (onUpdateView)
                onUpdateView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
        }
    };
    return DynamicComponentArrayManager;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
var  /**
 * @abstract
 * @template DataType
 */
WeekSummaryComponentFactory = /** @class */ (function (_super) {
    __extends(WeekSummaryComponentFactory, _super);
    function WeekSummaryComponentFactory(componentFactoryResolver, componentType) {
        return _super.call(this, componentFactoryResolver, componentType) || this;
    }
    /**
     * @param {?} container
     * @param {?} data
     * @param {?=} callbacks
     * @return {?}
     */
    WeekSummaryComponentFactory.prototype.insertDynamicComponent = /**
     * @param {?} container
     * @param {?} data
     * @param {?=} callbacks
     * @return {?}
     */
    function (container, data, callbacks) {
        var /** @type {?} */ position = this.findWeekPositionInContainer(container, data);
        return (position >= 0) ? _super.prototype.insertDynamicComponent.call(this, container, data, callbacks, position) : undefined;
    };
    /**
     * @param {?} container
     * @param {?} data
     * @return {?}
     */
    WeekSummaryComponentFactory.prototype.findWeekPositionInContainer = /**
     * @param {?} container
     * @param {?} data
     * @return {?}
     */
    function (container, data) {
        var /** @type {?} */ index;
        for (index = 0; index < container.length; index++) {
            var /** @type {?} */ componentInstance = container.get(index)['_view']['nodes'][1]['instance'];
            if (componentInstance.data.date && isSameDay(componentInstance.data.date, data.endDate)) {
                return index + 1;
            }
        }
        return -1;
    };
    return WeekSummaryComponentFactory;
}(DynamicComponentFactory));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var /** @type {?} */ moment$2 = require('moment');
var /** @type {?} */ hash = require('object-hash');
var /** @type {?} */ DAY_CELL_COMPONENT_FACTORY_PROVIDER = 'DayCellComponentFactory';
var /** @type {?} */ EVENT_COMPONENT_FACTORY_PROVIDER = 'EventComponentFactory';
var /** @type {?} */ WEEK_SUMMARY_COMPONENT_FACTORY = 'WeekSummaryComponentFactory';
var CalendarComponent = /** @class */ (function () {
    function CalendarComponent(dayCellComponentFactory, eventComponentFactory, weekSummaryComponentFactory) {
        this.dayCellComponentFactory = dayCellComponentFactory;
        this.eventComponentFactory = eventComponentFactory;
        this.weekSummaryComponentFactory = weekSummaryComponentFactory;
        /**
         * Week days label to display in the header
         * (default is english).
         */
        this.headers = [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday',
        ];
        /**
         * A list of events to display in the callendar.
         */
        this.events = [];
        /**
         * The name of the date attribute to use
         * when displaying the events (default is 'date').
         */
        this.dateAccessor = 'date';
        /**
         * The name of the id attribute to use
         * when displaying the events (default is 'id').
         */
        this.idAccessor = 'id';
        this.dynamicDayCellViewManager = new DynamicComponentArrayManager();
        this.dynamicWeekSummaryViewManager = new DynamicComponentArrayManager();
        this.oldHash = '';
        /**
         * Triggers a delete event emission
         * @param event the event to delete
         */
        this.deleteEmitter = new EventEmitter();
        /**
         * Triggers an update event emission
         * @param event the event to update
         */
        this.updateEmitter = new EventEmitter();
        /**
         * Triggers an add event emission
         * @param event the event to add
         */
        this.addEmitter = new EventEmitter();
    }
    /**
     * @return {?}
     */
    CalendarComponent.prototype.ngDoCheck = /**
     * @return {?}
     */
    function () {
        var /** @type {?} */ newHash = hash.sha1(this.events);
        if (newHash !== this.oldHash) {
            this.updateCalendar();
            this.oldHash = newHash;
        }
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    CalendarComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes["startDate"] || changes["endDate"] || changes["dateAccessor"]) {
            var /** @type {?} */ startDate = (changes["startDate"]) ? changes["startDate"].currentValue : this.startDate;
            var /** @type {?} */ endDate = (changes["endDate"]) ? changes["endDate"].currentValue : this.endDate;
            var /** @type {?} */ dateAccessor = (changes["dateAccessor"]) ? changes["dateAccessor"].currentValue : this.dateAccessor;
            this.dispatcher = new EventDispatcher(dateAccessor, startDate, endDate);
            this.updateCalendar();
        }
    };
    /**
     * @return {?}
     */
    CalendarComponent.prototype.updateCalendar = /**
     * @return {?}
     */
    function () {
        if (!this.dayCellComponentFactory) {
            this.days = [];
            throw 'day cell component factory (day-component-factory) was not set!';
        }
        else if (!this.dayGridContainer) {
            this.days = [];
            throw 'day grid (dayGrid) not found in template !';
        }
        else {
            this.dispatchEvents();
            this.updateDayCellViews();
            if (this.weekSummaryComponentFactory) {
                this.updateWeekSummaryCellViews();
            }
        }
    };
    /**
     * Sorts events by date
     * @return {?}
     */
    CalendarComponent.prototype.dispatchEvents = /**
     * Sorts events by date
     * @return {?}
     */
    function () {
        var _this = this;
        this.dispatcher.clear();
        try {
            for (var _a = __values(this.events), _b = _a.next(); !_b.done; _b = _a.next()) {
                var event_1 = _b.value;
                this.dispatcher.add(event_1);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_1) throw e_1.error; }
        }
        this.days = this.dispatcher.getDateArray().map(function (date) { return ({
            id: _this.dispatcher.getDayNumber(date),
            date: date,
            calendarStartDate: _this.startDate,
            calendarEndDate: _this.endDate,
            events: _this.dispatcher.getEvents(date),
            dateAccessor: _this.dateAccessor,
        }); });
        var e_1, _c;
    };
    /**
     * @return {?}
     */
    CalendarComponent.prototype.updateDayCellViews = /**
     * @return {?}
     */
    function () {
        this.dynamicDayCellViewManager.updateContainer(this.dayGridContainer, this.dayCellComponentFactory, this.days, 'id', {
            add: this.onAdd.bind(this),
            update: this.onUpdate.bind(this),
            delete: this.onDelete.bind(this)
        }, this.updateDayCell.bind(this), this.updateDayCell.bind(this), this.updateDayCell.bind(this));
    };
    /**
     * @return {?}
     */
    CalendarComponent.prototype.updateWeekSummaryCellViews = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.weeks = this.dispatcher.getWeekArray().map(function (week) { return ({
            id: getWeekNumber(week.startDate),
            startDate: week.startDate,
            endDate: week.endDate,
            calendarStartDate: _this.startDate,
            calendarEndDate: _this.endDate,
            events: _this.dispatcher.getEventsBetween(week.startDate, week.endDate),
            dateAccessor: _this.dateAccessor,
        }); });
        if (this.startDate !== this.oldStartDate || this.endDate !== this.oldEndDate) {
            this.dynamicWeekSummaryViewManager.clear();
            this.oldStartDate = this.startDate;
            this.oldEndDate = this.endDate;
        }
        this.dynamicWeekSummaryViewManager.updateContainer(this.dayGridContainer, this.weekSummaryComponentFactory, this.weeks, 'id', {
            add: this.onAdd.bind(this),
            update: this.onUpdate.bind(this),
            delete: this.onDelete.bind(this)
        });
    };
    /**
     * Triggers a day cell view internal update
     * @param {?} data
     * @param {?} component the component ref of the day cell to update
     * @return {?}
     */
    CalendarComponent.prototype.updateDayCell = /**
     * Triggers a day cell view internal update
     * @param {?} data
     * @param {?} component the component ref of the day cell to update
     * @return {?}
     */
    function (data, component) {
        var /** @type {?} */ instance = /** @type {?} */ (component.instance);
        instance.updateEventViews(this.eventComponentFactory, this.idAccessor);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CalendarComponent.prototype.onDelete = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.deleteEmitter.emit(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CalendarComponent.prototype.onUpdate = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.updateEmitter.emit(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CalendarComponent.prototype.onAdd = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.addEmitter.emit(event);
    };
    CalendarComponent.decorators = [
        { type: Component, args: [{
                    selector: 'angular-advanced-calendar',
                    template: "<h5 *ngFor='let name of headers' class='header'> {{name}} </h5>\n<ng-template #dayGrid></ng-template>\n<h5 *ngFor='let name of headers' class='footer'> {{name}} </h5>",
                    styles: ["@import url(https://fonts.googleapis.com/css?family=Raleway);.footer,.header{text-align:center;font-family:Raleway,sans-serif}"]
                },] },
    ];
    /** @nocollapse */
    CalendarComponent.ctorParameters = function () { return [
        { type: DayCellComponentFactory, decorators: [{ type: Inject, args: [DAY_CELL_COMPONENT_FACTORY_PROVIDER,] }] },
        { type: EventComponentFactory, decorators: [{ type: Optional }, { type: Inject, args: [EVENT_COMPONENT_FACTORY_PROVIDER,] }] },
        { type: WeekSummaryComponentFactory, decorators: [{ type: Optional }, { type: Inject, args: [WEEK_SUMMARY_COMPONENT_FACTORY,] }] }
    ]; };
    CalendarComponent.propDecorators = {
        startDate: [{ type: Input, args: ['start-date',] }],
        endDate: [{ type: Input, args: ['end-date',] }],
        headers: [{ type: Input, args: ['headers',] }],
        events: [{ type: Input, args: ['events',] }],
        dateAccessor: [{ type: Input, args: ['date-accessor',] }],
        idAccessor: [{ type: Input, args: ['id-accessor',] }],
        dayGridContainer: [{ type: ViewChild, args: ['dayGrid', { read: ViewContainerRef },] }],
        deleteEmitter: [{ type: Output, args: ['delete',] }],
        updateEmitter: [{ type: Output, args: ['update',] }],
        addEmitter: [{ type: Output, args: ['add',] }]
    };
    return CalendarComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var AngularAdvancedCalendarModule = /** @class */ (function () {
    function AngularAdvancedCalendarModule() {
    }
    AngularAdvancedCalendarModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                    ],
                    declarations: [
                        CalendarComponent,
                    ],
                    exports: [
                        CalendarComponent
                    ]
                },] },
    ];
    return AngularAdvancedCalendarModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
var DynamicComponent = /** @class */ (function () {
    function DynamicComponent() {
    }
    /**
     * @param {?} data
     * @return {?}
     */
    DynamicComponent.prototype.updateData = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        this.data = data;
    };
    DynamicComponent.propDecorators = {
        data: [{ type: Input, args: ['data',] }]
    };
    return DynamicComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType, CrudType
 */
var DynamicCrudComponent = /** @class */ (function (_super) {
    __extends(DynamicCrudComponent, _super);
    function DynamicCrudComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.deleteEmitter = new EventEmitter();
        _this.updateEmitter = new EventEmitter();
        _this.addEmitter = new EventEmitter();
        return _this;
    }
    /**
     * @param {?} name
     * @param {?} callback
     * @return {?}
     */
    DynamicCrudComponent.prototype.subscribe = /**
     * @param {?} name
     * @param {?} callback
     * @return {?}
     */
    function (name, callback) {
        switch (name) {
            case 'update':
                this.updateEmitter.subscribe(callback);
                break;
            case 'delete':
                this.deleteEmitter.subscribe(callback);
                break;
            case 'add':
                this.addEmitter.subscribe(callback);
                break;
            default:
                throw 'callback type \'' + name + '\' is not a known event callback !';
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    DynamicCrudComponent.prototype.onDelete = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.deleteEmitter.emit(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    DynamicCrudComponent.prototype.onUpdate = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.updateEmitter.emit(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    DynamicCrudComponent.prototype.onAdd = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.addEmitter.emit(event);
    };
    DynamicCrudComponent.propDecorators = {
        deleteEmitter: [{ type: Output, args: ['delete',] }],
        updateEmitter: [{ type: Output, args: ['update',] }],
        addEmitter: [{ type: Output, args: ['add',] }]
    };
    return DynamicCrudComponent;
}(DynamicComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
var AbstractDayCellComponent = /** @class */ (function (_super) {
    __extends(AbstractDayCellComponent, _super);
    function AbstractDayCellComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.dynamicEventViewManager = new DynamicComponentArrayManager();
        return _this;
    }
    /**
     * Method called to update the event views in the day cell
     * @param {?} eventComponentFactory
     * @param {?} idAccessor
     * @return {?}
     */
    AbstractDayCellComponent.prototype.updateEventViews = /**
     * Method called to update the event views in the day cell
     * @param {?} eventComponentFactory
     * @param {?} idAccessor
     * @return {?}
     */
    function (eventComponentFactory, idAccessor) {
        if (eventComponentFactory && this.eventViewContainer) {
            this.dynamicEventViewManager.updateContainer(this.eventViewContainer, eventComponentFactory, this.data.events, idAccessor, {
                delete: this.onDelete.bind(this),
                update: this.onUpdate.bind(this)
            });
        }
    };
    AbstractDayCellComponent.propDecorators = {
        eventViewContainer: [{ type: ViewChild, args: ['eventViewsContainer', { read: ViewContainerRef },] }]
    };
    return AbstractDayCellComponent;
}(DynamicCrudComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
var  /**
 * @abstract
 * @template DataType
 */
AbstractWeekSummaryComponent = /** @class */ (function (_super) {
    __extends(AbstractWeekSummaryComponent, _super);
    function AbstractWeekSummaryComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AbstractWeekSummaryComponent;
}(DynamicCrudComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template DataType
 */
var  /**
 * @abstract
 * @template DataType
 */
AbstractEventComponent = /** @class */ (function (_super) {
    __extends(AbstractEventComponent, _super);
    function AbstractEventComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AbstractEventComponent;
}(DynamicCrudComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { AngularAdvancedCalendarModule, AbstractDayCellComponent, DayCellComponentFactory, AbstractWeekSummaryComponent, WeekSummaryComponentFactory, AbstractEventComponent, EventComponentFactory, isSameDay, getWeekNumber, getFirstDayOfMonth, getFirstDayOfFirstWeekOfMonth, getLastDayOfMonth, geLastDayOfLastWeekOfMonth, CalendarComponent as ɵa, DynamicCrudComponent as ɵc, DynamicComponent as ɵd, DynamicComponentFactory as ɵb };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvdXRpbHMvZGF0ZS50cyIsIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvdXRpbHMvZGlzcGF0Y2hlci50cyIsIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvdmlld3MvZHluYW1pYy9keW5hbWljLmZhY3RvcnkudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2V2ZW50L2V2ZW50LmZhY3RvcnkudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2RheS1jZWxsL2RheS1jZWxsLmZhY3RvcnkudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2R5bmFtaWMvZHluYW1pYy1tYW5hZ2VyLmNvbXBvbmVudC50cyIsIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvdmlld3Mvd2Vlay1zdW1tYXJ5L3dlZWstc3VtbWFyeS5mYXRvcnkudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2NhbGVuZGFyL2NhbGVuZGFyLmNvbXBvbmVudC50cyIsIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci5tb2R1bGUudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2R5bmFtaWMvZHluYW1pYy5jb21wb25lbnQudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2R5bmFtaWMtY3J1ZC9keW5hbWljLWNydWQuY29tcG9uZW50LnRzIiwibmc6Ly9hbmd1bGFyLWFkdmFuY2VkLWNhbGVuZGFyL2xpYi92aWV3cy9kYXktY2VsbC9hYnN0cmFjdC1kYXktY2VsbC5jb21wb25lbnQudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL3dlZWstc3VtbWFyeS9hYnN0cmFjdC13ZWVrLXN1bW1hcnkuY29tcG9uZW50LnRzIiwibmc6Ly9hbmd1bGFyLWFkdmFuY2VkLWNhbGVuZGFyL2xpYi92aWV3cy9ldmVudC9hYnN0cmFjdC1ldmVudC5jb21wb25lbnQudHMiXSwic291cmNlc0NvbnRlbnQiOlsibGV0IG1vbWVudCA9IHJlcXVpcmUoJ21vbWVudCcpO1xuXG5leHBvcnQgZnVuY3Rpb24gaXNTYW1lRGF5KGEgOiBEYXRlLCBiIDogRGF0ZSkgOiBib29sZWFuXG57XG4gIHJldHVybiBmYWxzZSB8fCAoYSAmJiBiICYmIGEuZ2V0RnVsbFllYXIoKSA9PT0gYi5nZXRGdWxsWWVhcigpICYmIGEuZ2V0TW9udGgoKSA9PT0gYi5nZXRNb250aCgpICYmIGEuZ2V0RGF0ZSgpID09PSBiLmdldERhdGUoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRXZWVrTnVtYmVyKGRhdGUgOiBEYXRlKSA6IG51bWJlclxue1xuICByZXR1cm4gbW9tZW50KGRhdGUpLndlZWsoKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEZpcnN0RGF5T2ZNb250aChkYXRlIDogRGF0ZSkgOiBEYXRlXG57XG4gIHJldHVybiBtb21lbnQoZGF0ZSkuc3RhcnRPZignbW9udGgnKS50b0RhdGUoKTtcblxufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Rmlyc3REYXlPZkZpcnN0V2Vla09mTW9udGgoZGF0ZSA6IERhdGUpIDogRGF0ZVxue1xuICByZXR1cm4gbW9tZW50KGRhdGUpLnN0YXJ0T2YoJ21vbnRoJykuc3RhcnRPZignaXNvV2VlaycpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0TGFzdERheU9mTW9udGgoZGF0ZSA6IERhdGUpIDogRGF0ZVxue1xuICByZXR1cm4gbW9tZW50KGRhdGUpLmVuZE9mKCdtb250aCcpLnRvRGF0ZSgpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2VMYXN0RGF5T2ZMYXN0V2Vla09mTW9udGgoZGF0ZSA6IERhdGUpIDogRGF0ZVxue1xuICByZXR1cm4gbW9tZW50KGRhdGUpLmVuZE9mKCdtb250aCcpLmVuZE9mKCdpc29XZWVrJykudG9EYXRlKCk7XG59IiwibGV0IG1vbWVudCA9IHJlcXVpcmUoJ21vbWVudCcpO1xuXG4vKipcbiAqIERpc3BhdGNoZXMgZXZlbnRzIGluIGFuIGFycmF5IG9mIGRhdGVzIGRlcGVuZGluZ1xuICogYmFzZWQgb24gdGhlIGRhdGUgb2YgZWFjaCBldmVudC5cbiAqL1xuZXhwb3J0IGNsYXNzIEV2ZW50RGlzcGF0Y2hlclxue1xuICBwcml2YXRlIGRpc3BhdGNoIDogYW55W11bXTtcblxuICAvKipcbiAgICogQ3JlYXRlcyBhIG5ldyBldmVudCBkaXNwYXRjaGVyXG4gICAqIEBwYXJhbSBkYXRlQWNjZXNzb3IgdGhlIGRhdGUgbGFiZWwgdG8gdXNlIGluIHRoZSBldmVudHNcbiAgICogQHBhcmFtIHN0YXJ0RGF0ZSB0aGUgc3RhcnQgZGF0ZSBvZiB0aGUgcGVyaW9kIGluIHdoaWNoIHRvIGRpc3BhdGNoIGV2ZW50c1xuICAgKiBAcGFyYW0gZW5kRGF0ZSB0aGUgZW5kIGRhdGUgb2YgdGhlIHBlcmlvZCAoaW5jbHVzaXZlKSBpbiB3aGljaCB0byBkaXNwYXRjaCBldmVudHNcbiAgICovXG4gIHB1YmxpYyBjb25zdHJ1Y3RvcihwdWJsaWMgcmVhZG9ubHkgZGF0ZUFjY2Vzc29yIDogc3RyaW5nLCBwdWJsaWMgcmVhZG9ubHkgc3RhcnREYXRlIDogRGF0ZSwgcHVibGljIHJlYWRvbmx5IGVuZERhdGUgOiBEYXRlKSB7IH1cblxuICBwdWJsaWMgY2xlYXIoKSA6IHZvaWRcbiAge1xuICAgIHRoaXMuZGlzcGF0Y2ggPSBbXTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZ2V0VG90YWxOdW1iZXJPZkRheXMoKTsgaSsrKVxuICAgIHtcbiAgICAgIHRoaXMuZGlzcGF0Y2hbaV0gPSBbXTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgYWRkKGV2ZW50IDogYW55KVxuICB7XG4gICAgbGV0IGRhdGUgPSBldmVudFt0aGlzLmRhdGVBY2Nlc3Nvcl07XG4gICAgbGV0IGRpc3BhdGNoSW5kZXggPSB0aGlzLmdldERheU51bWJlcihkYXRlKTtcbiAgICBpZiAodGhpcy5kaXNwYXRjaFtkaXNwYXRjaEluZGV4XSlcbiAgICB7XG4gICAgICB0aGlzLmRpc3BhdGNoW2Rpc3BhdGNoSW5kZXhdLnB1c2goZXZlbnQpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBnZXRFdmVudHMoZGF0ZSA6IERhdGUpIDogYW55W11cbiAge1xuICAgIHJldHVybiB0aGlzLmRpc3BhdGNoW3RoaXMuZ2V0RGF5TnVtYmVyKGRhdGUpXTtcbiAgfVxuXG4gIHB1YmxpYyBnZXRFdmVudHNCZXR3ZWVuKHN0YXJ0RGF0ZSA6IERhdGUsIGVuZERhdGUgOiBEYXRlKVxuICB7XG4gICAgcmV0dXJuIHRoaXMuZGlzcGF0Y2guc2xpY2UodGhpcy5nZXREYXlOdW1iZXIoc3RhcnREYXRlKSwgdGhpcy5nZXREYXlOdW1iZXIoZW5kRGF0ZSkgKyAxKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIGFuIGFycmF5IG9mIHRoZSBkYXRlc1xuICAgKiBiZXR3ZWVuIHN0YXJ0RGF0ZSBhbmQgZW5kRGF0ZS5cbiAgICovXG4gIHB1YmxpYyBnZXREYXRlQXJyYXkoKVxuICB7XG4gICAgbGV0IGRheXMgPSBbXTtcbiAgICBsZXQgY3VycmVudCA9IG1vbWVudCh0aGlzLnN0YXJ0RGF0ZSk7XG4gICAgbGV0IGxhc3QgID0gbW9tZW50KHRoaXMuZW5kRGF0ZSk7XG4gICAgd2hpbGUoY3VycmVudCA8IGxhc3QpXG4gICAge1xuICAgICAgZGF5cy5wdXNoKGN1cnJlbnQudG9EYXRlKCkpO1xuICAgICAgY3VycmVudCA9IGN1cnJlbnQuYWRkKDEsICdkYXknKTtcbiAgICB9XG4gICAgcmV0dXJuIGRheXM7XG4gIH1cblxuICBwdWJsaWMgZ2V0V2Vla0FycmF5KClcbiAge1xuICAgIGxldCB3ZWVrcyA9IFtdO1xuICAgIGxldCBjdXJyZW50ID0gbW9tZW50KHRoaXMuc3RhcnREYXRlKTtcbiAgICBsZXQgbGFzdCAgPSBtb21lbnQodGhpcy5lbmREYXRlKTtcbiAgICB3aGlsZShjdXJyZW50IDwgbGFzdClcbiAgICB7XG4gICAgICB3ZWVrcy5wdXNoKHsgc3RhcnREYXRlOiBjdXJyZW50LnRvRGF0ZSgpLCBlbmREYXRlOiBjdXJyZW50LmFkZCg2LCAnZGF5JykudG9EYXRlKCkgfSk7XG4gICAgICBjdXJyZW50ID0gY3VycmVudC5hZGQoMSwgJ2RheScpO1xuICAgIH1cbiAgICByZXR1cm4gd2Vla3M7XG4gIH1cblxuICAvKipcbiAgICogUmV0dXJucyB0aGUgaW5kZXggb2YgYSBkYXRlIGluXG4gICAqIHRoZSBbc3RhcnREYXRlIC0gZW5kRGF0ZV0gcGVyaW9kLlxuICAgKiBAcGFyYW0gZGF0ZSB0aGUgZGF0ZSBmb3Igd2hpY2ggdG8gY29tcHV0ZSB0aGUgaW5kZXhcbiAgICovXG4gIHB1YmxpYyBnZXREYXlOdW1iZXIoZGF0ZSA6IERhdGUpXG4gIHtcbiAgICByZXR1cm4gbW9tZW50KGRhdGUpLmRpZmYobW9tZW50KHRoaXMuc3RhcnREYXRlKSwgJ2RheXMnKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSB0b3RhbCBudW1iZXIgb2YgZGF5c1xuICAgKiBpbiB0aGUgW3N0YXJ0RGF0ZSAtIGVuZERhdGVdIHBlcmlvZC5cbiAgICovXG4gIHB1YmxpYyBnZXRUb3RhbE51bWJlck9mRGF5cygpXG4gIHtcbiAgICByZXR1cm4gdGhpcy5nZXREYXlOdW1iZXIodGhpcy5lbmREYXRlKSArIDE7XG4gIH1cbn0iLCJpbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50IH0gZnJvbSBcIi4vZHluYW1pYy5jb21wb25lbnRcIjtcbmltcG9ydCB7IENvbXBvbmVudFJlZiwgVmlld0NvbnRhaW5lclJlZiwgQ29tcG9uZW50RmFjdG9yeSwgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBUeXBlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFN0cmluZ01hcCB9IGZyb20gXCIuLi8uLi90eXBlcy9tYXBzXCI7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBEeW5hbWljQ29tcG9uZW50RmFjdG9yeTxEYXRhVHlwZSwgQ29tcG9uZW50VHlwZSBleHRlbmRzIER5bmFtaWNDb21wb25lbnQ8RGF0YVR5cGU+Plxue1xuICBwcml2YXRlIGZhY3RvcnkgOiBDb21wb25lbnRGYWN0b3J5PENvbXBvbmVudFR5cGU+O1xuXG4gIHB1YmxpYyBjb25zdHJ1Y3Rvcihwcml2YXRlIHJlYWRvbmx5IGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlciA6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29tcG9uZW50VHlwZSA6IFR5cGU8Q29tcG9uZW50VHlwZT4pXG4gIHtcbiAgICB0aGlzLmZhY3RvcnkgPSB0aGlzLmNvbXBvbmVudEZhY3RvcnlSZXNvbHZlci5yZXNvbHZlQ29tcG9uZW50RmFjdG9yeShjb21wb25lbnRUeXBlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBJbnNlcnRzIGEgY29tcG9uZW50IGluIGEgdmlldyBjb250YWluZXJcbiAgICogQHBhcmFtIGNvbnRhaW5lciB0aGUgY29udGFpbmVyIGluIHdoaWNoIHdlIHdhbnQgdG8gZGlzcGxheSB0aGlzIGNvbXBvbmVudFxuICAgKiBAcGFyYW0gZGF0YSB0aGUgZGF0YSBvZiB0aGUgY29tcG9uZW50IHRvIGRpc3BsYXlcbiAgICogQHBhcmFtIGNhbGxiYWNrcyBhIG1hcCBvZiBjYWxsYmFja3MgdG8gc3Vic2NyaWJlIHRvXG4gICAqIEBwYXJhbSBpbmRleCB0aGUgcG9zaXRpb24gaW4gdGhlIGNvbnRhaW5lciB3aGVyZSB0byBhZGQgdGhlIGNvbXBvbmVudFxuICAgKi9cbiAgcHVibGljIGluc2VydER5bmFtaWNDb21wb25lbnQoY29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZiwgZGF0YSA6IERhdGFUeXBlLCBjYWxsYmFja3M/OiBTdHJpbmdNYXA8RnVuY3Rpb24+LCBpbmRleD8gOiBudW1iZXIpIDogQ29tcG9uZW50UmVmPENvbXBvbmVudFR5cGU+XG4gIHtcbiAgICBsZXQgY29tcG9uZW50UmVmID0gY29udGFpbmVyLmNyZWF0ZUNvbXBvbmVudCh0aGlzLmZhY3RvcnksIGluZGV4KTtcbiAgICBsZXQgaW5zdGFuY2UgPSA8Q29tcG9uZW50VHlwZT5jb21wb25lbnRSZWYuaW5zdGFuY2U7XG4gICAgaW5zdGFuY2UudXBkYXRlRGF0YShkYXRhKTtcbiAgICBpZiAoY2FsbGJhY2tzKVxuICAgIHtcbiAgICAgIGZvciAobGV0IG5hbWUgaW4gY2FsbGJhY2tzKVxuICAgICAge1xuICAgICAgICBpbnN0YW5jZS5zdWJzY3JpYmUobmFtZSwgY2FsbGJhY2tzW25hbWVdKTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGNvbXBvbmVudFJlZjtcbiAgfVxufSIsImltcG9ydCB7IER5bmFtaWNDb21wb25lbnRGYWN0b3J5IH0gZnJvbSBcIi4uL2R5bmFtaWMvZHluYW1pYy5mYWN0b3J5XCI7XG5pbXBvcnQgeyBBYnN0cmFjdEV2ZW50Q29tcG9uZW50IH0gZnJvbSBcIi4vYWJzdHJhY3QtZXZlbnQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBUeXBlLCBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRXZlbnRDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlPiBleHRlbmRzIER5bmFtaWNDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlLCBBYnN0cmFjdEV2ZW50Q29tcG9uZW50PERhdGFUeXBlPj5cbntcbiAgcHVibGljIGNvbnN0cnVjdG9yKGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlciA6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29tcG9uZW50VHlwZSA6IFR5cGU8QWJzdHJhY3RFdmVudENvbXBvbmVudDxEYXRhVHlwZT4+KVxuICB7XG4gICAgc3VwZXIoY29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb21wb25lbnRUeXBlKTtcbiAgfVxufSIsImltcG9ydCB7IER5bmFtaWNDb21wb25lbnRGYWN0b3J5IH0gZnJvbSAnLi4vZHluYW1pYy9keW5hbWljLmZhY3RvcnknO1xuaW1wb3J0IHsgQ2FsZW5kYXJEYXlEYXRhIH0gZnJvbSAnLi4vLi4vdHlwZXMvY2FsZW5kYXItZGF5JztcbmltcG9ydCB7IEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudCB9IGZyb20gJy4vYWJzdHJhY3QtZGF5LWNlbGwuY29tcG9uZW50JztcbmltcG9ydCB7IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgVHlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRGF5Q2VsbENvbXBvbmVudEZhY3Rvcnk8RGF0YVR5cGU+IGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudEZhY3Rvcnk8Q2FsZW5kYXJEYXlEYXRhPERhdGFUeXBlPiwgQWJzdHJhY3REYXlDZWxsQ29tcG9uZW50PERhdGFUeXBlPj5cbntcbiAgcHVibGljIGNvbnN0cnVjdG9yKGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlciA6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29tcG9uZW50VHlwZSA6IFR5cGU8QWJzdHJhY3REYXlDZWxsQ29tcG9uZW50PERhdGFUeXBlPj4pXG4gIHtcbiAgICBzdXBlcihjb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIGNvbXBvbmVudFR5cGUpO1xuICB9XG59IiwiaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudCB9IGZyb20gXCIuL2R5bmFtaWMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBWaWV3Q29udGFpbmVyUmVmLCBDb21wb25lbnRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudEZhY3RvcnkgfSBmcm9tIFwiLi9keW5hbWljLmZhY3RvcnlcIjtcbmltcG9ydCB7IFN0cmluZ01hcCwgTnVtZXJpY01hcCB9IGZyb20gXCIuLi8uLi90eXBlcy9tYXBzXCI7XG5cbmV4cG9ydCBjbGFzcyBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyPERhdGFUeXBlLCBDb21wb25lbnRUeXBlIGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudDxEYXRhVHlwZT4+XG57XG5cbiAgLyoqXG4gICAqIFN0b3JlcyB0aGUgcHJldmlvdXMgYXJyYXkgc3RhdGVcbiAgICovXG4gIHByaXZhdGUgb2xkRGF0YUFycmF5IDogRGF0YVR5cGVbXSA9IFtdO1xuICBcbiAgcHVibGljIGNsZWFyKClcbiAge1xuICAgIGZvciAobGV0IGlkIGluIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXApXG4gICAge1xuICAgICAgaWYgKHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbaWRdKVxuICAgICAge1xuICAgICAgICB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2lkXS5kZXN0cm95KCk7XG4gICAgICAgIGRlbGV0ZSB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2lkXTtcbiAgICAgIH1cbiAgICB9XG4gICAgdGhpcy5vbGREYXRhQXJyYXkgPSBbXTtcbiAgfVxuXG4gIC8qKlxuICAgKiBVcGRhdGVzLCBhZGQgb3IgcmVtb3ZlIGRhdGEgdmlld3MgYmFzZWRcbiAgICogb24gdGhlIG9sZCBkYXRhIGxpc3RcbiAgICovXG4gIHB1YmxpYyB1cGRhdGVDb250YWluZXIoXG4gICAgY29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZixcbiAgICBmYWN0b3J5IDogRHluYW1pY0NvbXBvbmVudEZhY3Rvcnk8RGF0YVR5cGUsIENvbXBvbmVudFR5cGU+LFxuICAgIGRhdGFBcnJheSA6IERhdGFUeXBlW10sXG4gICAgZGF0YUlkQWNjZXNzb3IgOiBzdHJpbmcsXG4gICAgZHluYW1pY0NvbXBvbmVudENhbGxiYWNrcz8gOiBTdHJpbmdNYXA8RnVuY3Rpb24+LFxuICAgIG9uQWRkVmlldz8gOiBGdW5jdGlvbixcbiAgICBvblVwZGF0ZVZpZXc/IDogRnVuY3Rpb24sXG4gICAgb25EZWxldGVWaWV3PyA6IEZ1bmN0aW9uXG4gICkgOiB2b2lkXG4gIHtcbiAgICBpZiAgKCFjb250YWluZXIpXG4gICAge1xuICAgICAgdGhyb3cgJ2NoaWxkIGNvbnRhaW5lciAoI2NoaWxkQ29udGFpbmVyKSBjb3VsZCBub3QgYmUgZm91bmQgaW4gdGhlIGNvbnRhaW5lciB2aWV3J1xuICAgIH1cbiAgICBmb3IgKGxldCBkYXRhIG9mIGRhdGFBcnJheSlcbiAgICB7XG4gICAgICBsZXQgYWxyZWFkeVByZXNlbnQgPSBmYWxzZTtcbiAgICAgIGZvciAobGV0IG9sZERhdGEgb2YgdGhpcy5vbGREYXRhQXJyYXkpXG4gICAgICB7XG4gICAgICAgIGlmIChkYXRhW2RhdGFJZEFjY2Vzc29yXSA9PT0gb2xkRGF0YVtkYXRhSWRBY2Nlc3Nvcl0pXG4gICAgICAgIHtcbiAgICAgICAgICBhbHJlYWR5UHJlc2VudCA9IHRydWU7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGlmIChhbHJlYWR5UHJlc2VudClcbiAgICAgIHtcbiAgICAgICAgdGhpcy51cGRhdGVEeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciwgZGF0YSwgZGF0YUlkQWNjZXNzb3IsIG9uVXBkYXRlVmlldyk7XG4gICAgICB9XG4gICAgICBlbHNlXG4gICAgICB7XG4gICAgICAgIHRoaXMuYWRkRHluYW1pY0NvbXBvbmVudChjb250YWluZXIsIGZhY3RvcnksIGRhdGEsIGRhdGFJZEFjY2Vzc29yLCBkeW5hbWljQ29tcG9uZW50Q2FsbGJhY2tzLCBvbkFkZFZpZXcpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZvciAobGV0IG9sZERhdGEgb2YgdGhpcy5vbGREYXRhQXJyYXkpXG4gICAge1xuICAgICAgbGV0IHN0aWxsUHJlc2VudCA9IGZhbHNlO1xuICAgICAgZm9yIChsZXQgZGF0YSBvZiBkYXRhQXJyYXkpXG4gICAgICB7XG4gICAgICAgIGlmIChkYXRhW2RhdGFJZEFjY2Vzc29yXSA9PT0gb2xkRGF0YVtkYXRhSWRBY2Nlc3Nvcl0pXG4gICAgICAgIHtcbiAgICAgICAgICBzdGlsbFByZXNlbnQgPSB0cnVlO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAoIXN0aWxsUHJlc2VudClcbiAgICAgIHtcbiAgICAgICAgdGhpcy5yZW1vdmVEeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciwgb2xkRGF0YSwgZGF0YUlkQWNjZXNzb3IsIG9uRGVsZXRlVmlldyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdGhpcy5vbGREYXRhQXJyYXkgPSBkYXRhQXJyYXk7XG4gIH1cblxuICAvKipcbiAgICogQSBudW1lcmljIG1hcCB1c2VkIHRvIHN0b3JlIHRoZSBkeW5hbWljIHZpZXdzXG4gICAqL1xuICBwcml2YXRlIGR5bmFtaWNWaWV3Q29tcG9uZW50TWFwIDogTnVtZXJpY01hcDxDb21wb25lbnRSZWY8Q29tcG9uZW50VHlwZT4+ID0ge307XG5cbiAgcHJpdmF0ZSByZW1vdmVEeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWYsIGRhdGEgOiBEYXRhVHlwZSwgZGF0YUlkQWNjZXNzb3IgOiBzdHJpbmcsIG9uRGVsZXRlVmlldz8gOiBGdW5jdGlvbikgOiB2b2lkXG4gIHtcbiAgICBpZiAodHlwZW9mIGRhdGFbZGF0YUlkQWNjZXNzb3JdICE9PSAnbnVtYmVyJylcbiAgICB7XG4gICAgICB0aHJvdyAnaWQgKCcgKyBkYXRhSWRBY2Nlc3NvciArICcpIG5vdCBmb3VuZCBpbiBkYXRhIHN0cnVjdHVyZSAoZGVsZXRlIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlIGlmICghdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0pXG4gICAge1xuICAgICAgdGhyb3cgJ2R5bmFtaWMgdmlldyBub3QgZm91bmQgaW4gdGhlIGR5bmFtaWMgdmlldyBjb250YWluZXIgKGRlbGV0ZSBkeW5hbWljIGNvbXBvbmVudCBtZXRob2QpJztcbiAgICB9XG4gICAgZWxzZVxuICAgIHtcbiAgICAgIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbZGF0YVtkYXRhSWRBY2Nlc3Nvcl1dLmRlc3Ryb3koKTtcbiAgICAgIGRlbGV0ZSB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXTtcbiAgICAgIGlmIChvbkRlbGV0ZVZpZXcpIG9uRGVsZXRlVmlldyhkYXRhLCB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBhZGREeW5hbWljQ29tcG9uZW50KFxuICAgIGNvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWYsXG4gICAgZmFjdG9yeSA6IER5bmFtaWNDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlLCBDb21wb25lbnRUeXBlPixcbiAgICBkYXRhIDogRGF0YVR5cGUsXG4gICAgZGF0YUlkQWNjZXNzb3IgOiBzdHJpbmcsXG4gICAgZHluYW1pY0NvbXBvbmVudENhbGxiYWNrcyA6IFN0cmluZ01hcDxGdW5jdGlvbj4sXG4gICAgb25BZGRWaWV3PyA6IEZ1bmN0aW9uXG4gICkgOiB2b2lkXG4gIHtcbiAgICBpZiAodHlwZW9mIGRhdGFbZGF0YUlkQWNjZXNzb3JdICE9PSAnbnVtYmVyJylcbiAgICB7XG4gICAgICB0aHJvdyAnaWQgKCcgKyBkYXRhSWRBY2Nlc3NvciArICcpIG5vdCBmb3VuZCBpbiBkYXRhIHN0cnVjdHVyZSAoYWRkIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlIGlmICh0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSlcbiAgICB7XG4gICAgICB0aHJvdyAnZHluYW1pYyB2aWV3IGFscmVhZHkgcHJlc2VudCBpbiB0aGUgZHluYW1pYyB2aWV3IGNvbnRhaW5lciAoYWRkIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlXG4gICAge1xuICAgICAgbGV0IGNvbXBvbmVudFJlZiA9IGZhY3RvcnkuaW5zZXJ0RHluYW1pY0NvbXBvbmVudChjb250YWluZXIsIGRhdGEsIGR5bmFtaWNDb21wb25lbnRDYWxsYmFja3MpO1xuICAgICAgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0gPSBjb21wb25lbnRSZWY7XG4gICAgICBpZiAob25BZGRWaWV3KSBvbkFkZFZpZXcoZGF0YSwgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0pO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBVcGRhdGVzIGFuIGRhdGEgdmlldyBtb2RlbFxuICAgKiBAcGFyYW0gZGF0YSB0aGUgbmV3IGRhdGEgbW9kZWwgdG8gdXNlXG4gICAqL1xuICBwcml2YXRlIHVwZGF0ZUR5bmFtaWNDb21wb25lbnQoY29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZiwgZGF0YSA6IERhdGFUeXBlLCBkYXRhSWRBY2Nlc3NvciA6IHN0cmluZywgb25VcGRhdGVWaWV3PyA6IEZ1bmN0aW9uKSA6IHZvaWRcbiAge1xuICAgIGlmICh0eXBlb2YgZGF0YVtkYXRhSWRBY2Nlc3Nvcl0gIT09ICdudW1iZXInKVxuICAgIHtcbiAgICAgIHRocm93ICdpZCAoJyArIGRhdGFJZEFjY2Vzc29yICsgJykgbm90IGZvdW5kIGluIGRhdGEgc3RydWN0dXJlIChhZGQgZHluYW1pYyBjb21wb25lbnQgbWV0aG9kKSc7XG4gICAgfVxuICAgIGVsc2UgaWYgKCF0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSlcbiAgICB7XG4gICAgICB0aHJvdyAnZHluYW1pYyB2aWV3IG5vdCBmb3VuZCBpbiB0aGUgZHluYW1pYyB2aWV3IGNvbnRhaW5lciAoYWRkIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlXG4gICAge1xuICAgICAgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0uaW5zdGFuY2UudXBkYXRlRGF0YShkYXRhKTtcbiAgICAgIGlmIChvblVwZGF0ZVZpZXcpIG9uVXBkYXRlVmlldyhkYXRhLCB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSk7XG4gICAgfVxuICB9XG59IiwiaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudEZhY3RvcnkgfSBmcm9tIFwiLi4vZHluYW1pYy9keW5hbWljLmZhY3RvcnlcIjtcbmltcG9ydCB7IENhbGVuZGFyV2Vla0RhdGEgfSBmcm9tIFwiLi4vLi4vdHlwZXMvY2FsZW5kYXItd2Vla1wiO1xuaW1wb3J0IHsgQWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudCB9IGZyb20gXCIuL2Fic3RyYWN0LXdlZWstc3VtbWFyeS5jb21wb25lbnRcIjtcbmltcG9ydCB7IENvbXBvbmVudFJlZiwgVmlld0NvbnRhaW5lclJlZiwgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBUeXBlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFN0cmluZ01hcCB9IGZyb20gXCIuLi8uLi90eXBlcy9tYXBzXCI7XG5pbXBvcnQgeyBpc1NhbWVEYXkgfSBmcm9tIFwiLi4vLi4vdXRpbHMvZGF0ZVwiO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgV2Vla1N1bW1hcnlDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlPiBleHRlbmRzIER5bmFtaWNDb21wb25lbnRGYWN0b3J5PENhbGVuZGFyV2Vla0RhdGE8RGF0YVR5cGU+LCBBYnN0cmFjdFdlZWtTdW1tYXJ5Q29tcG9uZW50PERhdGFUeXBlPj5cbntcbiAgcHVibGljIGNvbnN0cnVjdG9yKGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlciA6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29tcG9uZW50VHlwZSA6IFR5cGU8QWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudDxEYXRhVHlwZT4+KVxuICB7XG4gICAgc3VwZXIoY29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb21wb25lbnRUeXBlKTtcbiAgfVxuXG4gIHB1YmxpYyBpbnNlcnREeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWYsIGRhdGEgOiBDYWxlbmRhcldlZWtEYXRhPERhdGFUeXBlPiwgY2FsbGJhY2tzPzogU3RyaW5nTWFwPEZ1bmN0aW9uPikgOiBDb21wb25lbnRSZWY8QWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudDxEYXRhVHlwZT4+XG4gIHtcbiAgICBsZXQgcG9zaXRpb24gPSB0aGlzLmZpbmRXZWVrUG9zaXRpb25JbkNvbnRhaW5lcihjb250YWluZXIsIGRhdGEpO1xuICAgIHJldHVybiAocG9zaXRpb24gPj0gMCkgPyBzdXBlci5pbnNlcnREeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciwgZGF0YSwgY2FsbGJhY2tzLCBwb3NpdGlvbikgOiB1bmRlZmluZWQ7XG4gIH1cblxuICBwcml2YXRlIGZpbmRXZWVrUG9zaXRpb25JbkNvbnRhaW5lcihjb250YWluZXIgOiBWaWV3Q29udGFpbmVyUmVmLCBkYXRhIDogQ2FsZW5kYXJXZWVrRGF0YTxEYXRhVHlwZT4pIDogbnVtYmVyXG4gIHtcbiAgICBsZXQgaW5kZXg7XG4gICAgbGV0IGZvdW5kID0gZmFsc2U7XG4gICAgZm9yIChpbmRleCA9IDA7ICFmb3VuZCAmJiBpbmRleCA8IGNvbnRhaW5lci5sZW5ndGg7IGluZGV4KyspXG4gICAge1xuICAgICAgbGV0IGNvbXBvbmVudEluc3RhbmNlID0gY29udGFpbmVyLmdldChpbmRleClbJ192aWV3J11bJ25vZGVzJ11bMV1bJ2luc3RhbmNlJ107XG4gICAgICBpZiAoY29tcG9uZW50SW5zdGFuY2UuZGF0YS5kYXRlICYmIGlzU2FtZURheShjb21wb25lbnRJbnN0YW5jZS5kYXRhLmRhdGUsIGRhdGEuZW5kRGF0ZSkpXG4gICAgICB7XG4gICAgICAgIHJldHVybiBpbmRleCArIDE7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiAtMTtcbiAgfVxufSIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBTaW1wbGVDaGFuZ2VzLCBDb21wb25lbnRSZWYsIFZpZXdDaGlsZCwgVmlld0NvbnRhaW5lclJlZiwgSG9zdEJpbmRpbmcsIEluamVjdCwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgZ2V0V2Vla051bWJlciB9IGZyb20gJy4uLy4uL3V0aWxzL2RhdGUnO1xuaW1wb3J0IHsgRXZlbnREaXNwYXRjaGVyIH0gZnJvbSAnLi4vLi4vdXRpbHMvZGlzcGF0Y2hlcic7XG5pbXBvcnQgeyBBYnN0cmFjdERheUNlbGxDb21wb25lbnQgfSBmcm9tICcuLi9kYXktY2VsbC9hYnN0cmFjdC1kYXktY2VsbC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ2FsZW5kYXJEYXlEYXRhIH0gZnJvbSAnLi4vLi4vdHlwZXMvY2FsZW5kYXItZGF5JztcbmltcG9ydCB7IEV2ZW50IH0gZnJvbSAnLi4vLi4vdHlwZXMvZXZlbnQnO1xuaW1wb3J0IHsgQ2FsZW5kYXJXZWVrRGF0YSB9IGZyb20gJy4uLy4uL3R5cGVzL2NhbGVuZGFyLXdlZWsnO1xuaW1wb3J0IHsgQWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudCB9IGZyb20gJy4uL3dlZWstc3VtbWFyeS9hYnN0cmFjdC13ZWVrLXN1bW1hcnkuY29tcG9uZW50JztcbmltcG9ydCB7IEV2ZW50Q29tcG9uZW50RmFjdG9yeSB9IGZyb20gJy4uL2V2ZW50L2V2ZW50LmZhY3RvcnknO1xuaW1wb3J0IHsgRGF5Q2VsbENvbXBvbmVudEZhY3RvcnkgfSBmcm9tICcuLi9kYXktY2VsbC9kYXktY2VsbC5mYWN0b3J5JztcbmltcG9ydCB7IER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXIgfSBmcm9tICcuLi9keW5hbWljL2R5bmFtaWMtbWFuYWdlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgV2Vla1N1bW1hcnlDb21wb25lbnRGYWN0b3J5IH0gZnJvbSAnLi4vd2Vlay1zdW1tYXJ5L3dlZWstc3VtbWFyeS5mYXRvcnknO1xuXG5sZXQgbW9tZW50ID0gcmVxdWlyZSgnbW9tZW50Jyk7XG5sZXQgaGFzaCA9IHJlcXVpcmUoJ29iamVjdC1oYXNoJyk7XG5cbmV4cG9ydCBjb25zdCBEQVlfQ0VMTF9DT01QT05FTlRfRkFDVE9SWV9QUk9WSURFUiA9ICdEYXlDZWxsQ29tcG9uZW50RmFjdG9yeSc7XG5leHBvcnQgY29uc3QgRVZFTlRfQ09NUE9ORU5UX0ZBQ1RPUllfUFJPVklERVIgICAgPSAnRXZlbnRDb21wb25lbnRGYWN0b3J5JztcbmV4cG9ydCBjb25zdCBXRUVLX1NVTU1BUllfQ09NUE9ORU5UX0ZBQ1RPUlkgICAgICA9ICdXZWVrU3VtbWFyeUNvbXBvbmVudEZhY3RvcnknO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhbmd1bGFyLWFkdmFuY2VkLWNhbGVuZGFyJyxcbiAgdGVtcGxhdGU6IGA8aDUgKm5nRm9yPSdsZXQgbmFtZSBvZiBoZWFkZXJzJyBjbGFzcz0naGVhZGVyJz4ge3tuYW1lfX0gPC9oNT5cbjxuZy10ZW1wbGF0ZSAjZGF5R3JpZD48L25nLXRlbXBsYXRlPlxuPGg1ICpuZ0Zvcj0nbGV0IG5hbWUgb2YgaGVhZGVycycgY2xhc3M9J2Zvb3Rlcic+IHt7bmFtZX19IDwvaDU+YCxcbiAgc3R5bGVzOiBbYEBpbXBvcnQgdXJsKGh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1SYWxld2F5KTsuZm9vdGVyLC5oZWFkZXJ7dGV4dC1hbGlnbjpjZW50ZXI7Zm9udC1mYW1pbHk6UmFsZXdheSxzYW5zLXNlcmlmfWBdXG59KVxuZXhwb3J0IGNsYXNzIENhbGVuZGFyQ29tcG9uZW50XG57XG4gIC8qKlxuICAgKiBGaXJzdCBkYXRlIG9mIHRoZSBwZXJpb2QgdG8gZGlzcGxheSBpbiB0aGUgY2FsZW5kYXJcbiAgICovXG4gIEBJbnB1dCgnc3RhcnQtZGF0ZScpIHByaXZhdGUgc3RhcnREYXRlIDogRGF0ZTtcblxuICAvKipcbiAgICogTGFzdCBkYXRlIG9mIHRoZSBwZXJpb2QgdG8gZGlzcGxheSBpbiB0aGUgY2FsZW5kYXJcbiAgICovXG4gIEBJbnB1dCgnZW5kLWRhdGUnKSBwcml2YXRlIGVuZERhdGUgOiBEYXRlO1xuICBcbiAgLyoqXG4gICAqIFdlZWsgZGF5cyBsYWJlbCB0byBkaXNwbGF5IGluIHRoZSBoZWFkZXJcbiAgICogKGRlZmF1bHQgaXMgZW5nbGlzaCkuXG4gICAqL1xuICBASW5wdXQoJ2hlYWRlcnMnKSBwdWJsaWMgaGVhZGVycyA6IHN0cmluZ1tdID0gW1xuICAgICdNb25kYXknLFxuICAgICdUdWVzZGF5JyxcbiAgICAnV2VkbmVzZGF5JyxcbiAgICAnVGh1cnNkYXknLFxuICAgICdGcmlkYXknLFxuICAgICdTYXR1cmRheScsXG4gICAgJ1N1bmRheScsXG4gIF07XG5cbiAgLyoqXG4gICAqIEEgbGlzdCBvZiBldmVudHMgdG8gZGlzcGxheSBpbiB0aGUgY2FsbGVuZGFyLlxuICAgKi9cbiAgQElucHV0KCdldmVudHMnKSBwcml2YXRlIGV2ZW50cyA6IEV2ZW50W10gPSBbXTtcbiAgXG4gIC8qKlxuICAgKiBUaGUgbmFtZSBvZiB0aGUgZGF0ZSBhdHRyaWJ1dGUgdG8gdXNlXG4gICAqIHdoZW4gZGlzcGxheWluZyB0aGUgZXZlbnRzIChkZWZhdWx0IGlzICdkYXRlJykuXG4gICAqL1xuICBASW5wdXQoJ2RhdGUtYWNjZXNzb3InKSBwcml2YXRlIGRhdGVBY2Nlc3NvciA6IHN0cmluZyA9ICdkYXRlJztcblxuICAvKipcbiAgICogVGhlIG5hbWUgb2YgdGhlIGlkIGF0dHJpYnV0ZSB0byB1c2VcbiAgICogd2hlbiBkaXNwbGF5aW5nIHRoZSBldmVudHMgKGRlZmF1bHQgaXMgJ2lkJykuXG4gICAqL1xuICBASW5wdXQoJ2lkLWFjY2Vzc29yJykgcHJpdmF0ZSBpZEFjY2Vzc29yIDogc3RyaW5nID0gJ2lkJztcblxuICBwdWJsaWMgY29uc3RydWN0b3IoXG4gICAgQEluamVjdChEQVlfQ0VMTF9DT01QT05FTlRfRkFDVE9SWV9QUk9WSURFUikgcHVibGljIGRheUNlbGxDb21wb25lbnRGYWN0b3J5IDogRGF5Q2VsbENvbXBvbmVudEZhY3Rvcnk8RXZlbnQ+LFxuICAgIEBPcHRpb25hbCgpIEBJbmplY3QoRVZFTlRfQ09NUE9ORU5UX0ZBQ1RPUllfUFJPVklERVIpIHB1YmxpYyBldmVudENvbXBvbmVudEZhY3RvcnkgOiBFdmVudENvbXBvbmVudEZhY3Rvcnk8RXZlbnQ+LFxuICAgIEBPcHRpb25hbCgpIEBJbmplY3QoV0VFS19TVU1NQVJZX0NPTVBPTkVOVF9GQUNUT1JZKSBwdWJsaWMgd2Vla1N1bW1hcnlDb21wb25lbnRGYWN0b3J5IDogV2Vla1N1bW1hcnlDb21wb25lbnRGYWN0b3J5PEV2ZW50PlxuICApe31cblxuICAvKiAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gKi9cblxuICAvKipcbiAgICogVmlldyBjb250YWluZXIgc3RvcmluZyB0aGUgZHluYW1pYyBjb21wb25lbnRzXG4gICAqL1xuICBAVmlld0NoaWxkKCdkYXlHcmlkJywgeyByZWFkOiBWaWV3Q29udGFpbmVyUmVmIH0pXG4gIHByaXZhdGUgZGF5R3JpZENvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWY7XG4gIHByaXZhdGUgZHluYW1pY0RheUNlbGxWaWV3TWFuYWdlciA6IER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXI8RXZlbnQsIEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudDxFdmVudD4+XG4gICAgPSBuZXcgRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlcjxFdmVudCwgQWJzdHJhY3REYXlDZWxsQ29tcG9uZW50PEV2ZW50Pj4oKTtcbiAgcHJpdmF0ZSBkeW5hbWljV2Vla1N1bW1hcnlWaWV3TWFuYWdlciA6IER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXI8RXZlbnQsIEFic3RyYWN0V2Vla1N1bW1hcnlDb21wb25lbnQ8RXZlbnQ+PlxuICAgID0gbmV3IER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXI8RXZlbnQsIEFic3RyYWN0V2Vla1N1bW1hcnlDb21wb25lbnQ8RXZlbnQ+PigpO1xuXG4gIC8qIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAqL1xuXG4gIHByaXZhdGUgZGlzcGF0Y2hlciA6IEV2ZW50RGlzcGF0Y2hlcjtcbiAgcHJpdmF0ZSBkYXlzIDogQ2FsZW5kYXJEYXlEYXRhPEV2ZW50PltdO1xuICBwcml2YXRlIHdlZWtzIDogQ2FsZW5kYXJXZWVrRGF0YTxFdmVudD5bXTtcbiAgXG4gIHByaXZhdGUgb2xkSGFzaCA6IHN0cmluZyA9ICcnO1xuICBuZ0RvQ2hlY2soKVxuICB7XG4gICAgbGV0IG5ld0hhc2ggPSBoYXNoLnNoYTEodGhpcy5ldmVudHMpO1xuICAgIGlmIChuZXdIYXNoICE9PSB0aGlzLm9sZEhhc2gpXG4gICAge1xuICAgICAgdGhpcy51cGRhdGVDYWxlbmRhcigpO1xuICAgICAgdGhpcy5vbGRIYXNoID0gbmV3SGFzaDtcbiAgICB9XG4gIH1cblxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzIDogU2ltcGxlQ2hhbmdlcylcbiAge1xuICAgIGlmIChjaGFuZ2VzLnN0YXJ0RGF0ZSB8fCBjaGFuZ2VzLmVuZERhdGUgfHwgY2hhbmdlcy5kYXRlQWNjZXNzb3IpXG4gICAge1xuICAgICAgbGV0IHN0YXJ0RGF0ZSA9IChjaGFuZ2VzLnN0YXJ0RGF0ZSkgPyBjaGFuZ2VzLnN0YXJ0RGF0ZS5jdXJyZW50VmFsdWUgOiB0aGlzLnN0YXJ0RGF0ZTtcbiAgICAgIGxldCBlbmREYXRlID0gKGNoYW5nZXMuZW5kRGF0ZSkgPyBjaGFuZ2VzLmVuZERhdGUuY3VycmVudFZhbHVlIDogdGhpcy5lbmREYXRlO1xuICAgICAgbGV0IGRhdGVBY2Nlc3NvciA9IChjaGFuZ2VzLmRhdGVBY2Nlc3NvcikgPyBjaGFuZ2VzLmRhdGVBY2Nlc3Nvci5jdXJyZW50VmFsdWUgOiB0aGlzLmRhdGVBY2Nlc3NvcjtcblxuICAgICAgdGhpcy5kaXNwYXRjaGVyID0gbmV3IEV2ZW50RGlzcGF0Y2hlcihkYXRlQWNjZXNzb3IsIHN0YXJ0RGF0ZSwgZW5kRGF0ZSk7XG4gICAgICB0aGlzLnVwZGF0ZUNhbGVuZGFyKCk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIE9sZCBzdGFydCBhbmQgZW5kIGRhdGUgb2YgdGhlIGNhbGVuZGFyLlxuICAgKiBVc2VkIHRvIGtlZXAgdHJhY2sgb2YgdGhlIGNoYW5nZXMgaW4gcGVyaW9kXG4gICAqIGluIG9yZGVyIHRvIHVwZGF0ZSB0aGUgdmlldy5cbiAgICovXG4gIHByaXZhdGUgb2xkU3RhcnREYXRlIDogRGF0ZTtcbiAgcHJpdmF0ZSBvbGRFbmREYXRlIDogRGF0ZTtcblxuXG4gIHByaXZhdGUgdXBkYXRlQ2FsZW5kYXIoKVxuICB7XG4gICAgaWYgKCF0aGlzLmRheUNlbGxDb21wb25lbnRGYWN0b3J5KVxuICAgIHtcbiAgICAgIHRoaXMuZGF5cyA9IFtdO1xuICAgICAgdGhyb3cgJ2RheSBjZWxsIGNvbXBvbmVudCBmYWN0b3J5IChkYXktY29tcG9uZW50LWZhY3RvcnkpIHdhcyBub3Qgc2V0ISdcbiAgICB9XG4gICAgZWxzZSBpZiAoIXRoaXMuZGF5R3JpZENvbnRhaW5lcilcbiAgICB7XG4gICAgICB0aGlzLmRheXMgPSBbXTtcbiAgICAgIHRocm93ICdkYXkgZ3JpZCAoZGF5R3JpZCkgbm90IGZvdW5kIGluIHRlbXBsYXRlICEnXG4gICAgfVxuICAgIGVsc2VcbiAgICB7XG4gICAgICB0aGlzLmRpc3BhdGNoRXZlbnRzKCk7XG4gICAgICB0aGlzLnVwZGF0ZURheUNlbGxWaWV3cygpO1xuXG4gICAgICBpZiAodGhpcy53ZWVrU3VtbWFyeUNvbXBvbmVudEZhY3RvcnkpXG4gICAgICB7XG4gICAgICAgIHRoaXMudXBkYXRlV2Vla1N1bW1hcnlDZWxsVmlld3MoKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuXG4gIC8qKlxuICAgKiBTb3J0cyBldmVudHMgYnkgZGF0ZVxuICAgKi9cbiAgcHJpdmF0ZSBkaXNwYXRjaEV2ZW50cygpXG4gIHtcbiAgICB0aGlzLmRpc3BhdGNoZXIuY2xlYXIoKTtcbiAgICBmb3IgKGxldCBldmVudCBvZiB0aGlzLmV2ZW50cylcbiAgICB7XG4gICAgICB0aGlzLmRpc3BhdGNoZXIuYWRkKGV2ZW50KTtcbiAgICB9XG4gICAgdGhpcy5kYXlzID0gdGhpcy5kaXNwYXRjaGVyLmdldERhdGVBcnJheSgpLm1hcChkYXRlID0+ICh7XG4gICAgICBpZDogdGhpcy5kaXNwYXRjaGVyLmdldERheU51bWJlcihkYXRlKSxcbiAgICAgIGRhdGU6IGRhdGUsXG4gICAgICBjYWxlbmRhclN0YXJ0RGF0ZTogdGhpcy5zdGFydERhdGUsXG4gICAgICBjYWxlbmRhckVuZERhdGU6IHRoaXMuZW5kRGF0ZSxcbiAgICAgIGV2ZW50czogdGhpcy5kaXNwYXRjaGVyLmdldEV2ZW50cyhkYXRlKSxcbiAgICAgIGRhdGVBY2Nlc3NvcjogdGhpcy5kYXRlQWNjZXNzb3IsXG4gICAgfSkpO1xuICB9XG5cbiAgcHJpdmF0ZSB1cGRhdGVEYXlDZWxsVmlld3MoKVxuICB7XG4gICAgdGhpcy5keW5hbWljRGF5Q2VsbFZpZXdNYW5hZ2VyLnVwZGF0ZUNvbnRhaW5lcihcbiAgICAgIHRoaXMuZGF5R3JpZENvbnRhaW5lcixcbiAgICAgIHRoaXMuZGF5Q2VsbENvbXBvbmVudEZhY3RvcnksXG4gICAgICB0aGlzLmRheXMsXG4gICAgICAnaWQnLFxuICAgICAge1xuICAgICAgICBhZGQ6IHRoaXMub25BZGQuYmluZCh0aGlzKSxcbiAgICAgICAgdXBkYXRlOiB0aGlzLm9uVXBkYXRlLmJpbmQodGhpcyksXG4gICAgICAgIGRlbGV0ZTogdGhpcy5vbkRlbGV0ZS5iaW5kKHRoaXMpXG4gICAgICB9LFxuICAgICAgdGhpcy51cGRhdGVEYXlDZWxsLmJpbmQodGhpcyksXG4gICAgICB0aGlzLnVwZGF0ZURheUNlbGwuYmluZCh0aGlzKSxcbiAgICAgIHRoaXMudXBkYXRlRGF5Q2VsbC5iaW5kKHRoaXMpXG4gICAgKTtcbiAgfVxuXG4gIHByaXZhdGUgdXBkYXRlV2Vla1N1bW1hcnlDZWxsVmlld3MoKVxuICB7XG4gICAgdGhpcy53ZWVrcyA9IHRoaXMuZGlzcGF0Y2hlci5nZXRXZWVrQXJyYXkoKS5tYXAod2VlayA9PiAoe1xuICAgICAgaWQ6IGdldFdlZWtOdW1iZXIod2Vlay5zdGFydERhdGUpLFxuICAgICAgc3RhcnREYXRlOiB3ZWVrLnN0YXJ0RGF0ZSxcbiAgICAgIGVuZERhdGU6IHdlZWsuZW5kRGF0ZSxcbiAgICAgIGNhbGVuZGFyU3RhcnREYXRlOiB0aGlzLnN0YXJ0RGF0ZSxcbiAgICAgIGNhbGVuZGFyRW5kRGF0ZTogdGhpcy5lbmREYXRlLFxuICAgICAgZXZlbnRzOiB0aGlzLmRpc3BhdGNoZXIuZ2V0RXZlbnRzQmV0d2Vlbih3ZWVrLnN0YXJ0RGF0ZSwgd2Vlay5lbmREYXRlKSxcbiAgICAgIGRhdGVBY2Nlc3NvcjogdGhpcy5kYXRlQWNjZXNzb3IsXG4gICAgfSkpO1xuXG4gICAgaWYgKHRoaXMuc3RhcnREYXRlICE9PSB0aGlzLm9sZFN0YXJ0RGF0ZSB8fCB0aGlzLmVuZERhdGUgIT09IHRoaXMub2xkRW5kRGF0ZSlcbiAgICB7XG4gICAgICB0aGlzLmR5bmFtaWNXZWVrU3VtbWFyeVZpZXdNYW5hZ2VyLmNsZWFyKCk7XG4gICAgICB0aGlzLm9sZFN0YXJ0RGF0ZSA9IHRoaXMuc3RhcnREYXRlO1xuICAgICAgdGhpcy5vbGRFbmREYXRlID0gdGhpcy5lbmREYXRlO1xuICAgIH1cbiAgICBcbiAgICB0aGlzLmR5bmFtaWNXZWVrU3VtbWFyeVZpZXdNYW5hZ2VyLnVwZGF0ZUNvbnRhaW5lcihcbiAgICAgIHRoaXMuZGF5R3JpZENvbnRhaW5lcixcbiAgICAgIHRoaXMud2Vla1N1bW1hcnlDb21wb25lbnRGYWN0b3J5LFxuICAgICAgdGhpcy53ZWVrcyxcbiAgICAgICdpZCcsXG4gICAgICB7XG4gICAgICAgIGFkZDogdGhpcy5vbkFkZC5iaW5kKHRoaXMpLFxuICAgICAgICB1cGRhdGU6IHRoaXMub25VcGRhdGUuYmluZCh0aGlzKSxcbiAgICAgICAgZGVsZXRlOiB0aGlzLm9uRGVsZXRlLmJpbmQodGhpcylcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbiAgLyoqXG4gICAqIFRyaWdnZXJzIGEgZGF5IGNlbGwgdmlldyBpbnRlcm5hbCB1cGRhdGVcbiAgICogQHBhcmFtIGNvbXBvbmVudCB0aGUgY29tcG9uZW50IHJlZiBvZiB0aGUgZGF5IGNlbGwgdG8gdXBkYXRlXG4gICAqL1xuICBwcml2YXRlIHVwZGF0ZURheUNlbGwoZGF0YSA6IENhbGVuZGFyRGF5RGF0YTxFdmVudD4sIGNvbXBvbmVudCA6IENvbXBvbmVudFJlZjxBYnN0cmFjdERheUNlbGxDb21wb25lbnQ8RXZlbnQ+PikgOiB2b2lkXG4gIHtcbiAgICBsZXQgaW5zdGFuY2UgPSA8QWJzdHJhY3REYXlDZWxsQ29tcG9uZW50PEV2ZW50Pj5jb21wb25lbnQuaW5zdGFuY2U7XG4gICAgaW5zdGFuY2UudXBkYXRlRXZlbnRWaWV3cyh0aGlzLmV2ZW50Q29tcG9uZW50RmFjdG9yeSwgdGhpcy5pZEFjY2Vzc29yKTtcbiAgfVxuXG4gIC8qIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAqL1xuXG4gIC8qKlxuICAgKiBUcmlnZ2VycyBhIGRlbGV0ZSBldmVudCBlbWlzc2lvblxuICAgKiBAcGFyYW0gZXZlbnQgdGhlIGV2ZW50IHRvIGRlbGV0ZVxuICAgKi9cbiAgQE91dHB1dCgnZGVsZXRlJylcbiAgcHJpdmF0ZSBkZWxldGVFbWl0dGVyIDogRXZlbnRFbWl0dGVyPEV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXI8RXZlbnQ+KCk7XG4gIHByaXZhdGUgb25EZWxldGUoZXZlbnQ6IEV2ZW50KVxuICB7XG4gICAgdGhpcy5kZWxldGVFbWl0dGVyLmVtaXQoZXZlbnQpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRyaWdnZXJzIGFuIHVwZGF0ZSBldmVudCBlbWlzc2lvblxuICAgKiBAcGFyYW0gZXZlbnQgdGhlIGV2ZW50IHRvIHVwZGF0ZVxuICAgKi9cbiAgQE91dHB1dCgndXBkYXRlJylcbiAgcHJpdmF0ZSB1cGRhdGVFbWl0dGVyIDogRXZlbnRFbWl0dGVyPEV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXI8RXZlbnQ+KCk7XG4gIHByaXZhdGUgb25VcGRhdGUoZXZlbnQ6IEV2ZW50KVxuICB7XG4gICAgdGhpcy51cGRhdGVFbWl0dGVyLmVtaXQoZXZlbnQpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRyaWdnZXJzIGFuIGFkZCBldmVudCBlbWlzc2lvblxuICAgKiBAcGFyYW0gZXZlbnQgdGhlIGV2ZW50IHRvIGFkZFxuICAgKi9cbiAgQE91dHB1dCgnYWRkJylcbiAgcHJpdmF0ZSBhZGRFbWl0dGVyIDogRXZlbnRFbWl0dGVyPEV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXI8RXZlbnQ+KCk7XG4gIHByaXZhdGUgb25BZGQoZXZlbnQ6IEV2ZW50KVxuICB7XG4gICAgdGhpcy5hZGRFbWl0dGVyLmVtaXQoZXZlbnQpO1xuICB9XG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcblxuaW1wb3J0IHsgQ2FsZW5kYXJDb21wb25lbnQgfSBmcm9tICcuL3ZpZXdzL2NhbGVuZGFyL2NhbGVuZGFyLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIENhbGVuZGFyQ29tcG9uZW50LFxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgQ2FsZW5kYXJDb21wb25lbnRcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBBbmd1bGFyQWR2YW5jZWRDYWxlbmRhck1vZHVsZSB7IH1cbiIsImltcG9ydCB7IElucHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIER5bmFtaWNDb21wb25lbnQ8RGF0YVR5cGU+XG57XG4gIEBJbnB1dCgnZGF0YScpXG4gIHB1YmxpYyBkYXRhIDogRGF0YVR5cGU7XG4gIFxuICBwdWJsaWMgdXBkYXRlRGF0YShkYXRhIDogRGF0YVR5cGUpXG4gIHtcbiAgICB0aGlzLmRhdGEgPSBkYXRhO1xuICB9XG5cbiAgcHVibGljIGFic3RyYWN0IHN1YnNjcmliZShuYW1lIDogc3RyaW5nLCBjYWxsYmFjayA6IEZ1bmN0aW9uKSA6IHZvaWQ7XG59IiwiaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudCB9IGZyb20gXCIuLi9keW5hbWljL2R5bmFtaWMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBFdmVudEVtaXR0ZXIsIE91dHB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBEeW5hbWljQ3J1ZENvbXBvbmVudDxEYXRhVHlwZSwgQ3J1ZFR5cGU+IGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudDxEYXRhVHlwZT5cbntcbiAgcHVibGljIHN1YnNjcmliZShuYW1lIDogc3RyaW5nLCBjYWxsYmFjayA6IEZ1bmN0aW9uKSA6IHZvaWRcbiAge1xuICAgIHN3aXRjaChuYW1lKVxuICAgIHtcbiAgICAgIGNhc2UgJ3VwZGF0ZSc6XG4gICAgICAgIHRoaXMudXBkYXRlRW1pdHRlci5zdWJzY3JpYmUoY2FsbGJhY2spO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ2RlbGV0ZSc6XG4gICAgICAgIHRoaXMuZGVsZXRlRW1pdHRlci5zdWJzY3JpYmUoY2FsbGJhY2spO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ2FkZCc6XG4gICAgICAgIHRoaXMuYWRkRW1pdHRlci5zdWJzY3JpYmUoY2FsbGJhY2spO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHRocm93ICdjYWxsYmFjayB0eXBlIFxcJycgKyBuYW1lICsgJ1xcJyBpcyBub3QgYSBrbm93biBldmVudCBjYWxsYmFjayAhJztcbiAgICB9XG4gIH1cblxuICBAT3V0cHV0KCdkZWxldGUnKVxuICBwcml2YXRlIGRlbGV0ZUVtaXR0ZXIgOiBFdmVudEVtaXR0ZXI8Q3J1ZFR5cGU+ID0gbmV3IEV2ZW50RW1pdHRlcjxDcnVkVHlwZT4oKTtcbiAgcHJvdGVjdGVkIG9uRGVsZXRlKGV2ZW50OiBDcnVkVHlwZSlcbiAge1xuICAgIHRoaXMuZGVsZXRlRW1pdHRlci5lbWl0KGV2ZW50KTtcbiAgfVxuXG4gIEBPdXRwdXQoJ3VwZGF0ZScpXG4gIHByaXZhdGUgdXBkYXRlRW1pdHRlciA6IEV2ZW50RW1pdHRlcjxDcnVkVHlwZT4gPSBuZXcgRXZlbnRFbWl0dGVyPENydWRUeXBlPigpO1xuICBwcm90ZWN0ZWQgb25VcGRhdGUoZXZlbnQ6IENydWRUeXBlKVxuICB7XG4gICAgdGhpcy51cGRhdGVFbWl0dGVyLmVtaXQoZXZlbnQpO1xuICB9XG5cbiAgQE91dHB1dCgnYWRkJylcbiAgcHJpdmF0ZSBhZGRFbWl0dGVyIDogRXZlbnRFbWl0dGVyPENydWRUeXBlPiA9IG5ldyBFdmVudEVtaXR0ZXI8Q3J1ZFR5cGU+KCk7XG4gIHByb3RlY3RlZCBvbkFkZChldmVudDogQ3J1ZFR5cGUpXG4gIHtcbiAgICB0aGlzLmFkZEVtaXR0ZXIuZW1pdChldmVudCk7XG4gIH1cbn0iLCJpbXBvcnQgeyBEeW5hbWljQ3J1ZENvbXBvbmVudCB9IGZyb20gXCIuLi9keW5hbWljLWNydWQvZHluYW1pYy1jcnVkLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQ2FsZW5kYXJEYXlEYXRhIH0gZnJvbSBcIi4uLy4uL3R5cGVzL2NhbGVuZGFyLWRheVwiO1xuaW1wb3J0IHsgVmlld0NoaWxkLCBWaWV3Q29udGFpbmVyUmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXIgfSBmcm9tIFwiLi4vZHluYW1pYy9keW5hbWljLW1hbmFnZXIuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBBYnN0cmFjdEV2ZW50Q29tcG9uZW50IH0gZnJvbSBcIi4uL2V2ZW50L2Fic3RyYWN0LWV2ZW50LmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudEZhY3RvcnkgfSBmcm9tIFwiLi4vZHluYW1pYy9keW5hbWljLmZhY3RvcnlcIjtcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudDxEYXRhVHlwZT4gZXh0ZW5kcyBEeW5hbWljQ3J1ZENvbXBvbmVudDxDYWxlbmRhckRheURhdGE8RGF0YVR5cGU+LCBEYXRhVHlwZT5cbntcbiAgLyoqXG4gICAqIFZpZXcgY29udGFpbmVyIHN0b3JpbmcgdGhlIGR5bmFtaWMgY29tcG9uZW50c1xuICAgKi9cbiAgQFZpZXdDaGlsZCgnZXZlbnRWaWV3c0NvbnRhaW5lcicsIHsgcmVhZDogVmlld0NvbnRhaW5lclJlZiB9KVxuICBwcml2YXRlIGV2ZW50Vmlld0NvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWY7XG4gIHByaXZhdGUgZHluYW1pY0V2ZW50Vmlld01hbmFnZXIgOiBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyPERhdGFUeXBlLCBBYnN0cmFjdEV2ZW50Q29tcG9uZW50PERhdGFUeXBlPj5cbiAgICA9IG5ldyBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyPERhdGFUeXBlLCBBYnN0cmFjdEV2ZW50Q29tcG9uZW50PERhdGFUeXBlPj4oKTtcblxuICAvKipcbiAgICogTWV0aG9kIGNhbGxlZCB0byB1cGRhdGUgdGhlIGV2ZW50IHZpZXdzIGluIHRoZSBkYXkgY2VsbFxuICAgKi9cbiAgcHVibGljIHVwZGF0ZUV2ZW50Vmlld3MoZXZlbnRDb21wb25lbnRGYWN0b3J5IDogRHluYW1pY0NvbXBvbmVudEZhY3Rvcnk8RGF0YVR5cGUsIEFic3RyYWN0RXZlbnRDb21wb25lbnQ8RGF0YVR5cGU+PiwgaWRBY2Nlc3NvciA6IHN0cmluZylcbiAge1xuICAgIGlmIChldmVudENvbXBvbmVudEZhY3RvcnkgJiYgdGhpcy5ldmVudFZpZXdDb250YWluZXIpXG4gICAge1xuICAgICAgdGhpcy5keW5hbWljRXZlbnRWaWV3TWFuYWdlci51cGRhdGVDb250YWluZXIoXG4gICAgICAgIHRoaXMuZXZlbnRWaWV3Q29udGFpbmVyLFxuICAgICAgICBldmVudENvbXBvbmVudEZhY3RvcnksXG4gICAgICAgIHRoaXMuZGF0YS5ldmVudHMsXG4gICAgICAgIGlkQWNjZXNzb3IsXG4gICAgICAgIHtcbiAgICAgICAgICBkZWxldGU6IHRoaXMub25EZWxldGUuYmluZCh0aGlzKSxcbiAgICAgICAgICB1cGRhdGU6IHRoaXMub25VcGRhdGUuYmluZCh0aGlzKVxuICAgICAgICB9XG4gICAgICApO1xuICAgIH1cbiAgfVxufSIsImltcG9ydCB7IER5bmFtaWNDcnVkQ29tcG9uZW50IH0gZnJvbSBcIi4uL2R5bmFtaWMtY3J1ZC9keW5hbWljLWNydWQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBDYWxlbmRhcldlZWtEYXRhIH0gZnJvbSBcIi4uLy4uL3R5cGVzL2NhbGVuZGFyLXdlZWtcIjtcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEFic3RyYWN0V2Vla1N1bW1hcnlDb21wb25lbnQ8RGF0YVR5cGU+IGV4dGVuZHMgRHluYW1pY0NydWRDb21wb25lbnQ8Q2FsZW5kYXJXZWVrRGF0YTxEYXRhVHlwZT4sIERhdGFUeXBlPlxue1xufSIsImltcG9ydCB7IER5bmFtaWNDcnVkQ29tcG9uZW50IH0gZnJvbSBcIi4uL2R5bmFtaWMtY3J1ZC9keW5hbWljLWNydWQuY29tcG9uZW50XCI7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBBYnN0cmFjdEV2ZW50Q29tcG9uZW50PERhdGFUeXBlPiBleHRlbmRzIER5bmFtaWNDcnVkQ29tcG9uZW50PERhdGFUeXBlLCBEYXRhVHlwZT5cbntcbn0iXSwibmFtZXMiOlsibW9tZW50IiwidHNsaWJfMS5fX2V4dGVuZHMiLCJ0c2xpYl8xLl9fdmFsdWVzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLHFCQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7Ozs7OztBQUUvQixtQkFBMEIsQ0FBUSxFQUFFLENBQVE7SUFFMUMsT0FBTyxBQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUMsT0FBTyxFQUFFLEFBQUMsQ0FBQztDQUNqSTs7Ozs7QUFFRCx1QkFBOEIsSUFBVztJQUV2QyxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztDQUM1Qjs7Ozs7QUFFRCw0QkFBbUMsSUFBVztJQUU1QyxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7Q0FFL0M7Ozs7O0FBRUQsdUNBQThDLElBQVc7SUFFdkQsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztDQUN6RDs7Ozs7QUFFRCwyQkFBa0MsSUFBVztJQUUzQyxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7Q0FDN0M7Ozs7O0FBRUQsb0NBQTJDLElBQVc7SUFFcEQsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztDQUM5RDs7Ozs7O0FDL0JELHFCQUFJQSxRQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDOzs7OztBQU0vQjs7OztBQUFBOzZCQVVxQyxZQUFxQixFQUFrQixTQUFnQixFQUFrQixPQUFjO1FBQXZGLGlCQUFZLEdBQVosWUFBWSxDQUFTO1FBQWtCLGNBQVMsR0FBVCxTQUFTLENBQU87UUFBa0IsWUFBTyxHQUFQLE9BQU8sQ0FBTzs7Ozs7SUFFbkgsK0JBQUs7Ozs7UUFFVixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNuQixLQUFLLHFCQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUNwRDtZQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1NBQ3ZCOzs7Ozs7SUFHSSw2QkFBRzs7OztjQUFDLEtBQVc7UUFFcEIscUJBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDcEMscUJBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxFQUNoQztZQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzFDOzs7Ozs7SUFHSSxtQ0FBUzs7OztjQUFDLElBQVc7UUFFMUIsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs7Ozs7OztJQUd6QywwQ0FBZ0I7Ozs7O2NBQUMsU0FBZ0IsRUFBRSxPQUFjO1FBRXRELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDOzs7Ozs7O0lBT3BGLHNDQUFZOzs7Ozs7UUFFakIscUJBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNkLHFCQUFJLE9BQU8sR0FBR0EsUUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNyQyxxQkFBSSxJQUFJLEdBQUlBLFFBQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDakMsT0FBTSxPQUFPLEdBQUcsSUFBSSxFQUNwQjtZQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7WUFDNUIsT0FBTyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ2pDO1FBQ0QsT0FBTyxJQUFJLENBQUM7Ozs7O0lBR1Asc0NBQVk7Ozs7UUFFakIscUJBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUNmLHFCQUFJLE9BQU8sR0FBR0EsUUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNyQyxxQkFBSSxJQUFJLEdBQUlBLFFBQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDakMsT0FBTSxPQUFPLEdBQUcsSUFBSSxFQUNwQjtZQUNFLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDckYsT0FBTyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ2pDO1FBQ0QsT0FBTyxLQUFLLENBQUM7Ozs7Ozs7O0lBUVIsc0NBQVk7Ozs7OztjQUFDLElBQVc7UUFFN0IsT0FBT0EsUUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQ0EsUUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQzs7Ozs7OztJQU9wRCw4Q0FBb0I7Ozs7OztRQUV6QixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQzs7MEJBN0YvQztJQStGQyxDQUFBOzs7Ozs7Ozs7OztBQzNGRDs7Ozs7O0FBQUE7cUNBSXNDLHdCQUFtRCxFQUFFLGFBQW1DO1FBQXhGLDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBMkI7UUFFckYsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsdUJBQXVCLENBQUMsYUFBYSxDQUFDLENBQUM7Ozs7Ozs7Ozs7SUFVL0Usd0RBQXNCOzs7Ozs7OztjQUFDLFNBQTRCLEVBQUUsSUFBZSxFQUFFLFNBQStCLEVBQUUsS0FBZTtRQUUzSCxxQkFBSSxZQUFZLEdBQUcsU0FBUyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ2xFLHFCQUFJLFFBQVEscUJBQWtCLFlBQVksQ0FBQyxRQUFRLENBQUEsQ0FBQztRQUNwRCxRQUFRLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLElBQUksU0FBUyxFQUNiO1lBQ0UsS0FBSyxxQkFBSSxNQUFJLElBQUksU0FBUyxFQUMxQjtnQkFDRSxRQUFRLENBQUMsU0FBUyxDQUFDLE1BQUksRUFBRSxTQUFTLENBQUMsTUFBSSxDQUFDLENBQUMsQ0FBQzthQUMzQztTQUNGO1FBQ0QsT0FBTyxZQUFZLENBQUM7O2tDQWhDeEI7SUFrQ0M7Ozs7Ozs7Ozs7QUM5QkQ7Ozs7QUFBQTtJQUE4REMseUNBQW1FO21DQUU1Ryx3QkFBbUQsRUFBRSxhQUFzRDtlQUU1SCxrQkFBTSx3QkFBd0IsRUFBRSxhQUFhLENBQUM7O2dDQVJsRDtFQUk4RCx1QkFBdUIsRUFNcEY7Ozs7Ozs7Ozs7QUNMRDs7OztBQUFBO0lBQWdFQSwyQ0FBc0Y7cUNBRWpJLHdCQUFtRCxFQUFFLGFBQXdEO2VBRTlILGtCQUFNLHdCQUF3QixFQUFFLGFBQWEsQ0FBQzs7a0NBVGxEO0VBS2dFLHVCQUF1QixFQU10Rjs7Ozs7Ozs7OztBQ05EOzs7OztBQUFBOzs7Ozs0QkFNc0MsRUFBRTs7Ozt1Q0E4RXNDLEVBQUU7Ozs7O0lBNUV2RSw0Q0FBSzs7OztRQUVWLEtBQUsscUJBQUksRUFBRSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsRUFDM0M7WUFDRSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsRUFDcEM7Z0JBQ0UsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUMzQyxPQUFPLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUN6QztTQUNGO1FBQ0QsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7Ozs7Ozs7Ozs7Ozs7OztJQU9sQixzREFBZTs7Ozs7Ozs7Ozs7OztjQUNwQixTQUE0QixFQUM1QixPQUEwRCxFQUMxRCxTQUFzQixFQUN0QixjQUF1QixFQUN2Qix5QkFBZ0QsRUFDaEQsU0FBcUIsRUFDckIsWUFBd0IsRUFDeEIsWUFBd0I7UUFHeEIsSUFBSyxDQUFDLFNBQVMsRUFDZjtZQUNFLE1BQU0sNEVBQTRFLENBQUE7U0FDbkY7O1lBQ0QsS0FBaUIsSUFBQSxjQUFBQyxTQUFBLFNBQVMsQ0FBQSxvQ0FBQTtnQkFBckIsSUFBSSxJQUFJLHNCQUFBO2dCQUVYLHFCQUFJLGNBQWMsR0FBRyxLQUFLLENBQUM7O29CQUMzQixLQUFvQixJQUFBLEtBQUFBLFNBQUEsSUFBSSxDQUFDLFlBQVksQ0FBQSxnQkFBQTt3QkFBaEMsSUFBSSxPQUFPLFdBQUE7d0JBRWQsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssT0FBTyxDQUFDLGNBQWMsQ0FBQyxFQUNwRDs0QkFDRSxjQUFjLEdBQUcsSUFBSSxDQUFDOzRCQUN0QixNQUFNO3lCQUNQO3FCQUNGOzs7Ozs7Ozs7Z0JBQ0QsSUFBSSxjQUFjLEVBQ2xCO29CQUNFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSxZQUFZLENBQUMsQ0FBQztpQkFDNUU7cUJBRUQ7b0JBQ0UsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSx5QkFBeUIsRUFBRSxTQUFTLENBQUMsQ0FBQztpQkFDMUc7YUFDRjs7Ozs7Ozs7OztZQUVELEtBQW9CLElBQUEsS0FBQUEsU0FBQSxJQUFJLENBQUMsWUFBWSxDQUFBLGdCQUFBO2dCQUFoQyxJQUFJLE9BQU8sV0FBQTtnQkFFZCxxQkFBSSxZQUFZLEdBQUcsS0FBSyxDQUFDOztvQkFDekIsS0FBaUIsSUFBQSxjQUFBQSxTQUFBLFNBQVMsQ0FBQSxvQ0FBQTt3QkFBckIsSUFBSSxJQUFJLHNCQUFBO3dCQUVYLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFDcEQ7NEJBQ0UsWUFBWSxHQUFHLElBQUksQ0FBQzs0QkFDcEIsTUFBTTt5QkFDUDtxQkFDRjs7Ozs7Ozs7O2dCQUNELElBQUksQ0FBQyxZQUFZLEVBQ2pCO29CQUNFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxZQUFZLENBQUMsQ0FBQztpQkFDL0U7YUFDRjs7Ozs7Ozs7O1FBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUM7Ozs7Ozs7Ozs7SUFReEIsNkRBQXNCOzs7Ozs7O2NBQUMsU0FBNEIsRUFBRSxJQUFlLEVBQUUsY0FBdUIsRUFBRSxZQUF3QjtRQUU3SCxJQUFJLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLFFBQVEsRUFDNUM7WUFDRSxNQUFNLE1BQU0sR0FBRyxjQUFjLEdBQUcsaUVBQWlFLENBQUM7U0FDbkc7YUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUM1RDtZQUNFLE1BQU0sd0ZBQXdGLENBQUM7U0FDaEc7YUFFRDtZQUNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUM3RCxPQUFPLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUMxRCxJQUFJLFlBQVk7Z0JBQUUsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUMxRjs7Ozs7Ozs7Ozs7SUFHSywwREFBbUI7Ozs7Ozs7OztjQUN6QixTQUE0QixFQUM1QixPQUEwRCxFQUMxRCxJQUFlLEVBQ2YsY0FBdUIsRUFDdkIseUJBQStDLEVBQy9DLFNBQXFCO1FBR3JCLElBQUksT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssUUFBUSxFQUM1QztZQUNFLE1BQU0sTUFBTSxHQUFHLGNBQWMsR0FBRyw4REFBOEQsQ0FBQztTQUNoRzthQUNJLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUMzRDtZQUNFLE1BQU0sMkZBQTJGLENBQUM7U0FDbkc7YUFFRDtZQUNFLHFCQUFJLFlBQVksR0FBRyxPQUFPLENBQUMsc0JBQXNCLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSx5QkFBeUIsQ0FBQyxDQUFDO1lBQzlGLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsR0FBRyxZQUFZLENBQUM7WUFDbEUsSUFBSSxTQUFTO2dCQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDcEY7Ozs7Ozs7Ozs7SUFPSyw2REFBc0I7Ozs7Ozs7O2NBQUMsU0FBNEIsRUFBRSxJQUFlLEVBQUUsY0FBdUIsRUFBRSxZQUF3QjtRQUU3SCxJQUFJLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLFFBQVEsRUFDNUM7WUFDRSxNQUFNLE1BQU0sR0FBRyxjQUFjLEdBQUcsOERBQThELENBQUM7U0FDaEc7YUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUM1RDtZQUNFLE1BQU0scUZBQXFGLENBQUM7U0FDN0Y7YUFFRDtZQUNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzdFLElBQUksWUFBWTtnQkFBRSxZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzFGOzt1Q0F4Skw7SUEwSkMsQ0FBQTs7Ozs7Ozs7OztBQ25KRDs7OztBQUFBO0lBQW9FRCwrQ0FBMkY7eUNBRTFJLHdCQUFtRCxFQUFFLGFBQTREO2VBRWxJLGtCQUFNLHdCQUF3QixFQUFFLGFBQWEsQ0FBQzs7Ozs7Ozs7SUFHekMsNERBQXNCOzs7Ozs7Y0FBQyxTQUE0QixFQUFFLElBQWlDLEVBQUUsU0FBK0I7UUFFNUgscUJBQUksUUFBUSxHQUFHLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDakUsT0FBTyxDQUFDLFFBQVEsSUFBSSxDQUFDLElBQUksaUJBQU0sc0JBQXNCLFlBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsUUFBUSxDQUFDLEdBQUcsU0FBUyxDQUFDOzs7Ozs7O0lBR2xHLGlFQUEyQjs7Ozs7Y0FBQyxTQUE0QixFQUFFLElBQWlDO1FBRWpHLHFCQUFJLEtBQUssQ0FBQztRQUVWLEtBQUssS0FBSyxHQUFHLENBQUMsRUFBRSxBQUFVLEtBQUssR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUMzRDtZQUNFLHFCQUFJLGlCQUFpQixHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDOUUsSUFBSSxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsRUFDdkY7Z0JBQ0UsT0FBTyxLQUFLLEdBQUcsQ0FBQyxDQUFDO2FBQ2xCO1NBQ0Y7UUFDRCxPQUFPLENBQUMsQ0FBQyxDQUFDOztzQ0FoQ2Q7RUFPb0UsdUJBQXVCLEVBMkIxRjs7Ozs7O0FDcEJELHFCQUFJRCxRQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQy9CLHFCQUFJLElBQUksR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7QUFFbEMsQUFBTyxxQkFBTSxtQ0FBbUMsR0FBRyx5QkFBeUIsQ0FBQztBQUM3RSxBQUFPLHFCQUFNLGdDQUFnQyxHQUFNLHVCQUF1QixDQUFDO0FBQzNFLEFBQU8scUJBQU0sOEJBQThCLEdBQVEsNkJBQTZCLENBQUM7OytCQXFEekIsdUJBQXdELEVBQy9DLHFCQUFvRCxFQUN0RCwyQkFBZ0U7UUFGdkUsNEJBQXVCLEdBQXZCLHVCQUF1QixDQUFpQztRQUMvQywwQkFBcUIsR0FBckIscUJBQXFCLENBQStCO1FBQ3RELGdDQUEyQixHQUEzQiwyQkFBMkIsQ0FBcUM7Ozs7O3VCQTlCL0U7WUFDNUMsUUFBUTtZQUNSLFNBQVM7WUFDVCxXQUFXO1lBQ1gsVUFBVTtZQUNWLFFBQVE7WUFDUixVQUFVO1lBQ1YsUUFBUTtTQUNUOzs7O3NCQUsyQyxFQUFFOzs7Ozs0QkFNVSxNQUFNOzs7OzswQkFNVixJQUFJO3lDQWdCcEQsSUFBSSw0QkFBNEIsRUFBMEM7NkNBRTFFLElBQUksNEJBQTRCLEVBQThDO3VCQVF2RCxFQUFFOzs7Ozs2QkFpSmlCLElBQUksWUFBWSxFQUFTOzs7Ozs2QkFXekIsSUFBSSxZQUFZLEVBQVM7Ozs7OzBCQVc1QixJQUFJLFlBQVksRUFBUzs7Ozs7SUF0S3BFLHFDQUFTOzs7SUFBVDtRQUVFLHFCQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNyQyxJQUFJLE9BQU8sS0FBSyxJQUFJLENBQUMsT0FBTyxFQUM1QjtZQUNFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztTQUN4QjtLQUNGOzs7OztJQUVELHVDQUFXOzs7O0lBQVgsVUFBWSxPQUF1QjtRQUVqQyxJQUFJLE9BQU8saUJBQWMsT0FBTyxXQUFRLElBQUksT0FBTyxnQkFBYSxFQUNoRTtZQUNFLHFCQUFJLFNBQVMsR0FBRyxDQUFDLE9BQU8saUJBQWMsT0FBTyxjQUFXLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3RGLHFCQUFJLE9BQU8sR0FBRyxDQUFDLE9BQU8sZUFBWSxPQUFPLFlBQVMsWUFBWSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDOUUscUJBQUksWUFBWSxHQUFHLENBQUMsT0FBTyxvQkFBaUIsT0FBTyxpQkFBYyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUVsRyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksZUFBZSxDQUFDLFlBQVksRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDeEUsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1NBQ3ZCO0tBQ0Y7Ozs7SUFXTywwQ0FBYzs7OztRQUVwQixJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixFQUNqQztZQUNFLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1lBQ2YsTUFBTSxpRUFBaUUsQ0FBQTtTQUN4RTthQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQy9CO1lBQ0UsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7WUFDZixNQUFNLDRDQUE0QyxDQUFBO1NBQ25EO2FBRUQ7WUFDRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFFMUIsSUFBSSxJQUFJLENBQUMsMkJBQTJCLEVBQ3BDO2dCQUNFLElBQUksQ0FBQywwQkFBMEIsRUFBRSxDQUFDO2FBQ25DO1NBQ0Y7Ozs7OztJQU9LLDBDQUFjOzs7Ozs7UUFFcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7WUFDeEIsS0FBa0IsSUFBQSxLQUFBRSxTQUFBLElBQUksQ0FBQyxNQUFNLENBQUEsZ0JBQUE7Z0JBQXhCLElBQUksT0FBSyxXQUFBO2dCQUVaLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLE9BQUssQ0FBQyxDQUFDO2FBQzVCOzs7Ozs7Ozs7UUFDRCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLFFBQUM7WUFDdEQsRUFBRSxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQztZQUN0QyxJQUFJLEVBQUUsSUFBSTtZQUNWLGlCQUFpQixFQUFFLEtBQUksQ0FBQyxTQUFTO1lBQ2pDLGVBQWUsRUFBRSxLQUFJLENBQUMsT0FBTztZQUM3QixNQUFNLEVBQUUsS0FBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO1lBQ3ZDLFlBQVksRUFBRSxLQUFJLENBQUMsWUFBWTtTQUNoQyxJQUFDLENBQUMsQ0FBQzs7Ozs7O0lBR0UsOENBQWtCOzs7O1FBRXhCLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxlQUFlLENBQzVDLElBQUksQ0FBQyxnQkFBZ0IsRUFDckIsSUFBSSxDQUFDLHVCQUF1QixFQUM1QixJQUFJLENBQUMsSUFBSSxFQUNULElBQUksRUFDSjtZQUNFLEdBQUcsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUIsTUFBTSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUNoQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ2pDLEVBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUM3QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FDOUIsQ0FBQzs7Ozs7SUFHSSxzREFBMEI7Ozs7O1FBRWhDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJLElBQUksUUFBQztZQUN2RCxFQUFFLEVBQUUsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDakMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTO1lBQ3pCLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTztZQUNyQixpQkFBaUIsRUFBRSxLQUFJLENBQUMsU0FBUztZQUNqQyxlQUFlLEVBQUUsS0FBSSxDQUFDLE9BQU87WUFDN0IsTUFBTSxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ3RFLFlBQVksRUFBRSxLQUFJLENBQUMsWUFBWTtTQUNoQyxJQUFDLENBQUMsQ0FBQztRQUVKLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLFVBQVUsRUFDNUU7WUFDRSxJQUFJLENBQUMsNkJBQTZCLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDM0MsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ25DLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztTQUNoQztRQUVELElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxlQUFlLENBQ2hELElBQUksQ0FBQyxnQkFBZ0IsRUFDckIsSUFBSSxDQUFDLDJCQUEyQixFQUNoQyxJQUFJLENBQUMsS0FBSyxFQUNWLElBQUksRUFDSjtZQUNFLEdBQUcsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUIsTUFBTSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUNoQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ2pDLENBQ0YsQ0FBQzs7Ozs7Ozs7SUFPSSx5Q0FBYTs7Ozs7O2NBQUMsSUFBNkIsRUFBRSxTQUF5RDtRQUU1RyxxQkFBSSxRQUFRLHFCQUFvQyxTQUFTLENBQUMsUUFBUSxDQUFBLENBQUM7UUFDbkUsUUFBUSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Ozs7OztJQVdqRSxvQ0FBUTs7OztjQUFDLEtBQVk7UUFFM0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Ozs7OztJQVN6QixvQ0FBUTs7OztjQUFDLEtBQVk7UUFFM0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Ozs7OztJQVN6QixpQ0FBSzs7OztjQUFDLEtBQVk7UUFFeEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7OztnQkFwUC9CLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxRQUFRLEVBQUUsd0tBRW9EO29CQUM5RCxNQUFNLEVBQUUsQ0FBQyxnSUFBZ0ksQ0FBQztpQkFDM0k7Ozs7Z0JBakJRLHVCQUF1Qix1QkE4RDNCLE1BQU0sU0FBQyxtQ0FBbUM7Z0JBL0R0QyxxQkFBcUIsdUJBZ0V6QixRQUFRLFlBQUksTUFBTSxTQUFDLGdDQUFnQztnQkE3RC9DLDJCQUEyQix1QkE4RC9CLFFBQVEsWUFBSSxNQUFNLFNBQUMsOEJBQThCOzs7NEJBekNuRCxLQUFLLFNBQUMsWUFBWTswQkFLbEIsS0FBSyxTQUFDLFVBQVU7MEJBTWhCLEtBQUssU0FBQyxTQUFTO3lCQWFmLEtBQUssU0FBQyxRQUFROytCQU1kLEtBQUssU0FBQyxlQUFlOzZCQU1yQixLQUFLLFNBQUMsYUFBYTttQ0FhbkIsU0FBUyxTQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRTtnQ0E2Si9DLE1BQU0sU0FBQyxRQUFRO2dDQVdmLE1BQU0sU0FBQyxRQUFROzZCQVdmLE1BQU0sU0FBQyxLQUFLOzs0QkFyUWY7Ozs7Ozs7QUNBQTs7OztnQkFLQyxRQUFRLFNBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLFlBQVk7cUJBQ2I7b0JBQ0QsWUFBWSxFQUFFO3dCQUNaLGlCQUFpQjtxQkFDbEI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLGlCQUFpQjtxQkFDbEI7aUJBQ0Y7O3dDQWZEOzs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7O0lBT1MscUNBQVU7Ozs7Y0FBQyxJQUFlO1FBRS9CLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDOzs7dUJBTGxCLEtBQUssU0FBQyxNQUFNOzsyQkFKZjs7Ozs7Ozs7Ozs7O0lDR3VFRCx3Q0FBMEI7Ozs4QkFxQjlDLElBQUksWUFBWSxFQUFZOzhCQU81QixJQUFJLFlBQVksRUFBWTsyQkFPL0IsSUFBSSxZQUFZLEVBQVk7Ozs7Ozs7O0lBakNuRSx3Q0FBUzs7Ozs7Y0FBQyxJQUFhLEVBQUUsUUFBbUI7UUFFakQsUUFBTyxJQUFJO1lBRVQsS0FBSyxRQUFRO2dCQUNYLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN2QyxNQUFNO1lBQ1IsS0FBSyxRQUFRO2dCQUNYLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN2QyxNQUFNO1lBQ1IsS0FBSyxLQUFLO2dCQUNSLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNwQyxNQUFNO1lBQ1I7Z0JBQ0UsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLEdBQUcsb0NBQW9DLENBQUM7U0FDMUU7Ozs7OztJQUtPLHVDQUFROzs7O0lBQWxCLFVBQW1CLEtBQWU7UUFFaEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDaEM7Ozs7O0lBSVMsdUNBQVE7Ozs7SUFBbEIsVUFBbUIsS0FBZTtRQUVoQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUNoQzs7Ozs7SUFJUyxvQ0FBSzs7OztJQUFmLFVBQWdCLEtBQWU7UUFFN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDN0I7O2dDQW5CQSxNQUFNLFNBQUMsUUFBUTtnQ0FPZixNQUFNLFNBQUMsUUFBUTs2QkFPZixNQUFNLFNBQUMsS0FBSzs7K0JBckNmO0VBR3VFLGdCQUFnQjs7Ozs7Ozs7Ozs7SUNJdEJBLDRDQUF5RDs7O3dDQVFwSCxJQUFJLDRCQUE0QixFQUE4Qzs7Ozs7Ozs7O0lBSzNFLG1EQUFnQjs7Ozs7O2NBQUMscUJBQTJGLEVBQUUsVUFBbUI7UUFFdEksSUFBSSxxQkFBcUIsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQ3BEO1lBQ0UsSUFBSSxDQUFDLHVCQUF1QixDQUFDLGVBQWUsQ0FDMUMsSUFBSSxDQUFDLGtCQUFrQixFQUN2QixxQkFBcUIsRUFDckIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQ2hCLFVBQVUsRUFDVjtnQkFDRSxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNoQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2FBQ2pDLENBQ0YsQ0FBQztTQUNIOzs7cUNBdEJGLFNBQVMsU0FBQyxxQkFBcUIsRUFBRSxFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRTs7bUNBWjlEO0VBT2lFLG9CQUFvQjs7Ozs7Ozs7OztBQ0pyRjs7OztBQUFBO0lBQXFFQSxnREFBMEQ7Ozs7dUNBSC9IO0VBR3FFLG9CQUFvQixFQUV4Rjs7Ozs7Ozs7OztBQ0hEOzs7O0FBQUE7SUFBK0RBLDBDQUF3Qzs7OztpQ0FGdkc7RUFFK0Qsb0JBQW9CLEVBRWxGOzs7Ozs7Ozs7Ozs7OzsifQ==