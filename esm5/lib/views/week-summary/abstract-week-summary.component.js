/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { DynamicCrudComponent } from "../dynamic-crud/dynamic-crud.component";
/**
 * @abstract
 * @template DataType
 */
var /**
 * @abstract
 * @template DataType
 */
AbstractWeekSummaryComponent = /** @class */ (function (_super) {
    tslib_1.__extends(AbstractWeekSummaryComponent, _super);
    function AbstractWeekSummaryComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AbstractWeekSummaryComponent;
}(DynamicCrudComponent));
/**
 * @abstract
 * @template DataType
 */
export { AbstractWeekSummaryComponent };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWJzdHJhY3Qtd2Vlay1zdW1tYXJ5LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdmlld3Mvd2Vlay1zdW1tYXJ5L2Fic3RyYWN0LXdlZWstc3VtbWFyeS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQzs7Ozs7QUFHOUU7Ozs7QUFBQTtJQUFxRSx3REFBMEQ7Ozs7dUNBSC9IO0VBR3FFLG9CQUFvQixFQUV4RixDQUFBOzs7OztBQUZELHdDQUVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRHluYW1pY0NydWRDb21wb25lbnQgfSBmcm9tIFwiLi4vZHluYW1pYy1jcnVkL2R5bmFtaWMtY3J1ZC5jb21wb25lbnRcIjtcbmltcG9ydCB7IENhbGVuZGFyV2Vla0RhdGEgfSBmcm9tIFwiLi4vLi4vdHlwZXMvY2FsZW5kYXItd2Vla1wiO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudDxEYXRhVHlwZT4gZXh0ZW5kcyBEeW5hbWljQ3J1ZENvbXBvbmVudDxDYWxlbmRhcldlZWtEYXRhPERhdGFUeXBlPiwgRGF0YVR5cGU+XG57XG59Il19