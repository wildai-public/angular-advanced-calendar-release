/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { DynamicComponentFactory } from "../dynamic/dynamic.factory";
import { isSameDay } from "../../utils/date";
/**
 * @abstract
 * @template DataType
 */
var /**
 * @abstract
 * @template DataType
 */
WeekSummaryComponentFactory = /** @class */ (function (_super) {
    tslib_1.__extends(WeekSummaryComponentFactory, _super);
    function WeekSummaryComponentFactory(componentFactoryResolver, componentType) {
        return _super.call(this, componentFactoryResolver, componentType) || this;
    }
    /**
     * @param {?} container
     * @param {?} data
     * @param {?=} callbacks
     * @return {?}
     */
    WeekSummaryComponentFactory.prototype.insertDynamicComponent = /**
     * @param {?} container
     * @param {?} data
     * @param {?=} callbacks
     * @return {?}
     */
    function (container, data, callbacks) {
        var /** @type {?} */ position = this.findWeekPositionInContainer(container, data);
        return (position >= 0) ? _super.prototype.insertDynamicComponent.call(this, container, data, callbacks, position) : undefined;
    };
    /**
     * @param {?} container
     * @param {?} data
     * @return {?}
     */
    WeekSummaryComponentFactory.prototype.findWeekPositionInContainer = /**
     * @param {?} container
     * @param {?} data
     * @return {?}
     */
    function (container, data) {
        var /** @type {?} */ index;
        var /** @type {?} */ found = false;
        for (index = 0; !found && index < container.length; index++) {
            var /** @type {?} */ componentInstance = container.get(index)['_view']['nodes'][1]['instance'];
            if (componentInstance.data.date && isSameDay(componentInstance.data.date, data.endDate)) {
                return index + 1;
            }
        }
        return -1;
    };
    return WeekSummaryComponentFactory;
}(DynamicComponentFactory));
/**
 * @abstract
 * @template DataType
 */
export { WeekSummaryComponentFactory };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2Vlay1zdW1tYXJ5LmZhdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdmlld3Mvd2Vlay1zdW1tYXJ5L3dlZWstc3VtbWFyeS5mYXRvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUtyRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7Ozs7O0FBRTdDOzs7O0FBQUE7SUFBb0UsdURBQTJGO3lDQUUxSSx3QkFBbUQsRUFBRSxhQUE0RDtlQUVsSSxrQkFBTSx3QkFBd0IsRUFBRSxhQUFhLENBQUM7Ozs7Ozs7O0lBR3pDLDREQUFzQjs7Ozs7O2NBQUMsU0FBNEIsRUFBRSxJQUFpQyxFQUFFLFNBQStCO1FBRTVILHFCQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsMkJBQTJCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2pFLE1BQU0sQ0FBQyxDQUFDLFFBQVEsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQU0sc0JBQXNCLFlBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQzs7Ozs7OztJQUdsRyxpRUFBMkI7Ozs7O2NBQUMsU0FBNEIsRUFBRSxJQUFpQztRQUVqRyxxQkFBSSxLQUFLLENBQUM7UUFDVixxQkFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUUsQ0FBQyxLQUFLLElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQzNELENBQUM7WUFDQyxxQkFBSSxpQkFBaUIsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzlFLEVBQUUsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQ3hGLENBQUM7Z0JBQ0MsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7YUFDbEI7U0FDRjtRQUNELE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzs7c0NBaENkO0VBT29FLHVCQUF1QixFQTJCMUYsQ0FBQTs7Ozs7QUEzQkQsdUNBMkJDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudEZhY3RvcnkgfSBmcm9tIFwiLi4vZHluYW1pYy9keW5hbWljLmZhY3RvcnlcIjtcbmltcG9ydCB7IENhbGVuZGFyV2Vla0RhdGEgfSBmcm9tIFwiLi4vLi4vdHlwZXMvY2FsZW5kYXItd2Vla1wiO1xuaW1wb3J0IHsgQWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudCB9IGZyb20gXCIuL2Fic3RyYWN0LXdlZWstc3VtbWFyeS5jb21wb25lbnRcIjtcbmltcG9ydCB7IENvbXBvbmVudFJlZiwgVmlld0NvbnRhaW5lclJlZiwgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBUeXBlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFN0cmluZ01hcCB9IGZyb20gXCIuLi8uLi90eXBlcy9tYXBzXCI7XG5pbXBvcnQgeyBpc1NhbWVEYXkgfSBmcm9tIFwiLi4vLi4vdXRpbHMvZGF0ZVwiO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgV2Vla1N1bW1hcnlDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlPiBleHRlbmRzIER5bmFtaWNDb21wb25lbnRGYWN0b3J5PENhbGVuZGFyV2Vla0RhdGE8RGF0YVR5cGU+LCBBYnN0cmFjdFdlZWtTdW1tYXJ5Q29tcG9uZW50PERhdGFUeXBlPj5cbntcbiAgcHVibGljIGNvbnN0cnVjdG9yKGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlciA6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29tcG9uZW50VHlwZSA6IFR5cGU8QWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudDxEYXRhVHlwZT4+KVxuICB7XG4gICAgc3VwZXIoY29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb21wb25lbnRUeXBlKTtcbiAgfVxuXG4gIHB1YmxpYyBpbnNlcnREeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWYsIGRhdGEgOiBDYWxlbmRhcldlZWtEYXRhPERhdGFUeXBlPiwgY2FsbGJhY2tzPzogU3RyaW5nTWFwPEZ1bmN0aW9uPikgOiBDb21wb25lbnRSZWY8QWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudDxEYXRhVHlwZT4+XG4gIHtcbiAgICBsZXQgcG9zaXRpb24gPSB0aGlzLmZpbmRXZWVrUG9zaXRpb25JbkNvbnRhaW5lcihjb250YWluZXIsIGRhdGEpO1xuICAgIHJldHVybiAocG9zaXRpb24gPj0gMCkgPyBzdXBlci5pbnNlcnREeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciwgZGF0YSwgY2FsbGJhY2tzLCBwb3NpdGlvbikgOiB1bmRlZmluZWQ7XG4gIH1cblxuICBwcml2YXRlIGZpbmRXZWVrUG9zaXRpb25JbkNvbnRhaW5lcihjb250YWluZXIgOiBWaWV3Q29udGFpbmVyUmVmLCBkYXRhIDogQ2FsZW5kYXJXZWVrRGF0YTxEYXRhVHlwZT4pIDogbnVtYmVyXG4gIHtcbiAgICBsZXQgaW5kZXg7XG4gICAgbGV0IGZvdW5kID0gZmFsc2U7XG4gICAgZm9yIChpbmRleCA9IDA7ICFmb3VuZCAmJiBpbmRleCA8IGNvbnRhaW5lci5sZW5ndGg7IGluZGV4KyspXG4gICAge1xuICAgICAgbGV0IGNvbXBvbmVudEluc3RhbmNlID0gY29udGFpbmVyLmdldChpbmRleClbJ192aWV3J11bJ25vZGVzJ11bMV1bJ2luc3RhbmNlJ107XG4gICAgICBpZiAoY29tcG9uZW50SW5zdGFuY2UuZGF0YS5kYXRlICYmIGlzU2FtZURheShjb21wb25lbnRJbnN0YW5jZS5kYXRhLmRhdGUsIGRhdGEuZW5kRGF0ZSkpXG4gICAgICB7XG4gICAgICAgIHJldHVybiBpbmRleCArIDE7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiAtMTtcbiAgfVxufSJdfQ==