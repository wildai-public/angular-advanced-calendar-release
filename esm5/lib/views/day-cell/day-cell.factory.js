/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { DynamicComponentFactory } from '../dynamic/dynamic.factory';
/**
 * @abstract
 * @template DataType
 */
var /**
 * @abstract
 * @template DataType
 */
DayCellComponentFactory = /** @class */ (function (_super) {
    tslib_1.__extends(DayCellComponentFactory, _super);
    function DayCellComponentFactory(componentFactoryResolver, componentType) {
        return _super.call(this, componentFactoryResolver, componentType) || this;
    }
    return DayCellComponentFactory;
}(DynamicComponentFactory));
/**
 * @abstract
 * @template DataType
 */
export { DayCellComponentFactory };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF5LWNlbGwuZmFjdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdmlld3MvZGF5LWNlbGwvZGF5LWNlbGwuZmFjdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDRCQUE0QixDQUFDOzs7OztBQUtyRTs7OztBQUFBO0lBQWdFLG1EQUFzRjtxQ0FFakksd0JBQW1ELEVBQUUsYUFBd0Q7ZUFFOUgsa0JBQU0sd0JBQXdCLEVBQUUsYUFBYSxDQUFDOztrQ0FUbEQ7RUFLZ0UsdUJBQXVCLEVBTXRGLENBQUE7Ozs7O0FBTkQsbUNBTUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50RmFjdG9yeSB9IGZyb20gJy4uL2R5bmFtaWMvZHluYW1pYy5mYWN0b3J5JztcbmltcG9ydCB7IENhbGVuZGFyRGF5RGF0YSB9IGZyb20gJy4uLy4uL3R5cGVzL2NhbGVuZGFyLWRheSc7XG5pbXBvcnQgeyBBYnN0cmFjdERheUNlbGxDb21wb25lbnQgfSBmcm9tICcuL2Fic3RyYWN0LWRheS1jZWxsLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIFR5cGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIERheUNlbGxDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlPiBleHRlbmRzIER5bmFtaWNDb21wb25lbnRGYWN0b3J5PENhbGVuZGFyRGF5RGF0YTxEYXRhVHlwZT4sIEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudDxEYXRhVHlwZT4+XG57XG4gIHB1YmxpYyBjb25zdHJ1Y3Rvcihjb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIgOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIGNvbXBvbmVudFR5cGUgOiBUeXBlPEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudDxEYXRhVHlwZT4+KVxuICB7XG4gICAgc3VwZXIoY29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb21wb25lbnRUeXBlKTtcbiAgfVxufSJdfQ==