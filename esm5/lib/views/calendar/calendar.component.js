/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild, ViewContainerRef, Inject, Optional } from '@angular/core';
import { getWeekNumber } from '../../utils/date';
import { EventDispatcher } from '../../utils/dispatcher';
import { EventComponentFactory } from '../event/event.factory';
import { DayCellComponentFactory } from '../day-cell/day-cell.factory';
import { DynamicComponentArrayManager } from '../dynamic/dynamic-manager.component';
import { WeekSummaryComponentFactory } from '../week-summary/week-summary.fatory';
var /** @type {?} */ moment = require('moment');
var /** @type {?} */ hash = require('object-hash');
export var /** @type {?} */ DAY_CELL_COMPONENT_FACTORY_PROVIDER = 'DayCellComponentFactory';
export var /** @type {?} */ EVENT_COMPONENT_FACTORY_PROVIDER = 'EventComponentFactory';
export var /** @type {?} */ WEEK_SUMMARY_COMPONENT_FACTORY = 'WeekSummaryComponentFactory';
var CalendarComponent = /** @class */ (function () {
    function CalendarComponent(dayCellComponentFactory, eventComponentFactory, weekSummaryComponentFactory) {
        this.dayCellComponentFactory = dayCellComponentFactory;
        this.eventComponentFactory = eventComponentFactory;
        this.weekSummaryComponentFactory = weekSummaryComponentFactory;
        /**
         * Week days label to display in the header
         * (default is english).
         */
        this.headers = [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday',
        ];
        /**
         * A list of events to display in the callendar.
         */
        this.events = [];
        /**
         * The name of the date attribute to use
         * when displaying the events (default is 'date').
         */
        this.dateAccessor = 'date';
        /**
         * The name of the id attribute to use
         * when displaying the events (default is 'id').
         */
        this.idAccessor = 'id';
        this.dynamicDayCellViewManager = new DynamicComponentArrayManager();
        this.dynamicWeekSummaryViewManager = new DynamicComponentArrayManager();
        this.oldHash = '';
        /**
         * Triggers a delete event emission
         * @param event the event to delete
         */
        this.deleteEmitter = new EventEmitter();
        /**
         * Triggers an update event emission
         * @param event the event to update
         */
        this.updateEmitter = new EventEmitter();
        /**
         * Triggers an add event emission
         * @param event the event to add
         */
        this.addEmitter = new EventEmitter();
    }
    /**
     * @return {?}
     */
    CalendarComponent.prototype.ngDoCheck = /**
     * @return {?}
     */
    function () {
        var /** @type {?} */ newHash = hash.sha1(this.events);
        if (newHash !== this.oldHash) {
            this.updateCalendar();
            this.oldHash = newHash;
        }
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    CalendarComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes["startDate"] || changes["endDate"] || changes["dateAccessor"]) {
            var /** @type {?} */ startDate = (changes["startDate"]) ? changes["startDate"].currentValue : this.startDate;
            var /** @type {?} */ endDate = (changes["endDate"]) ? changes["endDate"].currentValue : this.endDate;
            var /** @type {?} */ dateAccessor = (changes["dateAccessor"]) ? changes["dateAccessor"].currentValue : this.dateAccessor;
            this.dispatcher = new EventDispatcher(dateAccessor, startDate, endDate);
            this.updateCalendar();
        }
    };
    /**
     * @return {?}
     */
    CalendarComponent.prototype.updateCalendar = /**
     * @return {?}
     */
    function () {
        if (!this.dayCellComponentFactory) {
            this.days = [];
            throw 'day cell component factory (day-component-factory) was not set!';
        }
        else if (!this.dayGridContainer) {
            this.days = [];
            throw 'day grid (dayGrid) not found in template !';
        }
        else {
            this.dispatchEvents();
            this.updateDayCellViews();
            if (this.weekSummaryComponentFactory) {
                this.updateWeekSummaryCellViews();
            }
        }
    };
    /**
     * Sorts events by date
     * @return {?}
     */
    CalendarComponent.prototype.dispatchEvents = /**
     * Sorts events by date
     * @return {?}
     */
    function () {
        var _this = this;
        this.dispatcher.clear();
        try {
            for (var _a = tslib_1.__values(this.events), _b = _a.next(); !_b.done; _b = _a.next()) {
                var event_1 = _b.value;
                this.dispatcher.add(event_1);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_1) throw e_1.error; }
        }
        this.days = this.dispatcher.getDateArray().map(function (date) { return ({
            id: _this.dispatcher.getDayNumber(date),
            date: date,
            calendarStartDate: _this.startDate,
            calendarEndDate: _this.endDate,
            events: _this.dispatcher.getEvents(date),
            dateAccessor: _this.dateAccessor,
        }); });
        var e_1, _c;
    };
    /**
     * @return {?}
     */
    CalendarComponent.prototype.updateDayCellViews = /**
     * @return {?}
     */
    function () {
        this.dynamicDayCellViewManager.updateContainer(this.dayGridContainer, this.dayCellComponentFactory, this.days, 'id', {
            add: this.onAdd.bind(this),
            update: this.onUpdate.bind(this),
            delete: this.onDelete.bind(this)
        }, this.updateDayCell.bind(this), this.updateDayCell.bind(this), this.updateDayCell.bind(this));
    };
    /**
     * @return {?}
     */
    CalendarComponent.prototype.updateWeekSummaryCellViews = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.weeks = this.dispatcher.getWeekArray().map(function (week) { return ({
            id: getWeekNumber(week.startDate),
            startDate: week.startDate,
            endDate: week.endDate,
            calendarStartDate: _this.startDate,
            calendarEndDate: _this.endDate,
            events: _this.dispatcher.getEventsBetween(week.startDate, week.endDate),
            dateAccessor: _this.dateAccessor,
        }); });
        if (this.startDate !== this.oldStartDate || this.endDate !== this.oldEndDate) {
            this.dynamicWeekSummaryViewManager.clear();
            this.oldStartDate = this.startDate;
            this.oldEndDate = this.endDate;
        }
        this.dynamicWeekSummaryViewManager.updateContainer(this.dayGridContainer, this.weekSummaryComponentFactory, this.weeks, 'id', {
            add: this.onAdd.bind(this),
            update: this.onUpdate.bind(this),
            delete: this.onDelete.bind(this)
        });
    };
    /**
     * Triggers a day cell view internal update
     * @param {?} data
     * @param {?} component the component ref of the day cell to update
     * @return {?}
     */
    CalendarComponent.prototype.updateDayCell = /**
     * Triggers a day cell view internal update
     * @param {?} data
     * @param {?} component the component ref of the day cell to update
     * @return {?}
     */
    function (data, component) {
        var /** @type {?} */ instance = /** @type {?} */ (component.instance);
        instance.updateEventViews(this.eventComponentFactory, this.idAccessor);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CalendarComponent.prototype.onDelete = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.deleteEmitter.emit(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CalendarComponent.prototype.onUpdate = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.updateEmitter.emit(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CalendarComponent.prototype.onAdd = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.addEmitter.emit(event);
    };
    CalendarComponent.decorators = [
        { type: Component, args: [{
                    selector: 'angular-advanced-calendar',
                    template: "<h5 *ngFor='let name of headers' class='header'> {{name}} </h5>\n<ng-template #dayGrid></ng-template>\n<h5 *ngFor='let name of headers' class='footer'> {{name}} </h5>",
                    styles: ["@import url(https://fonts.googleapis.com/css?family=Raleway);.footer,.header{text-align:center;font-family:Raleway,sans-serif}"]
                },] },
    ];
    /** @nocollapse */
    CalendarComponent.ctorParameters = function () { return [
        { type: DayCellComponentFactory, decorators: [{ type: Inject, args: [DAY_CELL_COMPONENT_FACTORY_PROVIDER,] }] },
        { type: EventComponentFactory, decorators: [{ type: Optional }, { type: Inject, args: [EVENT_COMPONENT_FACTORY_PROVIDER,] }] },
        { type: WeekSummaryComponentFactory, decorators: [{ type: Optional }, { type: Inject, args: [WEEK_SUMMARY_COMPONENT_FACTORY,] }] }
    ]; };
    CalendarComponent.propDecorators = {
        startDate: [{ type: Input, args: ['start-date',] }],
        endDate: [{ type: Input, args: ['end-date',] }],
        headers: [{ type: Input, args: ['headers',] }],
        events: [{ type: Input, args: ['events',] }],
        dateAccessor: [{ type: Input, args: ['date-accessor',] }],
        idAccessor: [{ type: Input, args: ['id-accessor',] }],
        dayGridContainer: [{ type: ViewChild, args: ['dayGrid', { read: ViewContainerRef },] }],
        deleteEmitter: [{ type: Output, args: ['delete',] }],
        updateEmitter: [{ type: Output, args: ['update',] }],
        addEmitter: [{ type: Output, args: ['add',] }]
    };
    return CalendarComponent;
}());
export { CalendarComponent };
function CalendarComponent_tsickle_Closure_declarations() {
    /**
     * First date of the period to display in the calendar
     * @type {?}
     */
    CalendarComponent.prototype.startDate;
    /**
     * Last date of the period to display in the calendar
     * @type {?}
     */
    CalendarComponent.prototype.endDate;
    /**
     * Week days label to display in the header
     * (default is english).
     * @type {?}
     */
    CalendarComponent.prototype.headers;
    /**
     * A list of events to display in the callendar.
     * @type {?}
     */
    CalendarComponent.prototype.events;
    /**
     * The name of the date attribute to use
     * when displaying the events (default is 'date').
     * @type {?}
     */
    CalendarComponent.prototype.dateAccessor;
    /**
     * The name of the id attribute to use
     * when displaying the events (default is 'id').
     * @type {?}
     */
    CalendarComponent.prototype.idAccessor;
    /**
     * View container storing the dynamic components
     * @type {?}
     */
    CalendarComponent.prototype.dayGridContainer;
    /** @type {?} */
    CalendarComponent.prototype.dynamicDayCellViewManager;
    /** @type {?} */
    CalendarComponent.prototype.dynamicWeekSummaryViewManager;
    /** @type {?} */
    CalendarComponent.prototype.dispatcher;
    /** @type {?} */
    CalendarComponent.prototype.days;
    /** @type {?} */
    CalendarComponent.prototype.weeks;
    /** @type {?} */
    CalendarComponent.prototype.oldHash;
    /**
     * Old start and end date of the calendar.
     * Used to keep track of the changes in period
     * in order to update the view.
     * @type {?}
     */
    CalendarComponent.prototype.oldStartDate;
    /** @type {?} */
    CalendarComponent.prototype.oldEndDate;
    /**
     * Triggers a delete event emission
     * \@param event the event to delete
     * @type {?}
     */
    CalendarComponent.prototype.deleteEmitter;
    /**
     * Triggers an update event emission
     * \@param event the event to update
     * @type {?}
     */
    CalendarComponent.prototype.updateEmitter;
    /**
     * Triggers an add event emission
     * \@param event the event to add
     * @type {?}
     */
    CalendarComponent.prototype.addEmitter;
    /** @type {?} */
    CalendarComponent.prototype.dayCellComponentFactory;
    /** @type {?} */
    CalendarComponent.prototype.eventComponentFactory;
    /** @type {?} */
    CalendarComponent.prototype.weekSummaryComponentFactory;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FsZW5kYXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci8iLCJzb3VyY2VzIjpbImxpYi92aWV3cy9jYWxlbmRhci9jYWxlbmRhci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUErQixTQUFTLEVBQUUsZ0JBQWdCLEVBQWUsTUFBTSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVoSyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDakQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBTXpELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQy9ELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ3ZFLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3BGLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBRWxGLHFCQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDL0IscUJBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQztBQUVsQyxNQUFNLENBQUMscUJBQU0sbUNBQW1DLEdBQUcseUJBQXlCLENBQUM7QUFDN0UsTUFBTSxDQUFDLHFCQUFNLGdDQUFnQyxHQUFNLHVCQUF1QixDQUFDO0FBQzNFLE1BQU0sQ0FBQyxxQkFBTSw4QkFBOEIsR0FBUSw2QkFBNkIsQ0FBQzs7K0JBcUR6Qix1QkFBd0QsRUFDL0MscUJBQW9ELEVBQ3RELDJCQUFnRTtRQUZ2RSw0QkFBdUIsR0FBdkIsdUJBQXVCLENBQWlDO1FBQy9DLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBK0I7UUFDdEQsZ0NBQTJCLEdBQTNCLDJCQUEyQixDQUFxQzs7Ozs7dUJBOUIvRTtZQUM1QyxRQUFRO1lBQ1IsU0FBUztZQUNULFdBQVc7WUFDWCxVQUFVO1lBQ1YsUUFBUTtZQUNSLFVBQVU7WUFDVixRQUFRO1NBQ1Q7Ozs7c0JBSzJDLEVBQUU7Ozs7OzRCQU1VLE1BQU07Ozs7OzBCQU1WLElBQUk7eUNBZ0JwRCxJQUFJLDRCQUE0QixFQUEwQzs2Q0FFMUUsSUFBSSw0QkFBNEIsRUFBOEM7dUJBUXZELEVBQUU7Ozs7OzZCQWlKaUIsSUFBSSxZQUFZLEVBQVM7Ozs7OzZCQVd6QixJQUFJLFlBQVksRUFBUzs7Ozs7MEJBVzVCLElBQUksWUFBWSxFQUFTOzs7OztJQXRLcEUscUNBQVM7OztJQUFUO1FBRUUscUJBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQzdCLENBQUM7WUFDQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7U0FDeEI7S0FDRjs7Ozs7SUFFRCx1Q0FBVzs7OztJQUFYLFVBQVksT0FBdUI7UUFFakMsRUFBRSxDQUFDLENBQUMsT0FBTyxpQkFBYyxPQUFPLFdBQVEsSUFBSSxPQUFPLGdCQUFhLENBQUMsQ0FDakUsQ0FBQztZQUNDLHFCQUFJLFNBQVMsR0FBRyxDQUFDLE9BQU8sY0FBVyxDQUFDLENBQUMsQ0FBQyxPQUFPLGNBQVcsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3RGLHFCQUFJLE9BQU8sR0FBRyxDQUFDLE9BQU8sWUFBUyxDQUFDLENBQUMsQ0FBQyxPQUFPLFlBQVMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQzlFLHFCQUFJLFlBQVksR0FBRyxDQUFDLE9BQU8saUJBQWMsQ0FBQyxDQUFDLENBQUMsT0FBTyxpQkFBYyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7WUFFbEcsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLGVBQWUsQ0FBQyxZQUFZLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3hFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN2QjtLQUNGOzs7O0lBV08sMENBQWM7Ozs7UUFFcEIsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FDbEMsQ0FBQztZQUNDLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1lBQ2YsTUFBTSxpRUFBaUUsQ0FBQTtTQUN4RTtRQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUNoQyxDQUFDO1lBQ0MsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7WUFDZixNQUFNLDRDQUE0QyxDQUFBO1NBQ25EO1FBQ0QsSUFBSSxDQUNKLENBQUM7WUFDQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFFMUIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQ3JDLENBQUM7Z0JBQ0MsSUFBSSxDQUFDLDBCQUEwQixFQUFFLENBQUM7YUFDbkM7U0FDRjs7Ozs7O0lBT0ssMENBQWM7Ozs7OztRQUVwQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDOztZQUN4QixHQUFHLENBQUMsQ0FBYyxJQUFBLEtBQUEsaUJBQUEsSUFBSSxDQUFDLE1BQU0sQ0FBQSxnQkFBQTtnQkFBeEIsSUFBSSxPQUFLLFdBQUE7Z0JBRVosSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsT0FBSyxDQUFDLENBQUM7YUFDNUI7Ozs7Ozs7OztRQUNELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxDQUFDO1lBQ3RELEVBQUUsRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7WUFDdEMsSUFBSSxFQUFFLElBQUk7WUFDVixpQkFBaUIsRUFBRSxLQUFJLENBQUMsU0FBUztZQUNqQyxlQUFlLEVBQUUsS0FBSSxDQUFDLE9BQU87WUFDN0IsTUFBTSxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztZQUN2QyxZQUFZLEVBQUUsS0FBSSxDQUFDLFlBQVk7U0FDaEMsQ0FBQyxFQVBxRCxDQU9yRCxDQUFDLENBQUM7Ozs7OztJQUdFLDhDQUFrQjs7OztRQUV4QixJQUFJLENBQUMseUJBQXlCLENBQUMsZUFBZSxDQUM1QyxJQUFJLENBQUMsZ0JBQWdCLEVBQ3JCLElBQUksQ0FBQyx1QkFBdUIsRUFDNUIsSUFBSSxDQUFDLElBQUksRUFDVCxJQUFJLEVBQ0o7WUFDRSxHQUFHLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzFCLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDaEMsTUFBTSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztTQUNqQyxFQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUM3QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFDN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQzlCLENBQUM7Ozs7O0lBR0ksc0RBQTBCOzs7OztRQUVoQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsQ0FBQztZQUN2RCxFQUFFLEVBQUUsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDakMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTO1lBQ3pCLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTztZQUNyQixpQkFBaUIsRUFBRSxLQUFJLENBQUMsU0FBUztZQUNqQyxlQUFlLEVBQUUsS0FBSSxDQUFDLE9BQU87WUFDN0IsTUFBTSxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ3RFLFlBQVksRUFBRSxLQUFJLENBQUMsWUFBWTtTQUNoQyxDQUFDLEVBUnNELENBUXRELENBQUMsQ0FBQztRQUVKLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FDN0UsQ0FBQztZQUNDLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUMzQyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDbkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1NBQ2hDO1FBRUQsSUFBSSxDQUFDLDZCQUE2QixDQUFDLGVBQWUsQ0FDaEQsSUFBSSxDQUFDLGdCQUFnQixFQUNyQixJQUFJLENBQUMsMkJBQTJCLEVBQ2hDLElBQUksQ0FBQyxLQUFLLEVBQ1YsSUFBSSxFQUNKO1lBQ0UsR0FBRyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUMxQixNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2hDLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDakMsQ0FDRixDQUFDOzs7Ozs7OztJQU9JLHlDQUFhOzs7Ozs7Y0FBQyxJQUE2QixFQUFFLFNBQXlEO1FBRTVHLHFCQUFJLFFBQVEscUJBQW9DLFNBQVMsQ0FBQyxRQUFRLENBQUEsQ0FBQztRQUNuRSxRQUFRLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs7Ozs7O0lBV2pFLG9DQUFROzs7O2NBQUMsS0FBWTtRQUUzQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs7Ozs7O0lBU3pCLG9DQUFROzs7O2NBQUMsS0FBWTtRQUUzQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs7Ozs7O0lBU3pCLGlDQUFLOzs7O2NBQUMsS0FBWTtRQUV4QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs7O2dCQXBQL0IsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLFFBQVEsRUFBRSx3S0FFb0Q7b0JBQzlELE1BQU0sRUFBRSxDQUFDLGdJQUFnSSxDQUFDO2lCQUMzSTs7OztnQkFqQlEsdUJBQXVCLHVCQThEM0IsTUFBTSxTQUFDLG1DQUFtQztnQkEvRHRDLHFCQUFxQix1QkFnRXpCLFFBQVEsWUFBSSxNQUFNLFNBQUMsZ0NBQWdDO2dCQTdEL0MsMkJBQTJCLHVCQThEL0IsUUFBUSxZQUFJLE1BQU0sU0FBQyw4QkFBOEI7Ozs0QkF6Q25ELEtBQUssU0FBQyxZQUFZOzBCQUtsQixLQUFLLFNBQUMsVUFBVTswQkFNaEIsS0FBSyxTQUFDLFNBQVM7eUJBYWYsS0FBSyxTQUFDLFFBQVE7K0JBTWQsS0FBSyxTQUFDLGVBQWU7NkJBTXJCLEtBQUssU0FBQyxhQUFhO21DQWFuQixTQUFTLFNBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixFQUFFO2dDQTZKL0MsTUFBTSxTQUFDLFFBQVE7Z0NBV2YsTUFBTSxTQUFDLFFBQVE7NkJBV2YsTUFBTSxTQUFDLEtBQUs7OzRCQXJRZjs7U0E0QmEsaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFNpbXBsZUNoYW5nZXMsIENvbXBvbmVudFJlZiwgVmlld0NoaWxkLCBWaWV3Q29udGFpbmVyUmVmLCBIb3N0QmluZGluZywgSW5qZWN0LCBPcHRpb25hbCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBnZXRXZWVrTnVtYmVyIH0gZnJvbSAnLi4vLi4vdXRpbHMvZGF0ZSc7XG5pbXBvcnQgeyBFdmVudERpc3BhdGNoZXIgfSBmcm9tICcuLi8uLi91dGlscy9kaXNwYXRjaGVyJztcbmltcG9ydCB7IEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudCB9IGZyb20gJy4uL2RheS1jZWxsL2Fic3RyYWN0LWRheS1jZWxsLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDYWxlbmRhckRheURhdGEgfSBmcm9tICcuLi8uLi90eXBlcy9jYWxlbmRhci1kYXknO1xuaW1wb3J0IHsgRXZlbnQgfSBmcm9tICcuLi8uLi90eXBlcy9ldmVudCc7XG5pbXBvcnQgeyBDYWxlbmRhcldlZWtEYXRhIH0gZnJvbSAnLi4vLi4vdHlwZXMvY2FsZW5kYXItd2Vlayc7XG5pbXBvcnQgeyBBYnN0cmFjdFdlZWtTdW1tYXJ5Q29tcG9uZW50IH0gZnJvbSAnLi4vd2Vlay1zdW1tYXJ5L2Fic3RyYWN0LXdlZWstc3VtbWFyeS5jb21wb25lbnQnO1xuaW1wb3J0IHsgRXZlbnRDb21wb25lbnRGYWN0b3J5IH0gZnJvbSAnLi4vZXZlbnQvZXZlbnQuZmFjdG9yeSc7XG5pbXBvcnQgeyBEYXlDZWxsQ29tcG9uZW50RmFjdG9yeSB9IGZyb20gJy4uL2RheS1jZWxsL2RheS1jZWxsLmZhY3RvcnknO1xuaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlciB9IGZyb20gJy4uL2R5bmFtaWMvZHluYW1pYy1tYW5hZ2VyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBXZWVrU3VtbWFyeUNvbXBvbmVudEZhY3RvcnkgfSBmcm9tICcuLi93ZWVrLXN1bW1hcnkvd2Vlay1zdW1tYXJ5LmZhdG9yeSc7XG5cbmxldCBtb21lbnQgPSByZXF1aXJlKCdtb21lbnQnKTtcbmxldCBoYXNoID0gcmVxdWlyZSgnb2JqZWN0LWhhc2gnKTtcblxuZXhwb3J0IGNvbnN0IERBWV9DRUxMX0NPTVBPTkVOVF9GQUNUT1JZX1BST1ZJREVSID0gJ0RheUNlbGxDb21wb25lbnRGYWN0b3J5JztcbmV4cG9ydCBjb25zdCBFVkVOVF9DT01QT05FTlRfRkFDVE9SWV9QUk9WSURFUiAgICA9ICdFdmVudENvbXBvbmVudEZhY3RvcnknO1xuZXhwb3J0IGNvbnN0IFdFRUtfU1VNTUFSWV9DT01QT05FTlRfRkFDVE9SWSAgICAgID0gJ1dlZWtTdW1tYXJ5Q29tcG9uZW50RmFjdG9yeSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXInLFxuICB0ZW1wbGF0ZTogYDxoNSAqbmdGb3I9J2xldCBuYW1lIG9mIGhlYWRlcnMnIGNsYXNzPSdoZWFkZXInPiB7e25hbWV9fSA8L2g1PlxuPG5nLXRlbXBsYXRlICNkYXlHcmlkPjwvbmctdGVtcGxhdGU+XG48aDUgKm5nRm9yPSdsZXQgbmFtZSBvZiBoZWFkZXJzJyBjbGFzcz0nZm9vdGVyJz4ge3tuYW1lfX0gPC9oNT5gLFxuICBzdHlsZXM6IFtgQGltcG9ydCB1cmwoaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PVJhbGV3YXkpOy5mb290ZXIsLmhlYWRlcnt0ZXh0LWFsaWduOmNlbnRlcjtmb250LWZhbWlseTpSYWxld2F5LHNhbnMtc2VyaWZ9YF1cbn0pXG5leHBvcnQgY2xhc3MgQ2FsZW5kYXJDb21wb25lbnRcbntcbiAgLyoqXG4gICAqIEZpcnN0IGRhdGUgb2YgdGhlIHBlcmlvZCB0byBkaXNwbGF5IGluIHRoZSBjYWxlbmRhclxuICAgKi9cbiAgQElucHV0KCdzdGFydC1kYXRlJykgcHJpdmF0ZSBzdGFydERhdGUgOiBEYXRlO1xuXG4gIC8qKlxuICAgKiBMYXN0IGRhdGUgb2YgdGhlIHBlcmlvZCB0byBkaXNwbGF5IGluIHRoZSBjYWxlbmRhclxuICAgKi9cbiAgQElucHV0KCdlbmQtZGF0ZScpIHByaXZhdGUgZW5kRGF0ZSA6IERhdGU7XG4gIFxuICAvKipcbiAgICogV2VlayBkYXlzIGxhYmVsIHRvIGRpc3BsYXkgaW4gdGhlIGhlYWRlclxuICAgKiAoZGVmYXVsdCBpcyBlbmdsaXNoKS5cbiAgICovXG4gIEBJbnB1dCgnaGVhZGVycycpIHB1YmxpYyBoZWFkZXJzIDogc3RyaW5nW10gPSBbXG4gICAgJ01vbmRheScsXG4gICAgJ1R1ZXNkYXknLFxuICAgICdXZWRuZXNkYXknLFxuICAgICdUaHVyc2RheScsXG4gICAgJ0ZyaWRheScsXG4gICAgJ1NhdHVyZGF5JyxcbiAgICAnU3VuZGF5JyxcbiAgXTtcblxuICAvKipcbiAgICogQSBsaXN0IG9mIGV2ZW50cyB0byBkaXNwbGF5IGluIHRoZSBjYWxsZW5kYXIuXG4gICAqL1xuICBASW5wdXQoJ2V2ZW50cycpIHByaXZhdGUgZXZlbnRzIDogRXZlbnRbXSA9IFtdO1xuICBcbiAgLyoqXG4gICAqIFRoZSBuYW1lIG9mIHRoZSBkYXRlIGF0dHJpYnV0ZSB0byB1c2VcbiAgICogd2hlbiBkaXNwbGF5aW5nIHRoZSBldmVudHMgKGRlZmF1bHQgaXMgJ2RhdGUnKS5cbiAgICovXG4gIEBJbnB1dCgnZGF0ZS1hY2Nlc3NvcicpIHByaXZhdGUgZGF0ZUFjY2Vzc29yIDogc3RyaW5nID0gJ2RhdGUnO1xuXG4gIC8qKlxuICAgKiBUaGUgbmFtZSBvZiB0aGUgaWQgYXR0cmlidXRlIHRvIHVzZVxuICAgKiB3aGVuIGRpc3BsYXlpbmcgdGhlIGV2ZW50cyAoZGVmYXVsdCBpcyAnaWQnKS5cbiAgICovXG4gIEBJbnB1dCgnaWQtYWNjZXNzb3InKSBwcml2YXRlIGlkQWNjZXNzb3IgOiBzdHJpbmcgPSAnaWQnO1xuXG4gIHB1YmxpYyBjb25zdHJ1Y3RvcihcbiAgICBASW5qZWN0KERBWV9DRUxMX0NPTVBPTkVOVF9GQUNUT1JZX1BST1ZJREVSKSBwdWJsaWMgZGF5Q2VsbENvbXBvbmVudEZhY3RvcnkgOiBEYXlDZWxsQ29tcG9uZW50RmFjdG9yeTxFdmVudD4sXG4gICAgQE9wdGlvbmFsKCkgQEluamVjdChFVkVOVF9DT01QT05FTlRfRkFDVE9SWV9QUk9WSURFUikgcHVibGljIGV2ZW50Q29tcG9uZW50RmFjdG9yeSA6IEV2ZW50Q29tcG9uZW50RmFjdG9yeTxFdmVudD4sXG4gICAgQE9wdGlvbmFsKCkgQEluamVjdChXRUVLX1NVTU1BUllfQ09NUE9ORU5UX0ZBQ1RPUlkpIHB1YmxpYyB3ZWVrU3VtbWFyeUNvbXBvbmVudEZhY3RvcnkgOiBXZWVrU3VtbWFyeUNvbXBvbmVudEZhY3Rvcnk8RXZlbnQ+XG4gICl7fVxuXG4gIC8qIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAqL1xuXG4gIC8qKlxuICAgKiBWaWV3IGNvbnRhaW5lciBzdG9yaW5nIHRoZSBkeW5hbWljIGNvbXBvbmVudHNcbiAgICovXG4gIEBWaWV3Q2hpbGQoJ2RheUdyaWQnLCB7IHJlYWQ6IFZpZXdDb250YWluZXJSZWYgfSlcbiAgcHJpdmF0ZSBkYXlHcmlkQ29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZjtcbiAgcHJpdmF0ZSBkeW5hbWljRGF5Q2VsbFZpZXdNYW5hZ2VyIDogRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlcjxFdmVudCwgQWJzdHJhY3REYXlDZWxsQ29tcG9uZW50PEV2ZW50Pj5cbiAgICA9IG5ldyBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyPEV2ZW50LCBBYnN0cmFjdERheUNlbGxDb21wb25lbnQ8RXZlbnQ+PigpO1xuICBwcml2YXRlIGR5bmFtaWNXZWVrU3VtbWFyeVZpZXdNYW5hZ2VyIDogRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlcjxFdmVudCwgQWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudDxFdmVudD4+XG4gICAgPSBuZXcgRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlcjxFdmVudCwgQWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudDxFdmVudD4+KCk7XG5cbiAgLyogLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tICovXG5cbiAgcHJpdmF0ZSBkaXNwYXRjaGVyIDogRXZlbnREaXNwYXRjaGVyO1xuICBwcml2YXRlIGRheXMgOiBDYWxlbmRhckRheURhdGE8RXZlbnQ+W107XG4gIHByaXZhdGUgd2Vla3MgOiBDYWxlbmRhcldlZWtEYXRhPEV2ZW50PltdO1xuICBcbiAgcHJpdmF0ZSBvbGRIYXNoIDogc3RyaW5nID0gJyc7XG4gIG5nRG9DaGVjaygpXG4gIHtcbiAgICBsZXQgbmV3SGFzaCA9IGhhc2guc2hhMSh0aGlzLmV2ZW50cyk7XG4gICAgaWYgKG5ld0hhc2ggIT09IHRoaXMub2xkSGFzaClcbiAgICB7XG4gICAgICB0aGlzLnVwZGF0ZUNhbGVuZGFyKCk7XG4gICAgICB0aGlzLm9sZEhhc2ggPSBuZXdIYXNoO1xuICAgIH1cbiAgfVxuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXMgOiBTaW1wbGVDaGFuZ2VzKVxuICB7XG4gICAgaWYgKGNoYW5nZXMuc3RhcnREYXRlIHx8IGNoYW5nZXMuZW5kRGF0ZSB8fCBjaGFuZ2VzLmRhdGVBY2Nlc3NvcilcbiAgICB7XG4gICAgICBsZXQgc3RhcnREYXRlID0gKGNoYW5nZXMuc3RhcnREYXRlKSA/IGNoYW5nZXMuc3RhcnREYXRlLmN1cnJlbnRWYWx1ZSA6IHRoaXMuc3RhcnREYXRlO1xuICAgICAgbGV0IGVuZERhdGUgPSAoY2hhbmdlcy5lbmREYXRlKSA/IGNoYW5nZXMuZW5kRGF0ZS5jdXJyZW50VmFsdWUgOiB0aGlzLmVuZERhdGU7XG4gICAgICBsZXQgZGF0ZUFjY2Vzc29yID0gKGNoYW5nZXMuZGF0ZUFjY2Vzc29yKSA/IGNoYW5nZXMuZGF0ZUFjY2Vzc29yLmN1cnJlbnRWYWx1ZSA6IHRoaXMuZGF0ZUFjY2Vzc29yO1xuXG4gICAgICB0aGlzLmRpc3BhdGNoZXIgPSBuZXcgRXZlbnREaXNwYXRjaGVyKGRhdGVBY2Nlc3Nvciwgc3RhcnREYXRlLCBlbmREYXRlKTtcbiAgICAgIHRoaXMudXBkYXRlQ2FsZW5kYXIoKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogT2xkIHN0YXJ0IGFuZCBlbmQgZGF0ZSBvZiB0aGUgY2FsZW5kYXIuXG4gICAqIFVzZWQgdG8ga2VlcCB0cmFjayBvZiB0aGUgY2hhbmdlcyBpbiBwZXJpb2RcbiAgICogaW4gb3JkZXIgdG8gdXBkYXRlIHRoZSB2aWV3LlxuICAgKi9cbiAgcHJpdmF0ZSBvbGRTdGFydERhdGUgOiBEYXRlO1xuICBwcml2YXRlIG9sZEVuZERhdGUgOiBEYXRlO1xuXG5cbiAgcHJpdmF0ZSB1cGRhdGVDYWxlbmRhcigpXG4gIHtcbiAgICBpZiAoIXRoaXMuZGF5Q2VsbENvbXBvbmVudEZhY3RvcnkpXG4gICAge1xuICAgICAgdGhpcy5kYXlzID0gW107XG4gICAgICB0aHJvdyAnZGF5IGNlbGwgY29tcG9uZW50IGZhY3RvcnkgKGRheS1jb21wb25lbnQtZmFjdG9yeSkgd2FzIG5vdCBzZXQhJ1xuICAgIH1cbiAgICBlbHNlIGlmICghdGhpcy5kYXlHcmlkQ29udGFpbmVyKVxuICAgIHtcbiAgICAgIHRoaXMuZGF5cyA9IFtdO1xuICAgICAgdGhyb3cgJ2RheSBncmlkIChkYXlHcmlkKSBub3QgZm91bmQgaW4gdGVtcGxhdGUgISdcbiAgICB9XG4gICAgZWxzZVxuICAgIHtcbiAgICAgIHRoaXMuZGlzcGF0Y2hFdmVudHMoKTtcbiAgICAgIHRoaXMudXBkYXRlRGF5Q2VsbFZpZXdzKCk7XG5cbiAgICAgIGlmICh0aGlzLndlZWtTdW1tYXJ5Q29tcG9uZW50RmFjdG9yeSlcbiAgICAgIHtcbiAgICAgICAgdGhpcy51cGRhdGVXZWVrU3VtbWFyeUNlbGxWaWV3cygpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG5cbiAgLyoqXG4gICAqIFNvcnRzIGV2ZW50cyBieSBkYXRlXG4gICAqL1xuICBwcml2YXRlIGRpc3BhdGNoRXZlbnRzKClcbiAge1xuICAgIHRoaXMuZGlzcGF0Y2hlci5jbGVhcigpO1xuICAgIGZvciAobGV0IGV2ZW50IG9mIHRoaXMuZXZlbnRzKVxuICAgIHtcbiAgICAgIHRoaXMuZGlzcGF0Y2hlci5hZGQoZXZlbnQpO1xuICAgIH1cbiAgICB0aGlzLmRheXMgPSB0aGlzLmRpc3BhdGNoZXIuZ2V0RGF0ZUFycmF5KCkubWFwKGRhdGUgPT4gKHtcbiAgICAgIGlkOiB0aGlzLmRpc3BhdGNoZXIuZ2V0RGF5TnVtYmVyKGRhdGUpLFxuICAgICAgZGF0ZTogZGF0ZSxcbiAgICAgIGNhbGVuZGFyU3RhcnREYXRlOiB0aGlzLnN0YXJ0RGF0ZSxcbiAgICAgIGNhbGVuZGFyRW5kRGF0ZTogdGhpcy5lbmREYXRlLFxuICAgICAgZXZlbnRzOiB0aGlzLmRpc3BhdGNoZXIuZ2V0RXZlbnRzKGRhdGUpLFxuICAgICAgZGF0ZUFjY2Vzc29yOiB0aGlzLmRhdGVBY2Nlc3NvcixcbiAgICB9KSk7XG4gIH1cblxuICBwcml2YXRlIHVwZGF0ZURheUNlbGxWaWV3cygpXG4gIHtcbiAgICB0aGlzLmR5bmFtaWNEYXlDZWxsVmlld01hbmFnZXIudXBkYXRlQ29udGFpbmVyKFxuICAgICAgdGhpcy5kYXlHcmlkQ29udGFpbmVyLFxuICAgICAgdGhpcy5kYXlDZWxsQ29tcG9uZW50RmFjdG9yeSxcbiAgICAgIHRoaXMuZGF5cyxcbiAgICAgICdpZCcsXG4gICAgICB7XG4gICAgICAgIGFkZDogdGhpcy5vbkFkZC5iaW5kKHRoaXMpLFxuICAgICAgICB1cGRhdGU6IHRoaXMub25VcGRhdGUuYmluZCh0aGlzKSxcbiAgICAgICAgZGVsZXRlOiB0aGlzLm9uRGVsZXRlLmJpbmQodGhpcylcbiAgICAgIH0sXG4gICAgICB0aGlzLnVwZGF0ZURheUNlbGwuYmluZCh0aGlzKSxcbiAgICAgIHRoaXMudXBkYXRlRGF5Q2VsbC5iaW5kKHRoaXMpLFxuICAgICAgdGhpcy51cGRhdGVEYXlDZWxsLmJpbmQodGhpcylcbiAgICApO1xuICB9XG5cbiAgcHJpdmF0ZSB1cGRhdGVXZWVrU3VtbWFyeUNlbGxWaWV3cygpXG4gIHtcbiAgICB0aGlzLndlZWtzID0gdGhpcy5kaXNwYXRjaGVyLmdldFdlZWtBcnJheSgpLm1hcCh3ZWVrID0+ICh7XG4gICAgICBpZDogZ2V0V2Vla051bWJlcih3ZWVrLnN0YXJ0RGF0ZSksXG4gICAgICBzdGFydERhdGU6IHdlZWsuc3RhcnREYXRlLFxuICAgICAgZW5kRGF0ZTogd2Vlay5lbmREYXRlLFxuICAgICAgY2FsZW5kYXJTdGFydERhdGU6IHRoaXMuc3RhcnREYXRlLFxuICAgICAgY2FsZW5kYXJFbmREYXRlOiB0aGlzLmVuZERhdGUsXG4gICAgICBldmVudHM6IHRoaXMuZGlzcGF0Y2hlci5nZXRFdmVudHNCZXR3ZWVuKHdlZWsuc3RhcnREYXRlLCB3ZWVrLmVuZERhdGUpLFxuICAgICAgZGF0ZUFjY2Vzc29yOiB0aGlzLmRhdGVBY2Nlc3NvcixcbiAgICB9KSk7XG5cbiAgICBpZiAodGhpcy5zdGFydERhdGUgIT09IHRoaXMub2xkU3RhcnREYXRlIHx8IHRoaXMuZW5kRGF0ZSAhPT0gdGhpcy5vbGRFbmREYXRlKVxuICAgIHtcbiAgICAgIHRoaXMuZHluYW1pY1dlZWtTdW1tYXJ5Vmlld01hbmFnZXIuY2xlYXIoKTtcbiAgICAgIHRoaXMub2xkU3RhcnREYXRlID0gdGhpcy5zdGFydERhdGU7XG4gICAgICB0aGlzLm9sZEVuZERhdGUgPSB0aGlzLmVuZERhdGU7XG4gICAgfVxuICAgIFxuICAgIHRoaXMuZHluYW1pY1dlZWtTdW1tYXJ5Vmlld01hbmFnZXIudXBkYXRlQ29udGFpbmVyKFxuICAgICAgdGhpcy5kYXlHcmlkQ29udGFpbmVyLFxuICAgICAgdGhpcy53ZWVrU3VtbWFyeUNvbXBvbmVudEZhY3RvcnksXG4gICAgICB0aGlzLndlZWtzLFxuICAgICAgJ2lkJyxcbiAgICAgIHtcbiAgICAgICAgYWRkOiB0aGlzLm9uQWRkLmJpbmQodGhpcyksXG4gICAgICAgIHVwZGF0ZTogdGhpcy5vblVwZGF0ZS5iaW5kKHRoaXMpLFxuICAgICAgICBkZWxldGU6IHRoaXMub25EZWxldGUuYmluZCh0aGlzKVxuICAgICAgfVxuICAgICk7XG4gIH1cblxuICAvKipcbiAgICogVHJpZ2dlcnMgYSBkYXkgY2VsbCB2aWV3IGludGVybmFsIHVwZGF0ZVxuICAgKiBAcGFyYW0gY29tcG9uZW50IHRoZSBjb21wb25lbnQgcmVmIG9mIHRoZSBkYXkgY2VsbCB0byB1cGRhdGVcbiAgICovXG4gIHByaXZhdGUgdXBkYXRlRGF5Q2VsbChkYXRhIDogQ2FsZW5kYXJEYXlEYXRhPEV2ZW50PiwgY29tcG9uZW50IDogQ29tcG9uZW50UmVmPEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudDxFdmVudD4+KSA6IHZvaWRcbiAge1xuICAgIGxldCBpbnN0YW5jZSA9IDxBYnN0cmFjdERheUNlbGxDb21wb25lbnQ8RXZlbnQ+PmNvbXBvbmVudC5pbnN0YW5jZTtcbiAgICBpbnN0YW5jZS51cGRhdGVFdmVudFZpZXdzKHRoaXMuZXZlbnRDb21wb25lbnRGYWN0b3J5LCB0aGlzLmlkQWNjZXNzb3IpO1xuICB9XG5cbiAgLyogLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tICovXG5cbiAgLyoqXG4gICAqIFRyaWdnZXJzIGEgZGVsZXRlIGV2ZW50IGVtaXNzaW9uXG4gICAqIEBwYXJhbSBldmVudCB0aGUgZXZlbnQgdG8gZGVsZXRlXG4gICAqL1xuICBAT3V0cHV0KCdkZWxldGUnKVxuICBwcml2YXRlIGRlbGV0ZUVtaXR0ZXIgOiBFdmVudEVtaXR0ZXI8RXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcjxFdmVudD4oKTtcbiAgcHJpdmF0ZSBvbkRlbGV0ZShldmVudDogRXZlbnQpXG4gIHtcbiAgICB0aGlzLmRlbGV0ZUVtaXR0ZXIuZW1pdChldmVudCk7XG4gIH1cblxuICAvKipcbiAgICogVHJpZ2dlcnMgYW4gdXBkYXRlIGV2ZW50IGVtaXNzaW9uXG4gICAqIEBwYXJhbSBldmVudCB0aGUgZXZlbnQgdG8gdXBkYXRlXG4gICAqL1xuICBAT3V0cHV0KCd1cGRhdGUnKVxuICBwcml2YXRlIHVwZGF0ZUVtaXR0ZXIgOiBFdmVudEVtaXR0ZXI8RXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcjxFdmVudD4oKTtcbiAgcHJpdmF0ZSBvblVwZGF0ZShldmVudDogRXZlbnQpXG4gIHtcbiAgICB0aGlzLnVwZGF0ZUVtaXR0ZXIuZW1pdChldmVudCk7XG4gIH1cblxuICAvKipcbiAgICogVHJpZ2dlcnMgYW4gYWRkIGV2ZW50IGVtaXNzaW9uXG4gICAqIEBwYXJhbSBldmVudCB0aGUgZXZlbnQgdG8gYWRkXG4gICAqL1xuICBAT3V0cHV0KCdhZGQnKVxuICBwcml2YXRlIGFkZEVtaXR0ZXIgOiBFdmVudEVtaXR0ZXI8RXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcjxFdmVudD4oKTtcbiAgcHJpdmF0ZSBvbkFkZChldmVudDogRXZlbnQpXG4gIHtcbiAgICB0aGlzLmFkZEVtaXR0ZXIuZW1pdChldmVudCk7XG4gIH1cbn1cbiJdfQ==