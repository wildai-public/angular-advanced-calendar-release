/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { DynamicComponent } from "../dynamic/dynamic.component";
import { EventEmitter, Output } from "@angular/core";
/**
 * @abstract
 * @template DataType, CrudType
 */
var DynamicCrudComponent = /** @class */ (function (_super) {
    tslib_1.__extends(DynamicCrudComponent, _super);
    function DynamicCrudComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.deleteEmitter = new EventEmitter();
        _this.updateEmitter = new EventEmitter();
        _this.addEmitter = new EventEmitter();
        return _this;
    }
    /**
     * @param {?} name
     * @param {?} callback
     * @return {?}
     */
    DynamicCrudComponent.prototype.subscribe = /**
     * @param {?} name
     * @param {?} callback
     * @return {?}
     */
    function (name, callback) {
        switch (name) {
            case 'update':
                this.updateEmitter.subscribe(callback);
                break;
            case 'delete':
                this.deleteEmitter.subscribe(callback);
                break;
            case 'add':
                this.addEmitter.subscribe(callback);
                break;
            default:
                throw 'callback type \'' + name + '\' is not a known event callback !';
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    DynamicCrudComponent.prototype.onDelete = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.deleteEmitter.emit(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    DynamicCrudComponent.prototype.onUpdate = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.updateEmitter.emit(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    DynamicCrudComponent.prototype.onAdd = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.addEmitter.emit(event);
    };
    DynamicCrudComponent.propDecorators = {
        deleteEmitter: [{ type: Output, args: ['delete',] }],
        updateEmitter: [{ type: Output, args: ['update',] }],
        addEmitter: [{ type: Output, args: ['add',] }]
    };
    return DynamicCrudComponent;
}(DynamicComponent));
export { DynamicCrudComponent };
function DynamicCrudComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    DynamicCrudComponent.prototype.deleteEmitter;
    /** @type {?} */
    DynamicCrudComponent.prototype.updateEmitter;
    /** @type {?} */
    DynamicCrudComponent.prototype.addEmitter;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy1jcnVkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdmlld3MvZHluYW1pYy1jcnVkL2R5bmFtaWMtY3J1ZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQzs7Ozs7O0lBRWtCLGdEQUEwQjs7OzhCQXFCOUMsSUFBSSxZQUFZLEVBQVk7OEJBTzVCLElBQUksWUFBWSxFQUFZOzJCQU8vQixJQUFJLFlBQVksRUFBWTs7Ozs7Ozs7SUFqQ25FLHdDQUFTOzs7OztjQUFDLElBQWEsRUFBRSxRQUFtQjtRQUVqRCxNQUFNLENBQUEsQ0FBQyxJQUFJLENBQUMsQ0FDWixDQUFDO1lBQ0MsS0FBSyxRQUFRO2dCQUNYLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN2QyxLQUFLLENBQUM7WUFDUixLQUFLLFFBQVE7Z0JBQ1gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3ZDLEtBQUssQ0FBQztZQUNSLEtBQUssS0FBSztnQkFDUixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDcEMsS0FBSyxDQUFDO1lBQ1I7Z0JBQ0UsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLEdBQUcsb0NBQW9DLENBQUM7U0FDMUU7Ozs7OztJQUtPLHVDQUFROzs7O0lBQWxCLFVBQW1CLEtBQWU7UUFFaEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDaEM7Ozs7O0lBSVMsdUNBQVE7Ozs7SUFBbEIsVUFBbUIsS0FBZTtRQUVoQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUNoQzs7Ozs7SUFJUyxvQ0FBSzs7OztJQUFmLFVBQWdCLEtBQWU7UUFFN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDN0I7O2dDQW5CQSxNQUFNLFNBQUMsUUFBUTtnQ0FPZixNQUFNLFNBQUMsUUFBUTs2QkFPZixNQUFNLFNBQUMsS0FBSzs7K0JBckNmO0VBR3VFLGdCQUFnQjtTQUFqRSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50IH0gZnJvbSBcIi4uL2R5bmFtaWMvZHluYW1pYy5jb21wb25lbnRcIjtcbmltcG9ydCB7IEV2ZW50RW1pdHRlciwgT3V0cHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIER5bmFtaWNDcnVkQ29tcG9uZW50PERhdGFUeXBlLCBDcnVkVHlwZT4gZXh0ZW5kcyBEeW5hbWljQ29tcG9uZW50PERhdGFUeXBlPlxue1xuICBwdWJsaWMgc3Vic2NyaWJlKG5hbWUgOiBzdHJpbmcsIGNhbGxiYWNrIDogRnVuY3Rpb24pIDogdm9pZFxuICB7XG4gICAgc3dpdGNoKG5hbWUpXG4gICAge1xuICAgICAgY2FzZSAndXBkYXRlJzpcbiAgICAgICAgdGhpcy51cGRhdGVFbWl0dGVyLnN1YnNjcmliZShjYWxsYmFjayk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnZGVsZXRlJzpcbiAgICAgICAgdGhpcy5kZWxldGVFbWl0dGVyLnN1YnNjcmliZShjYWxsYmFjayk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnYWRkJzpcbiAgICAgICAgdGhpcy5hZGRFbWl0dGVyLnN1YnNjcmliZShjYWxsYmFjayk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgdGhyb3cgJ2NhbGxiYWNrIHR5cGUgXFwnJyArIG5hbWUgKyAnXFwnIGlzIG5vdCBhIGtub3duIGV2ZW50IGNhbGxiYWNrICEnO1xuICAgIH1cbiAgfVxuXG4gIEBPdXRwdXQoJ2RlbGV0ZScpXG4gIHByaXZhdGUgZGVsZXRlRW1pdHRlciA6IEV2ZW50RW1pdHRlcjxDcnVkVHlwZT4gPSBuZXcgRXZlbnRFbWl0dGVyPENydWRUeXBlPigpO1xuICBwcm90ZWN0ZWQgb25EZWxldGUoZXZlbnQ6IENydWRUeXBlKVxuICB7XG4gICAgdGhpcy5kZWxldGVFbWl0dGVyLmVtaXQoZXZlbnQpO1xuICB9XG5cbiAgQE91dHB1dCgndXBkYXRlJylcbiAgcHJpdmF0ZSB1cGRhdGVFbWl0dGVyIDogRXZlbnRFbWl0dGVyPENydWRUeXBlPiA9IG5ldyBFdmVudEVtaXR0ZXI8Q3J1ZFR5cGU+KCk7XG4gIHByb3RlY3RlZCBvblVwZGF0ZShldmVudDogQ3J1ZFR5cGUpXG4gIHtcbiAgICB0aGlzLnVwZGF0ZUVtaXR0ZXIuZW1pdChldmVudCk7XG4gIH1cblxuICBAT3V0cHV0KCdhZGQnKVxuICBwcml2YXRlIGFkZEVtaXR0ZXIgOiBFdmVudEVtaXR0ZXI8Q3J1ZFR5cGU+ID0gbmV3IEV2ZW50RW1pdHRlcjxDcnVkVHlwZT4oKTtcbiAgcHJvdGVjdGVkIG9uQWRkKGV2ZW50OiBDcnVkVHlwZSlcbiAge1xuICAgIHRoaXMuYWRkRW1pdHRlci5lbWl0KGV2ZW50KTtcbiAgfVxufSJdfQ==