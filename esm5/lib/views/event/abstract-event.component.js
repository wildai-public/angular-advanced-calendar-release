/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { DynamicCrudComponent } from "../dynamic-crud/dynamic-crud.component";
/**
 * @abstract
 * @template DataType
 */
var /**
 * @abstract
 * @template DataType
 */
AbstractEventComponent = /** @class */ (function (_super) {
    tslib_1.__extends(AbstractEventComponent, _super);
    function AbstractEventComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AbstractEventComponent;
}(DynamicCrudComponent));
/**
 * @abstract
 * @template DataType
 */
export { AbstractEventComponent };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWJzdHJhY3QtZXZlbnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci8iLCJzb3VyY2VzIjpbImxpYi92aWV3cy9ldmVudC9hYnN0cmFjdC1ldmVudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQzs7Ozs7QUFFOUU7Ozs7QUFBQTtJQUErRCxrREFBd0M7Ozs7aUNBRnZHO0VBRStELG9CQUFvQixFQUVsRixDQUFBOzs7OztBQUZELGtDQUVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRHluYW1pY0NydWRDb21wb25lbnQgfSBmcm9tIFwiLi4vZHluYW1pYy1jcnVkL2R5bmFtaWMtY3J1ZC5jb21wb25lbnRcIjtcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEFic3RyYWN0RXZlbnRDb21wb25lbnQ8RGF0YVR5cGU+IGV4dGVuZHMgRHluYW1pY0NydWRDb21wb25lbnQ8RGF0YVR5cGUsIERhdGFUeXBlPlxue1xufSJdfQ==