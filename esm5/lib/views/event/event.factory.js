/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { DynamicComponentFactory } from "../dynamic/dynamic.factory";
/**
 * @abstract
 * @template DataType
 */
var /**
 * @abstract
 * @template DataType
 */
EventComponentFactory = /** @class */ (function (_super) {
    tslib_1.__extends(EventComponentFactory, _super);
    function EventComponentFactory(componentFactoryResolver, componentType) {
        return _super.call(this, componentFactoryResolver, componentType) || this;
    }
    return EventComponentFactory;
}(DynamicComponentFactory));
/**
 * @abstract
 * @template DataType
 */
export { EventComponentFactory };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnQuZmFjdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdmlld3MvZXZlbnQvZXZlbnQuZmFjdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDRCQUE0QixDQUFDOzs7OztBQUlyRTs7OztBQUFBO0lBQThELGlEQUFtRTttQ0FFNUcsd0JBQW1ELEVBQUUsYUFBc0Q7ZUFFNUgsa0JBQU0sd0JBQXdCLEVBQUUsYUFBYSxDQUFDOztnQ0FSbEQ7RUFJOEQsdUJBQXVCLEVBTXBGLENBQUE7Ozs7O0FBTkQsaUNBTUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50RmFjdG9yeSB9IGZyb20gXCIuLi9keW5hbWljL2R5bmFtaWMuZmFjdG9yeVwiO1xuaW1wb3J0IHsgQWJzdHJhY3RFdmVudENvbXBvbmVudCB9IGZyb20gXCIuL2Fic3RyYWN0LWV2ZW50LmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgVHlwZSwgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEV2ZW50Q29tcG9uZW50RmFjdG9yeTxEYXRhVHlwZT4gZXh0ZW5kcyBEeW5hbWljQ29tcG9uZW50RmFjdG9yeTxEYXRhVHlwZSwgQWJzdHJhY3RFdmVudENvbXBvbmVudDxEYXRhVHlwZT4+XG57XG4gIHB1YmxpYyBjb25zdHJ1Y3Rvcihjb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIgOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIGNvbXBvbmVudFR5cGUgOiBUeXBlPEFic3RyYWN0RXZlbnRDb21wb25lbnQ8RGF0YVR5cGU+PilcbiAge1xuICAgIHN1cGVyKGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29tcG9uZW50VHlwZSk7XG4gIH1cbn0iXX0=