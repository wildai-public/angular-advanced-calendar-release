/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
// unsupported: template constraints.
/**
 * @template DataType, ComponentType
 */
var 
// unsupported: template constraints.
/**
 * @template DataType, ComponentType
 */
DynamicComponentArrayManager = /** @class */ (function () {
    function DynamicComponentArrayManager() {
        /**
         * Stores the previous array state
         */
        this.oldDataArray = [];
        /**
         * A numeric map used to store the dynamic views
         */
        this.dynamicViewComponentMap = {};
    }
    /**
     * @return {?}
     */
    DynamicComponentArrayManager.prototype.clear = /**
     * @return {?}
     */
    function () {
        for (var /** @type {?} */ id in this.dynamicViewComponentMap) {
            if (this.dynamicViewComponentMap[id]) {
                this.dynamicViewComponentMap[id].destroy();
                delete this.dynamicViewComponentMap[id];
            }
        }
        this.oldDataArray = [];
    };
    /**
     * Updates, add or remove data views based
     * on the old data list
     * @param {?} container
     * @param {?} factory
     * @param {?} dataArray
     * @param {?} dataIdAccessor
     * @param {?=} dynamicComponentCallbacks
     * @param {?=} onAddView
     * @param {?=} onUpdateView
     * @param {?=} onDeleteView
     * @return {?}
     */
    DynamicComponentArrayManager.prototype.updateContainer = /**
     * Updates, add or remove data views based
     * on the old data list
     * @param {?} container
     * @param {?} factory
     * @param {?} dataArray
     * @param {?} dataIdAccessor
     * @param {?=} dynamicComponentCallbacks
     * @param {?=} onAddView
     * @param {?=} onUpdateView
     * @param {?=} onDeleteView
     * @return {?}
     */
    function (container, factory, dataArray, dataIdAccessor, dynamicComponentCallbacks, onAddView, onUpdateView, onDeleteView) {
        if (!container) {
            throw 'child container (#childContainer) could not be found in the container view';
        }
        try {
            for (var dataArray_1 = tslib_1.__values(dataArray), dataArray_1_1 = dataArray_1.next(); !dataArray_1_1.done; dataArray_1_1 = dataArray_1.next()) {
                var data = dataArray_1_1.value;
                var /** @type {?} */ alreadyPresent = false;
                try {
                    for (var _a = tslib_1.__values(this.oldDataArray), _b = _a.next(); !_b.done; _b = _a.next()) {
                        var oldData = _b.value;
                        if (data[dataIdAccessor] === oldData[dataIdAccessor]) {
                            alreadyPresent = true;
                            break;
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                if (alreadyPresent) {
                    this.updateDynamicComponent(container, data, dataIdAccessor, onUpdateView);
                }
                else {
                    this.addDynamicComponent(container, factory, data, dataIdAccessor, dynamicComponentCallbacks, onAddView);
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (dataArray_1_1 && !dataArray_1_1.done && (_d = dataArray_1.return)) _d.call(dataArray_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
        try {
            for (var _e = tslib_1.__values(this.oldDataArray), _f = _e.next(); !_f.done; _f = _e.next()) {
                var oldData = _f.value;
                var /** @type {?} */ stillPresent = false;
                try {
                    for (var dataArray_2 = tslib_1.__values(dataArray), dataArray_2_1 = dataArray_2.next(); !dataArray_2_1.done; dataArray_2_1 = dataArray_2.next()) {
                        var data = dataArray_2_1.value;
                        if (data[dataIdAccessor] === oldData[dataIdAccessor]) {
                            stillPresent = true;
                            break;
                        }
                    }
                }
                catch (e_3_1) { e_3 = { error: e_3_1 }; }
                finally {
                    try {
                        if (dataArray_2_1 && !dataArray_2_1.done && (_g = dataArray_2.return)) _g.call(dataArray_2);
                    }
                    finally { if (e_3) throw e_3.error; }
                }
                if (!stillPresent) {
                    this.removeDynamicComponent(container, oldData, dataIdAccessor, onDeleteView);
                }
            }
        }
        catch (e_4_1) { e_4 = { error: e_4_1 }; }
        finally {
            try {
                if (_f && !_f.done && (_h = _e.return)) _h.call(_e);
            }
            finally { if (e_4) throw e_4.error; }
        }
        this.oldDataArray = dataArray;
        var e_2, _d, e_1, _c, e_4, _h, e_3, _g;
    };
    /**
     * @param {?} container
     * @param {?} data
     * @param {?} dataIdAccessor
     * @param {?=} onDeleteView
     * @return {?}
     */
    DynamicComponentArrayManager.prototype.removeDynamicComponent = /**
     * @param {?} container
     * @param {?} data
     * @param {?} dataIdAccessor
     * @param {?=} onDeleteView
     * @return {?}
     */
    function (container, data, dataIdAccessor, onDeleteView) {
        if (typeof data[dataIdAccessor] !== 'number') {
            throw 'id (' + dataIdAccessor + ') not found in data structure (delete dynamic component method)';
        }
        else if (!this.dynamicViewComponentMap[data[dataIdAccessor]]) {
            throw 'dynamic view not found in the dynamic view container (delete dynamic component method)';
        }
        else {
            this.dynamicViewComponentMap[data[dataIdAccessor]].destroy();
            delete this.dynamicViewComponentMap[data[dataIdAccessor]];
            if (onDeleteView)
                onDeleteView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
        }
    };
    /**
     * @param {?} container
     * @param {?} factory
     * @param {?} data
     * @param {?} dataIdAccessor
     * @param {?} dynamicComponentCallbacks
     * @param {?=} onAddView
     * @return {?}
     */
    DynamicComponentArrayManager.prototype.addDynamicComponent = /**
     * @param {?} container
     * @param {?} factory
     * @param {?} data
     * @param {?} dataIdAccessor
     * @param {?} dynamicComponentCallbacks
     * @param {?=} onAddView
     * @return {?}
     */
    function (container, factory, data, dataIdAccessor, dynamicComponentCallbacks, onAddView) {
        if (typeof data[dataIdAccessor] !== 'number') {
            throw 'id (' + dataIdAccessor + ') not found in data structure (add dynamic component method)';
        }
        else if (this.dynamicViewComponentMap[data[dataIdAccessor]]) {
            throw 'dynamic view already present in the dynamic view container (add dynamic component method)';
        }
        else {
            var /** @type {?} */ componentRef = factory.insertDynamicComponent(container, data, dynamicComponentCallbacks);
            this.dynamicViewComponentMap[data[dataIdAccessor]] = componentRef;
            if (onAddView)
                onAddView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
        }
    };
    /**
     * Updates an data view model
     * @param {?} container
     * @param {?} data the new data model to use
     * @param {?} dataIdAccessor
     * @param {?=} onUpdateView
     * @return {?}
     */
    DynamicComponentArrayManager.prototype.updateDynamicComponent = /**
     * Updates an data view model
     * @param {?} container
     * @param {?} data the new data model to use
     * @param {?} dataIdAccessor
     * @param {?=} onUpdateView
     * @return {?}
     */
    function (container, data, dataIdAccessor, onUpdateView) {
        if (typeof data[dataIdAccessor] !== 'number') {
            throw 'id (' + dataIdAccessor + ') not found in data structure (add dynamic component method)';
        }
        else if (!this.dynamicViewComponentMap[data[dataIdAccessor]]) {
            throw 'dynamic view not found in the dynamic view container (add dynamic component method)';
        }
        else {
            this.dynamicViewComponentMap[data[dataIdAccessor]].instance.updateData(data);
            if (onUpdateView)
                onUpdateView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
        }
    };
    return DynamicComponentArrayManager;
}());
// unsupported: template constraints.
/**
 * @template DataType, ComponentType
 */
export { DynamicComponentArrayManager };
function DynamicComponentArrayManager_tsickle_Closure_declarations() {
    /**
     * Stores the previous array state
     * @type {?}
     */
    DynamicComponentArrayManager.prototype.oldDataArray;
    /**
     * A numeric map used to store the dynamic views
     * @type {?}
     */
    DynamicComponentArrayManager.prototype.dynamicViewComponentMap;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy1tYW5hZ2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdmlld3MvZHluYW1pYy9keW5hbWljLW1hbmFnZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUtBOzs7OztBQUFBOzs7Ozs0QkFNc0MsRUFBRTs7Ozt1Q0E4RXNDLEVBQUU7Ozs7O0lBNUV2RSw0Q0FBSzs7OztRQUVWLEdBQUcsQ0FBQyxDQUFDLHFCQUFJLEVBQUUsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FDNUMsQ0FBQztZQUNDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUNyQyxDQUFDO2dCQUNDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDM0MsT0FBTyxJQUFJLENBQUMsdUJBQXVCLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDekM7U0FDRjtRQUNELElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7SUFPbEIsc0RBQWU7Ozs7Ozs7Ozs7Ozs7Y0FDcEIsU0FBNEIsRUFDNUIsT0FBMEQsRUFDMUQsU0FBc0IsRUFDdEIsY0FBdUIsRUFDdkIseUJBQWdELEVBQ2hELFNBQXFCLEVBQ3JCLFlBQXdCLEVBQ3hCLFlBQXdCO1FBR3hCLEVBQUUsQ0FBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQ2hCLENBQUM7WUFDQyxNQUFNLDRFQUE0RSxDQUFBO1NBQ25GOztZQUNELEdBQUcsQ0FBQyxDQUFhLElBQUEsY0FBQSxpQkFBQSxTQUFTLENBQUEsb0NBQUE7Z0JBQXJCLElBQUksSUFBSSxzQkFBQTtnQkFFWCxxQkFBSSxjQUFjLEdBQUcsS0FBSyxDQUFDOztvQkFDM0IsR0FBRyxDQUFDLENBQWdCLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsWUFBWSxDQUFBLGdCQUFBO3dCQUFoQyxJQUFJLE9BQU8sV0FBQTt3QkFFZCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQ3JELENBQUM7NEJBQ0MsY0FBYyxHQUFHLElBQUksQ0FBQzs0QkFDdEIsS0FBSyxDQUFDO3lCQUNQO3FCQUNGOzs7Ozs7Ozs7Z0JBQ0QsRUFBRSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQ25CLENBQUM7b0JBQ0MsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFDO2lCQUM1RTtnQkFDRCxJQUFJLENBQ0osQ0FBQztvQkFDQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLHlCQUF5QixFQUFFLFNBQVMsQ0FBQyxDQUFDO2lCQUMxRzthQUNGOzs7Ozs7Ozs7O1lBRUQsR0FBRyxDQUFDLENBQWdCLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsWUFBWSxDQUFBLGdCQUFBO2dCQUFoQyxJQUFJLE9BQU8sV0FBQTtnQkFFZCxxQkFBSSxZQUFZLEdBQUcsS0FBSyxDQUFDOztvQkFDekIsR0FBRyxDQUFDLENBQWEsSUFBQSxjQUFBLGlCQUFBLFNBQVMsQ0FBQSxvQ0FBQTt3QkFBckIsSUFBSSxJQUFJLHNCQUFBO3dCQUVYLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FDckQsQ0FBQzs0QkFDQyxZQUFZLEdBQUcsSUFBSSxDQUFDOzRCQUNwQixLQUFLLENBQUM7eUJBQ1A7cUJBQ0Y7Ozs7Ozs7OztnQkFDRCxFQUFFLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUNsQixDQUFDO29CQUNDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxZQUFZLENBQUMsQ0FBQztpQkFDL0U7YUFDRjs7Ozs7Ozs7O1FBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUM7Ozs7Ozs7Ozs7SUFReEIsNkRBQXNCOzs7Ozs7O2NBQUMsU0FBNEIsRUFBRSxJQUFlLEVBQUUsY0FBdUIsRUFBRSxZQUF3QjtRQUU3SCxFQUFFLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FDN0MsQ0FBQztZQUNDLE1BQU0sTUFBTSxHQUFHLGNBQWMsR0FBRyxpRUFBaUUsQ0FBQztTQUNuRztRQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUM3RCxDQUFDO1lBQ0MsTUFBTSx3RkFBd0YsQ0FBQztTQUNoRztRQUNELElBQUksQ0FDSixDQUFDO1lBQ0MsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQzdELE9BQU8sSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQzFELEVBQUUsQ0FBQyxDQUFDLFlBQVksQ0FBQztnQkFBQyxZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzFGOzs7Ozs7Ozs7OztJQUdLLDBEQUFtQjs7Ozs7Ozs7O2NBQ3pCLFNBQTRCLEVBQzVCLE9BQTBELEVBQzFELElBQWUsRUFDZixjQUF1QixFQUN2Qix5QkFBK0MsRUFDL0MsU0FBcUI7UUFHckIsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssUUFBUSxDQUFDLENBQzdDLENBQUM7WUFDQyxNQUFNLE1BQU0sR0FBRyxjQUFjLEdBQUcsOERBQThELENBQUM7U0FDaEc7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQzVELENBQUM7WUFDQyxNQUFNLDJGQUEyRixDQUFDO1NBQ25HO1FBQ0QsSUFBSSxDQUNKLENBQUM7WUFDQyxxQkFBSSxZQUFZLEdBQUcsT0FBTyxDQUFDLHNCQUFzQixDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUseUJBQXlCLENBQUMsQ0FBQztZQUM5RixJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEdBQUcsWUFBWSxDQUFDO1lBQ2xFLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQztnQkFBQyxTQUFTLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3BGOzs7Ozs7Ozs7O0lBT0ssNkRBQXNCOzs7Ozs7OztjQUFDLFNBQTRCLEVBQUUsSUFBZSxFQUFFLGNBQXVCLEVBQUUsWUFBd0I7UUFFN0gsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssUUFBUSxDQUFDLENBQzdDLENBQUM7WUFDQyxNQUFNLE1BQU0sR0FBRyxjQUFjLEdBQUcsOERBQThELENBQUM7U0FDaEc7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FDN0QsQ0FBQztZQUNDLE1BQU0scUZBQXFGLENBQUM7U0FDN0Y7UUFDRCxJQUFJLENBQ0osQ0FBQztZQUNDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzdFLEVBQUUsQ0FBQyxDQUFDLFlBQVksQ0FBQztnQkFBQyxZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzFGOzt1Q0F4Skw7SUEwSkMsQ0FBQTs7Ozs7QUFySkQsd0NBcUpDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudCB9IGZyb20gXCIuL2R5bmFtaWMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBWaWV3Q29udGFpbmVyUmVmLCBDb21wb25lbnRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudEZhY3RvcnkgfSBmcm9tIFwiLi9keW5hbWljLmZhY3RvcnlcIjtcbmltcG9ydCB7IFN0cmluZ01hcCwgTnVtZXJpY01hcCB9IGZyb20gXCIuLi8uLi90eXBlcy9tYXBzXCI7XG5cbmV4cG9ydCBjbGFzcyBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyPERhdGFUeXBlLCBDb21wb25lbnRUeXBlIGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudDxEYXRhVHlwZT4+XG57XG5cbiAgLyoqXG4gICAqIFN0b3JlcyB0aGUgcHJldmlvdXMgYXJyYXkgc3RhdGVcbiAgICovXG4gIHByaXZhdGUgb2xkRGF0YUFycmF5IDogRGF0YVR5cGVbXSA9IFtdO1xuICBcbiAgcHVibGljIGNsZWFyKClcbiAge1xuICAgIGZvciAobGV0IGlkIGluIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXApXG4gICAge1xuICAgICAgaWYgKHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbaWRdKVxuICAgICAge1xuICAgICAgICB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2lkXS5kZXN0cm95KCk7XG4gICAgICAgIGRlbGV0ZSB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2lkXTtcbiAgICAgIH1cbiAgICB9XG4gICAgdGhpcy5vbGREYXRhQXJyYXkgPSBbXTtcbiAgfVxuXG4gIC8qKlxuICAgKiBVcGRhdGVzLCBhZGQgb3IgcmVtb3ZlIGRhdGEgdmlld3MgYmFzZWRcbiAgICogb24gdGhlIG9sZCBkYXRhIGxpc3RcbiAgICovXG4gIHB1YmxpYyB1cGRhdGVDb250YWluZXIoXG4gICAgY29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZixcbiAgICBmYWN0b3J5IDogRHluYW1pY0NvbXBvbmVudEZhY3Rvcnk8RGF0YVR5cGUsIENvbXBvbmVudFR5cGU+LFxuICAgIGRhdGFBcnJheSA6IERhdGFUeXBlW10sXG4gICAgZGF0YUlkQWNjZXNzb3IgOiBzdHJpbmcsXG4gICAgZHluYW1pY0NvbXBvbmVudENhbGxiYWNrcz8gOiBTdHJpbmdNYXA8RnVuY3Rpb24+LFxuICAgIG9uQWRkVmlldz8gOiBGdW5jdGlvbixcbiAgICBvblVwZGF0ZVZpZXc/IDogRnVuY3Rpb24sXG4gICAgb25EZWxldGVWaWV3PyA6IEZ1bmN0aW9uXG4gICkgOiB2b2lkXG4gIHtcbiAgICBpZiAgKCFjb250YWluZXIpXG4gICAge1xuICAgICAgdGhyb3cgJ2NoaWxkIGNvbnRhaW5lciAoI2NoaWxkQ29udGFpbmVyKSBjb3VsZCBub3QgYmUgZm91bmQgaW4gdGhlIGNvbnRhaW5lciB2aWV3J1xuICAgIH1cbiAgICBmb3IgKGxldCBkYXRhIG9mIGRhdGFBcnJheSlcbiAgICB7XG4gICAgICBsZXQgYWxyZWFkeVByZXNlbnQgPSBmYWxzZTtcbiAgICAgIGZvciAobGV0IG9sZERhdGEgb2YgdGhpcy5vbGREYXRhQXJyYXkpXG4gICAgICB7XG4gICAgICAgIGlmIChkYXRhW2RhdGFJZEFjY2Vzc29yXSA9PT0gb2xkRGF0YVtkYXRhSWRBY2Nlc3Nvcl0pXG4gICAgICAgIHtcbiAgICAgICAgICBhbHJlYWR5UHJlc2VudCA9IHRydWU7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGlmIChhbHJlYWR5UHJlc2VudClcbiAgICAgIHtcbiAgICAgICAgdGhpcy51cGRhdGVEeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciwgZGF0YSwgZGF0YUlkQWNjZXNzb3IsIG9uVXBkYXRlVmlldyk7XG4gICAgICB9XG4gICAgICBlbHNlXG4gICAgICB7XG4gICAgICAgIHRoaXMuYWRkRHluYW1pY0NvbXBvbmVudChjb250YWluZXIsIGZhY3RvcnksIGRhdGEsIGRhdGFJZEFjY2Vzc29yLCBkeW5hbWljQ29tcG9uZW50Q2FsbGJhY2tzLCBvbkFkZFZpZXcpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZvciAobGV0IG9sZERhdGEgb2YgdGhpcy5vbGREYXRhQXJyYXkpXG4gICAge1xuICAgICAgbGV0IHN0aWxsUHJlc2VudCA9IGZhbHNlO1xuICAgICAgZm9yIChsZXQgZGF0YSBvZiBkYXRhQXJyYXkpXG4gICAgICB7XG4gICAgICAgIGlmIChkYXRhW2RhdGFJZEFjY2Vzc29yXSA9PT0gb2xkRGF0YVtkYXRhSWRBY2Nlc3Nvcl0pXG4gICAgICAgIHtcbiAgICAgICAgICBzdGlsbFByZXNlbnQgPSB0cnVlO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAoIXN0aWxsUHJlc2VudClcbiAgICAgIHtcbiAgICAgICAgdGhpcy5yZW1vdmVEeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciwgb2xkRGF0YSwgZGF0YUlkQWNjZXNzb3IsIG9uRGVsZXRlVmlldyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdGhpcy5vbGREYXRhQXJyYXkgPSBkYXRhQXJyYXk7XG4gIH1cblxuICAvKipcbiAgICogQSBudW1lcmljIG1hcCB1c2VkIHRvIHN0b3JlIHRoZSBkeW5hbWljIHZpZXdzXG4gICAqL1xuICBwcml2YXRlIGR5bmFtaWNWaWV3Q29tcG9uZW50TWFwIDogTnVtZXJpY01hcDxDb21wb25lbnRSZWY8Q29tcG9uZW50VHlwZT4+ID0ge307XG5cbiAgcHJpdmF0ZSByZW1vdmVEeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWYsIGRhdGEgOiBEYXRhVHlwZSwgZGF0YUlkQWNjZXNzb3IgOiBzdHJpbmcsIG9uRGVsZXRlVmlldz8gOiBGdW5jdGlvbikgOiB2b2lkXG4gIHtcbiAgICBpZiAodHlwZW9mIGRhdGFbZGF0YUlkQWNjZXNzb3JdICE9PSAnbnVtYmVyJylcbiAgICB7XG4gICAgICB0aHJvdyAnaWQgKCcgKyBkYXRhSWRBY2Nlc3NvciArICcpIG5vdCBmb3VuZCBpbiBkYXRhIHN0cnVjdHVyZSAoZGVsZXRlIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlIGlmICghdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0pXG4gICAge1xuICAgICAgdGhyb3cgJ2R5bmFtaWMgdmlldyBub3QgZm91bmQgaW4gdGhlIGR5bmFtaWMgdmlldyBjb250YWluZXIgKGRlbGV0ZSBkeW5hbWljIGNvbXBvbmVudCBtZXRob2QpJztcbiAgICB9XG4gICAgZWxzZVxuICAgIHtcbiAgICAgIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbZGF0YVtkYXRhSWRBY2Nlc3Nvcl1dLmRlc3Ryb3koKTtcbiAgICAgIGRlbGV0ZSB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXTtcbiAgICAgIGlmIChvbkRlbGV0ZVZpZXcpIG9uRGVsZXRlVmlldyhkYXRhLCB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBhZGREeW5hbWljQ29tcG9uZW50KFxuICAgIGNvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWYsXG4gICAgZmFjdG9yeSA6IER5bmFtaWNDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlLCBDb21wb25lbnRUeXBlPixcbiAgICBkYXRhIDogRGF0YVR5cGUsXG4gICAgZGF0YUlkQWNjZXNzb3IgOiBzdHJpbmcsXG4gICAgZHluYW1pY0NvbXBvbmVudENhbGxiYWNrcyA6IFN0cmluZ01hcDxGdW5jdGlvbj4sXG4gICAgb25BZGRWaWV3PyA6IEZ1bmN0aW9uXG4gICkgOiB2b2lkXG4gIHtcbiAgICBpZiAodHlwZW9mIGRhdGFbZGF0YUlkQWNjZXNzb3JdICE9PSAnbnVtYmVyJylcbiAgICB7XG4gICAgICB0aHJvdyAnaWQgKCcgKyBkYXRhSWRBY2Nlc3NvciArICcpIG5vdCBmb3VuZCBpbiBkYXRhIHN0cnVjdHVyZSAoYWRkIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlIGlmICh0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSlcbiAgICB7XG4gICAgICB0aHJvdyAnZHluYW1pYyB2aWV3IGFscmVhZHkgcHJlc2VudCBpbiB0aGUgZHluYW1pYyB2aWV3IGNvbnRhaW5lciAoYWRkIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlXG4gICAge1xuICAgICAgbGV0IGNvbXBvbmVudFJlZiA9IGZhY3RvcnkuaW5zZXJ0RHluYW1pY0NvbXBvbmVudChjb250YWluZXIsIGRhdGEsIGR5bmFtaWNDb21wb25lbnRDYWxsYmFja3MpO1xuICAgICAgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0gPSBjb21wb25lbnRSZWY7XG4gICAgICBpZiAob25BZGRWaWV3KSBvbkFkZFZpZXcoZGF0YSwgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0pO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBVcGRhdGVzIGFuIGRhdGEgdmlldyBtb2RlbFxuICAgKiBAcGFyYW0gZGF0YSB0aGUgbmV3IGRhdGEgbW9kZWwgdG8gdXNlXG4gICAqL1xuICBwcml2YXRlIHVwZGF0ZUR5bmFtaWNDb21wb25lbnQoY29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZiwgZGF0YSA6IERhdGFUeXBlLCBkYXRhSWRBY2Nlc3NvciA6IHN0cmluZywgb25VcGRhdGVWaWV3PyA6IEZ1bmN0aW9uKSA6IHZvaWRcbiAge1xuICAgIGlmICh0eXBlb2YgZGF0YVtkYXRhSWRBY2Nlc3Nvcl0gIT09ICdudW1iZXInKVxuICAgIHtcbiAgICAgIHRocm93ICdpZCAoJyArIGRhdGFJZEFjY2Vzc29yICsgJykgbm90IGZvdW5kIGluIGRhdGEgc3RydWN0dXJlIChhZGQgZHluYW1pYyBjb21wb25lbnQgbWV0aG9kKSc7XG4gICAgfVxuICAgIGVsc2UgaWYgKCF0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSlcbiAgICB7XG4gICAgICB0aHJvdyAnZHluYW1pYyB2aWV3IG5vdCBmb3VuZCBpbiB0aGUgZHluYW1pYyB2aWV3IGNvbnRhaW5lciAoYWRkIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlXG4gICAge1xuICAgICAgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0uaW5zdGFuY2UudXBkYXRlRGF0YShkYXRhKTtcbiAgICAgIGlmIChvblVwZGF0ZVZpZXcpIG9uVXBkYXRlVmlldyhkYXRhLCB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSk7XG4gICAgfVxuICB9XG59Il19