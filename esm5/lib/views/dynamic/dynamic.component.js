/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Input } from "@angular/core";
/**
 * @abstract
 * @template DataType
 */
var DynamicComponent = /** @class */ (function () {
    function DynamicComponent() {
    }
    /**
     * @param {?} data
     * @return {?}
     */
    DynamicComponent.prototype.updateData = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        this.data = data;
    };
    DynamicComponent.propDecorators = {
        data: [{ type: Input, args: ['data',] }]
    };
    return DynamicComponent;
}());
export { DynamicComponent };
function DynamicComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    DynamicComponent.prototype.data;
    /**
     * @abstract
     * @param {?} name
     * @param {?} callback
     * @return {?}
     */
    DynamicComponent.prototype.subscribe = function (name, callback) { };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hbmd1bGFyLWFkdmFuY2VkLWNhbGVuZGFyLyIsInNvdXJjZXMiOlsibGliL3ZpZXdzL2R5bmFtaWMvZHluYW1pYy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7Ozs7OztJQU83QixxQ0FBVTs7OztjQUFDLElBQWU7UUFFL0IsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7Ozt1QkFMbEIsS0FBSyxTQUFDLE1BQU07OzJCQUpmOztTQUVzQixnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbnB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBEeW5hbWljQ29tcG9uZW50PERhdGFUeXBlPlxue1xuICBASW5wdXQoJ2RhdGEnKVxuICBwdWJsaWMgZGF0YSA6IERhdGFUeXBlO1xuICBcbiAgcHVibGljIHVwZGF0ZURhdGEoZGF0YSA6IERhdGFUeXBlKVxuICB7XG4gICAgdGhpcy5kYXRhID0gZGF0YTtcbiAgfVxuXG4gIHB1YmxpYyBhYnN0cmFjdCBzdWJzY3JpYmUobmFtZSA6IHN0cmluZywgY2FsbGJhY2sgOiBGdW5jdGlvbikgOiB2b2lkO1xufSJdfQ==