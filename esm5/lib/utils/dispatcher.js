/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var /** @type {?} */ moment = require('moment');
/**
 * Dispatches events in an array of dates depending
 * based on the date of each event.
 */
var /**
 * Dispatches events in an array of dates depending
 * based on the date of each event.
 */
EventDispatcher = /** @class */ (function () {
    function EventDispatcher(dateAccessor, startDate, endDate) {
        this.dateAccessor = dateAccessor;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    /**
     * @return {?}
     */
    EventDispatcher.prototype.clear = /**
     * @return {?}
     */
    function () {
        this.dispatch = [];
        for (var /** @type {?} */ i = 0; i < this.getTotalNumberOfDays(); i++) {
            this.dispatch[i] = [];
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    EventDispatcher.prototype.add = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        var /** @type {?} */ date = event[this.dateAccessor];
        var /** @type {?} */ dispatchIndex = this.getDayNumber(date);
        if (this.dispatch[dispatchIndex]) {
            this.dispatch[dispatchIndex].push(event);
        }
    };
    /**
     * @param {?} date
     * @return {?}
     */
    EventDispatcher.prototype.getEvents = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return this.dispatch[this.getDayNumber(date)];
    };
    /**
     * @param {?} startDate
     * @param {?} endDate
     * @return {?}
     */
    EventDispatcher.prototype.getEventsBetween = /**
     * @param {?} startDate
     * @param {?} endDate
     * @return {?}
     */
    function (startDate, endDate) {
        return this.dispatch.slice(this.getDayNumber(startDate), this.getDayNumber(endDate) + 1);
    };
    /**
     * Returns an array of the dates
     * between startDate and endDate.
     * @return {?}
     */
    EventDispatcher.prototype.getDateArray = /**
     * Returns an array of the dates
     * between startDate and endDate.
     * @return {?}
     */
    function () {
        var /** @type {?} */ days = [];
        var /** @type {?} */ current = moment(this.startDate);
        var /** @type {?} */ last = moment(this.endDate);
        while (current < last) {
            days.push(current.toDate());
            current = current.add(1, 'day');
        }
        return days;
    };
    /**
     * @return {?}
     */
    EventDispatcher.prototype.getWeekArray = /**
     * @return {?}
     */
    function () {
        var /** @type {?} */ weeks = [];
        var /** @type {?} */ current = moment(this.startDate);
        var /** @type {?} */ last = moment(this.endDate);
        while (current < last) {
            weeks.push({ startDate: current.toDate(), endDate: current.add(6, 'day').toDate() });
            current = current.add(1, 'day');
        }
        return weeks;
    };
    /**
     * Returns the index of a date in
     * the [startDate - endDate] period.
     * @param {?} date the date for which to compute the index
     * @return {?}
     */
    EventDispatcher.prototype.getDayNumber = /**
     * Returns the index of a date in
     * the [startDate - endDate] period.
     * @param {?} date the date for which to compute the index
     * @return {?}
     */
    function (date) {
        return moment(date).diff(moment(this.startDate), 'days');
    };
    /**
     * Returns the total number of days
     * in the [startDate - endDate] period.
     * @return {?}
     */
    EventDispatcher.prototype.getTotalNumberOfDays = /**
     * Returns the total number of days
     * in the [startDate - endDate] period.
     * @return {?}
     */
    function () {
        return this.getDayNumber(this.endDate) + 1;
    };
    return EventDispatcher;
}());
/**
 * Dispatches events in an array of dates depending
 * based on the date of each event.
 */
export { EventDispatcher };
function EventDispatcher_tsickle_Closure_declarations() {
    /** @type {?} */
    EventDispatcher.prototype.dispatch;
    /** @type {?} */
    EventDispatcher.prototype.dateAccessor;
    /** @type {?} */
    EventDispatcher.prototype.startDate;
    /** @type {?} */
    EventDispatcher.prototype.endDate;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzcGF0Y2hlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdXRpbHMvZGlzcGF0Y2hlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEscUJBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Ozs7QUFNL0I7Ozs7QUFBQTs2QkFVcUMsWUFBcUIsRUFBa0IsU0FBZ0IsRUFBa0IsT0FBYztRQUF2RixpQkFBWSxHQUFaLFlBQVksQ0FBUztRQUFrQixjQUFTLEdBQVQsU0FBUyxDQUFPO1FBQWtCLFlBQU8sR0FBUCxPQUFPLENBQU87Ozs7O0lBRW5ILCtCQUFLOzs7O1FBRVYsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbkIsR0FBRyxDQUFDLENBQUMscUJBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQ3BELENBQUM7WUFDQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUN2Qjs7Ozs7O0lBR0ksNkJBQUc7Ozs7Y0FBQyxLQUFXO1FBRXBCLHFCQUFJLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3BDLHFCQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FDakMsQ0FBQztZQUNDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzFDOzs7Ozs7SUFHSSxtQ0FBUzs7OztjQUFDLElBQVc7UUFFMUIsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDOzs7Ozs7O0lBR3pDLDBDQUFnQjs7Ozs7Y0FBQyxTQUFnQixFQUFFLE9BQWM7UUFFdEQsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzs7Ozs7OztJQU9wRixzQ0FBWTs7Ozs7O1FBRWpCLHFCQUFJLElBQUksR0FBRyxFQUFFLENBQUM7UUFDZCxxQkFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNyQyxxQkFBSSxJQUFJLEdBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNqQyxPQUFNLE9BQU8sR0FBRyxJQUFJLEVBQ3BCLENBQUM7WUFDQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1lBQzVCLE9BQU8sR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUNqQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7Ozs7O0lBR1Asc0NBQVk7Ozs7UUFFakIscUJBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUNmLHFCQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3JDLHFCQUFJLElBQUksR0FBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2pDLE9BQU0sT0FBTyxHQUFHLElBQUksRUFDcEIsQ0FBQztZQUNDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDckYsT0FBTyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ2pDO1FBQ0QsTUFBTSxDQUFDLEtBQUssQ0FBQzs7Ozs7Ozs7SUFRUixzQ0FBWTs7Ozs7O2NBQUMsSUFBVztRQUU3QixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDOzs7Ozs7O0lBT3BELDhDQUFvQjs7Ozs7O1FBRXpCLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7OzBCQTdGL0M7SUErRkMsQ0FBQTs7Ozs7QUF6RkQsMkJBeUZDIiwic291cmNlc0NvbnRlbnQiOlsibGV0IG1vbWVudCA9IHJlcXVpcmUoJ21vbWVudCcpO1xuXG4vKipcbiAqIERpc3BhdGNoZXMgZXZlbnRzIGluIGFuIGFycmF5IG9mIGRhdGVzIGRlcGVuZGluZ1xuICogYmFzZWQgb24gdGhlIGRhdGUgb2YgZWFjaCBldmVudC5cbiAqL1xuZXhwb3J0IGNsYXNzIEV2ZW50RGlzcGF0Y2hlclxue1xuICBwcml2YXRlIGRpc3BhdGNoIDogYW55W11bXTtcblxuICAvKipcbiAgICogQ3JlYXRlcyBhIG5ldyBldmVudCBkaXNwYXRjaGVyXG4gICAqIEBwYXJhbSBkYXRlQWNjZXNzb3IgdGhlIGRhdGUgbGFiZWwgdG8gdXNlIGluIHRoZSBldmVudHNcbiAgICogQHBhcmFtIHN0YXJ0RGF0ZSB0aGUgc3RhcnQgZGF0ZSBvZiB0aGUgcGVyaW9kIGluIHdoaWNoIHRvIGRpc3BhdGNoIGV2ZW50c1xuICAgKiBAcGFyYW0gZW5kRGF0ZSB0aGUgZW5kIGRhdGUgb2YgdGhlIHBlcmlvZCAoaW5jbHVzaXZlKSBpbiB3aGljaCB0byBkaXNwYXRjaCBldmVudHNcbiAgICovXG4gIHB1YmxpYyBjb25zdHJ1Y3RvcihwdWJsaWMgcmVhZG9ubHkgZGF0ZUFjY2Vzc29yIDogc3RyaW5nLCBwdWJsaWMgcmVhZG9ubHkgc3RhcnREYXRlIDogRGF0ZSwgcHVibGljIHJlYWRvbmx5IGVuZERhdGUgOiBEYXRlKSB7IH1cblxuICBwdWJsaWMgY2xlYXIoKSA6IHZvaWRcbiAge1xuICAgIHRoaXMuZGlzcGF0Y2ggPSBbXTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZ2V0VG90YWxOdW1iZXJPZkRheXMoKTsgaSsrKVxuICAgIHtcbiAgICAgIHRoaXMuZGlzcGF0Y2hbaV0gPSBbXTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgYWRkKGV2ZW50IDogYW55KVxuICB7XG4gICAgbGV0IGRhdGUgPSBldmVudFt0aGlzLmRhdGVBY2Nlc3Nvcl07XG4gICAgbGV0IGRpc3BhdGNoSW5kZXggPSB0aGlzLmdldERheU51bWJlcihkYXRlKTtcbiAgICBpZiAodGhpcy5kaXNwYXRjaFtkaXNwYXRjaEluZGV4XSlcbiAgICB7XG4gICAgICB0aGlzLmRpc3BhdGNoW2Rpc3BhdGNoSW5kZXhdLnB1c2goZXZlbnQpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBnZXRFdmVudHMoZGF0ZSA6IERhdGUpIDogYW55W11cbiAge1xuICAgIHJldHVybiB0aGlzLmRpc3BhdGNoW3RoaXMuZ2V0RGF5TnVtYmVyKGRhdGUpXTtcbiAgfVxuXG4gIHB1YmxpYyBnZXRFdmVudHNCZXR3ZWVuKHN0YXJ0RGF0ZSA6IERhdGUsIGVuZERhdGUgOiBEYXRlKVxuICB7XG4gICAgcmV0dXJuIHRoaXMuZGlzcGF0Y2guc2xpY2UodGhpcy5nZXREYXlOdW1iZXIoc3RhcnREYXRlKSwgdGhpcy5nZXREYXlOdW1iZXIoZW5kRGF0ZSkgKyAxKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIGFuIGFycmF5IG9mIHRoZSBkYXRlc1xuICAgKiBiZXR3ZWVuIHN0YXJ0RGF0ZSBhbmQgZW5kRGF0ZS5cbiAgICovXG4gIHB1YmxpYyBnZXREYXRlQXJyYXkoKVxuICB7XG4gICAgbGV0IGRheXMgPSBbXTtcbiAgICBsZXQgY3VycmVudCA9IG1vbWVudCh0aGlzLnN0YXJ0RGF0ZSk7XG4gICAgbGV0IGxhc3QgID0gbW9tZW50KHRoaXMuZW5kRGF0ZSk7XG4gICAgd2hpbGUoY3VycmVudCA8IGxhc3QpXG4gICAge1xuICAgICAgZGF5cy5wdXNoKGN1cnJlbnQudG9EYXRlKCkpO1xuICAgICAgY3VycmVudCA9IGN1cnJlbnQuYWRkKDEsICdkYXknKTtcbiAgICB9XG4gICAgcmV0dXJuIGRheXM7XG4gIH1cblxuICBwdWJsaWMgZ2V0V2Vla0FycmF5KClcbiAge1xuICAgIGxldCB3ZWVrcyA9IFtdO1xuICAgIGxldCBjdXJyZW50ID0gbW9tZW50KHRoaXMuc3RhcnREYXRlKTtcbiAgICBsZXQgbGFzdCAgPSBtb21lbnQodGhpcy5lbmREYXRlKTtcbiAgICB3aGlsZShjdXJyZW50IDwgbGFzdClcbiAgICB7XG4gICAgICB3ZWVrcy5wdXNoKHsgc3RhcnREYXRlOiBjdXJyZW50LnRvRGF0ZSgpLCBlbmREYXRlOiBjdXJyZW50LmFkZCg2LCAnZGF5JykudG9EYXRlKCkgfSk7XG4gICAgICBjdXJyZW50ID0gY3VycmVudC5hZGQoMSwgJ2RheScpO1xuICAgIH1cbiAgICByZXR1cm4gd2Vla3M7XG4gIH1cblxuICAvKipcbiAgICogUmV0dXJucyB0aGUgaW5kZXggb2YgYSBkYXRlIGluXG4gICAqIHRoZSBbc3RhcnREYXRlIC0gZW5kRGF0ZV0gcGVyaW9kLlxuICAgKiBAcGFyYW0gZGF0ZSB0aGUgZGF0ZSBmb3Igd2hpY2ggdG8gY29tcHV0ZSB0aGUgaW5kZXhcbiAgICovXG4gIHB1YmxpYyBnZXREYXlOdW1iZXIoZGF0ZSA6IERhdGUpXG4gIHtcbiAgICByZXR1cm4gbW9tZW50KGRhdGUpLmRpZmYobW9tZW50KHRoaXMuc3RhcnREYXRlKSwgJ2RheXMnKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSB0b3RhbCBudW1iZXIgb2YgZGF5c1xuICAgKiBpbiB0aGUgW3N0YXJ0RGF0ZSAtIGVuZERhdGVdIHBlcmlvZC5cbiAgICovXG4gIHB1YmxpYyBnZXRUb3RhbE51bWJlck9mRGF5cygpXG4gIHtcbiAgICByZXR1cm4gdGhpcy5nZXREYXlOdW1iZXIodGhpcy5lbmREYXRlKSArIDE7XG4gIH1cbn0iXX0=