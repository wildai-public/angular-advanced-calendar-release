/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var /** @type {?} */ moment = require('moment');
/**
 * @param {?} a
 * @param {?} b
 * @return {?}
 */
export function isSameDay(a, b) {
    return false || (a && b && a.getFullYear() === b.getFullYear() && a.getMonth() === b.getMonth() && a.getDate() === b.getDate());
}
/**
 * @param {?} date
 * @return {?}
 */
export function getWeekNumber(date) {
    return moment(date).week();
}
/**
 * @param {?} date
 * @return {?}
 */
export function getFirstDayOfMonth(date) {
    return moment(date).startOf('month').toDate();
}
/**
 * @param {?} date
 * @return {?}
 */
export function getFirstDayOfFirstWeekOfMonth(date) {
    return moment(date).startOf('month').startOf('isoWeek');
}
/**
 * @param {?} date
 * @return {?}
 */
export function getLastDayOfMonth(date) {
    return moment(date).endOf('month').toDate();
}
/**
 * @param {?} date
 * @return {?}
 */
export function geLastDayOfLastWeekOfMonth(date) {
    return moment(date).endOf('month').endOf('isoWeek').toDate();
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdXRpbHMvZGF0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEscUJBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Ozs7O0FBRS9CLE1BQU0sb0JBQW9CLENBQVEsRUFBRSxDQUFRO0lBRTFDLE1BQU0sQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7Q0FDakk7Ozs7O0FBRUQsTUFBTSx3QkFBd0IsSUFBVztJQUV2QyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0NBQzVCOzs7OztBQUVELE1BQU0sNkJBQTZCLElBQVc7SUFFNUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7Q0FFL0M7Ozs7O0FBRUQsTUFBTSx3Q0FBd0MsSUFBVztJQUV2RCxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7Q0FDekQ7Ozs7O0FBRUQsTUFBTSw0QkFBNEIsSUFBVztJQUUzQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztDQUM3Qzs7Ozs7QUFFRCxNQUFNLHFDQUFxQyxJQUFXO0lBRXBELE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztDQUM5RCIsInNvdXJjZXNDb250ZW50IjpbImxldCBtb21lbnQgPSByZXF1aXJlKCdtb21lbnQnKTtcblxuZXhwb3J0IGZ1bmN0aW9uIGlzU2FtZURheShhIDogRGF0ZSwgYiA6IERhdGUpIDogYm9vbGVhblxue1xuICByZXR1cm4gZmFsc2UgfHwgKGEgJiYgYiAmJiBhLmdldEZ1bGxZZWFyKCkgPT09IGIuZ2V0RnVsbFllYXIoKSAmJiBhLmdldE1vbnRoKCkgPT09IGIuZ2V0TW9udGgoKSAmJiBhLmdldERhdGUoKSA9PT0gYi5nZXREYXRlKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0V2Vla051bWJlcihkYXRlIDogRGF0ZSkgOiBudW1iZXJcbntcbiAgcmV0dXJuIG1vbWVudChkYXRlKS53ZWVrKCk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRGaXJzdERheU9mTW9udGgoZGF0ZSA6IERhdGUpIDogRGF0ZVxue1xuICByZXR1cm4gbW9tZW50KGRhdGUpLnN0YXJ0T2YoJ21vbnRoJykudG9EYXRlKCk7XG5cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEZpcnN0RGF5T2ZGaXJzdFdlZWtPZk1vbnRoKGRhdGUgOiBEYXRlKSA6IERhdGVcbntcbiAgcmV0dXJuIG1vbWVudChkYXRlKS5zdGFydE9mKCdtb250aCcpLnN0YXJ0T2YoJ2lzb1dlZWsnKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldExhc3REYXlPZk1vbnRoKGRhdGUgOiBEYXRlKSA6IERhdGVcbntcbiAgcmV0dXJuIG1vbWVudChkYXRlKS5lbmRPZignbW9udGgnKS50b0RhdGUoKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdlTGFzdERheU9mTGFzdFdlZWtPZk1vbnRoKGRhdGUgOiBEYXRlKSA6IERhdGVcbntcbiAgcmV0dXJuIG1vbWVudChkYXRlKS5lbmRPZignbW9udGgnKS5lbmRPZignaXNvV2VlaycpLnRvRGF0ZSgpO1xufSJdfQ==