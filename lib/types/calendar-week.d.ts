export interface CalendarWeekData<DataType> {
    startDate: Date;
    endDate: Date;
    calendarStartDate: Date;
    calendarEndDate: Date;
    events: DataType[][];
    dateAccessor: string;
}
