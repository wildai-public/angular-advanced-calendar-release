export interface NumericMap<T> {
    [id: number]: T;
}
export interface StringMap<DataType> {
    [name: string]: DataType;
}
