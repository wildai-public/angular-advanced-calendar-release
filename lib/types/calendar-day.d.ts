export interface CalendarDayData<DataType> {
    date: Date;
    calendarStartDate: Date;
    calendarEndDate: Date;
    events: DataType[];
    dateAccessor: string;
}
