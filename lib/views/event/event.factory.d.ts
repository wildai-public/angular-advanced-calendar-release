import { DynamicComponentFactory } from "../dynamic/dynamic.factory";
import { AbstractEventComponent } from "./abstract-event.component";
import { Type, ComponentFactoryResolver } from "@angular/core";
export declare abstract class EventComponentFactory<DataType> extends DynamicComponentFactory<DataType, AbstractEventComponent<DataType>> {
    constructor(componentFactoryResolver: ComponentFactoryResolver, componentType: Type<AbstractEventComponent<DataType>>);
}
