import { DynamicCrudComponent } from "../dynamic-crud/dynamic-crud.component";
import { CalendarDayData } from "../../types/calendar-day";
import { AbstractEventComponent } from "../event/abstract-event.component";
import { DynamicComponentFactory } from "../dynamic/dynamic.factory";
export declare abstract class AbstractDayCellComponent<DataType> extends DynamicCrudComponent<CalendarDayData<DataType>, DataType> {
    /**
     * View container storing the dynamic components
     */
    private eventViewContainer;
    private dynamicEventViewManager;
    /**
     * Method called to update the event views in the day cell
     */
    updateEventViews(eventComponentFactory: DynamicComponentFactory<DataType, AbstractEventComponent<DataType>>, idAccessor: string): void;
}
