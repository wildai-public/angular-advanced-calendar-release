import { DynamicComponentFactory } from '../dynamic/dynamic.factory';
import { CalendarDayData } from '../../types/calendar-day';
import { AbstractDayCellComponent } from './abstract-day-cell.component';
import { ComponentFactoryResolver, Type } from '@angular/core';
export declare abstract class DayCellComponentFactory<DataType> extends DynamicComponentFactory<CalendarDayData<DataType>, AbstractDayCellComponent<DataType>> {
    constructor(componentFactoryResolver: ComponentFactoryResolver, componentType: Type<AbstractDayCellComponent<DataType>>);
}
