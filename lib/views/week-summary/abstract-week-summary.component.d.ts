import { DynamicCrudComponent } from "../dynamic-crud/dynamic-crud.component";
import { CalendarWeekData } from "../../types/calendar-week";
export declare abstract class AbstractWeekSummaryComponent<DataType> extends DynamicCrudComponent<CalendarWeekData<DataType>, DataType> {
}
