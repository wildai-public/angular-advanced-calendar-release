import { DynamicComponentFactory } from "../dynamic/dynamic.factory";
import { CalendarWeekData } from "../../types/calendar-week";
import { AbstractWeekSummaryComponent } from "./abstract-week-summary.component";
import { ComponentRef, ViewContainerRef, ComponentFactoryResolver, Type } from "@angular/core";
import { StringMap } from "../../types/maps";
export declare abstract class WeekSummaryComponentFactory<DataType> extends DynamicComponentFactory<CalendarWeekData<DataType>, AbstractWeekSummaryComponent<DataType>> {
    constructor(componentFactoryResolver: ComponentFactoryResolver, componentType: Type<AbstractWeekSummaryComponent<DataType>>);
    insertDynamicComponent(container: ViewContainerRef, data: CalendarWeekData<DataType>, callbacks?: StringMap<Function>): ComponentRef<AbstractWeekSummaryComponent<DataType>>;
    private findWeekPositionInContainer(container, data);
}
