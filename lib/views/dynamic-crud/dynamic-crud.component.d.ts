import { DynamicComponent } from "../dynamic/dynamic.component";
export declare abstract class DynamicCrudComponent<DataType, CrudType> extends DynamicComponent<DataType> {
    subscribe(name: string, callback: Function): void;
    private deleteEmitter;
    protected onDelete(event: CrudType): void;
    private updateEmitter;
    protected onUpdate(event: CrudType): void;
    private addEmitter;
    protected onAdd(event: CrudType): void;
}
