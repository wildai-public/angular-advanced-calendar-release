import { SimpleChanges } from '@angular/core';
import { Event } from '../../types/event';
import { EventComponentFactory } from '../event/event.factory';
import { DayCellComponentFactory } from '../day-cell/day-cell.factory';
import { WeekSummaryComponentFactory } from '../week-summary/week-summary.fatory';
export declare const DAY_CELL_COMPONENT_FACTORY_PROVIDER = "DayCellComponentFactory";
export declare const EVENT_COMPONENT_FACTORY_PROVIDER = "EventComponentFactory";
export declare const WEEK_SUMMARY_COMPONENT_FACTORY = "WeekSummaryComponentFactory";
export declare class CalendarComponent {
    dayCellComponentFactory: DayCellComponentFactory<Event>;
    eventComponentFactory: EventComponentFactory<Event>;
    weekSummaryComponentFactory: WeekSummaryComponentFactory<Event>;
    /**
     * First date of the period to display in the calendar
     */
    private startDate;
    /**
     * Last date of the period to display in the calendar
     */
    private endDate;
    /**
     * Week days label to display in the header
     * (default is english).
     */
    headers: string[];
    /**
     * A list of events to display in the callendar.
     */
    private events;
    /**
     * The name of the date attribute to use
     * when displaying the events (default is 'date').
     */
    private dateAccessor;
    /**
     * The name of the id attribute to use
     * when displaying the events (default is 'id').
     */
    private idAccessor;
    constructor(dayCellComponentFactory: DayCellComponentFactory<Event>, eventComponentFactory: EventComponentFactory<Event>, weekSummaryComponentFactory: WeekSummaryComponentFactory<Event>);
    /**
     * View container storing the dynamic components
     */
    private dayGridContainer;
    private dynamicDayCellViewManager;
    private dynamicWeekSummaryViewManager;
    private dispatcher;
    private days;
    private weeks;
    private oldHash;
    ngDoCheck(): void;
    ngOnChanges(changes: SimpleChanges): void;
    /**
     * Old start and end date of the calendar.
     * Used to keep track of the changes in period
     * in order to update the view.
     */
    private oldStartDate;
    private oldEndDate;
    private updateCalendar();
    /**
     * Sorts events by date
     */
    private dispatchEvents();
    private updateDayCellViews();
    private updateWeekSummaryCellViews();
    /**
     * Triggers a day cell view internal update
     * @param component the component ref of the day cell to update
     */
    private updateDayCell(data, component);
    /**
     * Triggers a delete event emission
     * @param event the event to delete
     */
    private deleteEmitter;
    private onDelete(event);
    /**
     * Triggers an update event emission
     * @param event the event to update
     */
    private updateEmitter;
    private onUpdate(event);
    /**
     * Triggers an add event emission
     * @param event the event to add
     */
    private addEmitter;
    private onAdd(event);
}
