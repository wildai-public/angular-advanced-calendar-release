import { DynamicComponent } from "./dynamic.component";
import { ViewContainerRef } from "@angular/core";
import { DynamicComponentFactory } from "./dynamic.factory";
import { StringMap } from "../../types/maps";
export declare class DynamicComponentArrayManager<DataType, ComponentType extends DynamicComponent<DataType>> {
    /**
     * Stores the previous array state
     */
    private oldDataArray;
    clear(): void;
    /**
     * Updates, add or remove data views based
     * on the old data list
     */
    updateContainer(container: ViewContainerRef, factory: DynamicComponentFactory<DataType, ComponentType>, dataArray: DataType[], dataIdAccessor: string, dynamicComponentCallbacks?: StringMap<Function>, onAddView?: Function, onUpdateView?: Function, onDeleteView?: Function): void;
    /**
     * A numeric map used to store the dynamic views
     */
    private dynamicViewComponentMap;
    private removeDynamicComponent(container, data, dataIdAccessor, onDeleteView?);
    private addDynamicComponent(container, factory, data, dataIdAccessor, dynamicComponentCallbacks, onAddView?);
    /**
     * Updates an data view model
     * @param data the new data model to use
     */
    private updateDynamicComponent(container, data, dataIdAccessor, onUpdateView?);
}
