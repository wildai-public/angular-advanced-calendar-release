export declare abstract class DynamicComponent<DataType> {
    data: DataType;
    updateData(data: DataType): void;
    abstract subscribe(name: string, callback: Function): void;
}
