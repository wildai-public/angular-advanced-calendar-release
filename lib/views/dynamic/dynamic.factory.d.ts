import { DynamicComponent } from "./dynamic.component";
import { ComponentRef, ViewContainerRef, ComponentFactoryResolver, Type } from "@angular/core";
import { StringMap } from "../../types/maps";
export declare abstract class DynamicComponentFactory<DataType, ComponentType extends DynamicComponent<DataType>> {
    private readonly componentFactoryResolver;
    private factory;
    constructor(componentFactoryResolver: ComponentFactoryResolver, componentType: Type<ComponentType>);
    /**
     * Inserts a component in a view container
     * @param container the container in which we want to display this component
     * @param data the data of the component to display
     * @param callbacks a map of callbacks to subscribe to
     * @param index the position in the container where to add the component
     */
    insertDynamicComponent(container: ViewContainerRef, data: DataType, callbacks?: StringMap<Function>, index?: number): ComponentRef<ComponentType>;
}
