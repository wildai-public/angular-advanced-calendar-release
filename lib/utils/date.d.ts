export declare function isSameDay(a: Date, b: Date): boolean;
export declare function getWeekNumber(date: Date): number;
export declare function getFirstDayOfMonth(date: Date): Date;
export declare function getFirstDayOfFirstWeekOfMonth(date: Date): Date;
export declare function getLastDayOfMonth(date: Date): Date;
export declare function geLastDayOfLastWeekOfMonth(date: Date): Date;
