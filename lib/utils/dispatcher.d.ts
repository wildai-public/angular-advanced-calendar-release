/**
 * Dispatches events in an array of dates depending
 * based on the date of each event.
 */
export declare class EventDispatcher {
    readonly dateAccessor: string;
    readonly startDate: Date;
    readonly endDate: Date;
    private dispatch;
    /**
     * Creates a new event dispatcher
     * @param dateAccessor the date label to use in the events
     * @param startDate the start date of the period in which to dispatch events
     * @param endDate the end date of the period (inclusive) in which to dispatch events
     */
    constructor(dateAccessor: string, startDate: Date, endDate: Date);
    clear(): void;
    add(event: any): void;
    getEvents(date: Date): any[];
    getEventsBetween(startDate: Date, endDate: Date): any[][];
    /**
     * Returns an array of the dates
     * between startDate and endDate.
     */
    getDateArray(): any[];
    getWeekArray(): any[];
    /**
     * Returns the index of a date in
     * the [startDate - endDate] period.
     * @param date the date for which to compute the index
     */
    getDayNumber(date: Date): any;
    /**
     * Returns the total number of days
     * in the [startDate - endDate] period.
     */
    getTotalNumberOfDays(): any;
}
