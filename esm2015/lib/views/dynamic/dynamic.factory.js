/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
// unsupported: template constraints.
/**
 * @abstract
 * @template DataType, ComponentType
 */
export class DynamicComponentFactory {
    /**
     * @param {?} componentFactoryResolver
     * @param {?} componentType
     */
    constructor(componentFactoryResolver, componentType) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.factory = this.componentFactoryResolver.resolveComponentFactory(componentType);
    }
    /**
     * Inserts a component in a view container
     * @param {?} container the container in which we want to display this component
     * @param {?} data the data of the component to display
     * @param {?=} callbacks a map of callbacks to subscribe to
     * @param {?=} index the position in the container where to add the component
     * @return {?}
     */
    insertDynamicComponent(container, data, callbacks, index) {
        let /** @type {?} */ componentRef = container.createComponent(this.factory, index);
        let /** @type {?} */ instance = /** @type {?} */ (componentRef.instance);
        instance.updateData(data);
        if (callbacks) {
            for (let /** @type {?} */ name in callbacks) {
                instance.subscribe(name, callbacks[name]);
            }
        }
        return componentRef;
    }
}
function DynamicComponentFactory_tsickle_Closure_declarations() {
    /** @type {?} */
    DynamicComponentFactory.prototype.factory;
    /** @type {?} */
    DynamicComponentFactory.prototype.componentFactoryResolver;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy5mYWN0b3J5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci8iLCJzb3VyY2VzIjpbImxpYi92aWV3cy9keW5hbWljL2R5bmFtaWMuZmFjdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFJQSxNQUFNOzs7OztnQkFJZ0Msd0JBQW1ELEVBQUUsYUFBbUM7UUFBeEYsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUEyQjtRQUVyRixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyx1QkFBdUIsQ0FBQyxhQUFhLENBQUMsQ0FBQzs7Ozs7Ozs7OztJQVUvRSxzQkFBc0IsQ0FBQyxTQUE0QixFQUFFLElBQWUsRUFBRSxTQUErQixFQUFFLEtBQWU7UUFFM0gscUJBQUksWUFBWSxHQUFHLFNBQVMsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNsRSxxQkFBSSxRQUFRLHFCQUFrQixZQUFZLENBQUMsUUFBUSxDQUFBLENBQUM7UUFDcEQsUUFBUSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FDZCxDQUFDO1lBQ0MsR0FBRyxDQUFDLENBQUMscUJBQUksSUFBSSxJQUFJLFNBQVMsQ0FBQyxDQUMzQixDQUFDO2dCQUNDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQzNDO1NBQ0Y7UUFDRCxNQUFNLENBQUMsWUFBWSxDQUFDOztDQUV2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IER5bmFtaWNDb21wb25lbnQgfSBmcm9tIFwiLi9keW5hbWljLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQ29tcG9uZW50UmVmLCBWaWV3Q29udGFpbmVyUmVmLCBDb21wb25lbnRGYWN0b3J5LCBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIFR5cGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgU3RyaW5nTWFwIH0gZnJvbSBcIi4uLy4uL3R5cGVzL21hcHNcIjtcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIER5bmFtaWNDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlLCBDb21wb25lbnRUeXBlIGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudDxEYXRhVHlwZT4+XG57XG4gIHByaXZhdGUgZmFjdG9yeSA6IENvbXBvbmVudEZhY3Rvcnk8Q29tcG9uZW50VHlwZT47XG5cbiAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgcmVhZG9ubHkgY29tcG9uZW50RmFjdG9yeVJlc29sdmVyIDogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb21wb25lbnRUeXBlIDogVHlwZTxDb21wb25lbnRUeXBlPilcbiAge1xuICAgIHRoaXMuZmFjdG9yeSA9IHRoaXMuY29tcG9uZW50RmFjdG9yeVJlc29sdmVyLnJlc29sdmVDb21wb25lbnRGYWN0b3J5KGNvbXBvbmVudFR5cGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIEluc2VydHMgYSBjb21wb25lbnQgaW4gYSB2aWV3IGNvbnRhaW5lclxuICAgKiBAcGFyYW0gY29udGFpbmVyIHRoZSBjb250YWluZXIgaW4gd2hpY2ggd2Ugd2FudCB0byBkaXNwbGF5IHRoaXMgY29tcG9uZW50XG4gICAqIEBwYXJhbSBkYXRhIHRoZSBkYXRhIG9mIHRoZSBjb21wb25lbnQgdG8gZGlzcGxheVxuICAgKiBAcGFyYW0gY2FsbGJhY2tzIGEgbWFwIG9mIGNhbGxiYWNrcyB0byBzdWJzY3JpYmUgdG9cbiAgICogQHBhcmFtIGluZGV4IHRoZSBwb3NpdGlvbiBpbiB0aGUgY29udGFpbmVyIHdoZXJlIHRvIGFkZCB0aGUgY29tcG9uZW50XG4gICAqL1xuICBwdWJsaWMgaW5zZXJ0RHluYW1pY0NvbXBvbmVudChjb250YWluZXIgOiBWaWV3Q29udGFpbmVyUmVmLCBkYXRhIDogRGF0YVR5cGUsIGNhbGxiYWNrcz86IFN0cmluZ01hcDxGdW5jdGlvbj4sIGluZGV4PyA6IG51bWJlcikgOiBDb21wb25lbnRSZWY8Q29tcG9uZW50VHlwZT5cbiAge1xuICAgIGxldCBjb21wb25lbnRSZWYgPSBjb250YWluZXIuY3JlYXRlQ29tcG9uZW50KHRoaXMuZmFjdG9yeSwgaW5kZXgpO1xuICAgIGxldCBpbnN0YW5jZSA9IDxDb21wb25lbnRUeXBlPmNvbXBvbmVudFJlZi5pbnN0YW5jZTtcbiAgICBpbnN0YW5jZS51cGRhdGVEYXRhKGRhdGEpO1xuICAgIGlmIChjYWxsYmFja3MpXG4gICAge1xuICAgICAgZm9yIChsZXQgbmFtZSBpbiBjYWxsYmFja3MpXG4gICAgICB7XG4gICAgICAgIGluc3RhbmNlLnN1YnNjcmliZShuYW1lLCBjYWxsYmFja3NbbmFtZV0pO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gY29tcG9uZW50UmVmO1xuICB9XG59Il19