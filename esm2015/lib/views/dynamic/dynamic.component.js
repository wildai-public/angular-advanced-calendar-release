/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Input } from "@angular/core";
/**
 * @abstract
 * @template DataType
 */
export class DynamicComponent {
    /**
     * @param {?} data
     * @return {?}
     */
    updateData(data) {
        this.data = data;
    }
}
DynamicComponent.propDecorators = {
    data: [{ type: Input, args: ['data',] }]
};
function DynamicComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    DynamicComponent.prototype.data;
    /**
     * @abstract
     * @param {?} name
     * @param {?} callback
     * @return {?}
     */
    DynamicComponent.prototype.subscribe = function (name, callback) { };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hbmd1bGFyLWFkdmFuY2VkLWNhbGVuZGFyLyIsInNvdXJjZXMiOlsibGliL3ZpZXdzL2R5bmFtaWMvZHluYW1pYy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7O0FBRXRDLE1BQU07Ozs7O0lBS0csVUFBVSxDQUFDLElBQWU7UUFFL0IsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7Ozs7bUJBTGxCLEtBQUssU0FBQyxNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5wdXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRHluYW1pY0NvbXBvbmVudDxEYXRhVHlwZT5cbntcbiAgQElucHV0KCdkYXRhJylcbiAgcHVibGljIGRhdGEgOiBEYXRhVHlwZTtcbiAgXG4gIHB1YmxpYyB1cGRhdGVEYXRhKGRhdGEgOiBEYXRhVHlwZSlcbiAge1xuICAgIHRoaXMuZGF0YSA9IGRhdGE7XG4gIH1cblxuICBwdWJsaWMgYWJzdHJhY3Qgc3Vic2NyaWJlKG5hbWUgOiBzdHJpbmcsIGNhbGxiYWNrIDogRnVuY3Rpb24pIDogdm9pZDtcbn0iXX0=