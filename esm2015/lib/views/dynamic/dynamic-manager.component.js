/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
// unsupported: template constraints.
/**
 * @template DataType, ComponentType
 */
export class DynamicComponentArrayManager {
    constructor() {
        /**
         * Stores the previous array state
         */
        this.oldDataArray = [];
        /**
         * A numeric map used to store the dynamic views
         */
        this.dynamicViewComponentMap = {};
    }
    /**
     * @return {?}
     */
    clear() {
        for (let /** @type {?} */ id in this.dynamicViewComponentMap) {
            if (this.dynamicViewComponentMap[id]) {
                this.dynamicViewComponentMap[id].destroy();
                delete this.dynamicViewComponentMap[id];
            }
        }
        this.oldDataArray = [];
    }
    /**
     * Updates, add or remove data views based
     * on the old data list
     * @param {?} container
     * @param {?} factory
     * @param {?} dataArray
     * @param {?} dataIdAccessor
     * @param {?=} dynamicComponentCallbacks
     * @param {?=} onAddView
     * @param {?=} onUpdateView
     * @param {?=} onDeleteView
     * @return {?}
     */
    updateContainer(container, factory, dataArray, dataIdAccessor, dynamicComponentCallbacks, onAddView, onUpdateView, onDeleteView) {
        if (!container) {
            throw 'child container (#childContainer) could not be found in the container view';
        }
        for (let /** @type {?} */ data of dataArray) {
            let /** @type {?} */ alreadyPresent = false;
            for (let /** @type {?} */ oldData of this.oldDataArray) {
                if (data[dataIdAccessor] === oldData[dataIdAccessor]) {
                    alreadyPresent = true;
                    break;
                }
            }
            if (alreadyPresent) {
                this.updateDynamicComponent(container, data, dataIdAccessor, onUpdateView);
            }
            else {
                this.addDynamicComponent(container, factory, data, dataIdAccessor, dynamicComponentCallbacks, onAddView);
            }
        }
        for (let /** @type {?} */ oldData of this.oldDataArray) {
            let /** @type {?} */ stillPresent = false;
            for (let /** @type {?} */ data of dataArray) {
                if (data[dataIdAccessor] === oldData[dataIdAccessor]) {
                    stillPresent = true;
                    break;
                }
            }
            if (!stillPresent) {
                this.removeDynamicComponent(container, oldData, dataIdAccessor, onDeleteView);
            }
        }
        this.oldDataArray = dataArray;
    }
    /**
     * @param {?} container
     * @param {?} data
     * @param {?} dataIdAccessor
     * @param {?=} onDeleteView
     * @return {?}
     */
    removeDynamicComponent(container, data, dataIdAccessor, onDeleteView) {
        if (typeof data[dataIdAccessor] !== 'number') {
            throw 'id (' + dataIdAccessor + ') not found in data structure (delete dynamic component method)';
        }
        else if (!this.dynamicViewComponentMap[data[dataIdAccessor]]) {
            throw 'dynamic view not found in the dynamic view container (delete dynamic component method)';
        }
        else {
            this.dynamicViewComponentMap[data[dataIdAccessor]].destroy();
            delete this.dynamicViewComponentMap[data[dataIdAccessor]];
            if (onDeleteView)
                onDeleteView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
        }
    }
    /**
     * @param {?} container
     * @param {?} factory
     * @param {?} data
     * @param {?} dataIdAccessor
     * @param {?} dynamicComponentCallbacks
     * @param {?=} onAddView
     * @return {?}
     */
    addDynamicComponent(container, factory, data, dataIdAccessor, dynamicComponentCallbacks, onAddView) {
        if (typeof data[dataIdAccessor] !== 'number') {
            throw 'id (' + dataIdAccessor + ') not found in data structure (add dynamic component method)';
        }
        else if (this.dynamicViewComponentMap[data[dataIdAccessor]]) {
            throw 'dynamic view already present in the dynamic view container (add dynamic component method)';
        }
        else {
            let /** @type {?} */ componentRef = factory.insertDynamicComponent(container, data, dynamicComponentCallbacks);
            this.dynamicViewComponentMap[data[dataIdAccessor]] = componentRef;
            if (onAddView)
                onAddView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
        }
    }
    /**
     * Updates an data view model
     * @param {?} container
     * @param {?} data the new data model to use
     * @param {?} dataIdAccessor
     * @param {?=} onUpdateView
     * @return {?}
     */
    updateDynamicComponent(container, data, dataIdAccessor, onUpdateView) {
        if (typeof data[dataIdAccessor] !== 'number') {
            throw 'id (' + dataIdAccessor + ') not found in data structure (add dynamic component method)';
        }
        else if (!this.dynamicViewComponentMap[data[dataIdAccessor]]) {
            throw 'dynamic view not found in the dynamic view container (add dynamic component method)';
        }
        else {
            this.dynamicViewComponentMap[data[dataIdAccessor]].instance.updateData(data);
            if (onUpdateView)
                onUpdateView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
        }
    }
}
function DynamicComponentArrayManager_tsickle_Closure_declarations() {
    /**
     * Stores the previous array state
     * @type {?}
     */
    DynamicComponentArrayManager.prototype.oldDataArray;
    /**
     * A numeric map used to store the dynamic views
     * @type {?}
     */
    DynamicComponentArrayManager.prototype.dynamicViewComponentMap;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy1tYW5hZ2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdmlld3MvZHluYW1pYy9keW5hbWljLW1hbmFnZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBS0EsTUFBTTs7Ozs7NEJBTWdDLEVBQUU7Ozs7dUNBOEVzQyxFQUFFOzs7OztJQTVFdkUsS0FBSztRQUVWLEdBQUcsQ0FBQyxDQUFDLHFCQUFJLEVBQUUsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FDNUMsQ0FBQztZQUNDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUNyQyxDQUFDO2dCQUNDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDM0MsT0FBTyxJQUFJLENBQUMsdUJBQXVCLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDekM7U0FDRjtRQUNELElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7SUFPbEIsZUFBZSxDQUNwQixTQUE0QixFQUM1QixPQUEwRCxFQUMxRCxTQUFzQixFQUN0QixjQUF1QixFQUN2Qix5QkFBZ0QsRUFDaEQsU0FBcUIsRUFDckIsWUFBd0IsRUFDeEIsWUFBd0I7UUFHeEIsRUFBRSxDQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FDaEIsQ0FBQztZQUNDLE1BQU0sNEVBQTRFLENBQUE7U0FDbkY7UUFDRCxHQUFHLENBQUMsQ0FBQyxxQkFBSSxJQUFJLElBQUksU0FBUyxDQUFDLENBQzNCLENBQUM7WUFDQyxxQkFBSSxjQUFjLEdBQUcsS0FBSyxDQUFDO1lBQzNCLEdBQUcsQ0FBQyxDQUFDLHFCQUFJLE9BQU8sSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQ3RDLENBQUM7Z0JBQ0MsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUNyRCxDQUFDO29CQUNDLGNBQWMsR0FBRyxJQUFJLENBQUM7b0JBQ3RCLEtBQUssQ0FBQztpQkFDUDthQUNGO1lBQ0QsRUFBRSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQ25CLENBQUM7Z0JBQ0MsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFDO2FBQzVFO1lBQ0QsSUFBSSxDQUNKLENBQUM7Z0JBQ0MsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSx5QkFBeUIsRUFBRSxTQUFTLENBQUMsQ0FBQzthQUMxRztTQUNGO1FBRUQsR0FBRyxDQUFDLENBQUMscUJBQUksT0FBTyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FDdEMsQ0FBQztZQUNDLHFCQUFJLFlBQVksR0FBRyxLQUFLLENBQUM7WUFDekIsR0FBRyxDQUFDLENBQUMscUJBQUksSUFBSSxJQUFJLFNBQVMsQ0FBQyxDQUMzQixDQUFDO2dCQUNDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FDckQsQ0FBQztvQkFDQyxZQUFZLEdBQUcsSUFBSSxDQUFDO29CQUNwQixLQUFLLENBQUM7aUJBQ1A7YUFDRjtZQUNELEVBQUUsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQ2xCLENBQUM7Z0JBQ0MsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFDO2FBQy9FO1NBQ0Y7UUFFRCxJQUFJLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQzs7Ozs7Ozs7O0lBUXhCLHNCQUFzQixDQUFDLFNBQTRCLEVBQUUsSUFBZSxFQUFFLGNBQXVCLEVBQUUsWUFBd0I7UUFFN0gsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssUUFBUSxDQUFDLENBQzdDLENBQUM7WUFDQyxNQUFNLE1BQU0sR0FBRyxjQUFjLEdBQUcsaUVBQWlFLENBQUM7U0FDbkc7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FDN0QsQ0FBQztZQUNDLE1BQU0sd0ZBQXdGLENBQUM7U0FDaEc7UUFDRCxJQUFJLENBQ0osQ0FBQztZQUNDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUM3RCxPQUFPLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUMxRCxFQUFFLENBQUMsQ0FBQyxZQUFZLENBQUM7Z0JBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUMxRjs7Ozs7Ozs7Ozs7SUFHSyxtQkFBbUIsQ0FDekIsU0FBNEIsRUFDNUIsT0FBMEQsRUFDMUQsSUFBZSxFQUNmLGNBQXVCLEVBQ3ZCLHlCQUErQyxFQUMvQyxTQUFxQjtRQUdyQixFQUFFLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FDN0MsQ0FBQztZQUNDLE1BQU0sTUFBTSxHQUFHLGNBQWMsR0FBRyw4REFBOEQsQ0FBQztTQUNoRztRQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FDNUQsQ0FBQztZQUNDLE1BQU0sMkZBQTJGLENBQUM7U0FDbkc7UUFDRCxJQUFJLENBQ0osQ0FBQztZQUNDLHFCQUFJLFlBQVksR0FBRyxPQUFPLENBQUMsc0JBQXNCLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSx5QkFBeUIsQ0FBQyxDQUFDO1lBQzlGLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsR0FBRyxZQUFZLENBQUM7WUFDbEUsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDO2dCQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDcEY7Ozs7Ozs7Ozs7SUFPSyxzQkFBc0IsQ0FBQyxTQUE0QixFQUFFLElBQWUsRUFBRSxjQUF1QixFQUFFLFlBQXdCO1FBRTdILEVBQUUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLFFBQVEsQ0FBQyxDQUM3QyxDQUFDO1lBQ0MsTUFBTSxNQUFNLEdBQUcsY0FBYyxHQUFHLDhEQUE4RCxDQUFDO1NBQ2hHO1FBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQzdELENBQUM7WUFDQyxNQUFNLHFGQUFxRixDQUFDO1NBQzdGO1FBQ0QsSUFBSSxDQUNKLENBQUM7WUFDQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM3RSxFQUFFLENBQUMsQ0FBQyxZQUFZLENBQUM7Z0JBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUMxRjs7Q0FFSiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IER5bmFtaWNDb21wb25lbnQgfSBmcm9tIFwiLi9keW5hbWljLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgVmlld0NvbnRhaW5lclJlZiwgQ29tcG9uZW50UmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IER5bmFtaWNDb21wb25lbnRGYWN0b3J5IH0gZnJvbSBcIi4vZHluYW1pYy5mYWN0b3J5XCI7XG5pbXBvcnQgeyBTdHJpbmdNYXAsIE51bWVyaWNNYXAgfSBmcm9tIFwiLi4vLi4vdHlwZXMvbWFwc1wiO1xuXG5leHBvcnQgY2xhc3MgRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlcjxEYXRhVHlwZSwgQ29tcG9uZW50VHlwZSBleHRlbmRzIER5bmFtaWNDb21wb25lbnQ8RGF0YVR5cGU+Plxue1xuXG4gIC8qKlxuICAgKiBTdG9yZXMgdGhlIHByZXZpb3VzIGFycmF5IHN0YXRlXG4gICAqL1xuICBwcml2YXRlIG9sZERhdGFBcnJheSA6IERhdGFUeXBlW10gPSBbXTtcbiAgXG4gIHB1YmxpYyBjbGVhcigpXG4gIHtcbiAgICBmb3IgKGxldCBpZCBpbiB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwKVxuICAgIHtcbiAgICAgIGlmICh0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2lkXSlcbiAgICAgIHtcbiAgICAgICAgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtpZF0uZGVzdHJveSgpO1xuICAgICAgICBkZWxldGUgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtpZF07XG4gICAgICB9XG4gICAgfVxuICAgIHRoaXMub2xkRGF0YUFycmF5ID0gW107XG4gIH1cblxuICAvKipcbiAgICogVXBkYXRlcywgYWRkIG9yIHJlbW92ZSBkYXRhIHZpZXdzIGJhc2VkXG4gICAqIG9uIHRoZSBvbGQgZGF0YSBsaXN0XG4gICAqL1xuICBwdWJsaWMgdXBkYXRlQ29udGFpbmVyKFxuICAgIGNvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWYsXG4gICAgZmFjdG9yeSA6IER5bmFtaWNDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlLCBDb21wb25lbnRUeXBlPixcbiAgICBkYXRhQXJyYXkgOiBEYXRhVHlwZVtdLFxuICAgIGRhdGFJZEFjY2Vzc29yIDogc3RyaW5nLFxuICAgIGR5bmFtaWNDb21wb25lbnRDYWxsYmFja3M/IDogU3RyaW5nTWFwPEZ1bmN0aW9uPixcbiAgICBvbkFkZFZpZXc/IDogRnVuY3Rpb24sXG4gICAgb25VcGRhdGVWaWV3PyA6IEZ1bmN0aW9uLFxuICAgIG9uRGVsZXRlVmlldz8gOiBGdW5jdGlvblxuICApIDogdm9pZFxuICB7XG4gICAgaWYgICghY29udGFpbmVyKVxuICAgIHtcbiAgICAgIHRocm93ICdjaGlsZCBjb250YWluZXIgKCNjaGlsZENvbnRhaW5lcikgY291bGQgbm90IGJlIGZvdW5kIGluIHRoZSBjb250YWluZXIgdmlldydcbiAgICB9XG4gICAgZm9yIChsZXQgZGF0YSBvZiBkYXRhQXJyYXkpXG4gICAge1xuICAgICAgbGV0IGFscmVhZHlQcmVzZW50ID0gZmFsc2U7XG4gICAgICBmb3IgKGxldCBvbGREYXRhIG9mIHRoaXMub2xkRGF0YUFycmF5KVxuICAgICAge1xuICAgICAgICBpZiAoZGF0YVtkYXRhSWRBY2Nlc3Nvcl0gPT09IG9sZERhdGFbZGF0YUlkQWNjZXNzb3JdKVxuICAgICAgICB7XG4gICAgICAgICAgYWxyZWFkeVByZXNlbnQgPSB0cnVlO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAoYWxyZWFkeVByZXNlbnQpXG4gICAgICB7XG4gICAgICAgIHRoaXMudXBkYXRlRHluYW1pY0NvbXBvbmVudChjb250YWluZXIsIGRhdGEsIGRhdGFJZEFjY2Vzc29yLCBvblVwZGF0ZVZpZXcpO1xuICAgICAgfVxuICAgICAgZWxzZVxuICAgICAge1xuICAgICAgICB0aGlzLmFkZER5bmFtaWNDb21wb25lbnQoY29udGFpbmVyLCBmYWN0b3J5LCBkYXRhLCBkYXRhSWRBY2Nlc3NvciwgZHluYW1pY0NvbXBvbmVudENhbGxiYWNrcywgb25BZGRWaWV3KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmb3IgKGxldCBvbGREYXRhIG9mIHRoaXMub2xkRGF0YUFycmF5KVxuICAgIHtcbiAgICAgIGxldCBzdGlsbFByZXNlbnQgPSBmYWxzZTtcbiAgICAgIGZvciAobGV0IGRhdGEgb2YgZGF0YUFycmF5KVxuICAgICAge1xuICAgICAgICBpZiAoZGF0YVtkYXRhSWRBY2Nlc3Nvcl0gPT09IG9sZERhdGFbZGF0YUlkQWNjZXNzb3JdKVxuICAgICAgICB7XG4gICAgICAgICAgc3RpbGxQcmVzZW50ID0gdHJ1ZTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgaWYgKCFzdGlsbFByZXNlbnQpXG4gICAgICB7XG4gICAgICAgIHRoaXMucmVtb3ZlRHluYW1pY0NvbXBvbmVudChjb250YWluZXIsIG9sZERhdGEsIGRhdGFJZEFjY2Vzc29yLCBvbkRlbGV0ZVZpZXcpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMub2xkRGF0YUFycmF5ID0gZGF0YUFycmF5O1xuICB9XG5cbiAgLyoqXG4gICAqIEEgbnVtZXJpYyBtYXAgdXNlZCB0byBzdG9yZSB0aGUgZHluYW1pYyB2aWV3c1xuICAgKi9cbiAgcHJpdmF0ZSBkeW5hbWljVmlld0NvbXBvbmVudE1hcCA6IE51bWVyaWNNYXA8Q29tcG9uZW50UmVmPENvbXBvbmVudFR5cGU+PiA9IHt9O1xuXG4gIHByaXZhdGUgcmVtb3ZlRHluYW1pY0NvbXBvbmVudChjb250YWluZXIgOiBWaWV3Q29udGFpbmVyUmVmLCBkYXRhIDogRGF0YVR5cGUsIGRhdGFJZEFjY2Vzc29yIDogc3RyaW5nLCBvbkRlbGV0ZVZpZXc/IDogRnVuY3Rpb24pIDogdm9pZFxuICB7XG4gICAgaWYgKHR5cGVvZiBkYXRhW2RhdGFJZEFjY2Vzc29yXSAhPT0gJ251bWJlcicpXG4gICAge1xuICAgICAgdGhyb3cgJ2lkICgnICsgZGF0YUlkQWNjZXNzb3IgKyAnKSBub3QgZm91bmQgaW4gZGF0YSBzdHJ1Y3R1cmUgKGRlbGV0ZSBkeW5hbWljIGNvbXBvbmVudCBtZXRob2QpJztcbiAgICB9XG4gICAgZWxzZSBpZiAoIXRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbZGF0YVtkYXRhSWRBY2Nlc3Nvcl1dKVxuICAgIHtcbiAgICAgIHRocm93ICdkeW5hbWljIHZpZXcgbm90IGZvdW5kIGluIHRoZSBkeW5hbWljIHZpZXcgY29udGFpbmVyIChkZWxldGUgZHluYW1pYyBjb21wb25lbnQgbWV0aG9kKSc7XG4gICAgfVxuICAgIGVsc2VcbiAgICB7XG4gICAgICB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXS5kZXN0cm95KCk7XG4gICAgICBkZWxldGUgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV07XG4gICAgICBpZiAob25EZWxldGVWaWV3KSBvbkRlbGV0ZVZpZXcoZGF0YSwgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0pO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgYWRkRHluYW1pY0NvbXBvbmVudChcbiAgICBjb250YWluZXIgOiBWaWV3Q29udGFpbmVyUmVmLFxuICAgIGZhY3RvcnkgOiBEeW5hbWljQ29tcG9uZW50RmFjdG9yeTxEYXRhVHlwZSwgQ29tcG9uZW50VHlwZT4sXG4gICAgZGF0YSA6IERhdGFUeXBlLFxuICAgIGRhdGFJZEFjY2Vzc29yIDogc3RyaW5nLFxuICAgIGR5bmFtaWNDb21wb25lbnRDYWxsYmFja3MgOiBTdHJpbmdNYXA8RnVuY3Rpb24+LFxuICAgIG9uQWRkVmlldz8gOiBGdW5jdGlvblxuICApIDogdm9pZFxuICB7XG4gICAgaWYgKHR5cGVvZiBkYXRhW2RhdGFJZEFjY2Vzc29yXSAhPT0gJ251bWJlcicpXG4gICAge1xuICAgICAgdGhyb3cgJ2lkICgnICsgZGF0YUlkQWNjZXNzb3IgKyAnKSBub3QgZm91bmQgaW4gZGF0YSBzdHJ1Y3R1cmUgKGFkZCBkeW5hbWljIGNvbXBvbmVudCBtZXRob2QpJztcbiAgICB9XG4gICAgZWxzZSBpZiAodGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0pXG4gICAge1xuICAgICAgdGhyb3cgJ2R5bmFtaWMgdmlldyBhbHJlYWR5IHByZXNlbnQgaW4gdGhlIGR5bmFtaWMgdmlldyBjb250YWluZXIgKGFkZCBkeW5hbWljIGNvbXBvbmVudCBtZXRob2QpJztcbiAgICB9XG4gICAgZWxzZVxuICAgIHtcbiAgICAgIGxldCBjb21wb25lbnRSZWYgPSBmYWN0b3J5Lmluc2VydER5bmFtaWNDb21wb25lbnQoY29udGFpbmVyLCBkYXRhLCBkeW5hbWljQ29tcG9uZW50Q2FsbGJhY2tzKTtcbiAgICAgIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbZGF0YVtkYXRhSWRBY2Nlc3Nvcl1dID0gY29tcG9uZW50UmVmO1xuICAgICAgaWYgKG9uQWRkVmlldykgb25BZGRWaWV3KGRhdGEsIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbZGF0YVtkYXRhSWRBY2Nlc3Nvcl1dKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogVXBkYXRlcyBhbiBkYXRhIHZpZXcgbW9kZWxcbiAgICogQHBhcmFtIGRhdGEgdGhlIG5ldyBkYXRhIG1vZGVsIHRvIHVzZVxuICAgKi9cbiAgcHJpdmF0ZSB1cGRhdGVEeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWYsIGRhdGEgOiBEYXRhVHlwZSwgZGF0YUlkQWNjZXNzb3IgOiBzdHJpbmcsIG9uVXBkYXRlVmlldz8gOiBGdW5jdGlvbikgOiB2b2lkXG4gIHtcbiAgICBpZiAodHlwZW9mIGRhdGFbZGF0YUlkQWNjZXNzb3JdICE9PSAnbnVtYmVyJylcbiAgICB7XG4gICAgICB0aHJvdyAnaWQgKCcgKyBkYXRhSWRBY2Nlc3NvciArICcpIG5vdCBmb3VuZCBpbiBkYXRhIHN0cnVjdHVyZSAoYWRkIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlIGlmICghdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0pXG4gICAge1xuICAgICAgdGhyb3cgJ2R5bmFtaWMgdmlldyBub3QgZm91bmQgaW4gdGhlIGR5bmFtaWMgdmlldyBjb250YWluZXIgKGFkZCBkeW5hbWljIGNvbXBvbmVudCBtZXRob2QpJztcbiAgICB9XG4gICAgZWxzZVxuICAgIHtcbiAgICAgIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbZGF0YVtkYXRhSWRBY2Nlc3Nvcl1dLmluc3RhbmNlLnVwZGF0ZURhdGEoZGF0YSk7XG4gICAgICBpZiAob25VcGRhdGVWaWV3KSBvblVwZGF0ZVZpZXcoZGF0YSwgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0pO1xuICAgIH1cbiAgfVxufSJdfQ==