/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, Output, EventEmitter, ViewChild, ViewContainerRef, Inject, Optional } from '@angular/core';
import { getWeekNumber } from '../../utils/date';
import { EventDispatcher } from '../../utils/dispatcher';
import { EventComponentFactory } from '../event/event.factory';
import { DayCellComponentFactory } from '../day-cell/day-cell.factory';
import { DynamicComponentArrayManager } from '../dynamic/dynamic-manager.component';
import { WeekSummaryComponentFactory } from '../week-summary/week-summary.fatory';
let /** @type {?} */ moment = require('moment');
let /** @type {?} */ hash = require('object-hash');
export const /** @type {?} */ DAY_CELL_COMPONENT_FACTORY_PROVIDER = 'DayCellComponentFactory';
export const /** @type {?} */ EVENT_COMPONENT_FACTORY_PROVIDER = 'EventComponentFactory';
export const /** @type {?} */ WEEK_SUMMARY_COMPONENT_FACTORY = 'WeekSummaryComponentFactory';
export class CalendarComponent {
    /**
     * @param {?} dayCellComponentFactory
     * @param {?} eventComponentFactory
     * @param {?} weekSummaryComponentFactory
     */
    constructor(dayCellComponentFactory, eventComponentFactory, weekSummaryComponentFactory) {
        this.dayCellComponentFactory = dayCellComponentFactory;
        this.eventComponentFactory = eventComponentFactory;
        this.weekSummaryComponentFactory = weekSummaryComponentFactory;
        /**
         * Week days label to display in the header
         * (default is english).
         */
        this.headers = [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday',
        ];
        /**
         * A list of events to display in the callendar.
         */
        this.events = [];
        /**
         * The name of the date attribute to use
         * when displaying the events (default is 'date').
         */
        this.dateAccessor = 'date';
        /**
         * The name of the id attribute to use
         * when displaying the events (default is 'id').
         */
        this.idAccessor = 'id';
        this.dynamicDayCellViewManager = new DynamicComponentArrayManager();
        this.dynamicWeekSummaryViewManager = new DynamicComponentArrayManager();
        this.oldHash = '';
        /**
         * Triggers a delete event emission
         * @param event the event to delete
         */
        this.deleteEmitter = new EventEmitter();
        /**
         * Triggers an update event emission
         * @param event the event to update
         */
        this.updateEmitter = new EventEmitter();
        /**
         * Triggers an add event emission
         * @param event the event to add
         */
        this.addEmitter = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngDoCheck() {
        let /** @type {?} */ newHash = hash.sha1(this.events);
        if (newHash !== this.oldHash) {
            this.updateCalendar();
            this.oldHash = newHash;
        }
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes["startDate"] || changes["endDate"] || changes["dateAccessor"]) {
            let /** @type {?} */ startDate = (changes["startDate"]) ? changes["startDate"].currentValue : this.startDate;
            let /** @type {?} */ endDate = (changes["endDate"]) ? changes["endDate"].currentValue : this.endDate;
            let /** @type {?} */ dateAccessor = (changes["dateAccessor"]) ? changes["dateAccessor"].currentValue : this.dateAccessor;
            this.dispatcher = new EventDispatcher(dateAccessor, startDate, endDate);
            this.updateCalendar();
        }
    }
    /**
     * @return {?}
     */
    updateCalendar() {
        if (!this.dayCellComponentFactory) {
            this.days = [];
            throw 'day cell component factory (day-component-factory) was not set!';
        }
        else if (!this.dayGridContainer) {
            this.days = [];
            throw 'day grid (dayGrid) not found in template !';
        }
        else {
            this.dispatchEvents();
            this.updateDayCellViews();
            if (this.weekSummaryComponentFactory) {
                this.updateWeekSummaryCellViews();
            }
        }
    }
    /**
     * Sorts events by date
     * @return {?}
     */
    dispatchEvents() {
        this.dispatcher.clear();
        for (let /** @type {?} */ event of this.events) {
            this.dispatcher.add(event);
        }
        this.days = this.dispatcher.getDateArray().map(date => ({
            id: this.dispatcher.getDayNumber(date),
            date: date,
            calendarStartDate: this.startDate,
            calendarEndDate: this.endDate,
            events: this.dispatcher.getEvents(date),
            dateAccessor: this.dateAccessor,
        }));
    }
    /**
     * @return {?}
     */
    updateDayCellViews() {
        this.dynamicDayCellViewManager.updateContainer(this.dayGridContainer, this.dayCellComponentFactory, this.days, 'id', {
            add: this.onAdd.bind(this),
            update: this.onUpdate.bind(this),
            delete: this.onDelete.bind(this)
        }, this.updateDayCell.bind(this), this.updateDayCell.bind(this), this.updateDayCell.bind(this));
    }
    /**
     * @return {?}
     */
    updateWeekSummaryCellViews() {
        this.weeks = this.dispatcher.getWeekArray().map(week => ({
            id: getWeekNumber(week.startDate),
            startDate: week.startDate,
            endDate: week.endDate,
            calendarStartDate: this.startDate,
            calendarEndDate: this.endDate,
            events: this.dispatcher.getEventsBetween(week.startDate, week.endDate),
            dateAccessor: this.dateAccessor,
        }));
        if (this.startDate !== this.oldStartDate || this.endDate !== this.oldEndDate) {
            this.dynamicWeekSummaryViewManager.clear();
            this.oldStartDate = this.startDate;
            this.oldEndDate = this.endDate;
        }
        this.dynamicWeekSummaryViewManager.updateContainer(this.dayGridContainer, this.weekSummaryComponentFactory, this.weeks, 'id', {
            add: this.onAdd.bind(this),
            update: this.onUpdate.bind(this),
            delete: this.onDelete.bind(this)
        });
    }
    /**
     * Triggers a day cell view internal update
     * @param {?} data
     * @param {?} component the component ref of the day cell to update
     * @return {?}
     */
    updateDayCell(data, component) {
        let /** @type {?} */ instance = /** @type {?} */ (component.instance);
        instance.updateEventViews(this.eventComponentFactory, this.idAccessor);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onDelete(event) {
        this.deleteEmitter.emit(event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onUpdate(event) {
        this.updateEmitter.emit(event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onAdd(event) {
        this.addEmitter.emit(event);
    }
}
CalendarComponent.decorators = [
    { type: Component, args: [{
                selector: 'angular-advanced-calendar',
                template: `<h5 *ngFor='let name of headers' class='header'> {{name}} </h5>
<ng-template #dayGrid></ng-template>
<h5 *ngFor='let name of headers' class='footer'> {{name}} </h5>`,
                styles: [`@import url(https://fonts.googleapis.com/css?family=Raleway);.footer,.header{text-align:center;font-family:Raleway,sans-serif}`]
            },] },
];
/** @nocollapse */
CalendarComponent.ctorParameters = () => [
    { type: DayCellComponentFactory, decorators: [{ type: Inject, args: [DAY_CELL_COMPONENT_FACTORY_PROVIDER,] }] },
    { type: EventComponentFactory, decorators: [{ type: Optional }, { type: Inject, args: [EVENT_COMPONENT_FACTORY_PROVIDER,] }] },
    { type: WeekSummaryComponentFactory, decorators: [{ type: Optional }, { type: Inject, args: [WEEK_SUMMARY_COMPONENT_FACTORY,] }] }
];
CalendarComponent.propDecorators = {
    startDate: [{ type: Input, args: ['start-date',] }],
    endDate: [{ type: Input, args: ['end-date',] }],
    headers: [{ type: Input, args: ['headers',] }],
    events: [{ type: Input, args: ['events',] }],
    dateAccessor: [{ type: Input, args: ['date-accessor',] }],
    idAccessor: [{ type: Input, args: ['id-accessor',] }],
    dayGridContainer: [{ type: ViewChild, args: ['dayGrid', { read: ViewContainerRef },] }],
    deleteEmitter: [{ type: Output, args: ['delete',] }],
    updateEmitter: [{ type: Output, args: ['update',] }],
    addEmitter: [{ type: Output, args: ['add',] }]
};
function CalendarComponent_tsickle_Closure_declarations() {
    /**
     * First date of the period to display in the calendar
     * @type {?}
     */
    CalendarComponent.prototype.startDate;
    /**
     * Last date of the period to display in the calendar
     * @type {?}
     */
    CalendarComponent.prototype.endDate;
    /**
     * Week days label to display in the header
     * (default is english).
     * @type {?}
     */
    CalendarComponent.prototype.headers;
    /**
     * A list of events to display in the callendar.
     * @type {?}
     */
    CalendarComponent.prototype.events;
    /**
     * The name of the date attribute to use
     * when displaying the events (default is 'date').
     * @type {?}
     */
    CalendarComponent.prototype.dateAccessor;
    /**
     * The name of the id attribute to use
     * when displaying the events (default is 'id').
     * @type {?}
     */
    CalendarComponent.prototype.idAccessor;
    /**
     * View container storing the dynamic components
     * @type {?}
     */
    CalendarComponent.prototype.dayGridContainer;
    /** @type {?} */
    CalendarComponent.prototype.dynamicDayCellViewManager;
    /** @type {?} */
    CalendarComponent.prototype.dynamicWeekSummaryViewManager;
    /** @type {?} */
    CalendarComponent.prototype.dispatcher;
    /** @type {?} */
    CalendarComponent.prototype.days;
    /** @type {?} */
    CalendarComponent.prototype.weeks;
    /** @type {?} */
    CalendarComponent.prototype.oldHash;
    /**
     * Old start and end date of the calendar.
     * Used to keep track of the changes in period
     * in order to update the view.
     * @type {?}
     */
    CalendarComponent.prototype.oldStartDate;
    /** @type {?} */
    CalendarComponent.prototype.oldEndDate;
    /**
     * Triggers a delete event emission
     * \@param event the event to delete
     * @type {?}
     */
    CalendarComponent.prototype.deleteEmitter;
    /**
     * Triggers an update event emission
     * \@param event the event to update
     * @type {?}
     */
    CalendarComponent.prototype.updateEmitter;
    /**
     * Triggers an add event emission
     * \@param event the event to add
     * @type {?}
     */
    CalendarComponent.prototype.addEmitter;
    /** @type {?} */
    CalendarComponent.prototype.dayCellComponentFactory;
    /** @type {?} */
    CalendarComponent.prototype.eventComponentFactory;
    /** @type {?} */
    CalendarComponent.prototype.weekSummaryComponentFactory;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FsZW5kYXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci8iLCJzb3VyY2VzIjpbImxpYi92aWV3cy9jYWxlbmRhci9jYWxlbmRhci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQStCLFNBQVMsRUFBRSxnQkFBZ0IsRUFBZSxNQUFNLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRWhLLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFNekQsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDL0QsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDdkUsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDcEYsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFFbEYscUJBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMvQixxQkFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO0FBRWxDLE1BQU0sQ0FBQyx1QkFBTSxtQ0FBbUMsR0FBRyx5QkFBeUIsQ0FBQztBQUM3RSxNQUFNLENBQUMsdUJBQU0sZ0NBQWdDLEdBQU0sdUJBQXVCLENBQUM7QUFDM0UsTUFBTSxDQUFDLHVCQUFNLDhCQUE4QixHQUFRLDZCQUE2QixDQUFDO0FBU2pGLE1BQU07Ozs7OztnQkE0Q2tELHVCQUF3RCxFQUMvQyxxQkFBb0QsRUFDdEQsMkJBQWdFO1FBRnZFLDRCQUF1QixHQUF2Qix1QkFBdUIsQ0FBaUM7UUFDL0MsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUErQjtRQUN0RCxnQ0FBMkIsR0FBM0IsMkJBQTJCLENBQXFDOzs7Ozt1QkE5Qi9FO1lBQzVDLFFBQVE7WUFDUixTQUFTO1lBQ1QsV0FBVztZQUNYLFVBQVU7WUFDVixRQUFRO1lBQ1IsVUFBVTtZQUNWLFFBQVE7U0FDVDs7OztzQkFLMkMsRUFBRTs7Ozs7NEJBTVUsTUFBTTs7Ozs7MEJBTVYsSUFBSTt5Q0FnQnBELElBQUksNEJBQTRCLEVBQTBDOzZDQUUxRSxJQUFJLDRCQUE0QixFQUE4Qzt1QkFRdkQsRUFBRTs7Ozs7NkJBaUppQixJQUFJLFlBQVksRUFBUzs7Ozs7NkJBV3pCLElBQUksWUFBWSxFQUFTOzs7OzswQkFXNUIsSUFBSSxZQUFZLEVBQVM7Ozs7O0lBdEtwRSxTQUFTO1FBRVAscUJBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQzdCLENBQUM7WUFDQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7U0FDeEI7S0FDRjs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBdUI7UUFFakMsRUFBRSxDQUFDLENBQUMsT0FBTyxpQkFBYyxPQUFPLFdBQVEsSUFBSSxPQUFPLGdCQUFhLENBQUMsQ0FDakUsQ0FBQztZQUNDLHFCQUFJLFNBQVMsR0FBRyxDQUFDLE9BQU8sY0FBVyxDQUFDLENBQUMsQ0FBQyxPQUFPLGNBQVcsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3RGLHFCQUFJLE9BQU8sR0FBRyxDQUFDLE9BQU8sWUFBUyxDQUFDLENBQUMsQ0FBQyxPQUFPLFlBQVMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQzlFLHFCQUFJLFlBQVksR0FBRyxDQUFDLE9BQU8saUJBQWMsQ0FBQyxDQUFDLENBQUMsT0FBTyxpQkFBYyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7WUFFbEcsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLGVBQWUsQ0FBQyxZQUFZLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3hFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN2QjtLQUNGOzs7O0lBV08sY0FBYztRQUVwQixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUNsQyxDQUFDO1lBQ0MsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7WUFDZixNQUFNLGlFQUFpRSxDQUFBO1NBQ3hFO1FBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQ2hDLENBQUM7WUFDQyxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztZQUNmLE1BQU0sNENBQTRDLENBQUE7U0FDbkQ7UUFDRCxJQUFJLENBQ0osQ0FBQztZQUNDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN0QixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUUxQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FDckMsQ0FBQztnQkFDQyxJQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQzthQUNuQztTQUNGOzs7Ozs7SUFPSyxjQUFjO1FBRXBCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDeEIsR0FBRyxDQUFDLENBQUMscUJBQUksS0FBSyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FDOUIsQ0FBQztZQUNDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzVCO1FBQ0QsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDdEQsRUFBRSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQztZQUN0QyxJQUFJLEVBQUUsSUFBSTtZQUNWLGlCQUFpQixFQUFFLElBQUksQ0FBQyxTQUFTO1lBQ2pDLGVBQWUsRUFBRSxJQUFJLENBQUMsT0FBTztZQUM3QixNQUFNLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO1lBQ3ZDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWTtTQUNoQyxDQUFDLENBQUMsQ0FBQzs7Ozs7SUFHRSxrQkFBa0I7UUFFeEIsSUFBSSxDQUFDLHlCQUF5QixDQUFDLGVBQWUsQ0FDNUMsSUFBSSxDQUFDLGdCQUFnQixFQUNyQixJQUFJLENBQUMsdUJBQXVCLEVBQzVCLElBQUksQ0FBQyxJQUFJLEVBQ1QsSUFBSSxFQUNKO1lBQ0UsR0FBRyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUMxQixNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2hDLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDakMsRUFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFDN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUM5QixDQUFDOzs7OztJQUdJLDBCQUEwQjtRQUVoQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN2RCxFQUFFLEVBQUUsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDakMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTO1lBQ3pCLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTztZQUNyQixpQkFBaUIsRUFBRSxJQUFJLENBQUMsU0FBUztZQUNqQyxlQUFlLEVBQUUsSUFBSSxDQUFDLE9BQU87WUFDN0IsTUFBTSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ3RFLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWTtTQUNoQyxDQUFDLENBQUMsQ0FBQztRQUVKLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FDN0UsQ0FBQztZQUNDLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUMzQyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDbkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1NBQ2hDO1FBRUQsSUFBSSxDQUFDLDZCQUE2QixDQUFDLGVBQWUsQ0FDaEQsSUFBSSxDQUFDLGdCQUFnQixFQUNyQixJQUFJLENBQUMsMkJBQTJCLEVBQ2hDLElBQUksQ0FBQyxLQUFLLEVBQ1YsSUFBSSxFQUNKO1lBQ0UsR0FBRyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUMxQixNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2hDLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDakMsQ0FDRixDQUFDOzs7Ozs7OztJQU9JLGFBQWEsQ0FBQyxJQUE2QixFQUFFLFNBQXlEO1FBRTVHLHFCQUFJLFFBQVEscUJBQW9DLFNBQVMsQ0FBQyxRQUFRLENBQUEsQ0FBQztRQUNuRSxRQUFRLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs7Ozs7O0lBV2pFLFFBQVEsQ0FBQyxLQUFZO1FBRTNCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDOzs7Ozs7SUFTekIsUUFBUSxDQUFDLEtBQVk7UUFFM0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Ozs7OztJQVN6QixLQUFLLENBQUMsS0FBWTtRQUV4QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs7OztZQXBQL0IsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSwyQkFBMkI7Z0JBQ3JDLFFBQVEsRUFBRTs7Z0VBRW9EO2dCQUM5RCxNQUFNLEVBQUUsQ0FBQyxnSUFBZ0ksQ0FBQzthQUMzSTs7OztZQWpCUSx1QkFBdUIsdUJBOEQzQixNQUFNLFNBQUMsbUNBQW1DO1lBL0R0QyxxQkFBcUIsdUJBZ0V6QixRQUFRLFlBQUksTUFBTSxTQUFDLGdDQUFnQztZQTdEL0MsMkJBQTJCLHVCQThEL0IsUUFBUSxZQUFJLE1BQU0sU0FBQyw4QkFBOEI7Ozt3QkF6Q25ELEtBQUssU0FBQyxZQUFZO3NCQUtsQixLQUFLLFNBQUMsVUFBVTtzQkFNaEIsS0FBSyxTQUFDLFNBQVM7cUJBYWYsS0FBSyxTQUFDLFFBQVE7MkJBTWQsS0FBSyxTQUFDLGVBQWU7eUJBTXJCLEtBQUssU0FBQyxhQUFhOytCQWFuQixTQUFTLFNBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixFQUFFOzRCQTZKL0MsTUFBTSxTQUFDLFFBQVE7NEJBV2YsTUFBTSxTQUFDLFFBQVE7eUJBV2YsTUFBTSxTQUFDLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgU2ltcGxlQ2hhbmdlcywgQ29tcG9uZW50UmVmLCBWaWV3Q2hpbGQsIFZpZXdDb250YWluZXJSZWYsIEhvc3RCaW5kaW5nLCBJbmplY3QsIE9wdGlvbmFsIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IGdldFdlZWtOdW1iZXIgfSBmcm9tICcuLi8uLi91dGlscy9kYXRlJztcbmltcG9ydCB7IEV2ZW50RGlzcGF0Y2hlciB9IGZyb20gJy4uLy4uL3V0aWxzL2Rpc3BhdGNoZXInO1xuaW1wb3J0IHsgQWJzdHJhY3REYXlDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vZGF5LWNlbGwvYWJzdHJhY3QtZGF5LWNlbGwuY29tcG9uZW50JztcbmltcG9ydCB7IENhbGVuZGFyRGF5RGF0YSB9IGZyb20gJy4uLy4uL3R5cGVzL2NhbGVuZGFyLWRheSc7XG5pbXBvcnQgeyBFdmVudCB9IGZyb20gJy4uLy4uL3R5cGVzL2V2ZW50JztcbmltcG9ydCB7IENhbGVuZGFyV2Vla0RhdGEgfSBmcm9tICcuLi8uLi90eXBlcy9jYWxlbmRhci13ZWVrJztcbmltcG9ydCB7IEFic3RyYWN0V2Vla1N1bW1hcnlDb21wb25lbnQgfSBmcm9tICcuLi93ZWVrLXN1bW1hcnkvYWJzdHJhY3Qtd2Vlay1zdW1tYXJ5LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBFdmVudENvbXBvbmVudEZhY3RvcnkgfSBmcm9tICcuLi9ldmVudC9ldmVudC5mYWN0b3J5JztcbmltcG9ydCB7IERheUNlbGxDb21wb25lbnRGYWN0b3J5IH0gZnJvbSAnLi4vZGF5LWNlbGwvZGF5LWNlbGwuZmFjdG9yeSc7XG5pbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyIH0gZnJvbSAnLi4vZHluYW1pYy9keW5hbWljLW1hbmFnZXIuY29tcG9uZW50JztcbmltcG9ydCB7IFdlZWtTdW1tYXJ5Q29tcG9uZW50RmFjdG9yeSB9IGZyb20gJy4uL3dlZWstc3VtbWFyeS93ZWVrLXN1bW1hcnkuZmF0b3J5JztcblxubGV0IG1vbWVudCA9IHJlcXVpcmUoJ21vbWVudCcpO1xubGV0IGhhc2ggPSByZXF1aXJlKCdvYmplY3QtaGFzaCcpO1xuXG5leHBvcnQgY29uc3QgREFZX0NFTExfQ09NUE9ORU5UX0ZBQ1RPUllfUFJPVklERVIgPSAnRGF5Q2VsbENvbXBvbmVudEZhY3RvcnknO1xuZXhwb3J0IGNvbnN0IEVWRU5UX0NPTVBPTkVOVF9GQUNUT1JZX1BST1ZJREVSICAgID0gJ0V2ZW50Q29tcG9uZW50RmFjdG9yeSc7XG5leHBvcnQgY29uc3QgV0VFS19TVU1NQVJZX0NPTVBPTkVOVF9GQUNUT1JZICAgICAgPSAnV2Vla1N1bW1hcnlDb21wb25lbnRGYWN0b3J5JztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhcicsXG4gIHRlbXBsYXRlOiBgPGg1ICpuZ0Zvcj0nbGV0IG5hbWUgb2YgaGVhZGVycycgY2xhc3M9J2hlYWRlcic+IHt7bmFtZX19IDwvaDU+XG48bmctdGVtcGxhdGUgI2RheUdyaWQ+PC9uZy10ZW1wbGF0ZT5cbjxoNSAqbmdGb3I9J2xldCBuYW1lIG9mIGhlYWRlcnMnIGNsYXNzPSdmb290ZXInPiB7e25hbWV9fSA8L2g1PmAsXG4gIHN0eWxlczogW2BAaW1wb3J0IHVybChodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9UmFsZXdheSk7LmZvb3RlciwuaGVhZGVye3RleHQtYWxpZ246Y2VudGVyO2ZvbnQtZmFtaWx5OlJhbGV3YXksc2Fucy1zZXJpZn1gXVxufSlcbmV4cG9ydCBjbGFzcyBDYWxlbmRhckNvbXBvbmVudFxue1xuICAvKipcbiAgICogRmlyc3QgZGF0ZSBvZiB0aGUgcGVyaW9kIHRvIGRpc3BsYXkgaW4gdGhlIGNhbGVuZGFyXG4gICAqL1xuICBASW5wdXQoJ3N0YXJ0LWRhdGUnKSBwcml2YXRlIHN0YXJ0RGF0ZSA6IERhdGU7XG5cbiAgLyoqXG4gICAqIExhc3QgZGF0ZSBvZiB0aGUgcGVyaW9kIHRvIGRpc3BsYXkgaW4gdGhlIGNhbGVuZGFyXG4gICAqL1xuICBASW5wdXQoJ2VuZC1kYXRlJykgcHJpdmF0ZSBlbmREYXRlIDogRGF0ZTtcbiAgXG4gIC8qKlxuICAgKiBXZWVrIGRheXMgbGFiZWwgdG8gZGlzcGxheSBpbiB0aGUgaGVhZGVyXG4gICAqIChkZWZhdWx0IGlzIGVuZ2xpc2gpLlxuICAgKi9cbiAgQElucHV0KCdoZWFkZXJzJykgcHVibGljIGhlYWRlcnMgOiBzdHJpbmdbXSA9IFtcbiAgICAnTW9uZGF5JyxcbiAgICAnVHVlc2RheScsXG4gICAgJ1dlZG5lc2RheScsXG4gICAgJ1RodXJzZGF5JyxcbiAgICAnRnJpZGF5JyxcbiAgICAnU2F0dXJkYXknLFxuICAgICdTdW5kYXknLFxuICBdO1xuXG4gIC8qKlxuICAgKiBBIGxpc3Qgb2YgZXZlbnRzIHRvIGRpc3BsYXkgaW4gdGhlIGNhbGxlbmRhci5cbiAgICovXG4gIEBJbnB1dCgnZXZlbnRzJykgcHJpdmF0ZSBldmVudHMgOiBFdmVudFtdID0gW107XG4gIFxuICAvKipcbiAgICogVGhlIG5hbWUgb2YgdGhlIGRhdGUgYXR0cmlidXRlIHRvIHVzZVxuICAgKiB3aGVuIGRpc3BsYXlpbmcgdGhlIGV2ZW50cyAoZGVmYXVsdCBpcyAnZGF0ZScpLlxuICAgKi9cbiAgQElucHV0KCdkYXRlLWFjY2Vzc29yJykgcHJpdmF0ZSBkYXRlQWNjZXNzb3IgOiBzdHJpbmcgPSAnZGF0ZSc7XG5cbiAgLyoqXG4gICAqIFRoZSBuYW1lIG9mIHRoZSBpZCBhdHRyaWJ1dGUgdG8gdXNlXG4gICAqIHdoZW4gZGlzcGxheWluZyB0aGUgZXZlbnRzIChkZWZhdWx0IGlzICdpZCcpLlxuICAgKi9cbiAgQElucHV0KCdpZC1hY2Nlc3NvcicpIHByaXZhdGUgaWRBY2Nlc3NvciA6IHN0cmluZyA9ICdpZCc7XG5cbiAgcHVibGljIGNvbnN0cnVjdG9yKFxuICAgIEBJbmplY3QoREFZX0NFTExfQ09NUE9ORU5UX0ZBQ1RPUllfUFJPVklERVIpIHB1YmxpYyBkYXlDZWxsQ29tcG9uZW50RmFjdG9yeSA6IERheUNlbGxDb21wb25lbnRGYWN0b3J5PEV2ZW50PixcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KEVWRU5UX0NPTVBPTkVOVF9GQUNUT1JZX1BST1ZJREVSKSBwdWJsaWMgZXZlbnRDb21wb25lbnRGYWN0b3J5IDogRXZlbnRDb21wb25lbnRGYWN0b3J5PEV2ZW50PixcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KFdFRUtfU1VNTUFSWV9DT01QT05FTlRfRkFDVE9SWSkgcHVibGljIHdlZWtTdW1tYXJ5Q29tcG9uZW50RmFjdG9yeSA6IFdlZWtTdW1tYXJ5Q29tcG9uZW50RmFjdG9yeTxFdmVudD5cbiAgKXt9XG5cbiAgLyogLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tICovXG5cbiAgLyoqXG4gICAqIFZpZXcgY29udGFpbmVyIHN0b3JpbmcgdGhlIGR5bmFtaWMgY29tcG9uZW50c1xuICAgKi9cbiAgQFZpZXdDaGlsZCgnZGF5R3JpZCcsIHsgcmVhZDogVmlld0NvbnRhaW5lclJlZiB9KVxuICBwcml2YXRlIGRheUdyaWRDb250YWluZXIgOiBWaWV3Q29udGFpbmVyUmVmO1xuICBwcml2YXRlIGR5bmFtaWNEYXlDZWxsVmlld01hbmFnZXIgOiBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyPEV2ZW50LCBBYnN0cmFjdERheUNlbGxDb21wb25lbnQ8RXZlbnQ+PlxuICAgID0gbmV3IER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXI8RXZlbnQsIEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudDxFdmVudD4+KCk7XG4gIHByaXZhdGUgZHluYW1pY1dlZWtTdW1tYXJ5Vmlld01hbmFnZXIgOiBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyPEV2ZW50LCBBYnN0cmFjdFdlZWtTdW1tYXJ5Q29tcG9uZW50PEV2ZW50Pj5cbiAgICA9IG5ldyBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyPEV2ZW50LCBBYnN0cmFjdFdlZWtTdW1tYXJ5Q29tcG9uZW50PEV2ZW50Pj4oKTtcblxuICAvKiAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gKi9cblxuICBwcml2YXRlIGRpc3BhdGNoZXIgOiBFdmVudERpc3BhdGNoZXI7XG4gIHByaXZhdGUgZGF5cyA6IENhbGVuZGFyRGF5RGF0YTxFdmVudD5bXTtcbiAgcHJpdmF0ZSB3ZWVrcyA6IENhbGVuZGFyV2Vla0RhdGE8RXZlbnQ+W107XG4gIFxuICBwcml2YXRlIG9sZEhhc2ggOiBzdHJpbmcgPSAnJztcbiAgbmdEb0NoZWNrKClcbiAge1xuICAgIGxldCBuZXdIYXNoID0gaGFzaC5zaGExKHRoaXMuZXZlbnRzKTtcbiAgICBpZiAobmV3SGFzaCAhPT0gdGhpcy5vbGRIYXNoKVxuICAgIHtcbiAgICAgIHRoaXMudXBkYXRlQ2FsZW5kYXIoKTtcbiAgICAgIHRoaXMub2xkSGFzaCA9IG5ld0hhc2g7XG4gICAgfVxuICB9XG5cbiAgbmdPbkNoYW5nZXMoY2hhbmdlcyA6IFNpbXBsZUNoYW5nZXMpXG4gIHtcbiAgICBpZiAoY2hhbmdlcy5zdGFydERhdGUgfHwgY2hhbmdlcy5lbmREYXRlIHx8IGNoYW5nZXMuZGF0ZUFjY2Vzc29yKVxuICAgIHtcbiAgICAgIGxldCBzdGFydERhdGUgPSAoY2hhbmdlcy5zdGFydERhdGUpID8gY2hhbmdlcy5zdGFydERhdGUuY3VycmVudFZhbHVlIDogdGhpcy5zdGFydERhdGU7XG4gICAgICBsZXQgZW5kRGF0ZSA9IChjaGFuZ2VzLmVuZERhdGUpID8gY2hhbmdlcy5lbmREYXRlLmN1cnJlbnRWYWx1ZSA6IHRoaXMuZW5kRGF0ZTtcbiAgICAgIGxldCBkYXRlQWNjZXNzb3IgPSAoY2hhbmdlcy5kYXRlQWNjZXNzb3IpID8gY2hhbmdlcy5kYXRlQWNjZXNzb3IuY3VycmVudFZhbHVlIDogdGhpcy5kYXRlQWNjZXNzb3I7XG5cbiAgICAgIHRoaXMuZGlzcGF0Y2hlciA9IG5ldyBFdmVudERpc3BhdGNoZXIoZGF0ZUFjY2Vzc29yLCBzdGFydERhdGUsIGVuZERhdGUpO1xuICAgICAgdGhpcy51cGRhdGVDYWxlbmRhcigpO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBPbGQgc3RhcnQgYW5kIGVuZCBkYXRlIG9mIHRoZSBjYWxlbmRhci5cbiAgICogVXNlZCB0byBrZWVwIHRyYWNrIG9mIHRoZSBjaGFuZ2VzIGluIHBlcmlvZFxuICAgKiBpbiBvcmRlciB0byB1cGRhdGUgdGhlIHZpZXcuXG4gICAqL1xuICBwcml2YXRlIG9sZFN0YXJ0RGF0ZSA6IERhdGU7XG4gIHByaXZhdGUgb2xkRW5kRGF0ZSA6IERhdGU7XG5cblxuICBwcml2YXRlIHVwZGF0ZUNhbGVuZGFyKClcbiAge1xuICAgIGlmICghdGhpcy5kYXlDZWxsQ29tcG9uZW50RmFjdG9yeSlcbiAgICB7XG4gICAgICB0aGlzLmRheXMgPSBbXTtcbiAgICAgIHRocm93ICdkYXkgY2VsbCBjb21wb25lbnQgZmFjdG9yeSAoZGF5LWNvbXBvbmVudC1mYWN0b3J5KSB3YXMgbm90IHNldCEnXG4gICAgfVxuICAgIGVsc2UgaWYgKCF0aGlzLmRheUdyaWRDb250YWluZXIpXG4gICAge1xuICAgICAgdGhpcy5kYXlzID0gW107XG4gICAgICB0aHJvdyAnZGF5IGdyaWQgKGRheUdyaWQpIG5vdCBmb3VuZCBpbiB0ZW1wbGF0ZSAhJ1xuICAgIH1cbiAgICBlbHNlXG4gICAge1xuICAgICAgdGhpcy5kaXNwYXRjaEV2ZW50cygpO1xuICAgICAgdGhpcy51cGRhdGVEYXlDZWxsVmlld3MoKTtcblxuICAgICAgaWYgKHRoaXMud2Vla1N1bW1hcnlDb21wb25lbnRGYWN0b3J5KVxuICAgICAge1xuICAgICAgICB0aGlzLnVwZGF0ZVdlZWtTdW1tYXJ5Q2VsbFZpZXdzKCk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cblxuICAvKipcbiAgICogU29ydHMgZXZlbnRzIGJ5IGRhdGVcbiAgICovXG4gIHByaXZhdGUgZGlzcGF0Y2hFdmVudHMoKVxuICB7XG4gICAgdGhpcy5kaXNwYXRjaGVyLmNsZWFyKCk7XG4gICAgZm9yIChsZXQgZXZlbnQgb2YgdGhpcy5ldmVudHMpXG4gICAge1xuICAgICAgdGhpcy5kaXNwYXRjaGVyLmFkZChldmVudCk7XG4gICAgfVxuICAgIHRoaXMuZGF5cyA9IHRoaXMuZGlzcGF0Y2hlci5nZXREYXRlQXJyYXkoKS5tYXAoZGF0ZSA9PiAoe1xuICAgICAgaWQ6IHRoaXMuZGlzcGF0Y2hlci5nZXREYXlOdW1iZXIoZGF0ZSksXG4gICAgICBkYXRlOiBkYXRlLFxuICAgICAgY2FsZW5kYXJTdGFydERhdGU6IHRoaXMuc3RhcnREYXRlLFxuICAgICAgY2FsZW5kYXJFbmREYXRlOiB0aGlzLmVuZERhdGUsXG4gICAgICBldmVudHM6IHRoaXMuZGlzcGF0Y2hlci5nZXRFdmVudHMoZGF0ZSksXG4gICAgICBkYXRlQWNjZXNzb3I6IHRoaXMuZGF0ZUFjY2Vzc29yLFxuICAgIH0pKTtcbiAgfVxuXG4gIHByaXZhdGUgdXBkYXRlRGF5Q2VsbFZpZXdzKClcbiAge1xuICAgIHRoaXMuZHluYW1pY0RheUNlbGxWaWV3TWFuYWdlci51cGRhdGVDb250YWluZXIoXG4gICAgICB0aGlzLmRheUdyaWRDb250YWluZXIsXG4gICAgICB0aGlzLmRheUNlbGxDb21wb25lbnRGYWN0b3J5LFxuICAgICAgdGhpcy5kYXlzLFxuICAgICAgJ2lkJyxcbiAgICAgIHtcbiAgICAgICAgYWRkOiB0aGlzLm9uQWRkLmJpbmQodGhpcyksXG4gICAgICAgIHVwZGF0ZTogdGhpcy5vblVwZGF0ZS5iaW5kKHRoaXMpLFxuICAgICAgICBkZWxldGU6IHRoaXMub25EZWxldGUuYmluZCh0aGlzKVxuICAgICAgfSxcbiAgICAgIHRoaXMudXBkYXRlRGF5Q2VsbC5iaW5kKHRoaXMpLFxuICAgICAgdGhpcy51cGRhdGVEYXlDZWxsLmJpbmQodGhpcyksXG4gICAgICB0aGlzLnVwZGF0ZURheUNlbGwuYmluZCh0aGlzKVxuICAgICk7XG4gIH1cblxuICBwcml2YXRlIHVwZGF0ZVdlZWtTdW1tYXJ5Q2VsbFZpZXdzKClcbiAge1xuICAgIHRoaXMud2Vla3MgPSB0aGlzLmRpc3BhdGNoZXIuZ2V0V2Vla0FycmF5KCkubWFwKHdlZWsgPT4gKHtcbiAgICAgIGlkOiBnZXRXZWVrTnVtYmVyKHdlZWsuc3RhcnREYXRlKSxcbiAgICAgIHN0YXJ0RGF0ZTogd2Vlay5zdGFydERhdGUsXG4gICAgICBlbmREYXRlOiB3ZWVrLmVuZERhdGUsXG4gICAgICBjYWxlbmRhclN0YXJ0RGF0ZTogdGhpcy5zdGFydERhdGUsXG4gICAgICBjYWxlbmRhckVuZERhdGU6IHRoaXMuZW5kRGF0ZSxcbiAgICAgIGV2ZW50czogdGhpcy5kaXNwYXRjaGVyLmdldEV2ZW50c0JldHdlZW4od2Vlay5zdGFydERhdGUsIHdlZWsuZW5kRGF0ZSksXG4gICAgICBkYXRlQWNjZXNzb3I6IHRoaXMuZGF0ZUFjY2Vzc29yLFxuICAgIH0pKTtcblxuICAgIGlmICh0aGlzLnN0YXJ0RGF0ZSAhPT0gdGhpcy5vbGRTdGFydERhdGUgfHwgdGhpcy5lbmREYXRlICE9PSB0aGlzLm9sZEVuZERhdGUpXG4gICAge1xuICAgICAgdGhpcy5keW5hbWljV2Vla1N1bW1hcnlWaWV3TWFuYWdlci5jbGVhcigpO1xuICAgICAgdGhpcy5vbGRTdGFydERhdGUgPSB0aGlzLnN0YXJ0RGF0ZTtcbiAgICAgIHRoaXMub2xkRW5kRGF0ZSA9IHRoaXMuZW5kRGF0ZTtcbiAgICB9XG4gICAgXG4gICAgdGhpcy5keW5hbWljV2Vla1N1bW1hcnlWaWV3TWFuYWdlci51cGRhdGVDb250YWluZXIoXG4gICAgICB0aGlzLmRheUdyaWRDb250YWluZXIsXG4gICAgICB0aGlzLndlZWtTdW1tYXJ5Q29tcG9uZW50RmFjdG9yeSxcbiAgICAgIHRoaXMud2Vla3MsXG4gICAgICAnaWQnLFxuICAgICAge1xuICAgICAgICBhZGQ6IHRoaXMub25BZGQuYmluZCh0aGlzKSxcbiAgICAgICAgdXBkYXRlOiB0aGlzLm9uVXBkYXRlLmJpbmQodGhpcyksXG4gICAgICAgIGRlbGV0ZTogdGhpcy5vbkRlbGV0ZS5iaW5kKHRoaXMpXG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUcmlnZ2VycyBhIGRheSBjZWxsIHZpZXcgaW50ZXJuYWwgdXBkYXRlXG4gICAqIEBwYXJhbSBjb21wb25lbnQgdGhlIGNvbXBvbmVudCByZWYgb2YgdGhlIGRheSBjZWxsIHRvIHVwZGF0ZVxuICAgKi9cbiAgcHJpdmF0ZSB1cGRhdGVEYXlDZWxsKGRhdGEgOiBDYWxlbmRhckRheURhdGE8RXZlbnQ+LCBjb21wb25lbnQgOiBDb21wb25lbnRSZWY8QWJzdHJhY3REYXlDZWxsQ29tcG9uZW50PEV2ZW50Pj4pIDogdm9pZFxuICB7XG4gICAgbGV0IGluc3RhbmNlID0gPEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudDxFdmVudD4+Y29tcG9uZW50Lmluc3RhbmNlO1xuICAgIGluc3RhbmNlLnVwZGF0ZUV2ZW50Vmlld3ModGhpcy5ldmVudENvbXBvbmVudEZhY3RvcnksIHRoaXMuaWRBY2Nlc3Nvcik7XG4gIH1cblxuICAvKiAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gKi9cblxuICAvKipcbiAgICogVHJpZ2dlcnMgYSBkZWxldGUgZXZlbnQgZW1pc3Npb25cbiAgICogQHBhcmFtIGV2ZW50IHRoZSBldmVudCB0byBkZWxldGVcbiAgICovXG4gIEBPdXRwdXQoJ2RlbGV0ZScpXG4gIHByaXZhdGUgZGVsZXRlRW1pdHRlciA6IEV2ZW50RW1pdHRlcjxFdmVudD4gPSBuZXcgRXZlbnRFbWl0dGVyPEV2ZW50PigpO1xuICBwcml2YXRlIG9uRGVsZXRlKGV2ZW50OiBFdmVudClcbiAge1xuICAgIHRoaXMuZGVsZXRlRW1pdHRlci5lbWl0KGV2ZW50KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUcmlnZ2VycyBhbiB1cGRhdGUgZXZlbnQgZW1pc3Npb25cbiAgICogQHBhcmFtIGV2ZW50IHRoZSBldmVudCB0byB1cGRhdGVcbiAgICovXG4gIEBPdXRwdXQoJ3VwZGF0ZScpXG4gIHByaXZhdGUgdXBkYXRlRW1pdHRlciA6IEV2ZW50RW1pdHRlcjxFdmVudD4gPSBuZXcgRXZlbnRFbWl0dGVyPEV2ZW50PigpO1xuICBwcml2YXRlIG9uVXBkYXRlKGV2ZW50OiBFdmVudClcbiAge1xuICAgIHRoaXMudXBkYXRlRW1pdHRlci5lbWl0KGV2ZW50KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUcmlnZ2VycyBhbiBhZGQgZXZlbnQgZW1pc3Npb25cbiAgICogQHBhcmFtIGV2ZW50IHRoZSBldmVudCB0byBhZGRcbiAgICovXG4gIEBPdXRwdXQoJ2FkZCcpXG4gIHByaXZhdGUgYWRkRW1pdHRlciA6IEV2ZW50RW1pdHRlcjxFdmVudD4gPSBuZXcgRXZlbnRFbWl0dGVyPEV2ZW50PigpO1xuICBwcml2YXRlIG9uQWRkKGV2ZW50OiBFdmVudClcbiAge1xuICAgIHRoaXMuYWRkRW1pdHRlci5lbWl0KGV2ZW50KTtcbiAgfVxufVxuIl19