/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { DynamicComponentFactory } from "../dynamic/dynamic.factory";
import { isSameDay } from "../../utils/date";
/**
 * @abstract
 * @template DataType
 */
export class WeekSummaryComponentFactory extends DynamicComponentFactory {
    /**
     * @param {?} componentFactoryResolver
     * @param {?} componentType
     */
    constructor(componentFactoryResolver, componentType) {
        super(componentFactoryResolver, componentType);
    }
    /**
     * @param {?} container
     * @param {?} data
     * @param {?=} callbacks
     * @return {?}
     */
    insertDynamicComponent(container, data, callbacks) {
        let /** @type {?} */ position = this.findWeekPositionInContainer(container, data);
        return (position >= 0) ? super.insertDynamicComponent(container, data, callbacks, position) : undefined;
    }
    /**
     * @param {?} container
     * @param {?} data
     * @return {?}
     */
    findWeekPositionInContainer(container, data) {
        let /** @type {?} */ index;
        let /** @type {?} */ found = false;
        for (index = 0; !found && index < container.length; index++) {
            let /** @type {?} */ componentInstance = container.get(index)['_view']['nodes'][1]['instance'];
            if (componentInstance.data.date && isSameDay(componentInstance.data.date, data.endDate)) {
                return index + 1;
            }
        }
        return -1;
    }
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2Vlay1zdW1tYXJ5LmZhdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdmlld3Mvd2Vlay1zdW1tYXJ5L3dlZWstc3VtbWFyeS5mYXRvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBS3JFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQzs7Ozs7QUFFN0MsTUFBTSxrQ0FBc0QsU0FBUSx1QkFBMkY7Ozs7O2dCQUUxSSx3QkFBbUQsRUFBRSxhQUE0RDtRQUVsSSxLQUFLLENBQUMsd0JBQXdCLEVBQUUsYUFBYSxDQUFDLENBQUM7Ozs7Ozs7O0lBRzFDLHNCQUFzQixDQUFDLFNBQTRCLEVBQUUsSUFBaUMsRUFBRSxTQUErQjtRQUU1SCxxQkFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLDJCQUEyQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNqRSxNQUFNLENBQUMsQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDOzs7Ozs7O0lBR2xHLDJCQUEyQixDQUFDLFNBQTRCLEVBQUUsSUFBaUM7UUFFakcscUJBQUksS0FBSyxDQUFDO1FBQ1YscUJBQUksS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNsQixHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFLENBQUMsS0FBSyxJQUFJLEtBQUssR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUMzRCxDQUFDO1lBQ0MscUJBQUksaUJBQWlCLEdBQUcsU0FBUyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM5RSxFQUFFLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUN4RixDQUFDO2dCQUNDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO2FBQ2xCO1NBQ0Y7UUFDRCxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7O0NBRWIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50RmFjdG9yeSB9IGZyb20gXCIuLi9keW5hbWljL2R5bmFtaWMuZmFjdG9yeVwiO1xuaW1wb3J0IHsgQ2FsZW5kYXJXZWVrRGF0YSB9IGZyb20gXCIuLi8uLi90eXBlcy9jYWxlbmRhci13ZWVrXCI7XG5pbXBvcnQgeyBBYnN0cmFjdFdlZWtTdW1tYXJ5Q29tcG9uZW50IH0gZnJvbSBcIi4vYWJzdHJhY3Qtd2Vlay1zdW1tYXJ5LmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQ29tcG9uZW50UmVmLCBWaWV3Q29udGFpbmVyUmVmLCBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIFR5cGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgU3RyaW5nTWFwIH0gZnJvbSBcIi4uLy4uL3R5cGVzL21hcHNcIjtcbmltcG9ydCB7IGlzU2FtZURheSB9IGZyb20gXCIuLi8uLi91dGlscy9kYXRlXCI7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBXZWVrU3VtbWFyeUNvbXBvbmVudEZhY3Rvcnk8RGF0YVR5cGU+IGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudEZhY3Rvcnk8Q2FsZW5kYXJXZWVrRGF0YTxEYXRhVHlwZT4sIEFic3RyYWN0V2Vla1N1bW1hcnlDb21wb25lbnQ8RGF0YVR5cGU+Plxue1xuICBwdWJsaWMgY29uc3RydWN0b3IoY29tcG9uZW50RmFjdG9yeVJlc29sdmVyIDogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb21wb25lbnRUeXBlIDogVHlwZTxBYnN0cmFjdFdlZWtTdW1tYXJ5Q29tcG9uZW50PERhdGFUeXBlPj4pXG4gIHtcbiAgICBzdXBlcihjb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIGNvbXBvbmVudFR5cGUpO1xuICB9XG5cbiAgcHVibGljIGluc2VydER5bmFtaWNDb21wb25lbnQoY29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZiwgZGF0YSA6IENhbGVuZGFyV2Vla0RhdGE8RGF0YVR5cGU+LCBjYWxsYmFja3M/OiBTdHJpbmdNYXA8RnVuY3Rpb24+KSA6IENvbXBvbmVudFJlZjxBYnN0cmFjdFdlZWtTdW1tYXJ5Q29tcG9uZW50PERhdGFUeXBlPj5cbiAge1xuICAgIGxldCBwb3NpdGlvbiA9IHRoaXMuZmluZFdlZWtQb3NpdGlvbkluQ29udGFpbmVyKGNvbnRhaW5lciwgZGF0YSk7XG4gICAgcmV0dXJuIChwb3NpdGlvbiA+PSAwKSA/IHN1cGVyLmluc2VydER5bmFtaWNDb21wb25lbnQoY29udGFpbmVyLCBkYXRhLCBjYWxsYmFja3MsIHBvc2l0aW9uKSA6IHVuZGVmaW5lZDtcbiAgfVxuXG4gIHByaXZhdGUgZmluZFdlZWtQb3NpdGlvbkluQ29udGFpbmVyKGNvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWYsIGRhdGEgOiBDYWxlbmRhcldlZWtEYXRhPERhdGFUeXBlPikgOiBudW1iZXJcbiAge1xuICAgIGxldCBpbmRleDtcbiAgICBsZXQgZm91bmQgPSBmYWxzZTtcbiAgICBmb3IgKGluZGV4ID0gMDsgIWZvdW5kICYmIGluZGV4IDwgY29udGFpbmVyLmxlbmd0aDsgaW5kZXgrKylcbiAgICB7XG4gICAgICBsZXQgY29tcG9uZW50SW5zdGFuY2UgPSBjb250YWluZXIuZ2V0KGluZGV4KVsnX3ZpZXcnXVsnbm9kZXMnXVsxXVsnaW5zdGFuY2UnXTtcbiAgICAgIGlmIChjb21wb25lbnRJbnN0YW5jZS5kYXRhLmRhdGUgJiYgaXNTYW1lRGF5KGNvbXBvbmVudEluc3RhbmNlLmRhdGEuZGF0ZSwgZGF0YS5lbmREYXRlKSlcbiAgICAgIHtcbiAgICAgICAgcmV0dXJuIGluZGV4ICsgMTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIC0xO1xuICB9XG59Il19