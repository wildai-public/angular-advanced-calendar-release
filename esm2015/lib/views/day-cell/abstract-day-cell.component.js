/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { DynamicCrudComponent } from "../dynamic-crud/dynamic-crud.component";
import { ViewChild, ViewContainerRef } from "@angular/core";
import { DynamicComponentArrayManager } from "../dynamic/dynamic-manager.component";
/**
 * @abstract
 * @template DataType
 */
export class AbstractDayCellComponent extends DynamicCrudComponent {
    constructor() {
        super(...arguments);
        this.dynamicEventViewManager = new DynamicComponentArrayManager();
    }
    /**
     * Method called to update the event views in the day cell
     * @param {?} eventComponentFactory
     * @param {?} idAccessor
     * @return {?}
     */
    updateEventViews(eventComponentFactory, idAccessor) {
        if (eventComponentFactory && this.eventViewContainer) {
            this.dynamicEventViewManager.updateContainer(this.eventViewContainer, eventComponentFactory, this.data.events, idAccessor, {
                delete: this.onDelete.bind(this),
                update: this.onUpdate.bind(this)
            });
        }
    }
}
AbstractDayCellComponent.propDecorators = {
    eventViewContainer: [{ type: ViewChild, args: ['eventViewsContainer', { read: ViewContainerRef },] }]
};
function AbstractDayCellComponent_tsickle_Closure_declarations() {
    /**
     * View container storing the dynamic components
     * @type {?}
     */
    AbstractDayCellComponent.prototype.eventViewContainer;
    /** @type {?} */
    AbstractDayCellComponent.prototype.dynamicEventViewManager;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWJzdHJhY3QtZGF5LWNlbGwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci8iLCJzb3VyY2VzIjpbImxpYi92aWV3cy9kYXktY2VsbC9hYnN0cmFjdC1kYXktY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBRTlFLE9BQU8sRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDNUQsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sc0NBQXNDLENBQUM7Ozs7O0FBSXBGLE1BQU0sK0JBQW1ELFNBQVEsb0JBQXlEOzs7dUNBUXBILElBQUksNEJBQTRCLEVBQThDOzs7Ozs7OztJQUszRSxnQkFBZ0IsQ0FBQyxxQkFBMkYsRUFBRSxVQUFtQjtRQUV0SSxFQUFFLENBQUMsQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FDckQsQ0FBQztZQUNDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxlQUFlLENBQzFDLElBQUksQ0FBQyxrQkFBa0IsRUFDdkIscUJBQXFCLEVBQ3JCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUNoQixVQUFVLEVBQ1Y7Z0JBQ0UsTUFBTSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDaEMsTUFBTSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzthQUNqQyxDQUNGLENBQUM7U0FDSDs7OztpQ0F0QkYsU0FBUyxTQUFDLHFCQUFxQixFQUFFLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixFQUFFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRHluYW1pY0NydWRDb21wb25lbnQgfSBmcm9tIFwiLi4vZHluYW1pYy1jcnVkL2R5bmFtaWMtY3J1ZC5jb21wb25lbnRcIjtcbmltcG9ydCB7IENhbGVuZGFyRGF5RGF0YSB9IGZyb20gXCIuLi8uLi90eXBlcy9jYWxlbmRhci1kYXlcIjtcbmltcG9ydCB7IFZpZXdDaGlsZCwgVmlld0NvbnRhaW5lclJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyIH0gZnJvbSBcIi4uL2R5bmFtaWMvZHluYW1pYy1tYW5hZ2VyLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWJzdHJhY3RFdmVudENvbXBvbmVudCB9IGZyb20gXCIuLi9ldmVudC9hYnN0cmFjdC1ldmVudC5jb21wb25lbnRcIjtcbmltcG9ydCB7IER5bmFtaWNDb21wb25lbnRGYWN0b3J5IH0gZnJvbSBcIi4uL2R5bmFtaWMvZHluYW1pYy5mYWN0b3J5XCI7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBBYnN0cmFjdERheUNlbGxDb21wb25lbnQ8RGF0YVR5cGU+IGV4dGVuZHMgRHluYW1pY0NydWRDb21wb25lbnQ8Q2FsZW5kYXJEYXlEYXRhPERhdGFUeXBlPiwgRGF0YVR5cGU+XG57XG4gIC8qKlxuICAgKiBWaWV3IGNvbnRhaW5lciBzdG9yaW5nIHRoZSBkeW5hbWljIGNvbXBvbmVudHNcbiAgICovXG4gIEBWaWV3Q2hpbGQoJ2V2ZW50Vmlld3NDb250YWluZXInLCB7IHJlYWQ6IFZpZXdDb250YWluZXJSZWYgfSlcbiAgcHJpdmF0ZSBldmVudFZpZXdDb250YWluZXIgOiBWaWV3Q29udGFpbmVyUmVmO1xuICBwcml2YXRlIGR5bmFtaWNFdmVudFZpZXdNYW5hZ2VyIDogRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlcjxEYXRhVHlwZSwgQWJzdHJhY3RFdmVudENvbXBvbmVudDxEYXRhVHlwZT4+XG4gICAgPSBuZXcgRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlcjxEYXRhVHlwZSwgQWJzdHJhY3RFdmVudENvbXBvbmVudDxEYXRhVHlwZT4+KCk7XG5cbiAgLyoqXG4gICAqIE1ldGhvZCBjYWxsZWQgdG8gdXBkYXRlIHRoZSBldmVudCB2aWV3cyBpbiB0aGUgZGF5IGNlbGxcbiAgICovXG4gIHB1YmxpYyB1cGRhdGVFdmVudFZpZXdzKGV2ZW50Q29tcG9uZW50RmFjdG9yeSA6IER5bmFtaWNDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlLCBBYnN0cmFjdEV2ZW50Q29tcG9uZW50PERhdGFUeXBlPj4sIGlkQWNjZXNzb3IgOiBzdHJpbmcpXG4gIHtcbiAgICBpZiAoZXZlbnRDb21wb25lbnRGYWN0b3J5ICYmIHRoaXMuZXZlbnRWaWV3Q29udGFpbmVyKVxuICAgIHtcbiAgICAgIHRoaXMuZHluYW1pY0V2ZW50Vmlld01hbmFnZXIudXBkYXRlQ29udGFpbmVyKFxuICAgICAgICB0aGlzLmV2ZW50Vmlld0NvbnRhaW5lcixcbiAgICAgICAgZXZlbnRDb21wb25lbnRGYWN0b3J5LFxuICAgICAgICB0aGlzLmRhdGEuZXZlbnRzLFxuICAgICAgICBpZEFjY2Vzc29yLFxuICAgICAgICB7XG4gICAgICAgICAgZGVsZXRlOiB0aGlzLm9uRGVsZXRlLmJpbmQodGhpcyksXG4gICAgICAgICAgdXBkYXRlOiB0aGlzLm9uVXBkYXRlLmJpbmQodGhpcylcbiAgICAgICAgfVxuICAgICAgKTtcbiAgICB9XG4gIH1cbn0iXX0=