/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { DynamicComponent } from "../dynamic/dynamic.component";
import { EventEmitter, Output } from "@angular/core";
/**
 * @abstract
 * @template DataType, CrudType
 */
export class DynamicCrudComponent extends DynamicComponent {
    constructor() {
        super(...arguments);
        this.deleteEmitter = new EventEmitter();
        this.updateEmitter = new EventEmitter();
        this.addEmitter = new EventEmitter();
    }
    /**
     * @param {?} name
     * @param {?} callback
     * @return {?}
     */
    subscribe(name, callback) {
        switch (name) {
            case 'update':
                this.updateEmitter.subscribe(callback);
                break;
            case 'delete':
                this.deleteEmitter.subscribe(callback);
                break;
            case 'add':
                this.addEmitter.subscribe(callback);
                break;
            default:
                throw 'callback type \'' + name + '\' is not a known event callback !';
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onDelete(event) {
        this.deleteEmitter.emit(event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onUpdate(event) {
        this.updateEmitter.emit(event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onAdd(event) {
        this.addEmitter.emit(event);
    }
}
DynamicCrudComponent.propDecorators = {
    deleteEmitter: [{ type: Output, args: ['delete',] }],
    updateEmitter: [{ type: Output, args: ['update',] }],
    addEmitter: [{ type: Output, args: ['add',] }]
};
function DynamicCrudComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    DynamicCrudComponent.prototype.deleteEmitter;
    /** @type {?} */
    DynamicCrudComponent.prototype.updateEmitter;
    /** @type {?} */
    DynamicCrudComponent.prototype.addEmitter;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy1jcnVkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdmlld3MvZHluYW1pYy1jcnVkL2R5bmFtaWMtY3J1ZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ2hFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7OztBQUVyRCxNQUFNLDJCQUF5RCxTQUFRLGdCQUEwQjs7OzZCQXFCOUMsSUFBSSxZQUFZLEVBQVk7NkJBTzVCLElBQUksWUFBWSxFQUFZOzBCQU8vQixJQUFJLFlBQVksRUFBWTs7Ozs7OztJQWpDbkUsU0FBUyxDQUFDLElBQWEsRUFBRSxRQUFtQjtRQUVqRCxNQUFNLENBQUEsQ0FBQyxJQUFJLENBQUMsQ0FDWixDQUFDO1lBQ0MsS0FBSyxRQUFRO2dCQUNYLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN2QyxLQUFLLENBQUM7WUFDUixLQUFLLFFBQVE7Z0JBQ1gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3ZDLEtBQUssQ0FBQztZQUNSLEtBQUssS0FBSztnQkFDUixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDcEMsS0FBSyxDQUFDO1lBQ1I7Z0JBQ0UsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLEdBQUcsb0NBQW9DLENBQUM7U0FDMUU7Ozs7OztJQUtPLFFBQVEsQ0FBQyxLQUFlO1FBRWhDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ2hDOzs7OztJQUlTLFFBQVEsQ0FBQyxLQUFlO1FBRWhDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ2hDOzs7OztJQUlTLEtBQUssQ0FBQyxLQUFlO1FBRTdCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQzdCOzs7NEJBbkJBLE1BQU0sU0FBQyxRQUFROzRCQU9mLE1BQU0sU0FBQyxRQUFRO3lCQU9mLE1BQU0sU0FBQyxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudCB9IGZyb20gXCIuLi9keW5hbWljL2R5bmFtaWMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBFdmVudEVtaXR0ZXIsIE91dHB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBEeW5hbWljQ3J1ZENvbXBvbmVudDxEYXRhVHlwZSwgQ3J1ZFR5cGU+IGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudDxEYXRhVHlwZT5cbntcbiAgcHVibGljIHN1YnNjcmliZShuYW1lIDogc3RyaW5nLCBjYWxsYmFjayA6IEZ1bmN0aW9uKSA6IHZvaWRcbiAge1xuICAgIHN3aXRjaChuYW1lKVxuICAgIHtcbiAgICAgIGNhc2UgJ3VwZGF0ZSc6XG4gICAgICAgIHRoaXMudXBkYXRlRW1pdHRlci5zdWJzY3JpYmUoY2FsbGJhY2spO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ2RlbGV0ZSc6XG4gICAgICAgIHRoaXMuZGVsZXRlRW1pdHRlci5zdWJzY3JpYmUoY2FsbGJhY2spO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ2FkZCc6XG4gICAgICAgIHRoaXMuYWRkRW1pdHRlci5zdWJzY3JpYmUoY2FsbGJhY2spO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHRocm93ICdjYWxsYmFjayB0eXBlIFxcJycgKyBuYW1lICsgJ1xcJyBpcyBub3QgYSBrbm93biBldmVudCBjYWxsYmFjayAhJztcbiAgICB9XG4gIH1cblxuICBAT3V0cHV0KCdkZWxldGUnKVxuICBwcml2YXRlIGRlbGV0ZUVtaXR0ZXIgOiBFdmVudEVtaXR0ZXI8Q3J1ZFR5cGU+ID0gbmV3IEV2ZW50RW1pdHRlcjxDcnVkVHlwZT4oKTtcbiAgcHJvdGVjdGVkIG9uRGVsZXRlKGV2ZW50OiBDcnVkVHlwZSlcbiAge1xuICAgIHRoaXMuZGVsZXRlRW1pdHRlci5lbWl0KGV2ZW50KTtcbiAgfVxuXG4gIEBPdXRwdXQoJ3VwZGF0ZScpXG4gIHByaXZhdGUgdXBkYXRlRW1pdHRlciA6IEV2ZW50RW1pdHRlcjxDcnVkVHlwZT4gPSBuZXcgRXZlbnRFbWl0dGVyPENydWRUeXBlPigpO1xuICBwcm90ZWN0ZWQgb25VcGRhdGUoZXZlbnQ6IENydWRUeXBlKVxuICB7XG4gICAgdGhpcy51cGRhdGVFbWl0dGVyLmVtaXQoZXZlbnQpO1xuICB9XG5cbiAgQE91dHB1dCgnYWRkJylcbiAgcHJpdmF0ZSBhZGRFbWl0dGVyIDogRXZlbnRFbWl0dGVyPENydWRUeXBlPiA9IG5ldyBFdmVudEVtaXR0ZXI8Q3J1ZFR5cGU+KCk7XG4gIHByb3RlY3RlZCBvbkFkZChldmVudDogQ3J1ZFR5cGUpXG4gIHtcbiAgICB0aGlzLmFkZEVtaXR0ZXIuZW1pdChldmVudCk7XG4gIH1cbn0iXX0=