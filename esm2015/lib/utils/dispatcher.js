/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
let /** @type {?} */ moment = require('moment');
/**
 * Dispatches events in an array of dates depending
 * based on the date of each event.
 */
export class EventDispatcher {
    /**
     * Creates a new event dispatcher
     * @param {?} dateAccessor the date label to use in the events
     * @param {?} startDate the start date of the period in which to dispatch events
     * @param {?} endDate the end date of the period (inclusive) in which to dispatch events
     */
    constructor(dateAccessor, startDate, endDate) {
        this.dateAccessor = dateAccessor;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    /**
     * @return {?}
     */
    clear() {
        this.dispatch = [];
        for (let /** @type {?} */ i = 0; i < this.getTotalNumberOfDays(); i++) {
            this.dispatch[i] = [];
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    add(event) {
        let /** @type {?} */ date = event[this.dateAccessor];
        let /** @type {?} */ dispatchIndex = this.getDayNumber(date);
        if (this.dispatch[dispatchIndex]) {
            this.dispatch[dispatchIndex].push(event);
        }
    }
    /**
     * @param {?} date
     * @return {?}
     */
    getEvents(date) {
        return this.dispatch[this.getDayNumber(date)];
    }
    /**
     * @param {?} startDate
     * @param {?} endDate
     * @return {?}
     */
    getEventsBetween(startDate, endDate) {
        return this.dispatch.slice(this.getDayNumber(startDate), this.getDayNumber(endDate) + 1);
    }
    /**
     * Returns an array of the dates
     * between startDate and endDate.
     * @return {?}
     */
    getDateArray() {
        let /** @type {?} */ days = [];
        let /** @type {?} */ current = moment(this.startDate);
        let /** @type {?} */ last = moment(this.endDate);
        while (current < last) {
            days.push(current.toDate());
            current = current.add(1, 'day');
        }
        return days;
    }
    /**
     * @return {?}
     */
    getWeekArray() {
        let /** @type {?} */ weeks = [];
        let /** @type {?} */ current = moment(this.startDate);
        let /** @type {?} */ last = moment(this.endDate);
        while (current < last) {
            weeks.push({ startDate: current.toDate(), endDate: current.add(6, 'day').toDate() });
            current = current.add(1, 'day');
        }
        return weeks;
    }
    /**
     * Returns the index of a date in
     * the [startDate - endDate] period.
     * @param {?} date the date for which to compute the index
     * @return {?}
     */
    getDayNumber(date) {
        return moment(date).diff(moment(this.startDate), 'days');
    }
    /**
     * Returns the total number of days
     * in the [startDate - endDate] period.
     * @return {?}
     */
    getTotalNumberOfDays() {
        return this.getDayNumber(this.endDate) + 1;
    }
}
function EventDispatcher_tsickle_Closure_declarations() {
    /** @type {?} */
    EventDispatcher.prototype.dispatch;
    /** @type {?} */
    EventDispatcher.prototype.dateAccessor;
    /** @type {?} */
    EventDispatcher.prototype.startDate;
    /** @type {?} */
    EventDispatcher.prototype.endDate;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzcGF0Y2hlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdXRpbHMvZGlzcGF0Y2hlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEscUJBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Ozs7QUFNL0IsTUFBTTs7Ozs7OztnQkFVK0IsWUFBcUIsRUFBa0IsU0FBZ0IsRUFBa0IsT0FBYztRQUF2RixpQkFBWSxHQUFaLFlBQVksQ0FBUztRQUFrQixjQUFTLEdBQVQsU0FBUyxDQUFPO1FBQWtCLFlBQU8sR0FBUCxPQUFPLENBQU87Ozs7O0lBRW5ILEtBQUs7UUFFVixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNuQixHQUFHLENBQUMsQ0FBQyxxQkFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFDcEQsQ0FBQztZQUNDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1NBQ3ZCOzs7Ozs7SUFHSSxHQUFHLENBQUMsS0FBVztRQUVwQixxQkFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNwQyxxQkFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQ2pDLENBQUM7WUFDQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMxQzs7Ozs7O0lBR0ksU0FBUyxDQUFDLElBQVc7UUFFMUIsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDOzs7Ozs7O0lBR3pDLGdCQUFnQixDQUFDLFNBQWdCLEVBQUUsT0FBYztRQUV0RCxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDOzs7Ozs7O0lBT3BGLFlBQVk7UUFFakIscUJBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNkLHFCQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3JDLHFCQUFJLElBQUksR0FBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2pDLE9BQU0sT0FBTyxHQUFHLElBQUksRUFDcEIsQ0FBQztZQUNDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7WUFDNUIsT0FBTyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ2pDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQzs7Ozs7SUFHUCxZQUFZO1FBRWpCLHFCQUFJLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDZixxQkFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNyQyxxQkFBSSxJQUFJLEdBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNqQyxPQUFNLE9BQU8sR0FBRyxJQUFJLEVBQ3BCLENBQUM7WUFDQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ3JGLE9BQU8sR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUNqQztRQUNELE1BQU0sQ0FBQyxLQUFLLENBQUM7Ozs7Ozs7O0lBUVIsWUFBWSxDQUFDLElBQVc7UUFFN0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQzs7Ozs7OztJQU9wRCxvQkFBb0I7UUFFekIsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQzs7Q0FFOUMiLCJzb3VyY2VzQ29udGVudCI6WyJsZXQgbW9tZW50ID0gcmVxdWlyZSgnbW9tZW50Jyk7XG5cbi8qKlxuICogRGlzcGF0Y2hlcyBldmVudHMgaW4gYW4gYXJyYXkgb2YgZGF0ZXMgZGVwZW5kaW5nXG4gKiBiYXNlZCBvbiB0aGUgZGF0ZSBvZiBlYWNoIGV2ZW50LlxuICovXG5leHBvcnQgY2xhc3MgRXZlbnREaXNwYXRjaGVyXG57XG4gIHByaXZhdGUgZGlzcGF0Y2ggOiBhbnlbXVtdO1xuXG4gIC8qKlxuICAgKiBDcmVhdGVzIGEgbmV3IGV2ZW50IGRpc3BhdGNoZXJcbiAgICogQHBhcmFtIGRhdGVBY2Nlc3NvciB0aGUgZGF0ZSBsYWJlbCB0byB1c2UgaW4gdGhlIGV2ZW50c1xuICAgKiBAcGFyYW0gc3RhcnREYXRlIHRoZSBzdGFydCBkYXRlIG9mIHRoZSBwZXJpb2QgaW4gd2hpY2ggdG8gZGlzcGF0Y2ggZXZlbnRzXG4gICAqIEBwYXJhbSBlbmREYXRlIHRoZSBlbmQgZGF0ZSBvZiB0aGUgcGVyaW9kIChpbmNsdXNpdmUpIGluIHdoaWNoIHRvIGRpc3BhdGNoIGV2ZW50c1xuICAgKi9cbiAgcHVibGljIGNvbnN0cnVjdG9yKHB1YmxpYyByZWFkb25seSBkYXRlQWNjZXNzb3IgOiBzdHJpbmcsIHB1YmxpYyByZWFkb25seSBzdGFydERhdGUgOiBEYXRlLCBwdWJsaWMgcmVhZG9ubHkgZW5kRGF0ZSA6IERhdGUpIHsgfVxuXG4gIHB1YmxpYyBjbGVhcigpIDogdm9pZFxuICB7XG4gICAgdGhpcy5kaXNwYXRjaCA9IFtdO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5nZXRUb3RhbE51bWJlck9mRGF5cygpOyBpKyspXG4gICAge1xuICAgICAgdGhpcy5kaXNwYXRjaFtpXSA9IFtdO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBhZGQoZXZlbnQgOiBhbnkpXG4gIHtcbiAgICBsZXQgZGF0ZSA9IGV2ZW50W3RoaXMuZGF0ZUFjY2Vzc29yXTtcbiAgICBsZXQgZGlzcGF0Y2hJbmRleCA9IHRoaXMuZ2V0RGF5TnVtYmVyKGRhdGUpO1xuICAgIGlmICh0aGlzLmRpc3BhdGNoW2Rpc3BhdGNoSW5kZXhdKVxuICAgIHtcbiAgICAgIHRoaXMuZGlzcGF0Y2hbZGlzcGF0Y2hJbmRleF0ucHVzaChldmVudCk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGdldEV2ZW50cyhkYXRlIDogRGF0ZSkgOiBhbnlbXVxuICB7XG4gICAgcmV0dXJuIHRoaXMuZGlzcGF0Y2hbdGhpcy5nZXREYXlOdW1iZXIoZGF0ZSldO1xuICB9XG5cbiAgcHVibGljIGdldEV2ZW50c0JldHdlZW4oc3RhcnREYXRlIDogRGF0ZSwgZW5kRGF0ZSA6IERhdGUpXG4gIHtcbiAgICByZXR1cm4gdGhpcy5kaXNwYXRjaC5zbGljZSh0aGlzLmdldERheU51bWJlcihzdGFydERhdGUpLCB0aGlzLmdldERheU51bWJlcihlbmREYXRlKSArIDEpO1xuICB9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgYW4gYXJyYXkgb2YgdGhlIGRhdGVzXG4gICAqIGJldHdlZW4gc3RhcnREYXRlIGFuZCBlbmREYXRlLlxuICAgKi9cbiAgcHVibGljIGdldERhdGVBcnJheSgpXG4gIHtcbiAgICBsZXQgZGF5cyA9IFtdO1xuICAgIGxldCBjdXJyZW50ID0gbW9tZW50KHRoaXMuc3RhcnREYXRlKTtcbiAgICBsZXQgbGFzdCAgPSBtb21lbnQodGhpcy5lbmREYXRlKTtcbiAgICB3aGlsZShjdXJyZW50IDwgbGFzdClcbiAgICB7XG4gICAgICBkYXlzLnB1c2goY3VycmVudC50b0RhdGUoKSk7XG4gICAgICBjdXJyZW50ID0gY3VycmVudC5hZGQoMSwgJ2RheScpO1xuICAgIH1cbiAgICByZXR1cm4gZGF5cztcbiAgfVxuXG4gIHB1YmxpYyBnZXRXZWVrQXJyYXkoKVxuICB7XG4gICAgbGV0IHdlZWtzID0gW107XG4gICAgbGV0IGN1cnJlbnQgPSBtb21lbnQodGhpcy5zdGFydERhdGUpO1xuICAgIGxldCBsYXN0ICA9IG1vbWVudCh0aGlzLmVuZERhdGUpO1xuICAgIHdoaWxlKGN1cnJlbnQgPCBsYXN0KVxuICAgIHtcbiAgICAgIHdlZWtzLnB1c2goeyBzdGFydERhdGU6IGN1cnJlbnQudG9EYXRlKCksIGVuZERhdGU6IGN1cnJlbnQuYWRkKDYsICdkYXknKS50b0RhdGUoKSB9KTtcbiAgICAgIGN1cnJlbnQgPSBjdXJyZW50LmFkZCgxLCAnZGF5Jyk7XG4gICAgfVxuICAgIHJldHVybiB3ZWVrcztcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSBpbmRleCBvZiBhIGRhdGUgaW5cbiAgICogdGhlIFtzdGFydERhdGUgLSBlbmREYXRlXSBwZXJpb2QuXG4gICAqIEBwYXJhbSBkYXRlIHRoZSBkYXRlIGZvciB3aGljaCB0byBjb21wdXRlIHRoZSBpbmRleFxuICAgKi9cbiAgcHVibGljIGdldERheU51bWJlcihkYXRlIDogRGF0ZSlcbiAge1xuICAgIHJldHVybiBtb21lbnQoZGF0ZSkuZGlmZihtb21lbnQodGhpcy5zdGFydERhdGUpLCAnZGF5cycpO1xuICB9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdGhlIHRvdGFsIG51bWJlciBvZiBkYXlzXG4gICAqIGluIHRoZSBbc3RhcnREYXRlIC0gZW5kRGF0ZV0gcGVyaW9kLlxuICAgKi9cbiAgcHVibGljIGdldFRvdGFsTnVtYmVyT2ZEYXlzKClcbiAge1xuICAgIHJldHVybiB0aGlzLmdldERheU51bWJlcih0aGlzLmVuZERhdGUpICsgMTtcbiAgfVxufSJdfQ==