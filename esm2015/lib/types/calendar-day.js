/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @record
 * @template DataType
 */
export function CalendarDayData() { }
function CalendarDayData_tsickle_Closure_declarations() {
    /** @type {?} */
    CalendarDayData.prototype.date;
    /** @type {?} */
    CalendarDayData.prototype.calendarStartDate;
    /** @type {?} */
    CalendarDayData.prototype.calendarEndDate;
    /** @type {?} */
    CalendarDayData.prototype.events;
    /** @type {?} */
    CalendarDayData.prototype.dateAccessor;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FsZW5kYXItZGF5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci8iLCJzb3VyY2VzIjpbImxpYi90eXBlcy9jYWxlbmRhci1kYXkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgQ2FsZW5kYXJEYXlEYXRhPERhdGFUeXBlPlxue1xuICBkYXRlOiBEYXRlO1xuICBjYWxlbmRhclN0YXJ0RGF0ZTogRGF0ZTtcbiAgY2FsZW5kYXJFbmREYXRlOiBEYXRlO1xuICBldmVudHM6IERhdGFUeXBlW107XG4gIGRhdGVBY2Nlc3Nvcjogc3RyaW5nO1xufSJdfQ==