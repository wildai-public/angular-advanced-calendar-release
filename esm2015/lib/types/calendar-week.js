/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @record
 * @template DataType
 */
export function CalendarWeekData() { }
function CalendarWeekData_tsickle_Closure_declarations() {
    /** @type {?} */
    CalendarWeekData.prototype.startDate;
    /** @type {?} */
    CalendarWeekData.prototype.endDate;
    /** @type {?} */
    CalendarWeekData.prototype.calendarStartDate;
    /** @type {?} */
    CalendarWeekData.prototype.calendarEndDate;
    /** @type {?} */
    CalendarWeekData.prototype.events;
    /** @type {?} */
    CalendarWeekData.prototype.dateAccessor;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FsZW5kYXItd2Vlay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdHlwZXMvY2FsZW5kYXItd2Vlay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBDYWxlbmRhcldlZWtEYXRhPERhdGFUeXBlPlxue1xuICBzdGFydERhdGU6IERhdGU7XG4gIGVuZERhdGU6IERhdGU7XG4gIGNhbGVuZGFyU3RhcnREYXRlOiBEYXRlO1xuICBjYWxlbmRhckVuZERhdGU6IERhdGU7XG4gIGV2ZW50czogRGF0YVR5cGVbXVtdO1xuICBkYXRlQWNjZXNzb3I6IHN0cmluZztcbn0iXX0=