/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @record
 * @template T
 */
export function NumericMap() { }
function NumericMap_tsickle_Closure_declarations() {
    /* TODO: handle strange member:
    [id: number]: T;
    */
}
/**
 * @record
 * @template DataType
 */
export function StringMap() { }
function StringMap_tsickle_Closure_declarations() {
    /* TODO: handle strange member:
    [name: string]: DataType;
    */
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvdHlwZXMvbWFwcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBOdW1lcmljTWFwPFQ+XG57XG4gIFtpZDogbnVtYmVyXTogVDtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBTdHJpbmdNYXA8RGF0YVR5cGU+XG57XG4gIFtuYW1lOiBzdHJpbmddOiBEYXRhVHlwZTtcbn0iXX0=