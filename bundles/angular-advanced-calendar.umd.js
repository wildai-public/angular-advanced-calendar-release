(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('angular-advanced-calendar', ['exports', '@angular/core', '@angular/common'], factory) :
    (factory((global['angular-advanced-calendar'] = {}),global.ng.core,global.ng.common));
}(this, (function (exports,core,common) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (b.hasOwnProperty(p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m)
            return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length)
                    o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var /** @type {?} */ moment = require('moment');
    /**
     * @param {?} a
     * @param {?} b
     * @return {?}
     */
    function isSameDay(a, b) {
        return a && b && a.getFullYear() === b.getFullYear() && a.getMonth() === b.getMonth() && a.getDate() === b.getDate();
    }
    /**
     * @param {?} date
     * @return {?}
     */
    function getWeekNumber(date) {
        return moment(date).week();
    }
    /**
     * @param {?} date
     * @return {?}
     */
    function getFirstDayOfMonth(date) {
        return moment(date).startOf('month').toDate();
    }
    /**
     * @param {?} date
     * @return {?}
     */
    function getFirstDayOfFirstWeekOfMonth(date) {
        return moment(date).startOf('month').startOf('isoWeek');
    }
    /**
     * @param {?} date
     * @return {?}
     */
    function getLastDayOfMonth(date) {
        return moment(date).endOf('month').toDate();
    }
    /**
     * @param {?} date
     * @return {?}
     */
    function geLastDayOfLastWeekOfMonth(date) {
        return moment(date).endOf('month').endOf('isoWeek').toDate();
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var /** @type {?} */ moment$1 = require('moment');
    /**
     * Dispatches events in an array of dates depending
     * based on the date of each event.
     */
    var /**
     * Dispatches events in an array of dates depending
     * based on the date of each event.
     */ EventDispatcher = (function () {
        function EventDispatcher(dateAccessor, startDate, endDate) {
            this.dateAccessor = dateAccessor;
            this.startDate = startDate;
            this.endDate = endDate;
        }
        /**
         * @return {?}
         */
        EventDispatcher.prototype.clear = /**
         * @return {?}
         */
            function () {
                this.dispatch = [];
                for (var /** @type {?} */ i = 0; i < this.getTotalNumberOfDays(); i++) {
                    this.dispatch[i] = [];
                }
            };
        /**
         * @param {?} event
         * @return {?}
         */
        EventDispatcher.prototype.add = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                var /** @type {?} */ date = event[this.dateAccessor];
                var /** @type {?} */ dispatchIndex = this.getDayNumber(date);
                if (this.dispatch[dispatchIndex]) {
                    this.dispatch[dispatchIndex].push(event);
                }
            };
        /**
         * @param {?} date
         * @return {?}
         */
        EventDispatcher.prototype.getEvents = /**
         * @param {?} date
         * @return {?}
         */
            function (date) {
                return this.dispatch[this.getDayNumber(date)];
            };
        /**
         * @param {?} startDate
         * @param {?} endDate
         * @return {?}
         */
        EventDispatcher.prototype.getEventsBetween = /**
         * @param {?} startDate
         * @param {?} endDate
         * @return {?}
         */
            function (startDate, endDate) {
                return this.dispatch.slice(this.getDayNumber(startDate), this.getDayNumber(endDate) + 1);
            };
        /**
         * Returns an array of the dates
         * between startDate and endDate.
         * @return {?}
         */
        EventDispatcher.prototype.getDateArray = /**
         * Returns an array of the dates
         * between startDate and endDate.
         * @return {?}
         */
            function () {
                var /** @type {?} */ days = [];
                var /** @type {?} */ current = moment$1(this.startDate);
                var /** @type {?} */ last = moment$1(this.endDate);
                while (current < last) {
                    days.push(current.toDate());
                    current = current.add(1, 'day');
                }
                return days;
            };
        /**
         * @return {?}
         */
        EventDispatcher.prototype.getWeekArray = /**
         * @return {?}
         */
            function () {
                var /** @type {?} */ weeks = [];
                var /** @type {?} */ current = moment$1(this.startDate);
                var /** @type {?} */ last = moment$1(this.endDate);
                while (current < last) {
                    weeks.push({ startDate: current.toDate(), endDate: current.add(6, 'day').toDate() });
                    current = current.add(1, 'day');
                }
                return weeks;
            };
        /**
         * Returns the index of a date in
         * the [startDate - endDate] period.
         * @param {?} date the date for which to compute the index
         * @return {?}
         */
        EventDispatcher.prototype.getDayNumber = /**
         * Returns the index of a date in
         * the [startDate - endDate] period.
         * @param {?} date the date for which to compute the index
         * @return {?}
         */
            function (date) {
                return moment$1(date).diff(moment$1(this.startDate), 'days');
            };
        /**
         * Returns the total number of days
         * in the [startDate - endDate] period.
         * @return {?}
         */
        EventDispatcher.prototype.getTotalNumberOfDays = /**
         * Returns the total number of days
         * in the [startDate - endDate] period.
         * @return {?}
         */
            function () {
                return this.getDayNumber(this.endDate) + 1;
            };
        return EventDispatcher;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    // unsupported: template constraints.
    /**
     * @abstract
     * @template DataType, ComponentType
     */
    var  
    // unsupported: template constraints.
    /**
     * @abstract
     * @template DataType, ComponentType
     */
    DynamicComponentFactory = (function () {
        function DynamicComponentFactory(componentFactoryResolver, componentType) {
            this.componentFactoryResolver = componentFactoryResolver;
            this.factory = this.componentFactoryResolver.resolveComponentFactory(componentType);
        }
        /**
         * Inserts a component in a view container
         * @param {?} container the container in which we want to display this component
         * @param {?} data the data of the component to display
         * @param {?=} callbacks a map of callbacks to subscribe to
         * @param {?=} index the position in the container where to add the component
         * @return {?}
         */
        DynamicComponentFactory.prototype.insertDynamicComponent = /**
         * Inserts a component in a view container
         * @param {?} container the container in which we want to display this component
         * @param {?} data the data of the component to display
         * @param {?=} callbacks a map of callbacks to subscribe to
         * @param {?=} index the position in the container where to add the component
         * @return {?}
         */
            function (container, data, callbacks, index) {
                var /** @type {?} */ componentRef = container.createComponent(this.factory, index);
                var /** @type {?} */ instance = (componentRef.instance);
                instance.updateData(data);
                if (callbacks) {
                    for (var /** @type {?} */ name_1 in callbacks) {
                        instance.subscribe(name_1, callbacks[name_1]);
                    }
                }
                return componentRef;
            };
        return DynamicComponentFactory;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     * @template DataType
     */
    var /**
     * @abstract
     * @template DataType
     */ EventComponentFactory = (function (_super) {
        __extends(EventComponentFactory, _super);
        function EventComponentFactory(componentFactoryResolver, componentType) {
            return _super.call(this, componentFactoryResolver, componentType) || this;
        }
        return EventComponentFactory;
    }(DynamicComponentFactory));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     * @template DataType
     */
    var /**
     * @abstract
     * @template DataType
     */ DayCellComponentFactory = (function (_super) {
        __extends(DayCellComponentFactory, _super);
        function DayCellComponentFactory(componentFactoryResolver, componentType) {
            return _super.call(this, componentFactoryResolver, componentType) || this;
        }
        return DayCellComponentFactory;
    }(DynamicComponentFactory));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    // unsupported: template constraints.
    /**
     * @template DataType, ComponentType
     */
    var 
    // unsupported: template constraints.
    /**
     * @template DataType, ComponentType
     */
    DynamicComponentArrayManager = (function () {
        function DynamicComponentArrayManager() {
            /**
             * Stores the previous array state
             */
            this.oldDataArray = [];
            /**
             * A numeric map used to store the dynamic views
             */
            this.dynamicViewComponentMap = {};
        }
        /**
         * @return {?}
         */
        DynamicComponentArrayManager.prototype.clear = /**
         * @return {?}
         */
            function () {
                for (var /** @type {?} */ id in this.dynamicViewComponentMap) {
                    if (this.dynamicViewComponentMap[id]) {
                        this.dynamicViewComponentMap[id].destroy();
                        delete this.dynamicViewComponentMap[id];
                    }
                }
                this.oldDataArray = [];
            };
        /**
         * Updates, add or remove data views based
         * on the old data list
         * @param {?} container
         * @param {?} factory
         * @param {?} dataArray
         * @param {?} dataIdAccessor
         * @param {?=} dynamicComponentCallbacks
         * @param {?=} onAddView
         * @param {?=} onUpdateView
         * @param {?=} onDeleteView
         * @return {?}
         */
        DynamicComponentArrayManager.prototype.updateContainer = /**
         * Updates, add or remove data views based
         * on the old data list
         * @param {?} container
         * @param {?} factory
         * @param {?} dataArray
         * @param {?} dataIdAccessor
         * @param {?=} dynamicComponentCallbacks
         * @param {?=} onAddView
         * @param {?=} onUpdateView
         * @param {?=} onDeleteView
         * @return {?}
         */
            function (container, factory, dataArray, dataIdAccessor, dynamicComponentCallbacks, onAddView, onUpdateView, onDeleteView) {
                if (!container) {
                    throw 'child container (#childContainer) could not be found in the container view';
                }
                try {
                    for (var dataArray_1 = __values(dataArray), dataArray_1_1 = dataArray_1.next(); !dataArray_1_1.done; dataArray_1_1 = dataArray_1.next()) {
                        var data = dataArray_1_1.value;
                        var /** @type {?} */ alreadyPresent = false;
                        try {
                            for (var _a = __values(this.oldDataArray), _b = _a.next(); !_b.done; _b = _a.next()) {
                                var oldData = _b.value;
                                if (data[dataIdAccessor] === oldData[dataIdAccessor]) {
                                    alreadyPresent = true;
                                    break;
                                }
                            }
                        }
                        catch (e_1_1) {
                            e_1 = { error: e_1_1 };
                        }
                        finally {
                            try {
                                if (_b && !_b.done && (_c = _a.return))
                                    _c.call(_a);
                            }
                            finally {
                                if (e_1)
                                    throw e_1.error;
                            }
                        }
                        if (alreadyPresent) {
                            this.updateDynamicComponent(container, data, dataIdAccessor, onUpdateView);
                        }
                        else {
                            this.addDynamicComponent(container, factory, data, dataIdAccessor, dynamicComponentCallbacks, onAddView);
                        }
                    }
                }
                catch (e_2_1) {
                    e_2 = { error: e_2_1 };
                }
                finally {
                    try {
                        if (dataArray_1_1 && !dataArray_1_1.done && (_d = dataArray_1.return))
                            _d.call(dataArray_1);
                    }
                    finally {
                        if (e_2)
                            throw e_2.error;
                    }
                }
                try {
                    for (var _e = __values(this.oldDataArray), _f = _e.next(); !_f.done; _f = _e.next()) {
                        var oldData = _f.value;
                        var /** @type {?} */ stillPresent = false;
                        try {
                            for (var dataArray_2 = __values(dataArray), dataArray_2_1 = dataArray_2.next(); !dataArray_2_1.done; dataArray_2_1 = dataArray_2.next()) {
                                var data = dataArray_2_1.value;
                                if (data[dataIdAccessor] === oldData[dataIdAccessor]) {
                                    stillPresent = true;
                                    break;
                                }
                            }
                        }
                        catch (e_3_1) {
                            e_3 = { error: e_3_1 };
                        }
                        finally {
                            try {
                                if (dataArray_2_1 && !dataArray_2_1.done && (_g = dataArray_2.return))
                                    _g.call(dataArray_2);
                            }
                            finally {
                                if (e_3)
                                    throw e_3.error;
                            }
                        }
                        if (!stillPresent) {
                            this.removeDynamicComponent(container, oldData, dataIdAccessor, onDeleteView);
                        }
                    }
                }
                catch (e_4_1) {
                    e_4 = { error: e_4_1 };
                }
                finally {
                    try {
                        if (_f && !_f.done && (_h = _e.return))
                            _h.call(_e);
                    }
                    finally {
                        if (e_4)
                            throw e_4.error;
                    }
                }
                this.oldDataArray = dataArray;
                var e_2, _d, e_1, _c, e_4, _h, e_3, _g;
            };
        /**
         * @param {?} container
         * @param {?} data
         * @param {?} dataIdAccessor
         * @param {?=} onDeleteView
         * @return {?}
         */
        DynamicComponentArrayManager.prototype.removeDynamicComponent = /**
         * @param {?} container
         * @param {?} data
         * @param {?} dataIdAccessor
         * @param {?=} onDeleteView
         * @return {?}
         */
            function (container, data, dataIdAccessor, onDeleteView) {
                if (typeof data[dataIdAccessor] !== 'number') {
                    throw 'id (' + dataIdAccessor + ') not found in data structure (delete dynamic component method)';
                }
                else if (!this.dynamicViewComponentMap[data[dataIdAccessor]]) {
                    throw 'dynamic view not found in the dynamic view container (delete dynamic component method)';
                }
                else {
                    this.dynamicViewComponentMap[data[dataIdAccessor]].destroy();
                    delete this.dynamicViewComponentMap[data[dataIdAccessor]];
                    if (onDeleteView)
                        onDeleteView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
                }
            };
        /**
         * @param {?} container
         * @param {?} factory
         * @param {?} data
         * @param {?} dataIdAccessor
         * @param {?} dynamicComponentCallbacks
         * @param {?=} onAddView
         * @return {?}
         */
        DynamicComponentArrayManager.prototype.addDynamicComponent = /**
         * @param {?} container
         * @param {?} factory
         * @param {?} data
         * @param {?} dataIdAccessor
         * @param {?} dynamicComponentCallbacks
         * @param {?=} onAddView
         * @return {?}
         */
            function (container, factory, data, dataIdAccessor, dynamicComponentCallbacks, onAddView) {
                if (typeof data[dataIdAccessor] !== 'number') {
                    throw 'id (' + dataIdAccessor + ') not found in data structure (add dynamic component method)';
                }
                else if (this.dynamicViewComponentMap[data[dataIdAccessor]]) {
                    throw 'dynamic view already present in the dynamic view container (add dynamic component method)';
                }
                else {
                    var /** @type {?} */ componentRef = factory.insertDynamicComponent(container, data, dynamicComponentCallbacks);
                    this.dynamicViewComponentMap[data[dataIdAccessor]] = componentRef;
                    if (onAddView)
                        onAddView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
                }
            };
        /**
         * Updates an data view model
         * @param {?} container
         * @param {?} data the new data model to use
         * @param {?} dataIdAccessor
         * @param {?=} onUpdateView
         * @return {?}
         */
        DynamicComponentArrayManager.prototype.updateDynamicComponent = /**
         * Updates an data view model
         * @param {?} container
         * @param {?} data the new data model to use
         * @param {?} dataIdAccessor
         * @param {?=} onUpdateView
         * @return {?}
         */
            function (container, data, dataIdAccessor, onUpdateView) {
                if (typeof data[dataIdAccessor] !== 'number') {
                    throw 'id (' + dataIdAccessor + ') not found in data structure (add dynamic component method)';
                }
                else if (!this.dynamicViewComponentMap[data[dataIdAccessor]]) {
                    throw 'dynamic view not found in the dynamic view container (add dynamic component method)';
                }
                else {
                    this.dynamicViewComponentMap[data[dataIdAccessor]].instance.updateData(data);
                    if (onUpdateView)
                        onUpdateView(data, this.dynamicViewComponentMap[data[dataIdAccessor]]);
                }
            };
        return DynamicComponentArrayManager;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     * @template DataType
     */
    var /**
     * @abstract
     * @template DataType
     */ WeekSummaryComponentFactory = (function (_super) {
        __extends(WeekSummaryComponentFactory, _super);
        function WeekSummaryComponentFactory(componentFactoryResolver, componentType) {
            return _super.call(this, componentFactoryResolver, componentType) || this;
        }
        /**
         * @param {?} container
         * @param {?} data
         * @param {?=} callbacks
         * @return {?}
         */
        WeekSummaryComponentFactory.prototype.insertDynamicComponent = /**
         * @param {?} container
         * @param {?} data
         * @param {?=} callbacks
         * @return {?}
         */
            function (container, data, callbacks) {
                var /** @type {?} */ position = this.findWeekPositionInContainer(container, data);
                return (position >= 0) ? _super.prototype.insertDynamicComponent.call(this, container, data, callbacks, position) : undefined;
            };
        /**
         * @param {?} container
         * @param {?} data
         * @return {?}
         */
        WeekSummaryComponentFactory.prototype.findWeekPositionInContainer = /**
         * @param {?} container
         * @param {?} data
         * @return {?}
         */
            function (container, data) {
                var /** @type {?} */ index;
                for (index = 0; index < container.length; index++) {
                    var /** @type {?} */ componentInstance = container.get(index)['_view']['nodes'][1]['instance'];
                    if (componentInstance.data.date && isSameDay(componentInstance.data.date, data.endDate)) {
                        return index + 1;
                    }
                }
                return -1;
            };
        return WeekSummaryComponentFactory;
    }(DynamicComponentFactory));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var /** @type {?} */ moment$2 = require('moment');
    var /** @type {?} */ hash = require('object-hash');
    var /** @type {?} */ DAY_CELL_COMPONENT_FACTORY_PROVIDER = 'DayCellComponentFactory';
    var /** @type {?} */ EVENT_COMPONENT_FACTORY_PROVIDER = 'EventComponentFactory';
    var /** @type {?} */ WEEK_SUMMARY_COMPONENT_FACTORY = 'WeekSummaryComponentFactory';
    var CalendarComponent = (function () {
        function CalendarComponent(dayCellComponentFactory, eventComponentFactory, weekSummaryComponentFactory) {
            this.dayCellComponentFactory = dayCellComponentFactory;
            this.eventComponentFactory = eventComponentFactory;
            this.weekSummaryComponentFactory = weekSummaryComponentFactory;
            /**
             * Week days label to display in the header
             * (default is english).
             */
            this.headers = [
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday',
                'Sunday',
            ];
            /**
             * A list of events to display in the callendar.
             */
            this.events = [];
            /**
             * The name of the date attribute to use
             * when displaying the events (default is 'date').
             */
            this.dateAccessor = 'date';
            /**
             * The name of the id attribute to use
             * when displaying the events (default is 'id').
             */
            this.idAccessor = 'id';
            this.dynamicDayCellViewManager = new DynamicComponentArrayManager();
            this.dynamicWeekSummaryViewManager = new DynamicComponentArrayManager();
            this.oldHash = '';
            /**
             * Triggers a delete event emission
             * @param event the event to delete
             */
            this.deleteEmitter = new core.EventEmitter();
            /**
             * Triggers an update event emission
             * @param event the event to update
             */
            this.updateEmitter = new core.EventEmitter();
            /**
             * Triggers an add event emission
             * @param event the event to add
             */
            this.addEmitter = new core.EventEmitter();
        }
        /**
         * @return {?}
         */
        CalendarComponent.prototype.ngDoCheck = /**
         * @return {?}
         */
            function () {
                var /** @type {?} */ newHash = hash.sha1(this.events);
                if (newHash !== this.oldHash) {
                    this.updateCalendar();
                    this.oldHash = newHash;
                }
            };
        /**
         * @param {?} changes
         * @return {?}
         */
        CalendarComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                if (changes["startDate"] || changes["endDate"] || changes["dateAccessor"]) {
                    var /** @type {?} */ startDate = (changes["startDate"]) ? changes["startDate"].currentValue : this.startDate;
                    var /** @type {?} */ endDate = (changes["endDate"]) ? changes["endDate"].currentValue : this.endDate;
                    var /** @type {?} */ dateAccessor = (changes["dateAccessor"]) ? changes["dateAccessor"].currentValue : this.dateAccessor;
                    this.dispatcher = new EventDispatcher(dateAccessor, startDate, endDate);
                    this.updateCalendar();
                }
            };
        /**
         * @return {?}
         */
        CalendarComponent.prototype.updateCalendar = /**
         * @return {?}
         */
            function () {
                if (!this.dayCellComponentFactory) {
                    this.days = [];
                    throw 'day cell component factory (day-component-factory) was not set!';
                }
                else if (!this.dayGridContainer) {
                    this.days = [];
                    throw 'day grid (dayGrid) not found in template !';
                }
                else {
                    this.dispatchEvents();
                    this.updateDayCellViews();
                    if (this.weekSummaryComponentFactory) {
                        this.updateWeekSummaryCellViews();
                    }
                }
            };
        /**
         * Sorts events by date
         * @return {?}
         */
        CalendarComponent.prototype.dispatchEvents = /**
         * Sorts events by date
         * @return {?}
         */
            function () {
                var _this = this;
                this.dispatcher.clear();
                try {
                    for (var _a = __values(this.events), _b = _a.next(); !_b.done; _b = _a.next()) {
                        var event_1 = _b.value;
                        this.dispatcher.add(event_1);
                    }
                }
                catch (e_1_1) {
                    e_1 = { error: e_1_1 };
                }
                finally {
                    try {
                        if (_b && !_b.done && (_c = _a.return))
                            _c.call(_a);
                    }
                    finally {
                        if (e_1)
                            throw e_1.error;
                    }
                }
                this.days = this.dispatcher.getDateArray().map(function (date) {
                    return ({
                        id: _this.dispatcher.getDayNumber(date),
                        date: date,
                        calendarStartDate: _this.startDate,
                        calendarEndDate: _this.endDate,
                        events: _this.dispatcher.getEvents(date),
                        dateAccessor: _this.dateAccessor,
                    });
                });
                var e_1, _c;
            };
        /**
         * @return {?}
         */
        CalendarComponent.prototype.updateDayCellViews = /**
         * @return {?}
         */
            function () {
                this.dynamicDayCellViewManager.updateContainer(this.dayGridContainer, this.dayCellComponentFactory, this.days, 'id', {
                    add: this.onAdd.bind(this),
                    update: this.onUpdate.bind(this),
                    delete: this.onDelete.bind(this)
                }, this.updateDayCell.bind(this), this.updateDayCell.bind(this), this.updateDayCell.bind(this));
            };
        /**
         * @return {?}
         */
        CalendarComponent.prototype.updateWeekSummaryCellViews = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.weeks = this.dispatcher.getWeekArray().map(function (week) {
                    return ({
                        id: getWeekNumber(week.startDate),
                        startDate: week.startDate,
                        endDate: week.endDate,
                        calendarStartDate: _this.startDate,
                        calendarEndDate: _this.endDate,
                        events: _this.dispatcher.getEventsBetween(week.startDate, week.endDate),
                        dateAccessor: _this.dateAccessor,
                    });
                });
                if (this.startDate !== this.oldStartDate || this.endDate !== this.oldEndDate) {
                    this.dynamicWeekSummaryViewManager.clear();
                    this.oldStartDate = this.startDate;
                    this.oldEndDate = this.endDate;
                }
                this.dynamicWeekSummaryViewManager.updateContainer(this.dayGridContainer, this.weekSummaryComponentFactory, this.weeks, 'id', {
                    add: this.onAdd.bind(this),
                    update: this.onUpdate.bind(this),
                    delete: this.onDelete.bind(this)
                });
            };
        /**
         * Triggers a day cell view internal update
         * @param {?} data
         * @param {?} component the component ref of the day cell to update
         * @return {?}
         */
        CalendarComponent.prototype.updateDayCell = /**
         * Triggers a day cell view internal update
         * @param {?} data
         * @param {?} component the component ref of the day cell to update
         * @return {?}
         */
            function (data, component) {
                var /** @type {?} */ instance = (component.instance);
                instance.updateEventViews(this.eventComponentFactory, this.idAccessor);
            };
        /**
         * @param {?} event
         * @return {?}
         */
        CalendarComponent.prototype.onDelete = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                this.deleteEmitter.emit(event);
            };
        /**
         * @param {?} event
         * @return {?}
         */
        CalendarComponent.prototype.onUpdate = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                this.updateEmitter.emit(event);
            };
        /**
         * @param {?} event
         * @return {?}
         */
        CalendarComponent.prototype.onAdd = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                this.addEmitter.emit(event);
            };
        CalendarComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'angular-advanced-calendar',
                        template: "<h5 *ngFor='let name of headers' class='header'> {{name}} </h5>\n<ng-template #dayGrid></ng-template>\n<h5 *ngFor='let name of headers' class='footer'> {{name}} </h5>",
                        styles: ["@import url(https://fonts.googleapis.com/css?family=Raleway);.footer,.header{text-align:center;font-family:Raleway,sans-serif}"]
                    },] },
        ];
        /** @nocollapse */
        CalendarComponent.ctorParameters = function () {
            return [
                { type: DayCellComponentFactory, decorators: [{ type: core.Inject, args: [DAY_CELL_COMPONENT_FACTORY_PROVIDER,] }] },
                { type: EventComponentFactory, decorators: [{ type: core.Optional }, { type: core.Inject, args: [EVENT_COMPONENT_FACTORY_PROVIDER,] }] },
                { type: WeekSummaryComponentFactory, decorators: [{ type: core.Optional }, { type: core.Inject, args: [WEEK_SUMMARY_COMPONENT_FACTORY,] }] }
            ];
        };
        CalendarComponent.propDecorators = {
            startDate: [{ type: core.Input, args: ['start-date',] }],
            endDate: [{ type: core.Input, args: ['end-date',] }],
            headers: [{ type: core.Input, args: ['headers',] }],
            events: [{ type: core.Input, args: ['events',] }],
            dateAccessor: [{ type: core.Input, args: ['date-accessor',] }],
            idAccessor: [{ type: core.Input, args: ['id-accessor',] }],
            dayGridContainer: [{ type: core.ViewChild, args: ['dayGrid', { read: core.ViewContainerRef },] }],
            deleteEmitter: [{ type: core.Output, args: ['delete',] }],
            updateEmitter: [{ type: core.Output, args: ['update',] }],
            addEmitter: [{ type: core.Output, args: ['add',] }]
        };
        return CalendarComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var AngularAdvancedCalendarModule = (function () {
        function AngularAdvancedCalendarModule() {
        }
        AngularAdvancedCalendarModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            common.CommonModule,
                        ],
                        declarations: [
                            CalendarComponent,
                        ],
                        exports: [
                            CalendarComponent
                        ]
                    },] },
        ];
        return AngularAdvancedCalendarModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     * @template DataType
     */
    var DynamicComponent = (function () {
        function DynamicComponent() {
        }
        /**
         * @param {?} data
         * @return {?}
         */
        DynamicComponent.prototype.updateData = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                this.data = data;
            };
        DynamicComponent.propDecorators = {
            data: [{ type: core.Input, args: ['data',] }]
        };
        return DynamicComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     * @template DataType, CrudType
     */
    var DynamicCrudComponent = (function (_super) {
        __extends(DynamicCrudComponent, _super);
        function DynamicCrudComponent() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.deleteEmitter = new core.EventEmitter();
            _this.updateEmitter = new core.EventEmitter();
            _this.addEmitter = new core.EventEmitter();
            return _this;
        }
        /**
         * @param {?} name
         * @param {?} callback
         * @return {?}
         */
        DynamicCrudComponent.prototype.subscribe = /**
         * @param {?} name
         * @param {?} callback
         * @return {?}
         */
            function (name, callback) {
                switch (name) {
                    case 'update':
                        this.updateEmitter.subscribe(callback);
                        break;
                    case 'delete':
                        this.deleteEmitter.subscribe(callback);
                        break;
                    case 'add':
                        this.addEmitter.subscribe(callback);
                        break;
                    default:
                        throw 'callback type \'' + name + '\' is not a known event callback !';
                }
            };
        /**
         * @param {?} event
         * @return {?}
         */
        DynamicCrudComponent.prototype.onDelete = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                this.deleteEmitter.emit(event);
            };
        /**
         * @param {?} event
         * @return {?}
         */
        DynamicCrudComponent.prototype.onUpdate = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                this.updateEmitter.emit(event);
            };
        /**
         * @param {?} event
         * @return {?}
         */
        DynamicCrudComponent.prototype.onAdd = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                this.addEmitter.emit(event);
            };
        DynamicCrudComponent.propDecorators = {
            deleteEmitter: [{ type: core.Output, args: ['delete',] }],
            updateEmitter: [{ type: core.Output, args: ['update',] }],
            addEmitter: [{ type: core.Output, args: ['add',] }]
        };
        return DynamicCrudComponent;
    }(DynamicComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     * @template DataType
     */
    var AbstractDayCellComponent = (function (_super) {
        __extends(AbstractDayCellComponent, _super);
        function AbstractDayCellComponent() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.dynamicEventViewManager = new DynamicComponentArrayManager();
            return _this;
        }
        /**
         * Method called to update the event views in the day cell
         * @param {?} eventComponentFactory
         * @param {?} idAccessor
         * @return {?}
         */
        AbstractDayCellComponent.prototype.updateEventViews = /**
         * Method called to update the event views in the day cell
         * @param {?} eventComponentFactory
         * @param {?} idAccessor
         * @return {?}
         */
            function (eventComponentFactory, idAccessor) {
                if (eventComponentFactory && this.eventViewContainer) {
                    this.dynamicEventViewManager.updateContainer(this.eventViewContainer, eventComponentFactory, this.data.events, idAccessor, {
                        delete: this.onDelete.bind(this),
                        update: this.onUpdate.bind(this)
                    });
                }
            };
        AbstractDayCellComponent.propDecorators = {
            eventViewContainer: [{ type: core.ViewChild, args: ['eventViewsContainer', { read: core.ViewContainerRef },] }]
        };
        return AbstractDayCellComponent;
    }(DynamicCrudComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     * @template DataType
     */
    var /**
     * @abstract
     * @template DataType
     */ AbstractWeekSummaryComponent = (function (_super) {
        __extends(AbstractWeekSummaryComponent, _super);
        function AbstractWeekSummaryComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return AbstractWeekSummaryComponent;
    }(DynamicCrudComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     * @template DataType
     */
    var /**
     * @abstract
     * @template DataType
     */ AbstractEventComponent = (function (_super) {
        __extends(AbstractEventComponent, _super);
        function AbstractEventComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return AbstractEventComponent;
    }(DynamicCrudComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.AngularAdvancedCalendarModule = AngularAdvancedCalendarModule;
    exports.AbstractDayCellComponent = AbstractDayCellComponent;
    exports.DayCellComponentFactory = DayCellComponentFactory;
    exports.AbstractWeekSummaryComponent = AbstractWeekSummaryComponent;
    exports.WeekSummaryComponentFactory = WeekSummaryComponentFactory;
    exports.AbstractEventComponent = AbstractEventComponent;
    exports.EventComponentFactory = EventComponentFactory;
    exports.isSameDay = isSameDay;
    exports.getWeekNumber = getWeekNumber;
    exports.getFirstDayOfMonth = getFirstDayOfMonth;
    exports.getFirstDayOfFirstWeekOfMonth = getFirstDayOfFirstWeekOfMonth;
    exports.getLastDayOfMonth = getLastDayOfMonth;
    exports.geLastDayOfLastWeekOfMonth = geLastDayOfLastWeekOfMonth;
    exports.ɵa = CalendarComponent;
    exports.ɵc = DynamicCrudComponent;
    exports.ɵd = DynamicComponent;
    exports.ɵb = DynamicComponentFactory;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci51bWQuanMubWFwIiwic291cmNlcyI6W251bGwsIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvdXRpbHMvZGF0ZS50cyIsIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvdXRpbHMvZGlzcGF0Y2hlci50cyIsIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvdmlld3MvZHluYW1pYy9keW5hbWljLmZhY3RvcnkudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2V2ZW50L2V2ZW50LmZhY3RvcnkudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2RheS1jZWxsL2RheS1jZWxsLmZhY3RvcnkudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2R5bmFtaWMvZHluYW1pYy1tYW5hZ2VyLmNvbXBvbmVudC50cyIsIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvdmlld3Mvd2Vlay1zdW1tYXJ5L3dlZWstc3VtbWFyeS5mYXRvcnkudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2NhbGVuZGFyL2NhbGVuZGFyLmNvbXBvbmVudC50cyIsIm5nOi8vYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci9saWIvYW5ndWxhci1hZHZhbmNlZC1jYWxlbmRhci5tb2R1bGUudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2R5bmFtaWMvZHluYW1pYy5jb21wb25lbnQudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL2R5bmFtaWMtY3J1ZC9keW5hbWljLWNydWQuY29tcG9uZW50LnRzIiwibmc6Ly9hbmd1bGFyLWFkdmFuY2VkLWNhbGVuZGFyL2xpYi92aWV3cy9kYXktY2VsbC9hYnN0cmFjdC1kYXktY2VsbC5jb21wb25lbnQudHMiLCJuZzovL2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXIvbGliL3ZpZXdzL3dlZWstc3VtbWFyeS9hYnN0cmFjdC13ZWVrLXN1bW1hcnkuY29tcG9uZW50LnRzIiwibmc6Ly9hbmd1bGFyLWFkdmFuY2VkLWNhbGVuZGFyL2xpYi92aWV3cy9ldmVudC9hYnN0cmFjdC1ldmVudC5jb21wb25lbnQudHMiXSwic291cmNlc0NvbnRlbnQiOlsiLyohICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbkNvcHlyaWdodCAoYykgTWljcm9zb2Z0IENvcnBvcmF0aW9uLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2VcclxudGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGVcclxuTGljZW5zZSBhdCBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblRISVMgQ09ERSBJUyBQUk9WSURFRCBPTiBBTiAqQVMgSVMqIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcclxuS0lORCwgRUlUSEVSIEVYUFJFU1MgT1IgSU1QTElFRCwgSU5DTFVESU5HIFdJVEhPVVQgTElNSVRBVElPTiBBTlkgSU1QTElFRFxyXG5XQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgVElUTEUsIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLFxyXG5NRVJDSEFOVEFCTElUWSBPUiBOT04tSU5GUklOR0VNRU5ULlxyXG5cclxuU2VlIHRoZSBBcGFjaGUgVmVyc2lvbiAyLjAgTGljZW5zZSBmb3Igc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zXHJcbmFuZCBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiogKi9cclxuLyogZ2xvYmFsIFJlZmxlY3QsIFByb21pc2UgKi9cclxuXHJcbnZhciBleHRlbmRTdGF0aWNzID0gZnVuY3Rpb24oZCwgYikge1xyXG4gICAgZXh0ZW5kU3RhdGljcyA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiB8fFxyXG4gICAgICAgICh7IF9fcHJvdG9fXzogW10gfSBpbnN0YW5jZW9mIEFycmF5ICYmIGZ1bmN0aW9uIChkLCBiKSB7IGQuX19wcm90b19fID0gYjsgfSkgfHxcclxuICAgICAgICBmdW5jdGlvbiAoZCwgYikgeyBmb3IgKHZhciBwIGluIGIpIGlmIChiLmhhc093blByb3BlcnR5KHApKSBkW3BdID0gYltwXTsgfTtcclxuICAgIHJldHVybiBleHRlbmRTdGF0aWNzKGQsIGIpO1xyXG59O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZXh0ZW5kcyhkLCBiKSB7XHJcbiAgICBleHRlbmRTdGF0aWNzKGQsIGIpO1xyXG4gICAgZnVuY3Rpb24gX18oKSB7IHRoaXMuY29uc3RydWN0b3IgPSBkOyB9XHJcbiAgICBkLnByb3RvdHlwZSA9IGIgPT09IG51bGwgPyBPYmplY3QuY3JlYXRlKGIpIDogKF9fLnByb3RvdHlwZSA9IGIucHJvdG90eXBlLCBuZXcgX18oKSk7XHJcbn1cclxuXHJcbmV4cG9ydCB2YXIgX19hc3NpZ24gPSBmdW5jdGlvbigpIHtcclxuICAgIF9fYXNzaWduID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiBfX2Fzc2lnbih0KSB7XHJcbiAgICAgICAgZm9yICh2YXIgcywgaSA9IDEsIG4gPSBhcmd1bWVudHMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XHJcbiAgICAgICAgICAgIHMgPSBhcmd1bWVudHNbaV07XHJcbiAgICAgICAgICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSkgdFtwXSA9IHNbcF07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIF9fYXNzaWduLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3Jlc3QocywgZSkge1xyXG4gICAgdmFyIHQgPSB7fTtcclxuICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSAmJiBlLmluZGV4T2YocCkgPCAwKVxyXG4gICAgICAgIHRbcF0gPSBzW3BdO1xyXG4gICAgaWYgKHMgIT0gbnVsbCAmJiB0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gXCJmdW5jdGlvblwiKVxyXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBwID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzKTsgaSA8IHAubGVuZ3RoOyBpKyspIGlmIChlLmluZGV4T2YocFtpXSkgPCAwKVxyXG4gICAgICAgICAgICB0W3BbaV1dID0gc1twW2ldXTtcclxuICAgIHJldHVybiB0O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYykge1xyXG4gICAgdmFyIGMgPSBhcmd1bWVudHMubGVuZ3RoLCByID0gYyA8IDMgPyB0YXJnZXQgOiBkZXNjID09PSBudWxsID8gZGVzYyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IodGFyZ2V0LCBrZXkpIDogZGVzYywgZDtcclxuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5kZWNvcmF0ZSA9PT0gXCJmdW5jdGlvblwiKSByID0gUmVmbGVjdC5kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYyk7XHJcbiAgICBlbHNlIGZvciAodmFyIGkgPSBkZWNvcmF0b3JzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSBpZiAoZCA9IGRlY29yYXRvcnNbaV0pIHIgPSAoYyA8IDMgPyBkKHIpIDogYyA+IDMgPyBkKHRhcmdldCwga2V5LCByKSA6IGQodGFyZ2V0LCBrZXkpKSB8fCByO1xyXG4gICAgcmV0dXJuIGMgPiAzICYmIHIgJiYgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCByKSwgcjtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fcGFyYW0ocGFyYW1JbmRleCwgZGVjb3JhdG9yKSB7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKHRhcmdldCwga2V5KSB7IGRlY29yYXRvcih0YXJnZXQsIGtleSwgcGFyYW1JbmRleCk7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fbWV0YWRhdGEobWV0YWRhdGFLZXksIG1ldGFkYXRhVmFsdWUpIHtcclxuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5tZXRhZGF0YSA9PT0gXCJmdW5jdGlvblwiKSByZXR1cm4gUmVmbGVjdC5tZXRhZGF0YShtZXRhZGF0YUtleSwgbWV0YWRhdGFWYWx1ZSk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2F3YWl0ZXIodGhpc0FyZywgX2FyZ3VtZW50cywgUCwgZ2VuZXJhdG9yKSB7XHJcbiAgICByZXR1cm4gbmV3IChQIHx8IChQID0gUHJvbWlzZSkpKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcclxuICAgICAgICBmdW5jdGlvbiBmdWxmaWxsZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3IubmV4dCh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XHJcbiAgICAgICAgZnVuY3Rpb24gcmVqZWN0ZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3JbXCJ0aHJvd1wiXSh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XHJcbiAgICAgICAgZnVuY3Rpb24gc3RlcChyZXN1bHQpIHsgcmVzdWx0LmRvbmUgPyByZXNvbHZlKHJlc3VsdC52YWx1ZSkgOiBuZXcgUChmdW5jdGlvbiAocmVzb2x2ZSkgeyByZXNvbHZlKHJlc3VsdC52YWx1ZSk7IH0pLnRoZW4oZnVsZmlsbGVkLCByZWplY3RlZCk7IH1cclxuICAgICAgICBzdGVwKChnZW5lcmF0b3IgPSBnZW5lcmF0b3IuYXBwbHkodGhpc0FyZywgX2FyZ3VtZW50cyB8fCBbXSkpLm5leHQoKSk7XHJcbiAgICB9KTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZ2VuZXJhdG9yKHRoaXNBcmcsIGJvZHkpIHtcclxuICAgIHZhciBfID0geyBsYWJlbDogMCwgc2VudDogZnVuY3Rpb24oKSB7IGlmICh0WzBdICYgMSkgdGhyb3cgdFsxXTsgcmV0dXJuIHRbMV07IH0sIHRyeXM6IFtdLCBvcHM6IFtdIH0sIGYsIHksIHQsIGc7XHJcbiAgICByZXR1cm4gZyA9IHsgbmV4dDogdmVyYigwKSwgXCJ0aHJvd1wiOiB2ZXJiKDEpLCBcInJldHVyblwiOiB2ZXJiKDIpIH0sIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiAoZ1tTeW1ib2wuaXRlcmF0b3JdID0gZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KSwgZztcclxuICAgIGZ1bmN0aW9uIHZlcmIobikgeyByZXR1cm4gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIHN0ZXAoW24sIHZdKTsgfTsgfVxyXG4gICAgZnVuY3Rpb24gc3RlcChvcCkge1xyXG4gICAgICAgIGlmIChmKSB0aHJvdyBuZXcgVHlwZUVycm9yKFwiR2VuZXJhdG9yIGlzIGFscmVhZHkgZXhlY3V0aW5nLlwiKTtcclxuICAgICAgICB3aGlsZSAoXykgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKGYgPSAxLCB5ICYmICh0ID0gb3BbMF0gJiAyID8geVtcInJldHVyblwiXSA6IG9wWzBdID8geVtcInRocm93XCJdIHx8ICgodCA9IHlbXCJyZXR1cm5cIl0pICYmIHQuY2FsbCh5KSwgMCkgOiB5Lm5leHQpICYmICEodCA9IHQuY2FsbCh5LCBvcFsxXSkpLmRvbmUpIHJldHVybiB0O1xyXG4gICAgICAgICAgICBpZiAoeSA9IDAsIHQpIG9wID0gW29wWzBdICYgMiwgdC52YWx1ZV07XHJcbiAgICAgICAgICAgIHN3aXRjaCAob3BbMF0pIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgMDogY2FzZSAxOiB0ID0gb3A7IGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSA0OiBfLmxhYmVsKys7IHJldHVybiB7IHZhbHVlOiBvcFsxXSwgZG9uZTogZmFsc2UgfTtcclxuICAgICAgICAgICAgICAgIGNhc2UgNTogXy5sYWJlbCsrOyB5ID0gb3BbMV07IG9wID0gWzBdOyBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgIGNhc2UgNzogb3AgPSBfLm9wcy5wb3AoKTsgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEodCA9IF8udHJ5cywgdCA9IHQubGVuZ3RoID4gMCAmJiB0W3QubGVuZ3RoIC0gMV0pICYmIChvcFswXSA9PT0gNiB8fCBvcFswXSA9PT0gMikpIHsgXyA9IDA7IGNvbnRpbnVlOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSAzICYmICghdCB8fCAob3BbMV0gPiB0WzBdICYmIG9wWzFdIDwgdFszXSkpKSB7IF8ubGFiZWwgPSBvcFsxXTsgYnJlYWs7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDYgJiYgXy5sYWJlbCA8IHRbMV0pIHsgXy5sYWJlbCA9IHRbMV07IHQgPSBvcDsgYnJlYWs7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAodCAmJiBfLmxhYmVsIDwgdFsyXSkgeyBfLmxhYmVsID0gdFsyXTsgXy5vcHMucHVzaChvcCk7IGJyZWFrOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRbMl0pIF8ub3BzLnBvcCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIF8udHJ5cy5wb3AoKTsgY29udGludWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgb3AgPSBib2R5LmNhbGwodGhpc0FyZywgXyk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkgeyBvcCA9IFs2LCBlXTsgeSA9IDA7IH0gZmluYWxseSB7IGYgPSB0ID0gMDsgfVxyXG4gICAgICAgIGlmIChvcFswXSAmIDUpIHRocm93IG9wWzFdOyByZXR1cm4geyB2YWx1ZTogb3BbMF0gPyBvcFsxXSA6IHZvaWQgMCwgZG9uZTogdHJ1ZSB9O1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19leHBvcnRTdGFyKG0sIGV4cG9ydHMpIHtcclxuICAgIGZvciAodmFyIHAgaW4gbSkgaWYgKCFleHBvcnRzLmhhc093blByb3BlcnR5KHApKSBleHBvcnRzW3BdID0gbVtwXTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fdmFsdWVzKG8pIHtcclxuICAgIHZhciBtID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9bU3ltYm9sLml0ZXJhdG9yXSwgaSA9IDA7XHJcbiAgICBpZiAobSkgcmV0dXJuIG0uY2FsbChvKTtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgbmV4dDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBpZiAobyAmJiBpID49IG8ubGVuZ3RoKSBvID0gdm9pZCAwO1xyXG4gICAgICAgICAgICByZXR1cm4geyB2YWx1ZTogbyAmJiBvW2krK10sIGRvbmU6ICFvIH07XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fcmVhZChvLCBuKSB7XHJcbiAgICB2YXIgbSA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvW1N5bWJvbC5pdGVyYXRvcl07XHJcbiAgICBpZiAoIW0pIHJldHVybiBvO1xyXG4gICAgdmFyIGkgPSBtLmNhbGwobyksIHIsIGFyID0gW10sIGU7XHJcbiAgICB0cnkge1xyXG4gICAgICAgIHdoaWxlICgobiA9PT0gdm9pZCAwIHx8IG4tLSA+IDApICYmICEociA9IGkubmV4dCgpKS5kb25lKSBhci5wdXNoKHIudmFsdWUpO1xyXG4gICAgfVxyXG4gICAgY2F0Y2ggKGVycm9yKSB7IGUgPSB7IGVycm9yOiBlcnJvciB9OyB9XHJcbiAgICBmaW5hbGx5IHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBpZiAociAmJiAhci5kb25lICYmIChtID0gaVtcInJldHVyblwiXSkpIG0uY2FsbChpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZmluYWxseSB7IGlmIChlKSB0aHJvdyBlLmVycm9yOyB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gYXI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3NwcmVhZCgpIHtcclxuICAgIGZvciAodmFyIGFyID0gW10sIGkgPSAwOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKVxyXG4gICAgICAgIGFyID0gYXIuY29uY2F0KF9fcmVhZChhcmd1bWVudHNbaV0pKTtcclxuICAgIHJldHVybiBhcjtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXdhaXQodikge1xyXG4gICAgcmV0dXJuIHRoaXMgaW5zdGFuY2VvZiBfX2F3YWl0ID8gKHRoaXMudiA9IHYsIHRoaXMpIDogbmV3IF9fYXdhaXQodik7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2FzeW5jR2VuZXJhdG9yKHRoaXNBcmcsIF9hcmd1bWVudHMsIGdlbmVyYXRvcikge1xyXG4gICAgaWYgKCFTeW1ib2wuYXN5bmNJdGVyYXRvcikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN5bWJvbC5hc3luY0l0ZXJhdG9yIGlzIG5vdCBkZWZpbmVkLlwiKTtcclxuICAgIHZhciBnID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pLCBpLCBxID0gW107XHJcbiAgICByZXR1cm4gaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIpLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5hc3luY0l0ZXJhdG9yXSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXM7IH0sIGk7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgaWYgKGdbbl0pIGlbbl0gPSBmdW5jdGlvbiAodikgeyByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKGEsIGIpIHsgcS5wdXNoKFtuLCB2LCBhLCBiXSkgPiAxIHx8IHJlc3VtZShuLCB2KTsgfSk7IH07IH1cclxuICAgIGZ1bmN0aW9uIHJlc3VtZShuLCB2KSB7IHRyeSB7IHN0ZXAoZ1tuXSh2KSk7IH0gY2F0Y2ggKGUpIHsgc2V0dGxlKHFbMF1bM10sIGUpOyB9IH1cclxuICAgIGZ1bmN0aW9uIHN0ZXAocikgeyByLnZhbHVlIGluc3RhbmNlb2YgX19hd2FpdCA/IFByb21pc2UucmVzb2x2ZShyLnZhbHVlLnYpLnRoZW4oZnVsZmlsbCwgcmVqZWN0KSA6IHNldHRsZShxWzBdWzJdLCByKTsgfVxyXG4gICAgZnVuY3Rpb24gZnVsZmlsbCh2YWx1ZSkgeyByZXN1bWUoXCJuZXh0XCIsIHZhbHVlKTsgfVxyXG4gICAgZnVuY3Rpb24gcmVqZWN0KHZhbHVlKSB7IHJlc3VtZShcInRocm93XCIsIHZhbHVlKTsgfVxyXG4gICAgZnVuY3Rpb24gc2V0dGxlKGYsIHYpIHsgaWYgKGYodiksIHEuc2hpZnQoKSwgcS5sZW5ndGgpIHJlc3VtZShxWzBdWzBdLCBxWzBdWzFdKTsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hc3luY0RlbGVnYXRvcihvKSB7XHJcbiAgICB2YXIgaSwgcDtcclxuICAgIHJldHVybiBpID0ge30sIHZlcmIoXCJuZXh0XCIpLCB2ZXJiKFwidGhyb3dcIiwgZnVuY3Rpb24gKGUpIHsgdGhyb3cgZTsgfSksIHZlcmIoXCJyZXR1cm5cIiksIGlbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXM7IH0sIGk7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4sIGYpIHsgaVtuXSA9IG9bbl0gPyBmdW5jdGlvbiAodikgeyByZXR1cm4gKHAgPSAhcCkgPyB7IHZhbHVlOiBfX2F3YWl0KG9bbl0odikpLCBkb25lOiBuID09PSBcInJldHVyblwiIH0gOiBmID8gZih2KSA6IHY7IH0gOiBmOyB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2FzeW5jVmFsdWVzKG8pIHtcclxuICAgIGlmICghU3ltYm9sLmFzeW5jSXRlcmF0b3IpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJTeW1ib2wuYXN5bmNJdGVyYXRvciBpcyBub3QgZGVmaW5lZC5cIik7XHJcbiAgICB2YXIgbSA9IG9bU3ltYm9sLmFzeW5jSXRlcmF0b3JdLCBpO1xyXG4gICAgcmV0dXJuIG0gPyBtLmNhbGwobykgOiAobyA9IHR5cGVvZiBfX3ZhbHVlcyA9PT0gXCJmdW5jdGlvblwiID8gX192YWx1ZXMobykgOiBvW1N5bWJvbC5pdGVyYXRvcl0oKSwgaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIpLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5hc3luY0l0ZXJhdG9yXSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXM7IH0sIGkpO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IGlbbl0gPSBvW25dICYmIGZ1bmN0aW9uICh2KSB7IHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7IHYgPSBvW25dKHYpLCBzZXR0bGUocmVzb2x2ZSwgcmVqZWN0LCB2LmRvbmUsIHYudmFsdWUpOyB9KTsgfTsgfVxyXG4gICAgZnVuY3Rpb24gc2V0dGxlKHJlc29sdmUsIHJlamVjdCwgZCwgdikgeyBQcm9taXNlLnJlc29sdmUodikudGhlbihmdW5jdGlvbih2KSB7IHJlc29sdmUoeyB2YWx1ZTogdiwgZG9uZTogZCB9KTsgfSwgcmVqZWN0KTsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19tYWtlVGVtcGxhdGVPYmplY3QoY29va2VkLCByYXcpIHtcclxuICAgIGlmIChPYmplY3QuZGVmaW5lUHJvcGVydHkpIHsgT2JqZWN0LmRlZmluZVByb3BlcnR5KGNvb2tlZCwgXCJyYXdcIiwgeyB2YWx1ZTogcmF3IH0pOyB9IGVsc2UgeyBjb29rZWQucmF3ID0gcmF3OyB9XHJcbiAgICByZXR1cm4gY29va2VkO1xyXG59O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9faW1wb3J0U3Rhcihtb2QpIHtcclxuICAgIGlmIChtb2QgJiYgbW9kLl9fZXNNb2R1bGUpIHJldHVybiBtb2Q7XHJcbiAgICB2YXIgcmVzdWx0ID0ge307XHJcbiAgICBpZiAobW9kICE9IG51bGwpIGZvciAodmFyIGsgaW4gbW9kKSBpZiAoT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobW9kLCBrKSkgcmVzdWx0W2tdID0gbW9kW2tdO1xyXG4gICAgcmVzdWx0LmRlZmF1bHQgPSBtb2Q7XHJcbiAgICByZXR1cm4gcmVzdWx0O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19pbXBvcnREZWZhdWx0KG1vZCkge1xyXG4gICAgcmV0dXJuIChtb2QgJiYgbW9kLl9fZXNNb2R1bGUpID8gbW9kIDogeyBkZWZhdWx0OiBtb2QgfTtcclxufVxyXG4iLCJsZXQgbW9tZW50ID0gcmVxdWlyZSgnbW9tZW50Jyk7XG5cbmV4cG9ydCBmdW5jdGlvbiBpc1NhbWVEYXkoYSA6IERhdGUsIGIgOiBEYXRlKSA6IGJvb2xlYW5cbntcbiAgcmV0dXJuIGZhbHNlIHx8IChhICYmIGIgJiYgYS5nZXRGdWxsWWVhcigpID09PSBiLmdldEZ1bGxZZWFyKCkgJiYgYS5nZXRNb250aCgpID09PSBiLmdldE1vbnRoKCkgJiYgYS5nZXREYXRlKCkgPT09IGIuZ2V0RGF0ZSgpKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFdlZWtOdW1iZXIoZGF0ZSA6IERhdGUpIDogbnVtYmVyXG57XG4gIHJldHVybiBtb21lbnQoZGF0ZSkud2VlaygpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Rmlyc3REYXlPZk1vbnRoKGRhdGUgOiBEYXRlKSA6IERhdGVcbntcbiAgcmV0dXJuIG1vbWVudChkYXRlKS5zdGFydE9mKCdtb250aCcpLnRvRGF0ZSgpO1xuXG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRGaXJzdERheU9mRmlyc3RXZWVrT2ZNb250aChkYXRlIDogRGF0ZSkgOiBEYXRlXG57XG4gIHJldHVybiBtb21lbnQoZGF0ZSkuc3RhcnRPZignbW9udGgnKS5zdGFydE9mKCdpc29XZWVrJyk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRMYXN0RGF5T2ZNb250aChkYXRlIDogRGF0ZSkgOiBEYXRlXG57XG4gIHJldHVybiBtb21lbnQoZGF0ZSkuZW5kT2YoJ21vbnRoJykudG9EYXRlKCk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZUxhc3REYXlPZkxhc3RXZWVrT2ZNb250aChkYXRlIDogRGF0ZSkgOiBEYXRlXG57XG4gIHJldHVybiBtb21lbnQoZGF0ZSkuZW5kT2YoJ21vbnRoJykuZW5kT2YoJ2lzb1dlZWsnKS50b0RhdGUoKTtcbn0iLCJsZXQgbW9tZW50ID0gcmVxdWlyZSgnbW9tZW50Jyk7XG5cbi8qKlxuICogRGlzcGF0Y2hlcyBldmVudHMgaW4gYW4gYXJyYXkgb2YgZGF0ZXMgZGVwZW5kaW5nXG4gKiBiYXNlZCBvbiB0aGUgZGF0ZSBvZiBlYWNoIGV2ZW50LlxuICovXG5leHBvcnQgY2xhc3MgRXZlbnREaXNwYXRjaGVyXG57XG4gIHByaXZhdGUgZGlzcGF0Y2ggOiBhbnlbXVtdO1xuXG4gIC8qKlxuICAgKiBDcmVhdGVzIGEgbmV3IGV2ZW50IGRpc3BhdGNoZXJcbiAgICogQHBhcmFtIGRhdGVBY2Nlc3NvciB0aGUgZGF0ZSBsYWJlbCB0byB1c2UgaW4gdGhlIGV2ZW50c1xuICAgKiBAcGFyYW0gc3RhcnREYXRlIHRoZSBzdGFydCBkYXRlIG9mIHRoZSBwZXJpb2QgaW4gd2hpY2ggdG8gZGlzcGF0Y2ggZXZlbnRzXG4gICAqIEBwYXJhbSBlbmREYXRlIHRoZSBlbmQgZGF0ZSBvZiB0aGUgcGVyaW9kIChpbmNsdXNpdmUpIGluIHdoaWNoIHRvIGRpc3BhdGNoIGV2ZW50c1xuICAgKi9cbiAgcHVibGljIGNvbnN0cnVjdG9yKHB1YmxpYyByZWFkb25seSBkYXRlQWNjZXNzb3IgOiBzdHJpbmcsIHB1YmxpYyByZWFkb25seSBzdGFydERhdGUgOiBEYXRlLCBwdWJsaWMgcmVhZG9ubHkgZW5kRGF0ZSA6IERhdGUpIHsgfVxuXG4gIHB1YmxpYyBjbGVhcigpIDogdm9pZFxuICB7XG4gICAgdGhpcy5kaXNwYXRjaCA9IFtdO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5nZXRUb3RhbE51bWJlck9mRGF5cygpOyBpKyspXG4gICAge1xuICAgICAgdGhpcy5kaXNwYXRjaFtpXSA9IFtdO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBhZGQoZXZlbnQgOiBhbnkpXG4gIHtcbiAgICBsZXQgZGF0ZSA9IGV2ZW50W3RoaXMuZGF0ZUFjY2Vzc29yXTtcbiAgICBsZXQgZGlzcGF0Y2hJbmRleCA9IHRoaXMuZ2V0RGF5TnVtYmVyKGRhdGUpO1xuICAgIGlmICh0aGlzLmRpc3BhdGNoW2Rpc3BhdGNoSW5kZXhdKVxuICAgIHtcbiAgICAgIHRoaXMuZGlzcGF0Y2hbZGlzcGF0Y2hJbmRleF0ucHVzaChldmVudCk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGdldEV2ZW50cyhkYXRlIDogRGF0ZSkgOiBhbnlbXVxuICB7XG4gICAgcmV0dXJuIHRoaXMuZGlzcGF0Y2hbdGhpcy5nZXREYXlOdW1iZXIoZGF0ZSldO1xuICB9XG5cbiAgcHVibGljIGdldEV2ZW50c0JldHdlZW4oc3RhcnREYXRlIDogRGF0ZSwgZW5kRGF0ZSA6IERhdGUpXG4gIHtcbiAgICByZXR1cm4gdGhpcy5kaXNwYXRjaC5zbGljZSh0aGlzLmdldERheU51bWJlcihzdGFydERhdGUpLCB0aGlzLmdldERheU51bWJlcihlbmREYXRlKSArIDEpO1xuICB9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgYW4gYXJyYXkgb2YgdGhlIGRhdGVzXG4gICAqIGJldHdlZW4gc3RhcnREYXRlIGFuZCBlbmREYXRlLlxuICAgKi9cbiAgcHVibGljIGdldERhdGVBcnJheSgpXG4gIHtcbiAgICBsZXQgZGF5cyA9IFtdO1xuICAgIGxldCBjdXJyZW50ID0gbW9tZW50KHRoaXMuc3RhcnREYXRlKTtcbiAgICBsZXQgbGFzdCAgPSBtb21lbnQodGhpcy5lbmREYXRlKTtcbiAgICB3aGlsZShjdXJyZW50IDwgbGFzdClcbiAgICB7XG4gICAgICBkYXlzLnB1c2goY3VycmVudC50b0RhdGUoKSk7XG4gICAgICBjdXJyZW50ID0gY3VycmVudC5hZGQoMSwgJ2RheScpO1xuICAgIH1cbiAgICByZXR1cm4gZGF5cztcbiAgfVxuXG4gIHB1YmxpYyBnZXRXZWVrQXJyYXkoKVxuICB7XG4gICAgbGV0IHdlZWtzID0gW107XG4gICAgbGV0IGN1cnJlbnQgPSBtb21lbnQodGhpcy5zdGFydERhdGUpO1xuICAgIGxldCBsYXN0ICA9IG1vbWVudCh0aGlzLmVuZERhdGUpO1xuICAgIHdoaWxlKGN1cnJlbnQgPCBsYXN0KVxuICAgIHtcbiAgICAgIHdlZWtzLnB1c2goeyBzdGFydERhdGU6IGN1cnJlbnQudG9EYXRlKCksIGVuZERhdGU6IGN1cnJlbnQuYWRkKDYsICdkYXknKS50b0RhdGUoKSB9KTtcbiAgICAgIGN1cnJlbnQgPSBjdXJyZW50LmFkZCgxLCAnZGF5Jyk7XG4gICAgfVxuICAgIHJldHVybiB3ZWVrcztcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSBpbmRleCBvZiBhIGRhdGUgaW5cbiAgICogdGhlIFtzdGFydERhdGUgLSBlbmREYXRlXSBwZXJpb2QuXG4gICAqIEBwYXJhbSBkYXRlIHRoZSBkYXRlIGZvciB3aGljaCB0byBjb21wdXRlIHRoZSBpbmRleFxuICAgKi9cbiAgcHVibGljIGdldERheU51bWJlcihkYXRlIDogRGF0ZSlcbiAge1xuICAgIHJldHVybiBtb21lbnQoZGF0ZSkuZGlmZihtb21lbnQodGhpcy5zdGFydERhdGUpLCAnZGF5cycpO1xuICB9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdGhlIHRvdGFsIG51bWJlciBvZiBkYXlzXG4gICAqIGluIHRoZSBbc3RhcnREYXRlIC0gZW5kRGF0ZV0gcGVyaW9kLlxuICAgKi9cbiAgcHVibGljIGdldFRvdGFsTnVtYmVyT2ZEYXlzKClcbiAge1xuICAgIHJldHVybiB0aGlzLmdldERheU51bWJlcih0aGlzLmVuZERhdGUpICsgMTtcbiAgfVxufSIsImltcG9ydCB7IER5bmFtaWNDb21wb25lbnQgfSBmcm9tIFwiLi9keW5hbWljLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQ29tcG9uZW50UmVmLCBWaWV3Q29udGFpbmVyUmVmLCBDb21wb25lbnRGYWN0b3J5LCBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIFR5cGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgU3RyaW5nTWFwIH0gZnJvbSBcIi4uLy4uL3R5cGVzL21hcHNcIjtcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIER5bmFtaWNDb21wb25lbnRGYWN0b3J5PERhdGFUeXBlLCBDb21wb25lbnRUeXBlIGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudDxEYXRhVHlwZT4+XG57XG4gIHByaXZhdGUgZmFjdG9yeSA6IENvbXBvbmVudEZhY3Rvcnk8Q29tcG9uZW50VHlwZT47XG5cbiAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgcmVhZG9ubHkgY29tcG9uZW50RmFjdG9yeVJlc29sdmVyIDogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb21wb25lbnRUeXBlIDogVHlwZTxDb21wb25lbnRUeXBlPilcbiAge1xuICAgIHRoaXMuZmFjdG9yeSA9IHRoaXMuY29tcG9uZW50RmFjdG9yeVJlc29sdmVyLnJlc29sdmVDb21wb25lbnRGYWN0b3J5KGNvbXBvbmVudFR5cGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIEluc2VydHMgYSBjb21wb25lbnQgaW4gYSB2aWV3IGNvbnRhaW5lclxuICAgKiBAcGFyYW0gY29udGFpbmVyIHRoZSBjb250YWluZXIgaW4gd2hpY2ggd2Ugd2FudCB0byBkaXNwbGF5IHRoaXMgY29tcG9uZW50XG4gICAqIEBwYXJhbSBkYXRhIHRoZSBkYXRhIG9mIHRoZSBjb21wb25lbnQgdG8gZGlzcGxheVxuICAgKiBAcGFyYW0gY2FsbGJhY2tzIGEgbWFwIG9mIGNhbGxiYWNrcyB0byBzdWJzY3JpYmUgdG9cbiAgICogQHBhcmFtIGluZGV4IHRoZSBwb3NpdGlvbiBpbiB0aGUgY29udGFpbmVyIHdoZXJlIHRvIGFkZCB0aGUgY29tcG9uZW50XG4gICAqL1xuICBwdWJsaWMgaW5zZXJ0RHluYW1pY0NvbXBvbmVudChjb250YWluZXIgOiBWaWV3Q29udGFpbmVyUmVmLCBkYXRhIDogRGF0YVR5cGUsIGNhbGxiYWNrcz86IFN0cmluZ01hcDxGdW5jdGlvbj4sIGluZGV4PyA6IG51bWJlcikgOiBDb21wb25lbnRSZWY8Q29tcG9uZW50VHlwZT5cbiAge1xuICAgIGxldCBjb21wb25lbnRSZWYgPSBjb250YWluZXIuY3JlYXRlQ29tcG9uZW50KHRoaXMuZmFjdG9yeSwgaW5kZXgpO1xuICAgIGxldCBpbnN0YW5jZSA9IDxDb21wb25lbnRUeXBlPmNvbXBvbmVudFJlZi5pbnN0YW5jZTtcbiAgICBpbnN0YW5jZS51cGRhdGVEYXRhKGRhdGEpO1xuICAgIGlmIChjYWxsYmFja3MpXG4gICAge1xuICAgICAgZm9yIChsZXQgbmFtZSBpbiBjYWxsYmFja3MpXG4gICAgICB7XG4gICAgICAgIGluc3RhbmNlLnN1YnNjcmliZShuYW1lLCBjYWxsYmFja3NbbmFtZV0pO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gY29tcG9uZW50UmVmO1xuICB9XG59IiwiaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudEZhY3RvcnkgfSBmcm9tIFwiLi4vZHluYW1pYy9keW5hbWljLmZhY3RvcnlcIjtcbmltcG9ydCB7IEFic3RyYWN0RXZlbnRDb21wb25lbnQgfSBmcm9tIFwiLi9hYnN0cmFjdC1ldmVudC5jb21wb25lbnRcIjtcbmltcG9ydCB7IFR5cGUsIENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBFdmVudENvbXBvbmVudEZhY3Rvcnk8RGF0YVR5cGU+IGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudEZhY3Rvcnk8RGF0YVR5cGUsIEFic3RyYWN0RXZlbnRDb21wb25lbnQ8RGF0YVR5cGU+Plxue1xuICBwdWJsaWMgY29uc3RydWN0b3IoY29tcG9uZW50RmFjdG9yeVJlc29sdmVyIDogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb21wb25lbnRUeXBlIDogVHlwZTxBYnN0cmFjdEV2ZW50Q29tcG9uZW50PERhdGFUeXBlPj4pXG4gIHtcbiAgICBzdXBlcihjb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIGNvbXBvbmVudFR5cGUpO1xuICB9XG59IiwiaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudEZhY3RvcnkgfSBmcm9tICcuLi9keW5hbWljL2R5bmFtaWMuZmFjdG9yeSc7XG5pbXBvcnQgeyBDYWxlbmRhckRheURhdGEgfSBmcm9tICcuLi8uLi90eXBlcy9jYWxlbmRhci1kYXknO1xuaW1wb3J0IHsgQWJzdHJhY3REYXlDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi9hYnN0cmFjdC1kYXktY2VsbC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBUeXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBEYXlDZWxsQ29tcG9uZW50RmFjdG9yeTxEYXRhVHlwZT4gZXh0ZW5kcyBEeW5hbWljQ29tcG9uZW50RmFjdG9yeTxDYWxlbmRhckRheURhdGE8RGF0YVR5cGU+LCBBYnN0cmFjdERheUNlbGxDb21wb25lbnQ8RGF0YVR5cGU+Plxue1xuICBwdWJsaWMgY29uc3RydWN0b3IoY29tcG9uZW50RmFjdG9yeVJlc29sdmVyIDogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb21wb25lbnRUeXBlIDogVHlwZTxBYnN0cmFjdERheUNlbGxDb21wb25lbnQ8RGF0YVR5cGU+PilcbiAge1xuICAgIHN1cGVyKGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29tcG9uZW50VHlwZSk7XG4gIH1cbn0iLCJpbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50IH0gZnJvbSBcIi4vZHluYW1pYy5jb21wb25lbnRcIjtcbmltcG9ydCB7IFZpZXdDb250YWluZXJSZWYsIENvbXBvbmVudFJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50RmFjdG9yeSB9IGZyb20gXCIuL2R5bmFtaWMuZmFjdG9yeVwiO1xuaW1wb3J0IHsgU3RyaW5nTWFwLCBOdW1lcmljTWFwIH0gZnJvbSBcIi4uLy4uL3R5cGVzL21hcHNcIjtcblxuZXhwb3J0IGNsYXNzIER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXI8RGF0YVR5cGUsIENvbXBvbmVudFR5cGUgZXh0ZW5kcyBEeW5hbWljQ29tcG9uZW50PERhdGFUeXBlPj5cbntcblxuICAvKipcbiAgICogU3RvcmVzIHRoZSBwcmV2aW91cyBhcnJheSBzdGF0ZVxuICAgKi9cbiAgcHJpdmF0ZSBvbGREYXRhQXJyYXkgOiBEYXRhVHlwZVtdID0gW107XG4gIFxuICBwdWJsaWMgY2xlYXIoKVxuICB7XG4gICAgZm9yIChsZXQgaWQgaW4gdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcClcbiAgICB7XG4gICAgICBpZiAodGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtpZF0pXG4gICAgICB7XG4gICAgICAgIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbaWRdLmRlc3Ryb3koKTtcbiAgICAgICAgZGVsZXRlIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbaWRdO1xuICAgICAgfVxuICAgIH1cbiAgICB0aGlzLm9sZERhdGFBcnJheSA9IFtdO1xuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZXMsIGFkZCBvciByZW1vdmUgZGF0YSB2aWV3cyBiYXNlZFxuICAgKiBvbiB0aGUgb2xkIGRhdGEgbGlzdFxuICAgKi9cbiAgcHVibGljIHVwZGF0ZUNvbnRhaW5lcihcbiAgICBjb250YWluZXIgOiBWaWV3Q29udGFpbmVyUmVmLFxuICAgIGZhY3RvcnkgOiBEeW5hbWljQ29tcG9uZW50RmFjdG9yeTxEYXRhVHlwZSwgQ29tcG9uZW50VHlwZT4sXG4gICAgZGF0YUFycmF5IDogRGF0YVR5cGVbXSxcbiAgICBkYXRhSWRBY2Nlc3NvciA6IHN0cmluZyxcbiAgICBkeW5hbWljQ29tcG9uZW50Q2FsbGJhY2tzPyA6IFN0cmluZ01hcDxGdW5jdGlvbj4sXG4gICAgb25BZGRWaWV3PyA6IEZ1bmN0aW9uLFxuICAgIG9uVXBkYXRlVmlldz8gOiBGdW5jdGlvbixcbiAgICBvbkRlbGV0ZVZpZXc/IDogRnVuY3Rpb25cbiAgKSA6IHZvaWRcbiAge1xuICAgIGlmICAoIWNvbnRhaW5lcilcbiAgICB7XG4gICAgICB0aHJvdyAnY2hpbGQgY29udGFpbmVyICgjY2hpbGRDb250YWluZXIpIGNvdWxkIG5vdCBiZSBmb3VuZCBpbiB0aGUgY29udGFpbmVyIHZpZXcnXG4gICAgfVxuICAgIGZvciAobGV0IGRhdGEgb2YgZGF0YUFycmF5KVxuICAgIHtcbiAgICAgIGxldCBhbHJlYWR5UHJlc2VudCA9IGZhbHNlO1xuICAgICAgZm9yIChsZXQgb2xkRGF0YSBvZiB0aGlzLm9sZERhdGFBcnJheSlcbiAgICAgIHtcbiAgICAgICAgaWYgKGRhdGFbZGF0YUlkQWNjZXNzb3JdID09PSBvbGREYXRhW2RhdGFJZEFjY2Vzc29yXSlcbiAgICAgICAge1xuICAgICAgICAgIGFscmVhZHlQcmVzZW50ID0gdHJ1ZTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgaWYgKGFscmVhZHlQcmVzZW50KVxuICAgICAge1xuICAgICAgICB0aGlzLnVwZGF0ZUR5bmFtaWNDb21wb25lbnQoY29udGFpbmVyLCBkYXRhLCBkYXRhSWRBY2Nlc3Nvciwgb25VcGRhdGVWaWV3KTtcbiAgICAgIH1cbiAgICAgIGVsc2VcbiAgICAgIHtcbiAgICAgICAgdGhpcy5hZGREeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciwgZmFjdG9yeSwgZGF0YSwgZGF0YUlkQWNjZXNzb3IsIGR5bmFtaWNDb21wb25lbnRDYWxsYmFja3MsIG9uQWRkVmlldyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgZm9yIChsZXQgb2xkRGF0YSBvZiB0aGlzLm9sZERhdGFBcnJheSlcbiAgICB7XG4gICAgICBsZXQgc3RpbGxQcmVzZW50ID0gZmFsc2U7XG4gICAgICBmb3IgKGxldCBkYXRhIG9mIGRhdGFBcnJheSlcbiAgICAgIHtcbiAgICAgICAgaWYgKGRhdGFbZGF0YUlkQWNjZXNzb3JdID09PSBvbGREYXRhW2RhdGFJZEFjY2Vzc29yXSlcbiAgICAgICAge1xuICAgICAgICAgIHN0aWxsUHJlc2VudCA9IHRydWU7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGlmICghc3RpbGxQcmVzZW50KVxuICAgICAge1xuICAgICAgICB0aGlzLnJlbW92ZUR5bmFtaWNDb21wb25lbnQoY29udGFpbmVyLCBvbGREYXRhLCBkYXRhSWRBY2Nlc3Nvciwgb25EZWxldGVWaWV3KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLm9sZERhdGFBcnJheSA9IGRhdGFBcnJheTtcbiAgfVxuXG4gIC8qKlxuICAgKiBBIG51bWVyaWMgbWFwIHVzZWQgdG8gc3RvcmUgdGhlIGR5bmFtaWMgdmlld3NcbiAgICovXG4gIHByaXZhdGUgZHluYW1pY1ZpZXdDb21wb25lbnRNYXAgOiBOdW1lcmljTWFwPENvbXBvbmVudFJlZjxDb21wb25lbnRUeXBlPj4gPSB7fTtcblxuICBwcml2YXRlIHJlbW92ZUR5bmFtaWNDb21wb25lbnQoY29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZiwgZGF0YSA6IERhdGFUeXBlLCBkYXRhSWRBY2Nlc3NvciA6IHN0cmluZywgb25EZWxldGVWaWV3PyA6IEZ1bmN0aW9uKSA6IHZvaWRcbiAge1xuICAgIGlmICh0eXBlb2YgZGF0YVtkYXRhSWRBY2Nlc3Nvcl0gIT09ICdudW1iZXInKVxuICAgIHtcbiAgICAgIHRocm93ICdpZCAoJyArIGRhdGFJZEFjY2Vzc29yICsgJykgbm90IGZvdW5kIGluIGRhdGEgc3RydWN0dXJlIChkZWxldGUgZHluYW1pYyBjb21wb25lbnQgbWV0aG9kKSc7XG4gICAgfVxuICAgIGVsc2UgaWYgKCF0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSlcbiAgICB7XG4gICAgICB0aHJvdyAnZHluYW1pYyB2aWV3IG5vdCBmb3VuZCBpbiB0aGUgZHluYW1pYyB2aWV3IGNvbnRhaW5lciAoZGVsZXRlIGR5bmFtaWMgY29tcG9uZW50IG1ldGhvZCknO1xuICAgIH1cbiAgICBlbHNlXG4gICAge1xuICAgICAgdGhpcy5keW5hbWljVmlld0NvbXBvbmVudE1hcFtkYXRhW2RhdGFJZEFjY2Vzc29yXV0uZGVzdHJveSgpO1xuICAgICAgZGVsZXRlIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbZGF0YVtkYXRhSWRBY2Nlc3Nvcl1dO1xuICAgICAgaWYgKG9uRGVsZXRlVmlldykgb25EZWxldGVWaWV3KGRhdGEsIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbZGF0YVtkYXRhSWRBY2Nlc3Nvcl1dKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGFkZER5bmFtaWNDb21wb25lbnQoXG4gICAgY29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZixcbiAgICBmYWN0b3J5IDogRHluYW1pY0NvbXBvbmVudEZhY3Rvcnk8RGF0YVR5cGUsIENvbXBvbmVudFR5cGU+LFxuICAgIGRhdGEgOiBEYXRhVHlwZSxcbiAgICBkYXRhSWRBY2Nlc3NvciA6IHN0cmluZyxcbiAgICBkeW5hbWljQ29tcG9uZW50Q2FsbGJhY2tzIDogU3RyaW5nTWFwPEZ1bmN0aW9uPixcbiAgICBvbkFkZFZpZXc/IDogRnVuY3Rpb25cbiAgKSA6IHZvaWRcbiAge1xuICAgIGlmICh0eXBlb2YgZGF0YVtkYXRhSWRBY2Nlc3Nvcl0gIT09ICdudW1iZXInKVxuICAgIHtcbiAgICAgIHRocm93ICdpZCAoJyArIGRhdGFJZEFjY2Vzc29yICsgJykgbm90IGZvdW5kIGluIGRhdGEgc3RydWN0dXJlIChhZGQgZHluYW1pYyBjb21wb25lbnQgbWV0aG9kKSc7XG4gICAgfVxuICAgIGVsc2UgaWYgKHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbZGF0YVtkYXRhSWRBY2Nlc3Nvcl1dKVxuICAgIHtcbiAgICAgIHRocm93ICdkeW5hbWljIHZpZXcgYWxyZWFkeSBwcmVzZW50IGluIHRoZSBkeW5hbWljIHZpZXcgY29udGFpbmVyIChhZGQgZHluYW1pYyBjb21wb25lbnQgbWV0aG9kKSc7XG4gICAgfVxuICAgIGVsc2VcbiAgICB7XG4gICAgICBsZXQgY29tcG9uZW50UmVmID0gZmFjdG9yeS5pbnNlcnREeW5hbWljQ29tcG9uZW50KGNvbnRhaW5lciwgZGF0YSwgZHluYW1pY0NvbXBvbmVudENhbGxiYWNrcyk7XG4gICAgICB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSA9IGNvbXBvbmVudFJlZjtcbiAgICAgIGlmIChvbkFkZFZpZXcpIG9uQWRkVmlldyhkYXRhLCB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXSk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZXMgYW4gZGF0YSB2aWV3IG1vZGVsXG4gICAqIEBwYXJhbSBkYXRhIHRoZSBuZXcgZGF0YSBtb2RlbCB0byB1c2VcbiAgICovXG4gIHByaXZhdGUgdXBkYXRlRHluYW1pY0NvbXBvbmVudChjb250YWluZXIgOiBWaWV3Q29udGFpbmVyUmVmLCBkYXRhIDogRGF0YVR5cGUsIGRhdGFJZEFjY2Vzc29yIDogc3RyaW5nLCBvblVwZGF0ZVZpZXc/IDogRnVuY3Rpb24pIDogdm9pZFxuICB7XG4gICAgaWYgKHR5cGVvZiBkYXRhW2RhdGFJZEFjY2Vzc29yXSAhPT0gJ251bWJlcicpXG4gICAge1xuICAgICAgdGhyb3cgJ2lkICgnICsgZGF0YUlkQWNjZXNzb3IgKyAnKSBub3QgZm91bmQgaW4gZGF0YSBzdHJ1Y3R1cmUgKGFkZCBkeW5hbWljIGNvbXBvbmVudCBtZXRob2QpJztcbiAgICB9XG4gICAgZWxzZSBpZiAoIXRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbZGF0YVtkYXRhSWRBY2Nlc3Nvcl1dKVxuICAgIHtcbiAgICAgIHRocm93ICdkeW5hbWljIHZpZXcgbm90IGZvdW5kIGluIHRoZSBkeW5hbWljIHZpZXcgY29udGFpbmVyIChhZGQgZHluYW1pYyBjb21wb25lbnQgbWV0aG9kKSc7XG4gICAgfVxuICAgIGVsc2VcbiAgICB7XG4gICAgICB0aGlzLmR5bmFtaWNWaWV3Q29tcG9uZW50TWFwW2RhdGFbZGF0YUlkQWNjZXNzb3JdXS5pbnN0YW5jZS51cGRhdGVEYXRhKGRhdGEpO1xuICAgICAgaWYgKG9uVXBkYXRlVmlldykgb25VcGRhdGVWaWV3KGRhdGEsIHRoaXMuZHluYW1pY1ZpZXdDb21wb25lbnRNYXBbZGF0YVtkYXRhSWRBY2Nlc3Nvcl1dKTtcbiAgICB9XG4gIH1cbn0iLCJpbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50RmFjdG9yeSB9IGZyb20gXCIuLi9keW5hbWljL2R5bmFtaWMuZmFjdG9yeVwiO1xuaW1wb3J0IHsgQ2FsZW5kYXJXZWVrRGF0YSB9IGZyb20gXCIuLi8uLi90eXBlcy9jYWxlbmRhci13ZWVrXCI7XG5pbXBvcnQgeyBBYnN0cmFjdFdlZWtTdW1tYXJ5Q29tcG9uZW50IH0gZnJvbSBcIi4vYWJzdHJhY3Qtd2Vlay1zdW1tYXJ5LmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQ29tcG9uZW50UmVmLCBWaWV3Q29udGFpbmVyUmVmLCBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIFR5cGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgU3RyaW5nTWFwIH0gZnJvbSBcIi4uLy4uL3R5cGVzL21hcHNcIjtcbmltcG9ydCB7IGlzU2FtZURheSB9IGZyb20gXCIuLi8uLi91dGlscy9kYXRlXCI7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBXZWVrU3VtbWFyeUNvbXBvbmVudEZhY3Rvcnk8RGF0YVR5cGU+IGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudEZhY3Rvcnk8Q2FsZW5kYXJXZWVrRGF0YTxEYXRhVHlwZT4sIEFic3RyYWN0V2Vla1N1bW1hcnlDb21wb25lbnQ8RGF0YVR5cGU+Plxue1xuICBwdWJsaWMgY29uc3RydWN0b3IoY29tcG9uZW50RmFjdG9yeVJlc29sdmVyIDogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb21wb25lbnRUeXBlIDogVHlwZTxBYnN0cmFjdFdlZWtTdW1tYXJ5Q29tcG9uZW50PERhdGFUeXBlPj4pXG4gIHtcbiAgICBzdXBlcihjb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIGNvbXBvbmVudFR5cGUpO1xuICB9XG5cbiAgcHVibGljIGluc2VydER5bmFtaWNDb21wb25lbnQoY29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZiwgZGF0YSA6IENhbGVuZGFyV2Vla0RhdGE8RGF0YVR5cGU+LCBjYWxsYmFja3M/OiBTdHJpbmdNYXA8RnVuY3Rpb24+KSA6IENvbXBvbmVudFJlZjxBYnN0cmFjdFdlZWtTdW1tYXJ5Q29tcG9uZW50PERhdGFUeXBlPj5cbiAge1xuICAgIGxldCBwb3NpdGlvbiA9IHRoaXMuZmluZFdlZWtQb3NpdGlvbkluQ29udGFpbmVyKGNvbnRhaW5lciwgZGF0YSk7XG4gICAgcmV0dXJuIChwb3NpdGlvbiA+PSAwKSA/IHN1cGVyLmluc2VydER5bmFtaWNDb21wb25lbnQoY29udGFpbmVyLCBkYXRhLCBjYWxsYmFja3MsIHBvc2l0aW9uKSA6IHVuZGVmaW5lZDtcbiAgfVxuXG4gIHByaXZhdGUgZmluZFdlZWtQb3NpdGlvbkluQ29udGFpbmVyKGNvbnRhaW5lciA6IFZpZXdDb250YWluZXJSZWYsIGRhdGEgOiBDYWxlbmRhcldlZWtEYXRhPERhdGFUeXBlPikgOiBudW1iZXJcbiAge1xuICAgIGxldCBpbmRleDtcbiAgICBsZXQgZm91bmQgPSBmYWxzZTtcbiAgICBmb3IgKGluZGV4ID0gMDsgIWZvdW5kICYmIGluZGV4IDwgY29udGFpbmVyLmxlbmd0aDsgaW5kZXgrKylcbiAgICB7XG4gICAgICBsZXQgY29tcG9uZW50SW5zdGFuY2UgPSBjb250YWluZXIuZ2V0KGluZGV4KVsnX3ZpZXcnXVsnbm9kZXMnXVsxXVsnaW5zdGFuY2UnXTtcbiAgICAgIGlmIChjb21wb25lbnRJbnN0YW5jZS5kYXRhLmRhdGUgJiYgaXNTYW1lRGF5KGNvbXBvbmVudEluc3RhbmNlLmRhdGEuZGF0ZSwgZGF0YS5lbmREYXRlKSlcbiAgICAgIHtcbiAgICAgICAgcmV0dXJuIGluZGV4ICsgMTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIC0xO1xuICB9XG59IiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFNpbXBsZUNoYW5nZXMsIENvbXBvbmVudFJlZiwgVmlld0NoaWxkLCBWaWV3Q29udGFpbmVyUmVmLCBIb3N0QmluZGluZywgSW5qZWN0LCBPcHRpb25hbCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBnZXRXZWVrTnVtYmVyIH0gZnJvbSAnLi4vLi4vdXRpbHMvZGF0ZSc7XG5pbXBvcnQgeyBFdmVudERpc3BhdGNoZXIgfSBmcm9tICcuLi8uLi91dGlscy9kaXNwYXRjaGVyJztcbmltcG9ydCB7IEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudCB9IGZyb20gJy4uL2RheS1jZWxsL2Fic3RyYWN0LWRheS1jZWxsLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDYWxlbmRhckRheURhdGEgfSBmcm9tICcuLi8uLi90eXBlcy9jYWxlbmRhci1kYXknO1xuaW1wb3J0IHsgRXZlbnQgfSBmcm9tICcuLi8uLi90eXBlcy9ldmVudCc7XG5pbXBvcnQgeyBDYWxlbmRhcldlZWtEYXRhIH0gZnJvbSAnLi4vLi4vdHlwZXMvY2FsZW5kYXItd2Vlayc7XG5pbXBvcnQgeyBBYnN0cmFjdFdlZWtTdW1tYXJ5Q29tcG9uZW50IH0gZnJvbSAnLi4vd2Vlay1zdW1tYXJ5L2Fic3RyYWN0LXdlZWstc3VtbWFyeS5jb21wb25lbnQnO1xuaW1wb3J0IHsgRXZlbnRDb21wb25lbnRGYWN0b3J5IH0gZnJvbSAnLi4vZXZlbnQvZXZlbnQuZmFjdG9yeSc7XG5pbXBvcnQgeyBEYXlDZWxsQ29tcG9uZW50RmFjdG9yeSB9IGZyb20gJy4uL2RheS1jZWxsL2RheS1jZWxsLmZhY3RvcnknO1xuaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlciB9IGZyb20gJy4uL2R5bmFtaWMvZHluYW1pYy1tYW5hZ2VyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBXZWVrU3VtbWFyeUNvbXBvbmVudEZhY3RvcnkgfSBmcm9tICcuLi93ZWVrLXN1bW1hcnkvd2Vlay1zdW1tYXJ5LmZhdG9yeSc7XG5cbmxldCBtb21lbnQgPSByZXF1aXJlKCdtb21lbnQnKTtcbmxldCBoYXNoID0gcmVxdWlyZSgnb2JqZWN0LWhhc2gnKTtcblxuZXhwb3J0IGNvbnN0IERBWV9DRUxMX0NPTVBPTkVOVF9GQUNUT1JZX1BST1ZJREVSID0gJ0RheUNlbGxDb21wb25lbnRGYWN0b3J5JztcbmV4cG9ydCBjb25zdCBFVkVOVF9DT01QT05FTlRfRkFDVE9SWV9QUk9WSURFUiAgICA9ICdFdmVudENvbXBvbmVudEZhY3RvcnknO1xuZXhwb3J0IGNvbnN0IFdFRUtfU1VNTUFSWV9DT01QT05FTlRfRkFDVE9SWSAgICAgID0gJ1dlZWtTdW1tYXJ5Q29tcG9uZW50RmFjdG9yeSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FuZ3VsYXItYWR2YW5jZWQtY2FsZW5kYXInLFxuICB0ZW1wbGF0ZTogYDxoNSAqbmdGb3I9J2xldCBuYW1lIG9mIGhlYWRlcnMnIGNsYXNzPSdoZWFkZXInPiB7e25hbWV9fSA8L2g1PlxuPG5nLXRlbXBsYXRlICNkYXlHcmlkPjwvbmctdGVtcGxhdGU+XG48aDUgKm5nRm9yPSdsZXQgbmFtZSBvZiBoZWFkZXJzJyBjbGFzcz0nZm9vdGVyJz4ge3tuYW1lfX0gPC9oNT5gLFxuICBzdHlsZXM6IFtgQGltcG9ydCB1cmwoaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PVJhbGV3YXkpOy5mb290ZXIsLmhlYWRlcnt0ZXh0LWFsaWduOmNlbnRlcjtmb250LWZhbWlseTpSYWxld2F5LHNhbnMtc2VyaWZ9YF1cbn0pXG5leHBvcnQgY2xhc3MgQ2FsZW5kYXJDb21wb25lbnRcbntcbiAgLyoqXG4gICAqIEZpcnN0IGRhdGUgb2YgdGhlIHBlcmlvZCB0byBkaXNwbGF5IGluIHRoZSBjYWxlbmRhclxuICAgKi9cbiAgQElucHV0KCdzdGFydC1kYXRlJykgcHJpdmF0ZSBzdGFydERhdGUgOiBEYXRlO1xuXG4gIC8qKlxuICAgKiBMYXN0IGRhdGUgb2YgdGhlIHBlcmlvZCB0byBkaXNwbGF5IGluIHRoZSBjYWxlbmRhclxuICAgKi9cbiAgQElucHV0KCdlbmQtZGF0ZScpIHByaXZhdGUgZW5kRGF0ZSA6IERhdGU7XG4gIFxuICAvKipcbiAgICogV2VlayBkYXlzIGxhYmVsIHRvIGRpc3BsYXkgaW4gdGhlIGhlYWRlclxuICAgKiAoZGVmYXVsdCBpcyBlbmdsaXNoKS5cbiAgICovXG4gIEBJbnB1dCgnaGVhZGVycycpIHB1YmxpYyBoZWFkZXJzIDogc3RyaW5nW10gPSBbXG4gICAgJ01vbmRheScsXG4gICAgJ1R1ZXNkYXknLFxuICAgICdXZWRuZXNkYXknLFxuICAgICdUaHVyc2RheScsXG4gICAgJ0ZyaWRheScsXG4gICAgJ1NhdHVyZGF5JyxcbiAgICAnU3VuZGF5JyxcbiAgXTtcblxuICAvKipcbiAgICogQSBsaXN0IG9mIGV2ZW50cyB0byBkaXNwbGF5IGluIHRoZSBjYWxsZW5kYXIuXG4gICAqL1xuICBASW5wdXQoJ2V2ZW50cycpIHByaXZhdGUgZXZlbnRzIDogRXZlbnRbXSA9IFtdO1xuICBcbiAgLyoqXG4gICAqIFRoZSBuYW1lIG9mIHRoZSBkYXRlIGF0dHJpYnV0ZSB0byB1c2VcbiAgICogd2hlbiBkaXNwbGF5aW5nIHRoZSBldmVudHMgKGRlZmF1bHQgaXMgJ2RhdGUnKS5cbiAgICovXG4gIEBJbnB1dCgnZGF0ZS1hY2Nlc3NvcicpIHByaXZhdGUgZGF0ZUFjY2Vzc29yIDogc3RyaW5nID0gJ2RhdGUnO1xuXG4gIC8qKlxuICAgKiBUaGUgbmFtZSBvZiB0aGUgaWQgYXR0cmlidXRlIHRvIHVzZVxuICAgKiB3aGVuIGRpc3BsYXlpbmcgdGhlIGV2ZW50cyAoZGVmYXVsdCBpcyAnaWQnKS5cbiAgICovXG4gIEBJbnB1dCgnaWQtYWNjZXNzb3InKSBwcml2YXRlIGlkQWNjZXNzb3IgOiBzdHJpbmcgPSAnaWQnO1xuXG4gIHB1YmxpYyBjb25zdHJ1Y3RvcihcbiAgICBASW5qZWN0KERBWV9DRUxMX0NPTVBPTkVOVF9GQUNUT1JZX1BST1ZJREVSKSBwdWJsaWMgZGF5Q2VsbENvbXBvbmVudEZhY3RvcnkgOiBEYXlDZWxsQ29tcG9uZW50RmFjdG9yeTxFdmVudD4sXG4gICAgQE9wdGlvbmFsKCkgQEluamVjdChFVkVOVF9DT01QT05FTlRfRkFDVE9SWV9QUk9WSURFUikgcHVibGljIGV2ZW50Q29tcG9uZW50RmFjdG9yeSA6IEV2ZW50Q29tcG9uZW50RmFjdG9yeTxFdmVudD4sXG4gICAgQE9wdGlvbmFsKCkgQEluamVjdChXRUVLX1NVTU1BUllfQ09NUE9ORU5UX0ZBQ1RPUlkpIHB1YmxpYyB3ZWVrU3VtbWFyeUNvbXBvbmVudEZhY3RvcnkgOiBXZWVrU3VtbWFyeUNvbXBvbmVudEZhY3Rvcnk8RXZlbnQ+XG4gICl7fVxuXG4gIC8qIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAqL1xuXG4gIC8qKlxuICAgKiBWaWV3IGNvbnRhaW5lciBzdG9yaW5nIHRoZSBkeW5hbWljIGNvbXBvbmVudHNcbiAgICovXG4gIEBWaWV3Q2hpbGQoJ2RheUdyaWQnLCB7IHJlYWQ6IFZpZXdDb250YWluZXJSZWYgfSlcbiAgcHJpdmF0ZSBkYXlHcmlkQ29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZjtcbiAgcHJpdmF0ZSBkeW5hbWljRGF5Q2VsbFZpZXdNYW5hZ2VyIDogRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlcjxFdmVudCwgQWJzdHJhY3REYXlDZWxsQ29tcG9uZW50PEV2ZW50Pj5cbiAgICA9IG5ldyBEeW5hbWljQ29tcG9uZW50QXJyYXlNYW5hZ2VyPEV2ZW50LCBBYnN0cmFjdERheUNlbGxDb21wb25lbnQ8RXZlbnQ+PigpO1xuICBwcml2YXRlIGR5bmFtaWNXZWVrU3VtbWFyeVZpZXdNYW5hZ2VyIDogRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlcjxFdmVudCwgQWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudDxFdmVudD4+XG4gICAgPSBuZXcgRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlcjxFdmVudCwgQWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudDxFdmVudD4+KCk7XG5cbiAgLyogLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tICovXG5cbiAgcHJpdmF0ZSBkaXNwYXRjaGVyIDogRXZlbnREaXNwYXRjaGVyO1xuICBwcml2YXRlIGRheXMgOiBDYWxlbmRhckRheURhdGE8RXZlbnQ+W107XG4gIHByaXZhdGUgd2Vla3MgOiBDYWxlbmRhcldlZWtEYXRhPEV2ZW50PltdO1xuICBcbiAgcHJpdmF0ZSBvbGRIYXNoIDogc3RyaW5nID0gJyc7XG4gIG5nRG9DaGVjaygpXG4gIHtcbiAgICBsZXQgbmV3SGFzaCA9IGhhc2guc2hhMSh0aGlzLmV2ZW50cyk7XG4gICAgaWYgKG5ld0hhc2ggIT09IHRoaXMub2xkSGFzaClcbiAgICB7XG4gICAgICB0aGlzLnVwZGF0ZUNhbGVuZGFyKCk7XG4gICAgICB0aGlzLm9sZEhhc2ggPSBuZXdIYXNoO1xuICAgIH1cbiAgfVxuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXMgOiBTaW1wbGVDaGFuZ2VzKVxuICB7XG4gICAgaWYgKGNoYW5nZXMuc3RhcnREYXRlIHx8IGNoYW5nZXMuZW5kRGF0ZSB8fCBjaGFuZ2VzLmRhdGVBY2Nlc3NvcilcbiAgICB7XG4gICAgICBsZXQgc3RhcnREYXRlID0gKGNoYW5nZXMuc3RhcnREYXRlKSA/IGNoYW5nZXMuc3RhcnREYXRlLmN1cnJlbnRWYWx1ZSA6IHRoaXMuc3RhcnREYXRlO1xuICAgICAgbGV0IGVuZERhdGUgPSAoY2hhbmdlcy5lbmREYXRlKSA/IGNoYW5nZXMuZW5kRGF0ZS5jdXJyZW50VmFsdWUgOiB0aGlzLmVuZERhdGU7XG4gICAgICBsZXQgZGF0ZUFjY2Vzc29yID0gKGNoYW5nZXMuZGF0ZUFjY2Vzc29yKSA/IGNoYW5nZXMuZGF0ZUFjY2Vzc29yLmN1cnJlbnRWYWx1ZSA6IHRoaXMuZGF0ZUFjY2Vzc29yO1xuXG4gICAgICB0aGlzLmRpc3BhdGNoZXIgPSBuZXcgRXZlbnREaXNwYXRjaGVyKGRhdGVBY2Nlc3Nvciwgc3RhcnREYXRlLCBlbmREYXRlKTtcbiAgICAgIHRoaXMudXBkYXRlQ2FsZW5kYXIoKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogT2xkIHN0YXJ0IGFuZCBlbmQgZGF0ZSBvZiB0aGUgY2FsZW5kYXIuXG4gICAqIFVzZWQgdG8ga2VlcCB0cmFjayBvZiB0aGUgY2hhbmdlcyBpbiBwZXJpb2RcbiAgICogaW4gb3JkZXIgdG8gdXBkYXRlIHRoZSB2aWV3LlxuICAgKi9cbiAgcHJpdmF0ZSBvbGRTdGFydERhdGUgOiBEYXRlO1xuICBwcml2YXRlIG9sZEVuZERhdGUgOiBEYXRlO1xuXG5cbiAgcHJpdmF0ZSB1cGRhdGVDYWxlbmRhcigpXG4gIHtcbiAgICBpZiAoIXRoaXMuZGF5Q2VsbENvbXBvbmVudEZhY3RvcnkpXG4gICAge1xuICAgICAgdGhpcy5kYXlzID0gW107XG4gICAgICB0aHJvdyAnZGF5IGNlbGwgY29tcG9uZW50IGZhY3RvcnkgKGRheS1jb21wb25lbnQtZmFjdG9yeSkgd2FzIG5vdCBzZXQhJ1xuICAgIH1cbiAgICBlbHNlIGlmICghdGhpcy5kYXlHcmlkQ29udGFpbmVyKVxuICAgIHtcbiAgICAgIHRoaXMuZGF5cyA9IFtdO1xuICAgICAgdGhyb3cgJ2RheSBncmlkIChkYXlHcmlkKSBub3QgZm91bmQgaW4gdGVtcGxhdGUgISdcbiAgICB9XG4gICAgZWxzZVxuICAgIHtcbiAgICAgIHRoaXMuZGlzcGF0Y2hFdmVudHMoKTtcbiAgICAgIHRoaXMudXBkYXRlRGF5Q2VsbFZpZXdzKCk7XG5cbiAgICAgIGlmICh0aGlzLndlZWtTdW1tYXJ5Q29tcG9uZW50RmFjdG9yeSlcbiAgICAgIHtcbiAgICAgICAgdGhpcy51cGRhdGVXZWVrU3VtbWFyeUNlbGxWaWV3cygpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG5cbiAgLyoqXG4gICAqIFNvcnRzIGV2ZW50cyBieSBkYXRlXG4gICAqL1xuICBwcml2YXRlIGRpc3BhdGNoRXZlbnRzKClcbiAge1xuICAgIHRoaXMuZGlzcGF0Y2hlci5jbGVhcigpO1xuICAgIGZvciAobGV0IGV2ZW50IG9mIHRoaXMuZXZlbnRzKVxuICAgIHtcbiAgICAgIHRoaXMuZGlzcGF0Y2hlci5hZGQoZXZlbnQpO1xuICAgIH1cbiAgICB0aGlzLmRheXMgPSB0aGlzLmRpc3BhdGNoZXIuZ2V0RGF0ZUFycmF5KCkubWFwKGRhdGUgPT4gKHtcbiAgICAgIGlkOiB0aGlzLmRpc3BhdGNoZXIuZ2V0RGF5TnVtYmVyKGRhdGUpLFxuICAgICAgZGF0ZTogZGF0ZSxcbiAgICAgIGNhbGVuZGFyU3RhcnREYXRlOiB0aGlzLnN0YXJ0RGF0ZSxcbiAgICAgIGNhbGVuZGFyRW5kRGF0ZTogdGhpcy5lbmREYXRlLFxuICAgICAgZXZlbnRzOiB0aGlzLmRpc3BhdGNoZXIuZ2V0RXZlbnRzKGRhdGUpLFxuICAgICAgZGF0ZUFjY2Vzc29yOiB0aGlzLmRhdGVBY2Nlc3NvcixcbiAgICB9KSk7XG4gIH1cblxuICBwcml2YXRlIHVwZGF0ZURheUNlbGxWaWV3cygpXG4gIHtcbiAgICB0aGlzLmR5bmFtaWNEYXlDZWxsVmlld01hbmFnZXIudXBkYXRlQ29udGFpbmVyKFxuICAgICAgdGhpcy5kYXlHcmlkQ29udGFpbmVyLFxuICAgICAgdGhpcy5kYXlDZWxsQ29tcG9uZW50RmFjdG9yeSxcbiAgICAgIHRoaXMuZGF5cyxcbiAgICAgICdpZCcsXG4gICAgICB7XG4gICAgICAgIGFkZDogdGhpcy5vbkFkZC5iaW5kKHRoaXMpLFxuICAgICAgICB1cGRhdGU6IHRoaXMub25VcGRhdGUuYmluZCh0aGlzKSxcbiAgICAgICAgZGVsZXRlOiB0aGlzLm9uRGVsZXRlLmJpbmQodGhpcylcbiAgICAgIH0sXG4gICAgICB0aGlzLnVwZGF0ZURheUNlbGwuYmluZCh0aGlzKSxcbiAgICAgIHRoaXMudXBkYXRlRGF5Q2VsbC5iaW5kKHRoaXMpLFxuICAgICAgdGhpcy51cGRhdGVEYXlDZWxsLmJpbmQodGhpcylcbiAgICApO1xuICB9XG5cbiAgcHJpdmF0ZSB1cGRhdGVXZWVrU3VtbWFyeUNlbGxWaWV3cygpXG4gIHtcbiAgICB0aGlzLndlZWtzID0gdGhpcy5kaXNwYXRjaGVyLmdldFdlZWtBcnJheSgpLm1hcCh3ZWVrID0+ICh7XG4gICAgICBpZDogZ2V0V2Vla051bWJlcih3ZWVrLnN0YXJ0RGF0ZSksXG4gICAgICBzdGFydERhdGU6IHdlZWsuc3RhcnREYXRlLFxuICAgICAgZW5kRGF0ZTogd2Vlay5lbmREYXRlLFxuICAgICAgY2FsZW5kYXJTdGFydERhdGU6IHRoaXMuc3RhcnREYXRlLFxuICAgICAgY2FsZW5kYXJFbmREYXRlOiB0aGlzLmVuZERhdGUsXG4gICAgICBldmVudHM6IHRoaXMuZGlzcGF0Y2hlci5nZXRFdmVudHNCZXR3ZWVuKHdlZWsuc3RhcnREYXRlLCB3ZWVrLmVuZERhdGUpLFxuICAgICAgZGF0ZUFjY2Vzc29yOiB0aGlzLmRhdGVBY2Nlc3NvcixcbiAgICB9KSk7XG5cbiAgICBpZiAodGhpcy5zdGFydERhdGUgIT09IHRoaXMub2xkU3RhcnREYXRlIHx8IHRoaXMuZW5kRGF0ZSAhPT0gdGhpcy5vbGRFbmREYXRlKVxuICAgIHtcbiAgICAgIHRoaXMuZHluYW1pY1dlZWtTdW1tYXJ5Vmlld01hbmFnZXIuY2xlYXIoKTtcbiAgICAgIHRoaXMub2xkU3RhcnREYXRlID0gdGhpcy5zdGFydERhdGU7XG4gICAgICB0aGlzLm9sZEVuZERhdGUgPSB0aGlzLmVuZERhdGU7XG4gICAgfVxuICAgIFxuICAgIHRoaXMuZHluYW1pY1dlZWtTdW1tYXJ5Vmlld01hbmFnZXIudXBkYXRlQ29udGFpbmVyKFxuICAgICAgdGhpcy5kYXlHcmlkQ29udGFpbmVyLFxuICAgICAgdGhpcy53ZWVrU3VtbWFyeUNvbXBvbmVudEZhY3RvcnksXG4gICAgICB0aGlzLndlZWtzLFxuICAgICAgJ2lkJyxcbiAgICAgIHtcbiAgICAgICAgYWRkOiB0aGlzLm9uQWRkLmJpbmQodGhpcyksXG4gICAgICAgIHVwZGF0ZTogdGhpcy5vblVwZGF0ZS5iaW5kKHRoaXMpLFxuICAgICAgICBkZWxldGU6IHRoaXMub25EZWxldGUuYmluZCh0aGlzKVxuICAgICAgfVxuICAgICk7XG4gIH1cblxuICAvKipcbiAgICogVHJpZ2dlcnMgYSBkYXkgY2VsbCB2aWV3IGludGVybmFsIHVwZGF0ZVxuICAgKiBAcGFyYW0gY29tcG9uZW50IHRoZSBjb21wb25lbnQgcmVmIG9mIHRoZSBkYXkgY2VsbCB0byB1cGRhdGVcbiAgICovXG4gIHByaXZhdGUgdXBkYXRlRGF5Q2VsbChkYXRhIDogQ2FsZW5kYXJEYXlEYXRhPEV2ZW50PiwgY29tcG9uZW50IDogQ29tcG9uZW50UmVmPEFic3RyYWN0RGF5Q2VsbENvbXBvbmVudDxFdmVudD4+KSA6IHZvaWRcbiAge1xuICAgIGxldCBpbnN0YW5jZSA9IDxBYnN0cmFjdERheUNlbGxDb21wb25lbnQ8RXZlbnQ+PmNvbXBvbmVudC5pbnN0YW5jZTtcbiAgICBpbnN0YW5jZS51cGRhdGVFdmVudFZpZXdzKHRoaXMuZXZlbnRDb21wb25lbnRGYWN0b3J5LCB0aGlzLmlkQWNjZXNzb3IpO1xuICB9XG5cbiAgLyogLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tIC0tLSAtLS0gLS0tICovXG5cbiAgLyoqXG4gICAqIFRyaWdnZXJzIGEgZGVsZXRlIGV2ZW50IGVtaXNzaW9uXG4gICAqIEBwYXJhbSBldmVudCB0aGUgZXZlbnQgdG8gZGVsZXRlXG4gICAqL1xuICBAT3V0cHV0KCdkZWxldGUnKVxuICBwcml2YXRlIGRlbGV0ZUVtaXR0ZXIgOiBFdmVudEVtaXR0ZXI8RXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcjxFdmVudD4oKTtcbiAgcHJpdmF0ZSBvbkRlbGV0ZShldmVudDogRXZlbnQpXG4gIHtcbiAgICB0aGlzLmRlbGV0ZUVtaXR0ZXIuZW1pdChldmVudCk7XG4gIH1cblxuICAvKipcbiAgICogVHJpZ2dlcnMgYW4gdXBkYXRlIGV2ZW50IGVtaXNzaW9uXG4gICAqIEBwYXJhbSBldmVudCB0aGUgZXZlbnQgdG8gdXBkYXRlXG4gICAqL1xuICBAT3V0cHV0KCd1cGRhdGUnKVxuICBwcml2YXRlIHVwZGF0ZUVtaXR0ZXIgOiBFdmVudEVtaXR0ZXI8RXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcjxFdmVudD4oKTtcbiAgcHJpdmF0ZSBvblVwZGF0ZShldmVudDogRXZlbnQpXG4gIHtcbiAgICB0aGlzLnVwZGF0ZUVtaXR0ZXIuZW1pdChldmVudCk7XG4gIH1cblxuICAvKipcbiAgICogVHJpZ2dlcnMgYW4gYWRkIGV2ZW50IGVtaXNzaW9uXG4gICAqIEBwYXJhbSBldmVudCB0aGUgZXZlbnQgdG8gYWRkXG4gICAqL1xuICBAT3V0cHV0KCdhZGQnKVxuICBwcml2YXRlIGFkZEVtaXR0ZXIgOiBFdmVudEVtaXR0ZXI8RXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcjxFdmVudD4oKTtcbiAgcHJpdmF0ZSBvbkFkZChldmVudDogRXZlbnQpXG4gIHtcbiAgICB0aGlzLmFkZEVtaXR0ZXIuZW1pdChldmVudCk7XG4gIH1cbn1cbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5pbXBvcnQgeyBDYWxlbmRhckNvbXBvbmVudCB9IGZyb20gJy4vdmlld3MvY2FsZW5kYXIvY2FsZW5kYXIuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgQ2FsZW5kYXJDb21wb25lbnQsXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBDYWxlbmRhckNvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIEFuZ3VsYXJBZHZhbmNlZENhbGVuZGFyTW9kdWxlIHsgfVxuIiwiaW1wb3J0IHsgSW5wdXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRHluYW1pY0NvbXBvbmVudDxEYXRhVHlwZT5cbntcbiAgQElucHV0KCdkYXRhJylcbiAgcHVibGljIGRhdGEgOiBEYXRhVHlwZTtcbiAgXG4gIHB1YmxpYyB1cGRhdGVEYXRhKGRhdGEgOiBEYXRhVHlwZSlcbiAge1xuICAgIHRoaXMuZGF0YSA9IGRhdGE7XG4gIH1cblxuICBwdWJsaWMgYWJzdHJhY3Qgc3Vic2NyaWJlKG5hbWUgOiBzdHJpbmcsIGNhbGxiYWNrIDogRnVuY3Rpb24pIDogdm9pZDtcbn0iLCJpbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50IH0gZnJvbSBcIi4uL2R5bmFtaWMvZHluYW1pYy5jb21wb25lbnRcIjtcbmltcG9ydCB7IEV2ZW50RW1pdHRlciwgT3V0cHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIER5bmFtaWNDcnVkQ29tcG9uZW50PERhdGFUeXBlLCBDcnVkVHlwZT4gZXh0ZW5kcyBEeW5hbWljQ29tcG9uZW50PERhdGFUeXBlPlxue1xuICBwdWJsaWMgc3Vic2NyaWJlKG5hbWUgOiBzdHJpbmcsIGNhbGxiYWNrIDogRnVuY3Rpb24pIDogdm9pZFxuICB7XG4gICAgc3dpdGNoKG5hbWUpXG4gICAge1xuICAgICAgY2FzZSAndXBkYXRlJzpcbiAgICAgICAgdGhpcy51cGRhdGVFbWl0dGVyLnN1YnNjcmliZShjYWxsYmFjayk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnZGVsZXRlJzpcbiAgICAgICAgdGhpcy5kZWxldGVFbWl0dGVyLnN1YnNjcmliZShjYWxsYmFjayk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnYWRkJzpcbiAgICAgICAgdGhpcy5hZGRFbWl0dGVyLnN1YnNjcmliZShjYWxsYmFjayk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgdGhyb3cgJ2NhbGxiYWNrIHR5cGUgXFwnJyArIG5hbWUgKyAnXFwnIGlzIG5vdCBhIGtub3duIGV2ZW50IGNhbGxiYWNrICEnO1xuICAgIH1cbiAgfVxuXG4gIEBPdXRwdXQoJ2RlbGV0ZScpXG4gIHByaXZhdGUgZGVsZXRlRW1pdHRlciA6IEV2ZW50RW1pdHRlcjxDcnVkVHlwZT4gPSBuZXcgRXZlbnRFbWl0dGVyPENydWRUeXBlPigpO1xuICBwcm90ZWN0ZWQgb25EZWxldGUoZXZlbnQ6IENydWRUeXBlKVxuICB7XG4gICAgdGhpcy5kZWxldGVFbWl0dGVyLmVtaXQoZXZlbnQpO1xuICB9XG5cbiAgQE91dHB1dCgndXBkYXRlJylcbiAgcHJpdmF0ZSB1cGRhdGVFbWl0dGVyIDogRXZlbnRFbWl0dGVyPENydWRUeXBlPiA9IG5ldyBFdmVudEVtaXR0ZXI8Q3J1ZFR5cGU+KCk7XG4gIHByb3RlY3RlZCBvblVwZGF0ZShldmVudDogQ3J1ZFR5cGUpXG4gIHtcbiAgICB0aGlzLnVwZGF0ZUVtaXR0ZXIuZW1pdChldmVudCk7XG4gIH1cblxuICBAT3V0cHV0KCdhZGQnKVxuICBwcml2YXRlIGFkZEVtaXR0ZXIgOiBFdmVudEVtaXR0ZXI8Q3J1ZFR5cGU+ID0gbmV3IEV2ZW50RW1pdHRlcjxDcnVkVHlwZT4oKTtcbiAgcHJvdGVjdGVkIG9uQWRkKGV2ZW50OiBDcnVkVHlwZSlcbiAge1xuICAgIHRoaXMuYWRkRW1pdHRlci5lbWl0KGV2ZW50KTtcbiAgfVxufSIsImltcG9ydCB7IER5bmFtaWNDcnVkQ29tcG9uZW50IH0gZnJvbSBcIi4uL2R5bmFtaWMtY3J1ZC9keW5hbWljLWNydWQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBDYWxlbmRhckRheURhdGEgfSBmcm9tIFwiLi4vLi4vdHlwZXMvY2FsZW5kYXItZGF5XCI7XG5pbXBvcnQgeyBWaWV3Q2hpbGQsIFZpZXdDb250YWluZXJSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudEFycmF5TWFuYWdlciB9IGZyb20gXCIuLi9keW5hbWljL2R5bmFtaWMtbWFuYWdlci5jb21wb25lbnRcIjtcbmltcG9ydCB7IEFic3RyYWN0RXZlbnRDb21wb25lbnQgfSBmcm9tIFwiLi4vZXZlbnQvYWJzdHJhY3QtZXZlbnQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50RmFjdG9yeSB9IGZyb20gXCIuLi9keW5hbWljL2R5bmFtaWMuZmFjdG9yeVwiO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQWJzdHJhY3REYXlDZWxsQ29tcG9uZW50PERhdGFUeXBlPiBleHRlbmRzIER5bmFtaWNDcnVkQ29tcG9uZW50PENhbGVuZGFyRGF5RGF0YTxEYXRhVHlwZT4sIERhdGFUeXBlPlxue1xuICAvKipcbiAgICogVmlldyBjb250YWluZXIgc3RvcmluZyB0aGUgZHluYW1pYyBjb21wb25lbnRzXG4gICAqL1xuICBAVmlld0NoaWxkKCdldmVudFZpZXdzQ29udGFpbmVyJywgeyByZWFkOiBWaWV3Q29udGFpbmVyUmVmIH0pXG4gIHByaXZhdGUgZXZlbnRWaWV3Q29udGFpbmVyIDogVmlld0NvbnRhaW5lclJlZjtcbiAgcHJpdmF0ZSBkeW5hbWljRXZlbnRWaWV3TWFuYWdlciA6IER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXI8RGF0YVR5cGUsIEFic3RyYWN0RXZlbnRDb21wb25lbnQ8RGF0YVR5cGU+PlxuICAgID0gbmV3IER5bmFtaWNDb21wb25lbnRBcnJheU1hbmFnZXI8RGF0YVR5cGUsIEFic3RyYWN0RXZlbnRDb21wb25lbnQ8RGF0YVR5cGU+PigpO1xuXG4gIC8qKlxuICAgKiBNZXRob2QgY2FsbGVkIHRvIHVwZGF0ZSB0aGUgZXZlbnQgdmlld3MgaW4gdGhlIGRheSBjZWxsXG4gICAqL1xuICBwdWJsaWMgdXBkYXRlRXZlbnRWaWV3cyhldmVudENvbXBvbmVudEZhY3RvcnkgOiBEeW5hbWljQ29tcG9uZW50RmFjdG9yeTxEYXRhVHlwZSwgQWJzdHJhY3RFdmVudENvbXBvbmVudDxEYXRhVHlwZT4+LCBpZEFjY2Vzc29yIDogc3RyaW5nKVxuICB7XG4gICAgaWYgKGV2ZW50Q29tcG9uZW50RmFjdG9yeSAmJiB0aGlzLmV2ZW50Vmlld0NvbnRhaW5lcilcbiAgICB7XG4gICAgICB0aGlzLmR5bmFtaWNFdmVudFZpZXdNYW5hZ2VyLnVwZGF0ZUNvbnRhaW5lcihcbiAgICAgICAgdGhpcy5ldmVudFZpZXdDb250YWluZXIsXG4gICAgICAgIGV2ZW50Q29tcG9uZW50RmFjdG9yeSxcbiAgICAgICAgdGhpcy5kYXRhLmV2ZW50cyxcbiAgICAgICAgaWRBY2Nlc3NvcixcbiAgICAgICAge1xuICAgICAgICAgIGRlbGV0ZTogdGhpcy5vbkRlbGV0ZS5iaW5kKHRoaXMpLFxuICAgICAgICAgIHVwZGF0ZTogdGhpcy5vblVwZGF0ZS5iaW5kKHRoaXMpXG4gICAgICAgIH1cbiAgICAgICk7XG4gICAgfVxuICB9XG59IiwiaW1wb3J0IHsgRHluYW1pY0NydWRDb21wb25lbnQgfSBmcm9tIFwiLi4vZHluYW1pYy1jcnVkL2R5bmFtaWMtY3J1ZC5jb21wb25lbnRcIjtcbmltcG9ydCB7IENhbGVuZGFyV2Vla0RhdGEgfSBmcm9tIFwiLi4vLi4vdHlwZXMvY2FsZW5kYXItd2Vla1wiO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQWJzdHJhY3RXZWVrU3VtbWFyeUNvbXBvbmVudDxEYXRhVHlwZT4gZXh0ZW5kcyBEeW5hbWljQ3J1ZENvbXBvbmVudDxDYWxlbmRhcldlZWtEYXRhPERhdGFUeXBlPiwgRGF0YVR5cGU+XG57XG59IiwiaW1wb3J0IHsgRHluYW1pY0NydWRDb21wb25lbnQgfSBmcm9tIFwiLi4vZHluYW1pYy1jcnVkL2R5bmFtaWMtY3J1ZC5jb21wb25lbnRcIjtcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEFic3RyYWN0RXZlbnRDb21wb25lbnQ8RGF0YVR5cGU+IGV4dGVuZHMgRHluYW1pY0NydWRDb21wb25lbnQ8RGF0YVR5cGUsIERhdGFUeXBlPlxue1xufSJdLCJuYW1lcyI6WyJtb21lbnQiLCJ0c2xpYl8xLl9fZXh0ZW5kcyIsInRzbGliXzEuX192YWx1ZXMiLCJFdmVudEVtaXR0ZXIiLCJDb21wb25lbnQiLCJJbmplY3QiLCJPcHRpb25hbCIsIklucHV0IiwiVmlld0NoaWxkIiwiVmlld0NvbnRhaW5lclJlZiIsIk91dHB1dCIsIk5nTW9kdWxlIiwiQ29tbW9uTW9kdWxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7SUFBQTs7Ozs7Ozs7Ozs7Ozs7SUFjQTtJQUVBLElBQUksYUFBYSxHQUFHLFVBQVMsQ0FBQyxFQUFFLENBQUM7UUFDN0IsYUFBYSxHQUFHLE1BQU0sQ0FBQyxjQUFjO2FBQ2hDLEVBQUUsU0FBUyxFQUFFLEVBQUUsRUFBRSxZQUFZLEtBQUssSUFBSSxVQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQzVFLFVBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQUUsSUFBSSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztvQkFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMvRSxPQUFPLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDL0IsQ0FBQyxDQUFDO0FBRUYsdUJBQTBCLENBQUMsRUFBRSxDQUFDO1FBQzFCLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDcEIsZ0JBQWdCLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLEVBQUU7UUFDdkMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxDQUFDLEtBQUssSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN6RixDQUFDO0FBRUQsc0JBNkV5QixDQUFDO1FBQ3RCLElBQUksQ0FBQyxHQUFHLE9BQU8sTUFBTSxLQUFLLFVBQVUsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDO1lBQUUsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hCLE9BQU87WUFDSCxJQUFJLEVBQUU7Z0JBQ0YsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNO29CQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQztnQkFDbkMsT0FBTyxFQUFFLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDM0M7U0FDSixDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lDbkhELHFCQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7Ozs7OztBQUUvQix1QkFBMEIsQ0FBUSxFQUFFLENBQVE7UUFFMUMsT0FBTyxBQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUMsT0FBTyxFQUFFLEFBQUMsQ0FBQztLQUNqSTs7Ozs7QUFFRCwyQkFBOEIsSUFBVztRQUV2QyxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztLQUM1Qjs7Ozs7QUFFRCxnQ0FBbUMsSUFBVztRQUU1QyxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7S0FFL0M7Ozs7O0FBRUQsMkNBQThDLElBQVc7UUFFdkQsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztLQUN6RDs7Ozs7QUFFRCwrQkFBa0MsSUFBVztRQUUzQyxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7S0FDN0M7Ozs7O0FBRUQsd0NBQTJDLElBQVc7UUFFcEQsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztLQUM5RDs7Ozs7O0lDL0JELHFCQUFJQSxRQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDOzs7OztJQU0vQjs7O1FBQUE7aUNBVXFDLFlBQXFCLEVBQWtCLFNBQWdCLEVBQWtCLE9BQWM7WUFBdkYsaUJBQVksR0FBWixZQUFZLENBQVM7WUFBa0IsY0FBUyxHQUFULFNBQVMsQ0FBTztZQUFrQixZQUFPLEdBQVAsT0FBTyxDQUFPOzs7OztRQUVuSCwrQkFBSzs7OztnQkFFVixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztnQkFDbkIsS0FBSyxxQkFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFDcEQ7b0JBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7aUJBQ3ZCOzs7Ozs7UUFHSSw2QkFBRzs7OztzQkFBQyxLQUFXO2dCQUVwQixxQkFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDcEMscUJBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzVDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsRUFDaEM7b0JBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzFDOzs7Ozs7UUFHSSxtQ0FBUzs7OztzQkFBQyxJQUFXO2dCQUUxQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDOzs7Ozs7O1FBR3pDLDBDQUFnQjs7Ozs7c0JBQUMsU0FBZ0IsRUFBRSxPQUFjO2dCQUV0RCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzs7Ozs7OztRQU9wRixzQ0FBWTs7Ozs7O2dCQUVqQixxQkFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDO2dCQUNkLHFCQUFJLE9BQU8sR0FBR0EsUUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDckMscUJBQUksSUFBSSxHQUFJQSxRQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNqQyxPQUFNLE9BQU8sR0FBRyxJQUFJLEVBQ3BCO29CQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7b0JBQzVCLE9BQU8sR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztpQkFDakM7Z0JBQ0QsT0FBTyxJQUFJLENBQUM7Ozs7O1FBR1Asc0NBQVk7Ozs7Z0JBRWpCLHFCQUFJLEtBQUssR0FBRyxFQUFFLENBQUM7Z0JBQ2YscUJBQUksT0FBTyxHQUFHQSxRQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNyQyxxQkFBSSxJQUFJLEdBQUlBLFFBQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ2pDLE9BQU0sT0FBTyxHQUFHLElBQUksRUFDcEI7b0JBQ0UsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLFNBQVMsRUFBRSxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsT0FBTyxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQztvQkFDckYsT0FBTyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO2lCQUNqQztnQkFDRCxPQUFPLEtBQUssQ0FBQzs7Ozs7Ozs7UUFRUixzQ0FBWTs7Ozs7O3NCQUFDLElBQVc7Z0JBRTdCLE9BQU9BLFFBQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUNBLFFBQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7Ozs7Ozs7UUFPcEQsOENBQW9COzs7Ozs7Z0JBRXpCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDOzs4QkE3Ri9DO1FBK0ZDLENBQUE7Ozs7Ozs7Ozs7O0FDM0ZEOzs7Ozs7SUFBQTt5Q0FJc0Msd0JBQW1ELEVBQUUsYUFBbUM7WUFBeEYsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUEyQjtZQUVyRixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyx1QkFBdUIsQ0FBQyxhQUFhLENBQUMsQ0FBQzs7Ozs7Ozs7OztRQVUvRSx3REFBc0I7Ozs7Ozs7O3NCQUFDLFNBQTRCLEVBQUUsSUFBZSxFQUFFLFNBQStCLEVBQUUsS0FBZTtnQkFFM0gscUJBQUksWUFBWSxHQUFHLFNBQVMsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDbEUscUJBQUksUUFBUSxJQUFrQixZQUFZLENBQUMsUUFBUSxDQUFBLENBQUM7Z0JBQ3BELFFBQVEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzFCLElBQUksU0FBUyxFQUNiO29CQUNFLEtBQUsscUJBQUksTUFBSSxJQUFJLFNBQVMsRUFDMUI7d0JBQ0UsUUFBUSxDQUFDLFNBQVMsQ0FBQyxNQUFJLEVBQUUsU0FBUyxDQUFDLE1BQUksQ0FBQyxDQUFDLENBQUM7cUJBQzNDO2lCQUNGO2dCQUNELE9BQU8sWUFBWSxDQUFDOztzQ0FoQ3hCO1FBa0NDOzs7Ozs7Ozs7O0FDOUJEOzs7UUFBQTtRQUE4REMseUNBQW1FO3VDQUU1Ryx3QkFBbUQsRUFBRSxhQUFzRDttQkFFNUgsa0JBQU0sd0JBQXdCLEVBQUUsYUFBYSxDQUFDOztvQ0FSbEQ7TUFJOEQsdUJBQXVCLEVBTXBGOzs7Ozs7Ozs7O0FDTEQ7OztRQUFBO1FBQWdFQSwyQ0FBc0Y7eUNBRWpJLHdCQUFtRCxFQUFFLGFBQXdEO21CQUU5SCxrQkFBTSx3QkFBd0IsRUFBRSxhQUFhLENBQUM7O3NDQVRsRDtNQUtnRSx1QkFBdUIsRUFNdEY7Ozs7Ozs7Ozs7SUNORDs7Ozs7SUFBQTs7Ozs7Z0NBTXNDLEVBQUU7Ozs7MkNBOEVzQyxFQUFFOzs7OztRQTVFdkUsNENBQUs7Ozs7Z0JBRVYsS0FBSyxxQkFBSSxFQUFFLElBQUksSUFBSSxDQUFDLHVCQUF1QixFQUMzQztvQkFDRSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsRUFDcEM7d0JBQ0UsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO3dCQUMzQyxPQUFPLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztxQkFDekM7aUJBQ0Y7Z0JBQ0QsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7Ozs7Ozs7Ozs7Ozs7OztRQU9sQixzREFBZTs7Ozs7Ozs7Ozs7OztzQkFDcEIsU0FBNEIsRUFDNUIsT0FBMEQsRUFDMUQsU0FBc0IsRUFDdEIsY0FBdUIsRUFDdkIseUJBQWdELEVBQ2hELFNBQXFCLEVBQ3JCLFlBQXdCLEVBQ3hCLFlBQXdCO2dCQUd4QixJQUFLLENBQUMsU0FBUyxFQUNmO29CQUNFLE1BQU0sNEVBQTRFLENBQUE7aUJBQ25GOztvQkFDRCxLQUFpQixJQUFBLGNBQUFDLFNBQUEsU0FBUyxDQUFBLG9DQUFBO3dCQUFyQixJQUFJLElBQUksc0JBQUE7d0JBRVgscUJBQUksY0FBYyxHQUFHLEtBQUssQ0FBQzs7NEJBQzNCLEtBQW9CLElBQUEsS0FBQUEsU0FBQSxJQUFJLENBQUMsWUFBWSxDQUFBLGdCQUFBO2dDQUFoQyxJQUFJLE9BQU8sV0FBQTtnQ0FFZCxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxPQUFPLENBQUMsY0FBYyxDQUFDLEVBQ3BEO29DQUNFLGNBQWMsR0FBRyxJQUFJLENBQUM7b0NBQ3RCLE1BQU07aUNBQ1A7NkJBQ0Y7Ozs7Ozs7Ozs7Ozs7Ozt3QkFDRCxJQUFJLGNBQWMsRUFDbEI7NEJBQ0UsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFDO3lCQUM1RTs2QkFFRDs0QkFDRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLHlCQUF5QixFQUFFLFNBQVMsQ0FBQyxDQUFDO3lCQUMxRztxQkFDRjs7Ozs7Ozs7Ozs7Ozs7OztvQkFFRCxLQUFvQixJQUFBLEtBQUFBLFNBQUEsSUFBSSxDQUFDLFlBQVksQ0FBQSxnQkFBQTt3QkFBaEMsSUFBSSxPQUFPLFdBQUE7d0JBRWQscUJBQUksWUFBWSxHQUFHLEtBQUssQ0FBQzs7NEJBQ3pCLEtBQWlCLElBQUEsY0FBQUEsU0FBQSxTQUFTLENBQUEsb0NBQUE7Z0NBQXJCLElBQUksSUFBSSxzQkFBQTtnQ0FFWCxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxPQUFPLENBQUMsY0FBYyxDQUFDLEVBQ3BEO29DQUNFLFlBQVksR0FBRyxJQUFJLENBQUM7b0NBQ3BCLE1BQU07aUNBQ1A7NkJBQ0Y7Ozs7Ozs7Ozs7Ozs7Ozt3QkFDRCxJQUFJLENBQUMsWUFBWSxFQUNqQjs0QkFDRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsWUFBWSxDQUFDLENBQUM7eUJBQy9FO3FCQUNGOzs7Ozs7Ozs7Ozs7Ozs7Z0JBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUM7Ozs7Ozs7Ozs7UUFReEIsNkRBQXNCOzs7Ozs7O3NCQUFDLFNBQTRCLEVBQUUsSUFBZSxFQUFFLGNBQXVCLEVBQUUsWUFBd0I7Z0JBRTdILElBQUksT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssUUFBUSxFQUM1QztvQkFDRSxNQUFNLE1BQU0sR0FBRyxjQUFjLEdBQUcsaUVBQWlFLENBQUM7aUJBQ25HO3FCQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQzVEO29CQUNFLE1BQU0sd0ZBQXdGLENBQUM7aUJBQ2hHO3FCQUVEO29CQUNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFDN0QsT0FBTyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7b0JBQzFELElBQUksWUFBWTt3QkFBRSxZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMxRjs7Ozs7Ozs7Ozs7UUFHSywwREFBbUI7Ozs7Ozs7OztzQkFDekIsU0FBNEIsRUFDNUIsT0FBMEQsRUFDMUQsSUFBZSxFQUNmLGNBQXVCLEVBQ3ZCLHlCQUErQyxFQUMvQyxTQUFxQjtnQkFHckIsSUFBSSxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxRQUFRLEVBQzVDO29CQUNFLE1BQU0sTUFBTSxHQUFHLGNBQWMsR0FBRyw4REFBOEQsQ0FBQztpQkFDaEc7cUJBQ0ksSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQzNEO29CQUNFLE1BQU0sMkZBQTJGLENBQUM7aUJBQ25HO3FCQUVEO29CQUNFLHFCQUFJLFlBQVksR0FBRyxPQUFPLENBQUMsc0JBQXNCLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSx5QkFBeUIsQ0FBQyxDQUFDO29CQUM5RixJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEdBQUcsWUFBWSxDQUFDO29CQUNsRSxJQUFJLFNBQVM7d0JBQUUsU0FBUyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDcEY7Ozs7Ozs7Ozs7UUFPSyw2REFBc0I7Ozs7Ozs7O3NCQUFDLFNBQTRCLEVBQUUsSUFBZSxFQUFFLGNBQXVCLEVBQUUsWUFBd0I7Z0JBRTdILElBQUksT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssUUFBUSxFQUM1QztvQkFDRSxNQUFNLE1BQU0sR0FBRyxjQUFjLEdBQUcsOERBQThELENBQUM7aUJBQ2hHO3FCQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQzVEO29CQUNFLE1BQU0scUZBQXFGLENBQUM7aUJBQzdGO3FCQUVEO29CQUNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUM3RSxJQUFJLFlBQVk7d0JBQUUsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDMUY7OzJDQXhKTDtRQTBKQyxDQUFBOzs7Ozs7Ozs7O0FDbkpEOzs7UUFBQTtRQUFvRUQsK0NBQTJGOzZDQUUxSSx3QkFBbUQsRUFBRSxhQUE0RDttQkFFbEksa0JBQU0sd0JBQXdCLEVBQUUsYUFBYSxDQUFDOzs7Ozs7OztRQUd6Qyw0REFBc0I7Ozs7OztzQkFBQyxTQUE0QixFQUFFLElBQWlDLEVBQUUsU0FBK0I7Z0JBRTVILHFCQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsMkJBQTJCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUNqRSxPQUFPLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxpQkFBTSxzQkFBc0IsWUFBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLENBQUMsR0FBRyxTQUFTLENBQUM7Ozs7Ozs7UUFHbEcsaUVBQTJCOzs7OztzQkFBQyxTQUE0QixFQUFFLElBQWlDO2dCQUVqRyxxQkFBSSxLQUFLLENBQUM7Z0JBRVYsS0FBSyxLQUFLLEdBQUcsQ0FBQyxFQUFFLEFBQVUsS0FBSyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQzNEO29CQUNFLHFCQUFJLGlCQUFpQixHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQzlFLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQ3ZGO3dCQUNFLE9BQU8sS0FBSyxHQUFHLENBQUMsQ0FBQztxQkFDbEI7aUJBQ0Y7Z0JBQ0QsT0FBTyxDQUFDLENBQUMsQ0FBQzs7MENBaENkO01BT29FLHVCQUF1QixFQTJCMUY7Ozs7OztJQ3BCRCxxQkFBSUQsUUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMvQixxQkFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO0FBRWxDLElBQU8scUJBQU0sbUNBQW1DLEdBQUcseUJBQXlCLENBQUM7QUFDN0UsSUFBTyxxQkFBTSxnQ0FBZ0MsR0FBTSx1QkFBdUIsQ0FBQztBQUMzRSxJQUFPLHFCQUFNLDhCQUE4QixHQUFRLDZCQUE2QixDQUFDOzttQ0FxRHpCLHVCQUF3RCxFQUMvQyxxQkFBb0QsRUFDdEQsMkJBQWdFO1lBRnZFLDRCQUF1QixHQUF2Qix1QkFBdUIsQ0FBaUM7WUFDL0MsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUErQjtZQUN0RCxnQ0FBMkIsR0FBM0IsMkJBQTJCLENBQXFDOzs7OzsyQkE5Qi9FO2dCQUM1QyxRQUFRO2dCQUNSLFNBQVM7Z0JBQ1QsV0FBVztnQkFDWCxVQUFVO2dCQUNWLFFBQVE7Z0JBQ1IsVUFBVTtnQkFDVixRQUFRO2FBQ1Q7Ozs7MEJBSzJDLEVBQUU7Ozs7O2dDQU1VLE1BQU07Ozs7OzhCQU1WLElBQUk7NkNBZ0JwRCxJQUFJLDRCQUE0QixFQUEwQztpREFFMUUsSUFBSSw0QkFBNEIsRUFBOEM7MkJBUXZELEVBQUU7Ozs7O2lDQWlKaUIsSUFBSUcsaUJBQVksRUFBUzs7Ozs7aUNBV3pCLElBQUlBLGlCQUFZLEVBQVM7Ozs7OzhCQVc1QixJQUFJQSxpQkFBWSxFQUFTOzs7OztRQXRLcEUscUNBQVM7OztZQUFUO2dCQUVFLHFCQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDckMsSUFBSSxPQUFPLEtBQUssSUFBSSxDQUFDLE9BQU8sRUFDNUI7b0JBQ0UsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO29CQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztpQkFDeEI7YUFDRjs7Ozs7UUFFRCx1Q0FBVzs7OztZQUFYLFVBQVksT0FBdUI7Z0JBRWpDLElBQUksT0FBTyxpQkFBYyxPQUFPLFdBQVEsSUFBSSxPQUFPLGdCQUFhLEVBQ2hFO29CQUNFLHFCQUFJLFNBQVMsR0FBRyxDQUFDLE9BQU8saUJBQWMsT0FBTyxjQUFXLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO29CQUN0RixxQkFBSSxPQUFPLEdBQUcsQ0FBQyxPQUFPLGVBQVksT0FBTyxZQUFTLFlBQVksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO29CQUM5RSxxQkFBSSxZQUFZLEdBQUcsQ0FBQyxPQUFPLG9CQUFpQixPQUFPLGlCQUFjLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO29CQUVsRyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksZUFBZSxDQUFDLFlBQVksRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7b0JBQ3hFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztpQkFDdkI7YUFDRjs7OztRQVdPLDBDQUFjOzs7O2dCQUVwQixJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixFQUNqQztvQkFDRSxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztvQkFDZixNQUFNLGlFQUFpRSxDQUFBO2lCQUN4RTtxQkFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUMvQjtvQkFDRSxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztvQkFDZixNQUFNLDRDQUE0QyxDQUFBO2lCQUNuRDtxQkFFRDtvQkFDRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7b0JBQ3RCLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO29CQUUxQixJQUFJLElBQUksQ0FBQywyQkFBMkIsRUFDcEM7d0JBQ0UsSUFBSSxDQUFDLDBCQUEwQixFQUFFLENBQUM7cUJBQ25DO2lCQUNGOzs7Ozs7UUFPSywwQ0FBYzs7Ozs7O2dCQUVwQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDOztvQkFDeEIsS0FBa0IsSUFBQSxLQUFBRCxTQUFBLElBQUksQ0FBQyxNQUFNLENBQUEsZ0JBQUE7d0JBQXhCLElBQUksT0FBSyxXQUFBO3dCQUVaLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLE9BQUssQ0FBQyxDQUFDO3FCQUM1Qjs7Ozs7Ozs7Ozs7Ozs7O2dCQUNELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO29CQUFJLFFBQUM7d0JBQ3RELEVBQUUsRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7d0JBQ3RDLElBQUksRUFBRSxJQUFJO3dCQUNWLGlCQUFpQixFQUFFLEtBQUksQ0FBQyxTQUFTO3dCQUNqQyxlQUFlLEVBQUUsS0FBSSxDQUFDLE9BQU87d0JBQzdCLE1BQU0sRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7d0JBQ3ZDLFlBQVksRUFBRSxLQUFJLENBQUMsWUFBWTtxQkFDaEM7aUJBQUMsQ0FBQyxDQUFDOzs7Ozs7UUFHRSw4Q0FBa0I7Ozs7Z0JBRXhCLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxlQUFlLENBQzVDLElBQUksQ0FBQyxnQkFBZ0IsRUFDckIsSUFBSSxDQUFDLHVCQUF1QixFQUM1QixJQUFJLENBQUMsSUFBSSxFQUNULElBQUksRUFDSjtvQkFDRSxHQUFHLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUMxQixNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUNoQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2lCQUNqQyxFQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUM3QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFDN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQzlCLENBQUM7Ozs7O1FBR0ksc0RBQTBCOzs7OztnQkFFaEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUk7b0JBQUksUUFBQzt3QkFDdkQsRUFBRSxFQUFFLGFBQWEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO3dCQUNqQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7d0JBQ3pCLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTzt3QkFDckIsaUJBQWlCLEVBQUUsS0FBSSxDQUFDLFNBQVM7d0JBQ2pDLGVBQWUsRUFBRSxLQUFJLENBQUMsT0FBTzt3QkFDN0IsTUFBTSxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDO3dCQUN0RSxZQUFZLEVBQUUsS0FBSSxDQUFDLFlBQVk7cUJBQ2hDO2lCQUFDLENBQUMsQ0FBQztnQkFFSixJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLElBQUksQ0FBQyxVQUFVLEVBQzVFO29CQUNFLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDM0MsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO29CQUNuQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7aUJBQ2hDO2dCQUVELElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxlQUFlLENBQ2hELElBQUksQ0FBQyxnQkFBZ0IsRUFDckIsSUFBSSxDQUFDLDJCQUEyQixFQUNoQyxJQUFJLENBQUMsS0FBSyxFQUNWLElBQUksRUFDSjtvQkFDRSxHQUFHLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUMxQixNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUNoQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2lCQUNqQyxDQUNGLENBQUM7Ozs7Ozs7O1FBT0kseUNBQWE7Ozs7OztzQkFBQyxJQUE2QixFQUFFLFNBQXlEO2dCQUU1RyxxQkFBSSxRQUFRLElBQW9DLFNBQVMsQ0FBQyxRQUFRLENBQUEsQ0FBQztnQkFDbkUsUUFBUSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Ozs7OztRQVdqRSxvQ0FBUTs7OztzQkFBQyxLQUFZO2dCQUUzQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs7Ozs7O1FBU3pCLG9DQUFROzs7O3NCQUFDLEtBQVk7Z0JBRTNCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDOzs7Ozs7UUFTekIsaUNBQUs7Ozs7c0JBQUMsS0FBWTtnQkFFeEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7OztvQkFwUC9CRSxjQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLDJCQUEyQjt3QkFDckMsUUFBUSxFQUFFLHdLQUVvRDt3QkFDOUQsTUFBTSxFQUFFLENBQUMsZ0lBQWdJLENBQUM7cUJBQzNJOzs7Ozt3QkFqQlEsdUJBQXVCLHVCQThEM0JDLFdBQU0sU0FBQyxtQ0FBbUM7d0JBL0R0QyxxQkFBcUIsdUJBZ0V6QkMsYUFBUSxZQUFJRCxXQUFNLFNBQUMsZ0NBQWdDO3dCQTdEL0MsMkJBQTJCLHVCQThEL0JDLGFBQVEsWUFBSUQsV0FBTSxTQUFDLDhCQUE4Qjs7OztnQ0F6Q25ERSxVQUFLLFNBQUMsWUFBWTs4QkFLbEJBLFVBQUssU0FBQyxVQUFVOzhCQU1oQkEsVUFBSyxTQUFDLFNBQVM7NkJBYWZBLFVBQUssU0FBQyxRQUFRO21DQU1kQSxVQUFLLFNBQUMsZUFBZTtpQ0FNckJBLFVBQUssU0FBQyxhQUFhO3VDQWFuQkMsY0FBUyxTQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRUMscUJBQWdCLEVBQUU7b0NBNkovQ0MsV0FBTSxTQUFDLFFBQVE7b0NBV2ZBLFdBQU0sU0FBQyxRQUFRO2lDQVdmQSxXQUFNLFNBQUMsS0FBSzs7Z0NBclFmOzs7Ozs7O0FDQUE7Ozs7b0JBS0NDLGFBQVEsU0FBQzt3QkFDUixPQUFPLEVBQUU7NEJBQ1BDLG1CQUFZO3lCQUNiO3dCQUNELFlBQVksRUFBRTs0QkFDWixpQkFBaUI7eUJBQ2xCO3dCQUNELE9BQU8sRUFBRTs0QkFDUCxpQkFBaUI7eUJBQ2xCO3FCQUNGOzs0Q0FmRDs7Ozs7OztBQ0FBOzs7Ozs7Ozs7OztRQU9TLHFDQUFVOzs7O3NCQUFDLElBQWU7Z0JBRS9CLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDOzs7MkJBTGxCTCxVQUFLLFNBQUMsTUFBTTs7K0JBSmY7Ozs7Ozs7Ozs7OztRQ0d1RU4sd0NBQTBCOzs7a0NBcUI5QyxJQUFJRSxpQkFBWSxFQUFZO2tDQU81QixJQUFJQSxpQkFBWSxFQUFZOytCQU8vQixJQUFJQSxpQkFBWSxFQUFZOzs7Ozs7OztRQWpDbkUsd0NBQVM7Ozs7O3NCQUFDLElBQWEsRUFBRSxRQUFtQjtnQkFFakQsUUFBTyxJQUFJO29CQUVULEtBQUssUUFBUTt3QkFDWCxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDdkMsTUFBTTtvQkFDUixLQUFLLFFBQVE7d0JBQ1gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQ3ZDLE1BQU07b0JBQ1IsS0FBSyxLQUFLO3dCQUNSLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUNwQyxNQUFNO29CQUNSO3dCQUNFLE1BQU0sa0JBQWtCLEdBQUcsSUFBSSxHQUFHLG9DQUFvQyxDQUFDO2lCQUMxRTs7Ozs7O1FBS08sdUNBQVE7Ozs7WUFBbEIsVUFBbUIsS0FBZTtnQkFFaEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDaEM7Ozs7O1FBSVMsdUNBQVE7Ozs7WUFBbEIsVUFBbUIsS0FBZTtnQkFFaEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDaEM7Ozs7O1FBSVMsb0NBQUs7Ozs7WUFBZixVQUFnQixLQUFlO2dCQUU3QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUM3Qjs7b0NBbkJBTyxXQUFNLFNBQUMsUUFBUTtvQ0FPZkEsV0FBTSxTQUFDLFFBQVE7aUNBT2ZBLFdBQU0sU0FBQyxLQUFLOzttQ0FyQ2Y7TUFHdUUsZ0JBQWdCOzs7Ozs7Ozs7OztRQ0l0QlQsNENBQXlEOzs7NENBUXBILElBQUksNEJBQTRCLEVBQThDOzs7Ozs7Ozs7UUFLM0UsbURBQWdCOzs7Ozs7c0JBQUMscUJBQTJGLEVBQUUsVUFBbUI7Z0JBRXRJLElBQUkscUJBQXFCLElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUNwRDtvQkFDRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsZUFBZSxDQUMxQyxJQUFJLENBQUMsa0JBQWtCLEVBQ3ZCLHFCQUFxQixFQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFDaEIsVUFBVSxFQUNWO3dCQUNFLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7d0JBQ2hDLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7cUJBQ2pDLENBQ0YsQ0FBQztpQkFDSDs7O3lDQXRCRk8sY0FBUyxTQUFDLHFCQUFxQixFQUFFLEVBQUUsSUFBSSxFQUFFQyxxQkFBZ0IsRUFBRTs7dUNBWjlEO01BT2lFLG9CQUFvQjs7Ozs7Ozs7OztBQ0pyRjs7O1FBQUE7UUFBcUVSLGdEQUEwRDs7OzsyQ0FIL0g7TUFHcUUsb0JBQW9CLEVBRXhGOzs7Ozs7Ozs7O0FDSEQ7OztRQUFBO1FBQStEQSwwQ0FBd0M7Ozs7cUNBRnZHO01BRStELG9CQUFvQixFQUVsRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=